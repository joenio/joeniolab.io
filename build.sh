#!/bin/sh

git clone --depth 1 https://gitlab.com/joenio/images.git /tmp/images
mv /tmp/images/images images
mv /tmp/images/thumbnails thumbnails
jekyll build
