Joenio Costa Blog
=================

Código-fonte do blog pessoal http://joenio.me implementado usando [Jekyll][jekyll], uma
ferramenta para criação de sites estáticos.

* reveal.js 3.6.0
* jekyll 4.2.0

Instalação do Jekyll
--------------------

O Jekyll está empacotado no Debian mas não está presente no testing (out/2015),
a versão do sid instala no testing, basta fazer o download do pacote e instalar
manualmente:

* http://tracker.debian.org/pkg/jekyll

Subir servidor local com docker compose
---------------------------------------

    docker-compose up

O jekyll estará disponível no endereço http://localhost:4000

Galeria de fotos
------------------

Algumas fotos publicadas no blog vem de um repositório externo em
https://gitlab.com/joenio/images, o pipeline do Gitlab CI definido no arquivo
`.gitlab-ci.yml` clona esse repositório e copia os arquivos em `thumbnails/` e
`images/` antes de executar o comando `jekyll build` para gerar o HTML estático
do site antes de publicar em https://joenio.me

Por exemplo, o post abaixo possui uma galeria ao final da página:
- https://joenio.me/poetry-attack-01-iclc-2023/

As imagens dessa galeria vem do seguinte repositório:
- https://gitlab.com/joenio/images

Dicas de como escrever bons títulos para os posts
-------------------------------------------------

* http://www.conversion.com.br/blog/como-escrever-titulos-que-recebem-300-mais-visitas/
* http://viverdeblog.com/como-escrever-titulos/

Repositórios de imagens e arte livre
------------------------------------

* http://commons.wikimedia.org
* http://deviantart.com

Snippets
--------

```html
<iframe class="youtube" src="https://www.youtube.com/embed/$ID?rel=0" frameborder="0" allowfullscreen></iframe>
```

Collections
-----------

- `avatars/*` - my profile photos (it is not a jekyll collection)
- `files/*` - images and assets used by blog posts, pages, etc
- `_ascii/*` - colecao de ascii art
- `_pages/*` - conteudo geral, quasi-wiki
- `_posts/*` - blog posts
- `_slides/*` - slides reveal.js

Salvar reveal.js slides em PDF via commandline
----------------------------------------------

```sh
SLUG=dublang-5min-intro
chromium --headless --screenshot="${SLUG}-0.png" "https://joenio.me/${SLUG}/#/0"
chromium --headless --screenshot="${SLUG}-1.png" "https://joenio.me/${SLUG}/#/1"
chromium --headless --screenshot="${SLUG}-2.png" "https://joenio.me/${SLUG}/#/2"
etc...
```

[jekyll]: http://jekyllrb.com
