help:
	@printf "[joenio.gitlab.io] help\n\n"
	@printf "   make up       - run docker-compose\n"
	@printf "   make down     - stop docker-compose\n"
	@printf "   make logs     - watch docker logs\n"
	@printf "   make dev      - run up, firefox and gvim\n"

dev: up
	sleep 5 && firefox http://localhost:4000 &
	gvim . &
	docker-compose logs -f

logs:
	docker-compose logs -f

up:
	docker-compose up -d

down:
	docker-compose down
