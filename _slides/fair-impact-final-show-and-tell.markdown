---
title: "fair-impact: 3rd Workshop - Show and Tell"
lead: >
  fair-impact final show and tell 5min talk presented during
  the last/3rd Workshop at 2nd October 2024, online
theme: night
version: 2024.10.02
---

<!--
- FRSM, Path I:
  - [ ] Prepare for the final show
    - [ ] Final show & tell - will include Path 2 participants on “Guidance for RS”
      - [ ] Work you have done
      - [ ] What you have learned
      - [ ] Did your approach/understanding change?
- RSMD, Path II:
  - [ ] In the folder create a Google slide presentation with your "show and tell"
    - Minimum 1 slide and max  5 slides (Please do not do more than 5!!)
    - This is a way to see what you have produced in the project
    - It may contain screenshots
    - It may contain links
    - It may contain a demo video
    - when finalized, after October 2nd, we will ask you to submit your
      presentation to Zenodo and add the DOI in your reporting file
-->

<section>
## fair-impact

### 3rd Workshop: Show and Tell

#### _by: Joenio Marques da Costa_

<small>_02 October 2024_</small>
</section>



<section>
### Work I have done

- Installed and tested F-UJI locally
  - Opened 2 Pull-Requests: [#45](https://github.com/softwaresaved/fuji/pull/45) and [#46](https://github.com/softwaresaved/fuji/pull/46)
- Added comments to the [D5.2 report](https://docs.google.com/document/d/1Ye4bnClsTPyOhvoyKgIBF5GZDxHZQOqGrg1aY9Y5a8Y/edit)
- Analizo assessed with the [FRSM](https://rslab.gitlab.io/fair-impact/data/frsm-checklist-analizo.html) and [RSMD](https://rslab.gitlab.io/fair-impact/data/rsmd-checklist-analizo.html)
- Analizo [added](https://research-software-directory.org/software/analizo) to Research Software Directory
- Portuguese blogpost about [FRSM](https://joenio.me/metricas-fair-para-research-software-frsm) and [RSMD](https://joenio.me/o-guia-de-metadados-para-research-software-rsmd)
- Checklist templates for [RSMD](https://rslab.gitlab.io/fair-impact/data/rsmd-checklist-template.html) and [FRSM](https://rslab.gitlab.io/fair-impact/data/frsm-checklist-template.html)
- [Add](https://github.com/citation-file-format/cffconvert/pull/391) biblatex-software support for cffconvert
- New Analizo version [1.25.5](https://github.com/analizo/analizo/blob/main/CHANGELOG.md#1255---2024-09-30)
- More info: [rslab.gitlab.io/fair-impact](https://rslab.gitlab.io/fair-impact)
</section>

<section>
### What I have learned

- Learned about the F-UJI tool
- Learned about Research Software Metadata
- Learned about Research Software FAIR Metrics
- Better understanding about CodeMeta
- How to use SWHID and DOI for Software
- How to add Software to RSD metadata record
</section>


<section>
#### Did my approach change?

Yes.<br/>
<small>I've decided to focus in 1 project only:</small>

✅ Analizo<br/>
❌ ~~Cortext~~
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>01 Oct 2024, Online, FAIR-IMPACT: Open Call last/3rd Workshop</li>
</ul>
</section>
</section>
