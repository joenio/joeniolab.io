---
title: "Making sound and music with Live Coding: First steps on Debian"
lead: >
  This workshop is going to show the first steps on how to make sounds using
  live coding environments, live coding environments are sound engine synthesis
  usually controlled by source code evaluation live. The live coding
  environments presented on the workshop are Tidal Cycles and SuperCollider,
  and also some words about the tools Jack and PipeWire.
theme: serif
version: draft
---

<section>
## Making sound and music with Live Coding:

### First steps on Debian
</section>

<section>
### Schedule

* Live coding definition
* Tools and languages
* Install and setup
* Live coding practice 🎧
</section>

<section>
### Live coding is...

* Sound art, visual art, others...
* A community building technology free from utilitarism constraints

See also:
* [on-the-fly.documentary](https://www.youtube.com/watch?v=ntFMuvv2-TY&list=LL&index=6) (2022)
* [Eulerroom Equinox TOPLAP Community][youtube-equinox-2020] (2020)
* [Algorave Generation Resident Advisor](https://www.youtube.com/watch?v=S2EZqikCIfY) (2019)
* [Live-Coding programming masterly music](https://www.youtube.com/watch?v=Ix2b_qFYfAA) (2018)
* [Algorave: algorithmic dance culture](https://www.youtube.com/watch?v=nAGjTYa95HM&t=704s) (2017)
</section>

<section>
### Community

<table><tr>
<td style="text-align: center">
<img src="/files/300px-Toplap.png" />
<br/>
<a href="https://toplap.org">TOPLAP</a>
</td>
<td style="text-align: center">
<img src="/files/algorave_logo.png" />
<br/>
<a href="https://algorave.com">Algorave</a>
</td>
</tr></table>
</section>

<section>
### The Stack
![](/files/tidalcycles-stack.svg)
</section>

<section>
<img width="55%" src="/files/atom-editor-logo-wine.svg" />

_Atom is a free and open-source text and source code editor._

[atom.io](https://atom.io)
</section>

<section>
<img width="60%" src="/files/tidalcyclesid.svg" />

_TidalCycles is a live coding environment for musical improvisation and composition, it is a domain-specific language embedded in Haskell._

[tidalcycles.org](https://tidalcycles.org)
</section>

<section>

<img src="/files/supercollider.svg" width="20%" style="vertical-align: middle" /> <span style="font-size:1.5em">&nbsp;SuperCollider</span>

_SuperCollider is an audio server, programming language, and IDE for sound synthesis and algorithmic composition._

[supercollider.github.io](https://supercollider.github.io)
</section>

<!-- SuperCollider 3, an object-oriented language for sound synthesis and digital signal processing (DSP) -->

<section>
<img src="/files/jack-logo.png" width="60%" />

_JACK is a professional sound server and pair of daemons to provide real-time, low-latency connections for audio and MIDI data._

[jackaudio.org](https://jackaudio.org)
</section>

<section>
<img src="/files/pulseaudio-logo.png" width="50%" />

_PulseAudio is a sound server system for POSIX OSes and it is an integral part of all relevant modern Linux distributions._

[pulseaudio.org](https://pulseaudio.org)
</section>

<section>
<img src="/files/pipewire-page-logo.svg" width="55%" />

_PipeWire improve handling of audio and video under Linux that can be used to support the use cases currently handled by both pulseaudio and JACK._

[pipewire.org](https://pipewire.org)
</section>

<section>
## Let's Start

<span style="font-size: 8em">🎧</span>
</section>

<section>
<pre><code data-trim class="bash">
$ sudo apt install \
    cabal-install \
    git \
    jackd2 \
    supercollider \
    sc3-plugins
</code></pre>

<pre><code data-trim class="bash">
$ scide
</code></pre>

<pre><code data-trim class="supercollider">
Quarks.checkForUpdates({
  Quarks.install("SuperDirt", "v1.7.3");
  thisProcess.recompile();
});
</code></pre>
</section>

<section>
![](/files/scide-superdirt-install.png)
</section>

<section>
![](/files/scide-hello-world-demo.png)
</section>

<section>
![](/files/qjackctl-conexions.png)
</section>

<section>
#### Install Tidal Cycles

<pre><code data-trim class="bash">
$ cabal update
$ cabal install tidal --lib
</code></pre>

&nbsp;

#### Install Atom and tidalcycles plugin

* Install from [Atom.io](https://atom.io), [Flatpak.org](https://flatpak.org) or [Snapcraft.io](https://snapcraft.io)
* Edit ↪ Preferences ↪ Install ↪ "tidalcycles"
* Create and save a file as "debconf22.tidal"
* Packages ↪ TidalCycles ↪ Boot Tidal Cycles
</section>

<section>
<span style="font-size: 1.25em; float: right; position: absolute; top: 80px; right: 50px ">🎧</span>
![](/files/tidalcycles-syntax.svg)
</section>

<section>
![](/files/tidalcycles-and-superdirt-connection.svg)
</section>

<section>
![](/files/tidalcycles-syntax-explained.svg)
</section>


<section>
![](/files/tidalcycles1-Page-3-9.png)

_cycle = 1 second_
<!-- source: https://suzuki-kengo.net/tidalcycles-vimtidal-tutorial/ -->
</section>

<section>
<span style="font-size: 1.25em; float: right; position: absolute; top: 80px; right: 50px ">🎧</span>
![](/files/tidalcycles-syntax-2.svg)
</section>

<section>
![](/files/tidalcycles1-Page-3-10.png)

_cycle = 1 second_
<!-- source: https://suzuki-kengo.net/tidalcycles-vimtidal-tutorial/ -->
</section>


<section>
![](/files/tidalcycles-and-superdirt-connection-2.svg)
</section>


<section>
![](/files/tidalcycles1-Page-3-9-two-cycles.png)

_cycle = 1 second_
</section>

<section>
### Alternatives

* ChucK
* FoxDot
* Gibber
* ORCΛ
* Pure Data
* Sonic Pi

See also: [github.com/toplap/awesome-livecoding](https://github.com/toplap/awesome-livecoding)
</section>

<!--

<section>
</section>

<section>
</section>

<section>
</section>

-->

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>19 July 2022, ITP, Prizren, Kosovo, <a href="https://debconf22.debconf.org/talks/70-making-sound-and-music-with-live-coding-first-steps-on-debian">DebConf22</a></li>
</ul>
</section>
</section>

[youtube-equinox-2020]: https://www.youtube.com/watch?v=qcE_a8IXk8o
