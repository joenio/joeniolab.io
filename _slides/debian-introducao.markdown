---
title: Debian - Introdução
theme: simple
version: 2024.05.14
---

<section>
## Debian

<img src="/files/debian.png" style="background: white" />

O Sistema Operacional Universal<br/>
[www.debian.org](https://www.debian.org)
</section>

<section>
# Debian

* Lançado em 1993
* Criado para ser uma distribuição Linux aberta, segundo o espírito do Linux e do GNU
* Em 1994 e 1995 o sistema dpkg ganhou notoriedade
* O projeto tem uma enorme comunidade de desenvolvedores trabalhando sob um contrato social
</section>

<section>
## Versões do Debian

O ciclo de desenvolvimento das versões do Debian tem três fases

* _unstable_ (instável) = Sid
* _testing_ (teste) = Trixie (13)
* _stable_ (estável) = Bookworm (12)

<small>
<!--
Histórico de versões:<br/>
Bullseye (11),
Buster (10),
Stretch (9),
Jessie (8),
Wheezy (7),
Squeeze (6.0),
Lenny (5.0),
Etch (4.0),
Sarge (3.1),
Woody (3.0),
Potato (2.2),
Slink (2.1),
Hamm (2.0),
Bo (1.3),
Rex (1.2),
Buzz (1.1).<br/>
-->
<small>Fonte: <a href="https://wiki.debian.org/DebianReleases">https://wiki.debian.org/DebianReleases</a></small>
</small>
</section>

<section>
<section>
## Sistema de pacotes

* Desenvolvedores Debian mantém pacotes
* "Upstream" é o desenvolvedor/autor do software
* Cada novo upload entra na versão "unstable"
* Após algum tempo de testes entra no "testing"
</section>
<section>
![](/files/debian-package-cycle-2016.svg)
</section>
</section>


<section>
## Ferramentas

* dpkg
* Apt (apt, apt-get, apt-cache, ...)
* Aptitude
* Synaptic
</section>

<section>
## Exemplos

<section>
#### dpkg

<pre><code class="bash">
dpkg -i libjs-impress_0.5.3-1_all.deb

</code></pre>

<pre><code class="bash">
dpkg -r libjs-impress

</code></pre>
</section>
<section>
#### apt

<pre><code class="bash">
apt-cache search impress.js
hovercraft - generator for impress.js presentations from reStructuredText
libjs-impress - JavaScript library to make animated presentations

</code></pre>

<pre><code class="bash">
apt-get install libjs-impress

</code></pre>
</section>
<section>
#### aptitude

<pre><code class="bash">
aptitude search libjs-impress
p  libjs-impress  -  JavaScript library to make animated presentations

</code></pre>

<pre><code class="bash">
aptitude install libjs-impress

</code></pre>
</section>

<section>
#### synaptic

<img width="60%" src="/files/synaptic-screenshot-2024.png" />
</section>
</section>

<section>
### Repositórios de pacotes Debian

<pre><code class="bash">
root@debian:~# cat /etc/apt/sources.list

deb http://deb.debian.org/debian testing main

</code></pre>

<small>
**main** é a seção principal do repositório Debian contendo<br/>
pacotes aderentes a _Definição Debian de Software Livre_,<br/>
outras seções: **contrib**, **non-free** e **non-free-firmware**
<br/>
<small>Fonte: <a href="https://wiki.debian.org/pt_BR/SourcesList">https://wiki.debian.org/pt_BR/SourcesList</a></small>
</small>

</section>

<section>
### Anatomia de um pacote Debian

<pre><code class="bash">
$ dpkg -c libjs-impress_0.5.3-1_all.deb 
... ./
... ./usr/
... ./usr/share/
... ./usr/share/doc/
... ./usr/share/doc/libjs-impress/
... ./usr/share/doc/libjs-impress/examples/
... ./usr/share/doc/libjs-impress/examples/index.html
... ./usr/share/doc/libjs-impress/examples/css/
... ./usr/share/doc/libjs-impress/examples/css/impress-demo.css
... ./usr/share/doc/libjs-impress/examples/favicon.png
... ./usr/share/doc/libjs-impress/copyright
... ./usr/share/doc/libjs-impress/README.md.gz
... ./usr/share/doc/libjs-impress/changelog.Debian.gz
... ./usr/share/javascript/
... ./usr/share/javascript/impress/
... ./usr/share/javascript/impress/impress.js
... ./usr/share/javascript/impress/impress.min.js
... ./usr/share/lintian/
... ./usr/share/lintian/overrides/
... ./usr/share/lintian/overrides/libjs-impress

</code></pre>

</section>

<section>
## * ./control

<img src="/files/screenshots/libjs-impress-deb-control.png" />
</section>

<section>
#### Dependências de `libhttp-message-perl`

![](/files/libhttp-message-perl-graph.png)

```
debtree libhttp-message-perl > graph.dot
dot -T png -o libhttp-message-perl-graph.png graph.dot
```
</section>

<!-- debtree libhttp-message-perl > graph.dot && dot -T png -o libhttp-message-perl-graph.png graph.dot -->

<section>
### Oficina de empacotamento Debian

(Parte 1)

[{{ site.url }}/tutorial-pacote-debian-parte1](/tutorial-pacote-debian-parte1)
</section>

<section>
# Lintian
</section>

<section>
# Autopkgtest
- chroot
</section>

<section>
# Salsa
</section>

<section>
## Bug Tracking System
- ITP
- RFP
</section>

<section>
### Oficina de empacotamento Debian

(Parte 2)

[{{ site.url }}/tutorial-pacote-debian-parte2](/tutorial-pacote-debian-parte2)
</section>

<section data-background="#c4a000" data-background-transition="slide">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>14 de Maio de 2024, Online,
    <a href="https://edisciplinas.usp.br/enrol/index.php?id=118237">MAC0470/5856 - Desenvolvimento de Software Livre</a>
    - USP
  </li>
  <li>16 de Maio de 2024, Online,
    <a href="https://edisciplinas.usp.br/enrol/index.php?id=118237">MAC0470/5856 - Desenvolvimento de Software Livre</a>
    - USP
  </li>
</ul>
</section>
</section>
