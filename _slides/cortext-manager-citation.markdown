---
title: CorTexT Manager citation | LISIS Atelier GTC
theme: simple
version: 2022.11.10
---

<section>
### Research Software invisibility <br/> and <br/> CorTexT Manager citation
</section>

<section>
### problem

![](/files/scientific-reputation-diagram-en.png)

</section>

<section>
### what the community has done

- new institutes: [SSI](https://www.software.ac.uk), [US-RSE](https://us-rse.org), [ReSA](https://www.researchsoft.org), [NL-RSE](https://nl-rse.org)
- new role: [Research Software Engineer (RSE)](https://www.software-carpentry.org/blog/2015/06/what-is-a-research-software-engineer.html)
- guides: [FAIR4RS](https://doi.org/10.15497/RDA00068), [fair-software.eu](https://fair-software.eu)
- standards: [cff](https://citation-file-format.github.io), [codemeta](https://codemeta.github.io)
- events: [RSECon2022](https://rsecon2022.society-rse.org), [The Future of Research Software](https://future-of-research-software.org)
- directory: [Research Software Directory](https://research-software-directory.org)
- archive & preservation: [Software Heritage](https://www.softwareheritage.org)

</section>

<section>
### what we've done in cortext manager

- guides:
  - [How to cite CorTexT Manager](https://docs.cortext.net/how-to-cite-cortext-manager) for cortext users
  - [Authors: How to cite CorTexT Manager](https://github.com/cortext/how-to-cite-cortext/blob/main/AUTHORS.md) for cortext team
- analysis: [CorText Manager around the world](https://www.cortext.net/publications)
- sustainability: [new people](https://www.cortext.net/members), IT infrastructure
- visibility: [talks and trainings](https://docs.cortext.net/training-materials)

</section>

<section>
### what remain to be done

- open source: [GNU GPL](https://www.gnu.org/philosophy/free-software-even-more-important.en.html), BSD, MIT, ...
- archive: [SWH archive](https://archive.softwareheritage.org)
- persistent identifier: [SWHID](https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html)
- permanent RSE positions

</section>

<section>
### recommendations

- Open Source and publish code you write
- [Cite CorTexT Manager](https://docs.cortext.net/how-to-cite-cortext-manager) and other tools

</section>

<section>

<img src="/files/cortext-manager-citation-screenshot.png" />

</section>

<section>
### references

- <small>Pierre Alliez, Roberto Di Cosmo, Benjamin Guedj, Alain Girault, Mohand-Said Hacid, et al.. Attributing and Referencing (Research) Software: Best Practices and Outlook from Inria. 2019. hal-02135891v1</small>
- <small>Depositing scientific software into Software Heritage,  septembre 28, 2018, https://www.softwareheritage.org/2018/09/28/depositing-scientific-software-into-software-heritage/?lang=fr</small>
- <small>[CodeMeta 2017] Matthew B. Jones, Carl Boettiger, Abby Cabunoc Mayes, Arfon Smith, Peter Slaughter, Kyle Niemeyer, Yolanda Gil, Martin Fenner, Krzysztof Nowak, Mark Hahnel, Luke Coy, Alice Allen, Mercè Crosas, Ashley Sands, Neil Chue Hong, Patricia Cruse, Daniel S. Katz, Carole Goble. 2017. CodeMeta: an exchange schema for software metadata. Version 2.0. KNB Data Repository. doi:10.5063/schema/codemeta-2.0</small>
- <small>[Di Cosmo, Gruenpeter and Zacchiroli 2018] Roberto Di Cosmo, Morane Gruenpeter, Stefano Zacchiroli. Identifiers for Digital Objects: the Case of Software Source Code Preservation. iPRES 2018 – 15th International Conference on Digital Preservation, Sep 2018, Boston, United States. pp.1-9. `hal-01865790v3`</small>
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>10 November 2022, 10h, Online, <a href="http://umr-lisis.fr/recherches/axe/axe-1">Atelier Groupe Thématique LISIS</a></li>
</ul>
</section>
</section>
