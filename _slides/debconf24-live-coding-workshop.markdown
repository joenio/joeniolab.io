---
title: "DebConf24 - Live Coding Workshop"
lead: >
  This workshop is going to show the first steps on how to make sounds using
  live coding environments, live coding environments are sound engine synthesis
  usually controlled by source code evaluation live. The live coding
  environments presented on the workshop is Sonic Pi.
theme: serif
version: 2024.07.30
---


<section>
## Live Coding tools on Debian

### How to install, setup and play 

_by: Joenio Marques da Costa<br/>and Mari Moura_
</section>


<section data-background-image="/files/goodbye-baloon-inverse.jpg">
<h4 style="color:white">Joenio Marques da Costa</h4>
<a href="http://joenio.me/about" style="color:blue; text-shadow: 0 0 2px gray">http://joenio.me/about</a>

<small style="color:white">_from Brazil living in France_</small>

<img style="position:relative; float:right; width: 31%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 550px; font-size: 0.9em; color: white">
- Research Software Engineer
- Work at <a href="https://www.cortext.net" style="color:blue; text-shadow: 0 0 2px gray">Cortext.net</a> Platform
- Software Heritage Ambassador
- Data Univ Eiffel Ambassador
- Debian ~~Contributor~~ Developer
- Computer Artist
</div>
</section>


<section data-background-image="/files/goodbye-baloon-inverse.jpg">
<h4 style="color:white">Mari Moura</h4>
<a href="http://marimoura.4two.art" style="color:blue; text-shadow: 0 0 2px gray">http://marimoura.4two.art</a>

<small style="color:white">_from Brazil living in France_</small>

<img style="position:relative; float:right; width: 31%" src="/files/marimoura_imagem_1.jpg" />
<div style="float: left; max-width: 550px; font-size: 0.9em; color: white">
- Black Brazilian performer artist
- PhD in art and technology
- Undergraduate student in Systems Analysis and development
- Python beginner
</div>
</section>


<section>
### Schedule

* Live coding introduction
* Tools and languages
* Install and setup Sonic Pi
* Live coding practice 🎧
</section>


<section>
Live coding is a new direction in electronic music and video: live coders
expose and rewire the innards of software while it generates improvised music
and/or visuals.
</section>


<section>
## toplap.org

<img src="/files/300px-Toplap.png" />

TOPLAP
has been collectively developing, exploring and promoting live coding since it
was formed in a smoky bar in Hamburg in 2004.
</section>


<section>
## algorave.com

<img src="/files/algorave_logo.png" />

Algorave is made from "sounds wholly or predominantly characterised by the
emission of a succession of repetitive conditionals".
</section>


<section>
# Tools and languages
</section>


<section>
SuperCollider<br/>
[supercollider.github.io](https://supercollider.github.io)

<img src="/files/supercollider.svg" width="15%" />

SuperCollider is an audio server, programming language, and IDE for sound
synthesis and algorithmic composition.
</section>


<section>
Sonic Pi<br/>
[sonic-pi.net](https://sonic-pi.net)

<img src="/files/sonic-pi-logo.png" width="23%" />

The Live Coding music synth for everyone, Sonic Pi has a Ruby-like syntax.
</section>


<section>
JACK<br/>
[jackaudio.org](https://jackaudio.org)

<img src="/files/jack-logo.png" width="40%" />

JACK is a professional sound server and pair of daemons to provide real-time,
low-latency connections for audio and MIDI data.
</section>


<section>
PipeWire<br/>
[pipewire.org](https://pipewire.org)

<img src="/files/pipewire-page-logo.svg" width="40%" />

PipeWire improve handling of audio and video under Linux that can be used to
support the use cases currently handled by both pulseaudio and JACK.
</section>


<section>
## How to install and setup
# Sonic Pi
</section>


<section>
### Install Sonic Pi

<br/>
Debian:

<pre><code class="bash">
sudo apt install sonic-pi

</code></pre>

<br/>
Windows or Mac:


<pre><code class="text">
Download from http://sonic-pi.net

</code></pre>
</section>


<section>
<img src="/files/sonic-pi-GUI.png" style="width:100%; max-width:100%; max-height:100%" />
</section>


<section>
#### Key concepts
### Sample vs Synthesizer

<br/>
Sample
<audio src="/files/water-to-glass.wav" controls>
  <a href="/files/water-to-glass.wav">water-to-glass.wav</a>
</audio><small style="margin:-20px">`water-to-glass.wav`</small>

Synthesizer
<audio src="/files/pitfall-ouro.wav" controls>
  <a href="/files/pitfall-ouro.wav">pitfall-ouro.wav</a>
</audio><small style="margin:-20px">`Pitfall! - Atari 2600`</small>
</section>


<section>
## Let's Start

<span style="font-size: 8em">🎧</span>
</section>


<section>
#### Sonic Pi language
### Sample vs Synthesizer

<br/>
Sample
<pre><code class="ruby">
sample :drum_cowbell

</code></pre>

<br/>
Synthesizer
<pre><code class="ruby">
play 60

</code></pre>
</section>


<section>
#### Sonic Pi functions
### Sample, Play and Sleep

<pre><code class="ruby">
sample :drum_cowbell
sleep 0.5
play 60
sleep 0.5
sample :drum_cowbell
sleep 0.5
play 60
sleep 0.5
sample :drum_cowbell

</code></pre>
</section>



<section>
#### Sonic Pi functions
### Live Loop

<pre><code class="ruby">
live_loop :myloop do
  sample :drum_cowbell
  sleep 0.5
  play 60
  sleep 0.5
end

</code></pre>
</section>



<section>
#### Sonic Pi function parameters
### Parameter '`rate`'

<pre><code class="ruby">
live_loop :myloop do
  sample :drum_cowbell, rate: 0.5
  sleep 0.5
  play 60
  sleep 0.5
end

</code></pre>

- Default rate is `1`
- Must not be `0` (zero)
</section>


<section>
## Comments

<pre><code class="ruby">
live_loop :myloop do
  #sample :drum_cowbell, rate: 0.5
  sample :drum_cowbell, rate: 0.2
  sleep 0.5
  #play 60
  play 120
  sleep 0.5
end

</code></pre>

- Lines starting by **#** are not executed
</section>

<section>
#### Sonic Pi function parameters
### Parameter '`amp`' (Amplitude)

<pre><code class="ruby">
sample :drum_cowbell, amp: 0.5
sleep 0.5
play 60, amp: 0.5
sleep 0.5

</code></pre>

- `amp = 0.5` means volume = `50%`
- Recommended values: `0` to `1`
</section>

<section>
#### Sonic Pi function parameters
### Parameter '`pan`' (Panning)

<pre><code class="ruby">
sample :drum_cowbell, pan: -1
sleep 0.5
play 60, pan: 1
sleep 0.5

</code></pre>

- Value `-1` means `100%` on the left side
- Value `1` means `100%` on the right side
- Possible values between `-1` to `1`
</section>

<section>
#### Sonic Pi function parameters
### Selecting the synthesizer

<pre><code class="ruby">
use_synth :saw
play 60

</code></pre>
</section>


<section>
## Challenge!

Create 2 live loops, each one with a different synthesizer, playing alternately.

<span style="font-size: 5em">🧗</span>
</section>



<section>
#### Sonic Pi language
### Lists and the `choose` function

<pre><code class="ruby">
loop do
  play choose([60, 65, 72])
  sleep 1
end

</code></pre>
</section>


<section>
#### Sonic Pi language
### Other loop functions

<pre><code class="ruby">
3.times do
  play 60
  sleep 1
end

</code></pre>

<pre><code class="ruby">
loop do
  play 60
  sleep 1
end

</code></pre>
</section>

<section>
### How to add custom sample files
</section>

<section>
### How to record a Sonic Pi session
</section>


<section>
#### Sonic Pi alternatives

* ChucK
* FoxDot
* Gibber
* ORCΛ
* Pure Data
* Tidal Cycles

See also: [github.com/toplap/awesome-livecoding](https://github.com/toplap/awesome-livecoding)
</section>

<!--

<section>
</section>

<section>
</section>

<section>
</section>

-->

<section>
## Live coding videos and documentaries

* [on-the-fly.documentary](https://www.youtube.com/watch?v=ntFMuvv2-TY&list=LL&index=6) (2022)
* [Eulerroom Equinox TOPLAP Community][youtube-equinox-2020] (2020)
* [Algorave Generation Resident Advisor](https://www.youtube.com/watch?v=S2EZqikCIfY) (2019)
* [Live-Coding programming masterly music](https://www.youtube.com/watch?v=Ix2b_qFYfAA) (2018)
* [Algorave: algorithmic dance culture](https://www.youtube.com/watch?v=nAGjTYa95HM&t=704s) (2017)
</section>


<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>01 Aug 2024, PKNU, Busan, South Korea, <a href="https://debconf24.debconf.org/talks/81-live-coding-tools-on-debian-how-to-install-setup-and-play/">DebConf24</a></li>
</ul>
</section>
</section>

[youtube-equinox-2020]: https://www.youtube.com/watch?v=qcE_a8IXk8o
