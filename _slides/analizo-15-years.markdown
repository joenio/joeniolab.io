---
title: "Analizo, 15 years of a multi-language Research Software tool for source code analysis"
lead: >
  ...
theme: night
version: draft
---

<!--
Analizo is a free, multi-language, extensible source code analysis. It supports
the extraction and calculation of a fair number of source code metrics,
generation of dependency graphs, and software evolution analysis. It was
created in 2008 by Antonio Terceiro as a support tool for his PhD thesis and
has been co-developed by Antonio Terceiro, Joenio Costa, João Miranda and Paulo
Meirelles, nowadays, Joenio Costa is the only active maintainer of the project.
Since its creation, the tool has been used by many researchers who are studying
topics around software quality, software evolution, source code quality
metrics, software complexity and so on, many of those studies using Analizo
were published in the literature. In this talk, the Analizo tool will be
presented, a quick practical demonstration will be done to show how the tool
works, an historical analysis of the tool will be shared with details about
citation in the literature, showing the moments with more activity on the
project and number of collaborations in terms of research studies published and
collaborations in terms of source code development.

Analizo website: https://www.analizo.org
GitHub repository: https://github.com/analizo/analizo
CPAN module: https://metacpan.org/dist/Analizo

FOSDEM Online Open Research room:
https://research-fosdem.github.io/2024-online-schedule.html
-->

<section>
<img style="width: 20%" src="https://www.analizo.org/analizo.png"/>

#### Analizo, 15 years of a multi-language Research Software tool for source code analysis

&nbsp;

#### _by Joenio M. Costa_
</section>

<section data-background-image="/files/goodbye-baloon-inverse.jpg">
#### Joenio Marques da Costa
<a href="http://joenio.me">http://joenio.me</a>

&nbsp;

<img style="position:relative; float:right; width: 40%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 500px; font-size: 0.95em">
 - Research Software Engineer
 - Work at [CorTexT](https://www.cortext.net) Platform
 - Software Heritage Ambassador
 - Debian Contributor
 - Live Coder and Visual Artist
</div>
</section>

<section>
<img style="width: 20%" src="https://www.analizo.org/analizo.png"/>

Analizo means "analysis" in Esperanto

- Extraction and calculation of source code metrics
- Generation of dependency graphs
- Software evolution analysis
- Support for: C, C++, Java, C# and Python (beta)
- Free Software

[www.analizo.org](https://www.analizo.org)
</section>

<section>
#### Analizo architecture

<img style="width: 60%" src="/files/analizo-architecture.png"/>
</section>

<section>
### 2008

- Analizo was created as a support tool for the Antonio Terceiro's PhD thesis
- Analizo started as a [modified version](https://github.com/terceiro/egypt) of
  [egypt tool, by Andreas Gustafsson](http://www.gson.org/egypt) (1994-2022)
</section>

<section>
### 2008

Paulo Meirelles's started his PhD thesis
[Source code metrics tracking on free and open source projects](https://doi.org/10.11606/T.45.2013.tde-27082013-090242)<br/>
(ptbr: Monitoramento de métricas de código-fonte em projetos de software livre)

Technological objectives: Kalibro, Mezuro, Analizo.
</section>

<section>
### 2009

Joenio Costa's Undergrad Monograph:

Extraction of Dependency Informations among Modules of C/C++ Programs<br/>
(ptbr: [Extração de Informações de Dependência entre Módulos de Programas C/C++](https://github.com/joenio/monografia-ucsal-2009/blob/8d9556fe09c758cd90020d7842d4e2c28747d88e/monografia.pdf)
</section>

<section>
### 2010

Software Paper:

[Analizo: an Extensible Multi-Language Source Code Analysis and Visualization Toolkit](publications/analizo-cbsoft2010-tools.pdf). Tools Session of the *1st Brazilian Conference on Software*.
</section>


<section>
### 2010

Lucianna Thomaz Almeida and João Machini de Miranda's undergrad monograph:

[Código Limpo e seu Mapeamento para Métricas de Código Fonte](https://bcc.ime.usp.br/tccs/2010/lucianna-joao/arquivos/monografia.pdf).
- Collaborated with Analizo cleaning the code and improving the source code quality.
</section>

<section>
### 2013

- [Antonio Terceiro's PhD thesis submitted](https://terceiro.xyz/2012/02/28/thesis-submitted) (UFBA) after 5 years of work.
- Paulo Meirelles's PhD thesis submitted (USP).
</section>

<section>
### 2014

Lucas Kanashiro Duarte undergrad monograph submitted
[Análise de Métricas de Código Fonte: Além das Métricas de Design de Código](https://fga.unb.br/articles/0000/7978/TCC1_Lucas_Kanashiro.pdf).

- Evolved the Analizo on the context of the monograph...
</section>

<section>
#### Analizo core developers and maintainers

&nbsp;
##### **2008-2014** (6y)
  - Antonio Terceiro: PhD + his students
  - Paulo Meirelles: PhD + his students
  - Joenio M. Costa: Undergrad

&nbsp;
##### **2015-today** (9y)
  - Joenio M. Costa: Volunteer
</section>

<section data-background-color='white'>
#### Analizo source code size

![](/files/analizo-code-size-evolution.png)
</section>

<section data-background-color='white'>
#### Analizo software paper citations

![](/files/analizo-paper-citations-evolution.png)
</section>

<section>
### 2014

### I've started a master thesis.
### Should I evolve the Analizo?
</section>

<section data-background='#A43'>
<section>
### What if I spend +3 years improving Analizo during the master?
</section>
<section>
### After that who will maintain the project to keep it evolving and useful?
</section>
<section>
### The effort to invest time during my master to improve a software tool is worthy?
</section>
</section>

<section>
### 2014-2017

Master thesis about sustainability of static analysis tools:

[On the sustainability of academic software: the case of static analysis tools](https://dl.acm.org/doi/10.1145/3266237.3266243)

- _And I've decided to not include Analizo evolution on the objectives of the master project..._
</section>

<section>
### Software must be recognized as scientific contribution and incentives on collaboration are nedded.

I feel (no evidence) that scientists collaborate on research activities but compete on software tool development.
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>10 Feb 2024, Online, <a href="https://research-fosdem.github.io/2024-online-schedule">Open Research Online Devroom talks</a></li>
</ul>
</section>
</section>
