---
title: "Live Coding - IMAC E4 projet tuteuré"
version: 2024.09.19
theme: serif
---

<section>
<small>19 septembre 2024</small>

### Live Coding: art, science et technologie

#### IMAC E4 projet tuteuré

<small><br/>Joenio Marques da Costa et Mari Nicácio de Moura</small>
</section>

<section data-background-image="/files/goodbye-baloon-inverse.jpg">
<h4 style="color:white">Joenio Marques da Costa</h4>
<a href="http://joenio.me/about" style="color:blue; text-shadow: 0 0 2px gray">http://joenio.me/about</a>

<small style="color:white">_from Brazil living in France_</small>

<img style="position:relative; float:right; width: 31%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 550px; font-size: 0.9em; color: white">
- Research Software Engineer
- Work at <a href="https://www.cortext.net" style="color:blue; text-shadow: 0 0 2px gray">Cortext.net</a> Platform
- Software Heritage Ambassador
- Data Univ Eiffel Ambassador
- Debian Developer
- Computer Artist
</div>
</section>


<section data-background-image="/files/goodbye-baloon-inverse.jpg">
<h4 style="color:white">Mari Moura</h4>
<a href="http://marimoura.4two.art" style="color:blue; text-shadow: 0 0 2px gray">http://marimoura.4two.art</a>

<small style="color:white">_from Brazil living in France_</small>

<img style="position:relative; float:right; width: 31%" src="/files/marimoura_imagem_1.jpg" />
<div style="float: left; max-width: 550px; font-size: 0.9em; color: white">
- Black Brazilian performer artist
- PhD in art and technology
- Undergraduate student in Systems Analysis and development
- Python beginner
</div>
</section>

<section>
Live coding est une pratique de création artistique basée sur l'utilisation de
programmation interactive improvisée.
</section>

<section>
### Live Coding
#### IMAC E4 projet:

- Développeur logiciels libres
- Conférences
- Ateliers
- Concerts
</section>


<section>
Compétence technique requise :

- La programmation en quelque niveau,<br/>
  débutant ou avancé.
- Linux et Debian en niveau utilisateur.
- Logiciel Libre et Open Source,<br/>
  la philosophie et les principles.
- Éditeur de textes Vim ou Neovim.
</section>

<section>
### Le logiciel libre:
## dublang

[https://dublang.4two.art](https://dublang.4two.art)
<br/>
<img src="https://dublang.4two.art/artwork/dublang-logo.png" width="5%" />
<br/>
Projet de développment <small style="vertical-align: middle">(langage de script Lua)</small>:

Porter le dublang dans l'autres éditeurs de texte.
</section>


<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>19 September 2024, Copernic, University Gustave Eiffel, Marne-la-Vallee - [ESIEE IMAC](https://www.esiee.fr/formations/ingenieur/filieres/imac)</li>
</ul>
</section>
</section>
