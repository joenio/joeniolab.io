---
title: "fair-impact: 2min video for the stand-up round"
lead: >
  fair-impact  2min video for the stand-up round presented at 4 July
  2024 online
theme: night
version: 2024.07.04
---

<section>
## fair-impact

#### 2min video for the stand-up round

#### Where I am now

##### _by Joenio M. Costa_

<small>_4 July 2024_</small>
</section>

<section>
#### Status update

- ✅ Improvements and fixes for the RSMD
  - [Pull-Request !5](https://github.com/FAIR-IMPACT/RSMD-guidelines/pull/5) (close)
  - [Pull-Request !6](https://github.com/FAIR-IMPACT/RSMD-guidelines/pull/6) (close)
  - [Pull-Request !7](https://github.com/FAIR-IMPACT/RSMD-guidelines/pull/7) (close)
  - [Issue #8](https://github.com/FAIR-IMPACT/RSMD-guidelines/issues/8) (open)
- ✅ Review the [RSMD guidelines](https://fair-impact.github.io/RSMD-guidelines)
- ✅ Write ptbr blogpost about RSMD
  - [O Guia de metadados para Research Software](https://joenio.me/o-guia-de-metadados-para-research-software-rsmd)
- ✅ Add biblatex-software support to cffconvert 
  - [Issue #152](https://github.com/citation-file-format/cffconvert/issues/152) and [Pull-Request !391](https://github.com/citation-file-format/cffconvert/pull/391) (open)
</section>

<section>
#### Next steps

- Apply RSMD to Analizo and Cortext
- User Interface: CFFINIT vs. CodeMeta generator
- Report about RSMD results applied to Analizo
- Report about RSMD results applied to Cortext
</section>


<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>4 Jul 2024, Video, FAIR-IMPACT: RSMD day sprint, 2min video for the stand-up round</li>
</ul>
</section>
</section>
