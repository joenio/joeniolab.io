---
title: Sustentabilidade de Software para Pesquisa - O papel do Software na Ciência
theme: simple
version: 2024.10.18
---

<!--
Tempo da palestra:
25 minutos + 5 minutos para perguntas

Mini-bio:
Engenheiro de software para pesquisa na Cortext Platform,
doutorando na UFBA pesquisando sustentabilidade de software,
desenvolvedor Debian e embaixador do Sofware Heritage.

Titulo da palestra:
Sustentabilidade de Software para Pesquisa: O papel do Software na Ciência

Resumo da palestra (abstract):
Esta palestra apresenta o papel central do software nas pesquisas
científicas e como o desenvolvimento de software para pesquisa
se relaciona com o modelo de publicações acadêmica.

Afiliação e titulação:
Université Gustave Eiffel
Universidade Federal da Bahia

Uma foto:
https://joenio.me/avatars/joenio-2024-1048x1280.jpg
-->

<!-- -------------------------------- -->

<section>
<small><img style="width:150px" src="/files/pubparis-logo.jpg"/><br/>_18 Out 2024_</small>

### Sustentabilidade de<br/>Software para Pesquisa:
### O papel do Software na Ciência

--

<small>Joenio Marques da Costa</small>
</section>

<!-- -------------------------------- -->

<section data-background-image="/files/goodbye-baloon-inverse.jpg">
<h4 style="color:white">Joenio Marques da Costa</h4>
<a href="http://joenio.me/sobre" style="color:blue; text-shadow: 0 0 2px gray">http://joenio.me/sobre</a>

<br/>

<img style="position:relative; float:right; width: 28%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 650px; font-size: 0.9em; color: white">
- Engenheiro de Software para Pesquisa
- Trabalha na Plataforma <a href="https://www.cortext.net" style="color:blue; text-shadow: 0 0 2px gray">Cortext.net</a>
- Embaixador Software Heritage
- Embaixador Data Univ Eiffel
- Desenvolvedor Debian
- Artista Computacional
</div>
</section>

<!-- -------------------------------- -->

<section>
## Agenda

<br/>

#### Trajetória e história pessoal

## +

#### Pesquisa sobre Software para Pesquisa
</section>

<!-- -------------------------------- -->

<section>
### Camaçari - Bahia 🏖️

- 1994 - Ensino fundamental, médio, e técnico<!-- ate: 1999 -->
- 1998 - Slackware Linux e Software Livre<!-- ate: 2024 -->
- 2001 - Estágio em informática<!-- ate: 2004 -->
- 2001 - Banda Los Benzenos<!-- ate: 2015 -->
</section>

<!-- -------------------------------- -->

<section>
#### Camaçari, Los Benzenos, 2001 - 2015

![](/files/losbenzenos-photo.jpg)
<br/>
<small>Banda Los Benzenos</small><br/>
<small>[losbenzenos.gitlab.io](https://losbenzenos.gitlab.io)</small>
</section>

<!-- -------------------------------- -->

<section>
### Salvador - Bahia 🪇

- 2000 - Graduação UCSAL<!-- ate: 2009 -->
- 2004 - Estágio como programador<!-- ate: 2005 -->
- 2005 - Trabalho como programador<!-- ate: 2007 -->
- 2006 - Cooperativa Colivre<!-- ate: 2015 -->
</section>

<!-- -------------------------------- -->

<section>
#### Salvador, Colivre, 2006 - 2015

<img style="width:60%" src="/files/colivre-2007.jpg"/>
<br/>
<small>Colivre: Cooperativa de Trabalho em Tecnologias Livres</small><br/>
<small>[colivre.coop.br](https://colivre.coop.br)</small>
</section>

<!-- -------------------------------- -->

<section>
### Brasília - Distrito Federal 🏟️

- 2014 - Engenheiro de software LAPPIS lab<!-- ate: 2018 -->
- 2014 - Mestrado UFBA<!-- ate: 2017 -->
- 2015 - Consultor IBICT<!-- ate: 2018 -->
- 2019 - Sem trabalho, CNJ, Cortext<!-- ate: 2020 -->
</section>

<!-- -------------------------------- -->

<section data-background-color="">
#### Brasília, UnB - Faculdade do Gama, 2014 - 2018

<img style="width:80%" src="/files/microdebconf-bsb-group-photo_1024_from_collabora_site.jpg"/>
<br/>
<small>LAPPIS: Laboratório Aplicado de Produção Pesquisa e Inovação em Software</small><br/>
<small>[www.lappis.rocks](https://www.lappis.rocks)</small>
</section>

<!-- -------------------------------- -->

<section>
### Paris - France 🥐

- 2020 - Plataforma Cortext, LISIS lab<!-- ate: hoje -->
- 2022 - Embaixador Software Heritage<!-- ate: hoje -->
- 2024 - Desenvolvedor Debian
- 2024 - Doutorado UFBA<!-- ate: hoje -->
</section>

<!-- -------------------------------- -->

<section>
#### Paris, Cortext, 2020 - 2024

<img style="width:85%" src="/files/cortext-team-2024.jpg"/>
<br/>
<small>Cortext: Plataforma de apoio à pesquisa em métodos digitais aplicados às ciências sociais</small><br/>
<small>[www.cortext.net](https://www.cortext.net)</small>
</section>

<!-- -------------------------------- -->

<section>
## Graduação UCSAL
<small>2000 - 2009</small>

Monografia, 2009:

[Extração de Informações de Dependência entre Módulos de Programas C/C++](https://github.com/joenio/monografia-ucsal-2009/blob/8d9556fe09c758cd90020d7842d4e2c28747d88e/monografia.pdf)

Software paper, 2010:

[Analizo: an Extensible Multi-Language Source Code Analysis and Visualization Toolkit](publications/analizo-cbsoft2010-tools.pdf)
</section>

<!-- -------------------------------- -->

<section>
#### Analizo

<img style="width:70%" src="/files/analizo-site-2024.png"/>

<small>[www.analizo.org](https://www.analizo.org)</small>
</section>

<!-- -------------------------------- -->

<section data-background='#A43'>
## Mestrado UFBA
<small>2014 - 2017</small>

Preocupações:

- Mais 3 anos desenvolvendo o Analizo?
- Vale à pena investir este tempo?
- O que acontece após o final do mestrado?
</section>

<!-- -------------------------------- -->

<section>
## Mestrado UFBA
<small>2014 - 2017</small>

Dissertação, 2017:

[Sustentabilidade Técnica de Software Acadêmico no Domínio de Ferramentas de Análise Estática](https://repositorio.ufba.br/handle/ri/32468)

Paper, 2018:

[On the sustainability of academic software: the case of static analysis tools](https://dl.acm.org/doi/10.1145/3266237.3266243)
</section>

<!-- -------------------------------- -->

<section>
### Software para Pesquisa<br/><small>(Research Software)</small>

É qualquer software criado para apoiar pesquisas,
usado para coletar, processar ou analisar dados.

Podem ser protótipos escritos pelos cientistas,
ou produtos completos desenvolvidos profissionalmente.

<small class="cite">
M. Gruenpeter, "Defining Research Software: a controversial discussion".<br/>
Zenodo, sept. 13, 2021. doi: [10.5281/zenodo.5504016][].
</small>
</section>

<!-- -------------------------------- -->

<section>
#### Modelo de processo de Software para Pesquisa

<img src="/files/process-model-scientific-software.png" />

<small class="cite">
HOWISON, J. et al. "Understanding the scientific software ecosystem and its impact:<br/>
Current and future measures". Research Evaluation, p. 454–470, 2015.
doi: [10.1093/reseval/rvv014][]
</small>
</section>

<!-- -------------------------------- -->

<section>
#### Incentivos de reputação e práticas de Software para Pesquisa

<img src="/files/scientific-reputation-diagram.png" style="background: white" />

<small class="cite">
HOWISON, J.; HERBSLEB, J. D. "Scientific software production: incentives and collaboration".<br/>
In: Proceedings of the ACM 2011 conference., 2011. doi: [10.1145/1958824.1958904][]
</small>
</section>

<!-- -------------------------------- -->

<section>
## Sustentabilidade

individual - social - econômica - ambiental - **técnica**

<small class="cite">
BECKER, C. et al. **The karlskrona manifesto for sustainability design**. CoRR,
abs/1410.6968, 2014.
</small>
</section>

<!-- -------------------------------- -->

<section>
### Sustentabilidade técnica de software

A dimensão técnica diz respeito a capacidade do software de perdurar e de
continuar sendo suportado ao longo do tempo, implicando em qualidades de
longevidade e manutenção.

</section>

<!-- -------------------------------- -->

<section>
#### Software Sustainability Institute
🇬🇧<br/>
<img style="width:50%" src="/files/SSI_PRIMARY-LOGO.svg"/>
<br/>
<small>[www.software.ac.uk](https://www.software.ac.uk)</small>

"Better Software, Better Research"
</section>

<!-- -------------------------------- -->

<section>
#### United States Research Software Engineer Association
🇺🇸<br/>
<img style="width:20%" src="/files/us-rse-main_logo_transparent.png"/>
<br/>
<small>[us-rse.org](https://us-rse.org)</small>

"A community who make research software happen"
</section>

<!-- -------------------------------- -->

<section>
#### Software Heritage
🇫🇷<br/>
<img style="width:40%" src="/files/SWH-logo_anim-texte1.gif"/>
<br/>
<small>[www.softwareheritage.org](https://www.softwareheritage.org)</small>

"Collect and preserve software in source code form"
</section>

<!-- -------------------------------- -->

<section data-background="#c4a000">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li markdown="1">
  18 Outubro 2024, [PUB Paris](https://www.eventbrite.fr/e/pub-paris-de-ciencia-tecnologia-e-inovacao-na-franca-registration-1032555791167), MIE Rennes
  </li>
</ul>
</section>
</section>

[10.5281/zenodo.5504016]: https://doi.org/10.5281/zenodo.5504016
[10.48550/arXiv.1807.01772]: https://doi.org/10.48550/arXiv.1807.01772
[10.1093/reseval/rvv014]: https://doi.org/10.1093/reseval/rvv014
[10.1145/1958824.1958904]: https://doi.org/10.1145/1958824.1958904
