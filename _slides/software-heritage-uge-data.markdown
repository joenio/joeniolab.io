---
title: Software Heritage - Atelier Data Univ Gustave Eiffel
theme: beige
transition: convex
version: 2023.11.13
---

<!--

Presentation about Software Heritage to Univ Gustave Eiffel at 13/11/2023.
- Event: RDV DATA sur les plateformes dediees aus logiciels.

Talk title:

Software Heritage, the persistent source-code archive: What is it and how to use it?

Talk description:

[TODO: review] In this talk we will learn the basics about Software Heritage
project, the goals and the main issues addressed by the project. With practical
examples, we'll see how to archive software persistently and how to cite
software properly in scientific publications using SWHID.

Target audience:
- People with some knowledge of Git and Gitlab, maybe Researche Software Engineers
  
Talk structure:
- 20 minutes talk
- 20 minutes discussions

Talk recorded and available at:
- https://clap.univ-eiffel.fr/channels/#atelier-data-univ-eiffel

Topics to include in the talk:
- OK General information about SWH is not priority
- OK Important to talk about best practices of software management
- OK Try to focus on the needs then link archive 
- OK Demonstrate how to archive repositories on SWH
- Give example of dead links that doesnt work, see in practice how archive is important
- Stress that it is not only for software developers, but also to archive and preserve
  software source code digital artefacts in science in a borad way
- OK Why SWH if ther is Zenodo, HAL, etc... (Answer: development history, git log, etc....)
- OK Zenodo is nice to archive code and data but it is not made for the specific characteristics of software code
  OK Take a look, reference: https://hedgedoc.softwareheritage.org/8Rsxd0lRSniw3x1NLIGfCg?view#
- OK Another good argument is the existence of SWHID
- ---
- What are the theoretical difference between SWH x VCS (git, svn, etc)
- mostrar os numeros do arquivo pode ser bom p convencer novos usuarios
  - https://docs.softwareheritage.org/devel/architecture/overview.html

Links:

- ???

-->

<section>
### Software Heritage,<br/>the persistent source-code archive:<br/>What is it and how to use it?

![](/files/logo-univ-gustave-eiffel-2020.svg)
<br/><small>
13 November 2023<br/>
Atelier Data Univ Eiffel - Les plateformes pour développer, partager et archiver les logiciels.
</small>
</section>

<section data-background='white'>
Joenio Marques da Costa

<img src='/files/image_ambassador_22_joenio-768x430.png' width='50%' />

<small>
Research Software Engineer at [LISIS lab](http://umr-lisis.fr)<br/>
⚬ [CorTexT platform](https://cortext.net)<br/>
⚬ [Research Infrastructure for STI (RISIS)](https://risis2.eu)
</small>

<small style='font-size: 0.4em'>
Software Heritage Ambassador - November 22, 2022<br/>[Introducing our newest ambassador, Joenio Marques da Costa](https://www.softwareheritage.org/2022/11/22/22nd-ambassador-joenio-marques-da-costa)
</small>
</section>

<section data-background='white'>
![](/files/software-heritage-logo-noicon.svg)

![](/files/SWH-logo_anim-texte1.gif)

_Universal software source code archive_
<small style="display:block">[softwareheritage.org](https://www.softwareheritage.org)</small>
</section>

<section>
![](/files/software-heritage-logo-title.svg)<br/>
![](/files/swh-dataflow-merkle-1.png)
<br/>🛈 <small style="vertical-align: middle">
_More than 150M projects, almost **10 billion** unique source files as of January 2021_
</small>
</section>

<section data-background='white'>
### Software Heritage archive

<img src="/files/swh-source-files-graph-2023.png" width='60%' />

<small>**17 billion** unique source files as of November 2023</small>
</section>

<section>
# why?

- 2015, Google Code and Gitorious.org shutdown
- 2019, BitBucket announces Mercurial VCS sunset
- 2020, BitBucket erases 250.000+ repositories
- 2021, Inria's old gforge.inria.fr was shutdown
- 2022, ~~GitLab.com considers erasing all projects that are inactive for a year~~<br/>
  <small>[GitLab U-turns on deleting dormant projects after backlash](https://www.theregister.com/2022/08/05/gitlab_reverses_deletion_policy)</small>
</section>

<section>
### Software Heritage
# =
### GitHub, GitLab, BitBucket, Google Code ... ?
<small class='fragment'>
[Version Control with Git](http://swcarpentry.github.io/git-novice) <img src='/files/the-carpentries-logo.svg' width='15%' style='margin-left: 5px; margin-top: 5px' />
</small>
</section>

<section>
<section>
### Why not?
Zenodo, HAL, figshare... ?

<img src="/files/swh-zenodo-meme-imgflip.jpg" width="40%" /><br/>
<small style="font-size: 0.4em">_Source: Image from imgflip [meme generator](https://imgflip.com/memegenerator/4081988/What-if-i-told-you)_</small>
</section>

<section>
### Cause
Software is not Data

<img src="/files/swh-features-meme-imgflip.jpg" width="40%" /><br/>
<small style="font-size: 0.4em">_Source: Image from imgflip [meme generator](https://imgflip.com/memegenerator/Success-Kid)_</small>
</section>
</section>


<section data-background='white'>
![](/files/shw-users-page.png)

Features

</section>

<section data-background='white'>
### Software Heritage archive
<img src='/files/browse_search_swh.jpeg' width='30%' />

_The long term source code archive._

<small>[archive.softwareheritage.org](https://archive.softwareheritage.org)</small>

<aside class="notes">
Do a live demonstration archiving the repository
[github.com/joenio/swhid-citation-example](https://github.com/joenio/swhid-citation-example)
that is not yet archived.
</aside>
</section>

<section data-background='white'>
### Software ~~Heritage~~ Hash identifier (SWHID)
<img src='/files/swhid_provider_resolver.png' width='30%' />

_Intrinsic identifiers for digital objects._

<small>[www.swhid.org](https://www.swhid.org)</small>
</section>

<section>
![](https://www.softwareheritage.org/wp-content/uploads/2020/06/software_ids-Page-2.png)

<small>_One type of identifier can’t answer all use cases, we need both intrinsic identifiers and extrinsic identifiers for software research outputs._<br/><br/>
[softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers](https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers)</small>
</section>

<section>
### What can be identified with a SWHID?

<img src='/files/upload_9d9303cc5952cf3d9e101aa8313ca276.png' width='70%' />

<small>
[softwareheritage.org/faq/#32_What_can_be_identified_with_a_SWHID](https://www.softwareheritage.org/faq/#32_What_can_be_identified_with_a_SWHID)
</small>
</section>

<section>
SWHID howtos:
- [HOWTO archive and reference your code](https://www.softwareheritage.org/howto-archive-and-reference-your-code)
- [Archiving and Referencing Source Code with Software Heritage](https://link.springer.com/chapter/10.1007/978-3-030-52200-1_36)
- [https://ctan.org/pkg/biblatex-software](https://ctan.org/pkg/biblatex-software)

Examples of SWHID use:
- [Toward Long-Term and Archivable Reproducibility](https://arxiv.org/pdf/2006.03018.pdf)
- [Carving out the low surface brightness universe with NoiseChisel](https://arxiv.org/pdf/1909.11230.pdf)
- [AutoClassWeb: a simple web interface for Bayesian clustering of omics data](https://bmcresnotes.biomedcentral.com/articles/10.1186/s13104-022-06129-6#availability-of-data-and-materials)
</section>

<section>
<img src='/files/swhid-citation-example.png' width='60%' style='box-shadow: 1px 1px 5px gray'/>

<small>Source code: [github.com/joenio/swhid-citation-example](https://github.com/joenio/swhid-citation-example)</small>
</section>

<section>
SWHID Approved Specification

<img src="/files/swhid-mail-approved-specs-1.1.png" width="60%" style="box-shadow: 0 0 10px #CCC" />
<br/>
<small>
Source: [team mailing list](https://groups.google.com/g/swhid-discuss)<br/>
[The SWHID Specification Version 1.1](https://www.swhid.org/specification/v1.1).<br/>
_The next step is the submission to ISO in order to become a standard._
</small>
</section>

<section>
### Extra references:

- [FAIR Principles for Research Software (FAIR4RS Principles)](https://doi.org/10.15497/RDA00068)
- [Five recommendations for FAIR software](https://fair-software.eu)
- [Research Software Directory](https://research-software-directory.org)
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>13 November 2023, Online, <a href="https://recherche.data.gouv.fr/fr/page/data-univ-eiffel-atelier-data-universite-gustave-eiffel">
    Data Univ Eiffel - RDV DATA sur les plateformes dediees aus logiciels
  </a></li>
</ul>
</section>
</section>
