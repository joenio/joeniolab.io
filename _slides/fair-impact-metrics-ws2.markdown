---
title: "fair-impact: Lightning talk 5min introduction"
lead: >
  fair-impact lighting talk slides for the worksession 2 presented at 21 June
  2024 online
theme: night
version: 2024.06.21
---

<section>
## fair-impact

### Lightning talk 5min introduction

#### _by Joenio M. Costa_

<small>_21 June 2024_</small>
</section>

<section data-background-image="/files/goodbye-baloon-inverse.jpg">
#### Joenio Marques da Costa
<a href="http://joenio.me/about">http://joenio.me/about</a>

&nbsp;

<img style="position:relative; float:right; width: 35%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 550px; font-size: 0.95em">
- Research Software Engineer
- Work at [Cortext](https://www.cortext.net) Platform
- Software Heritage Ambassador
- Data Univ Eiffel Ambassador
- Debian Contributor
- Computer Artist
</div>
</section>

<section>
## My work

- RSE at Gustave Eiffel University, France
- Backend software development and QA
- DevOps, documentation and management
- PhD candidate at Federal Univ. of Bahia, Brazil
  - Topic: Research Software Sustainability
</section>


<section>
## My proposal

- Apply F-UJI to Analizo and Cortext
- Improve Analizo and Cortext documentation
  - in terms of visibility, citation and findable
- Deploy F-UJI fork locally, test and evaluate
- Create a command line tool for F-UJI
- Write report about F-UJI results
</section>

<section>
<img style="width: 20%" src="https://www.analizo.org/analizo.png"/><br/>
Analizo is a multi-language Research Software tool for source code analysis

- Extraction and calculation of source code metrics
- Generation of dependency graphs
- Software evolution analysis
- Support for: C, C++, Java, C# and Python (beta)
- Free Software, written in Perl

[www.analizo.org](https://www.analizo.org)
</section>

<section data-background="#666">
<img style="width: 30%" src="/files/cortext-logo.png"/><br/>
Cortext is a web platform for data analysis and visualization for social sciences and humanities

- Partially Free Software, written in PHP, JS, Perl, Python
- Multiple repositories, maintained by a team of 5 developers
- Hosted by LISIS laboratory, funded by IFRIS and INRAE

[www.cortext.net](https://www.cortext.net)
</section>

<section data-background="white" style="color:black">
<small>F-UJI report for Analizo<br/>
<small>[https://github.com/analizo/analizo](https://github.com/analizo/analizo)</small>
</small><br/>
<img src="/files/fuji-metrics-analizo-report.png" style="width:90%"/><br/>
<small><small>_F-UJI report generated in 20 June 2024_</small></small>
</section>

<section>
## Progress

- Assess Analizo using the selected set of FRSM metrics
- Read and add comments [FAIR-IMPACT Metrics for Software](https://fair-impact.eu/metrics-software)
- Read and add comments [Research Software Metadata Guidelines (RSMD)](https://fair-impact.github.io/RSMD-guidelines)
</section>

<section>
## Problems and ideas

- Recommendation **RSMD-6.6** is mentioned on:
  - [fair-impact.eu/metrics-software](https://fair-impact.eu/metrics-software)
- But there is no **RSMD-6.6** listed on:
  - [fair-impact.github.io/RSMD-guidelines](https://fair-impact.github.io/RSMD-guidelines)
- [A new issue was opened on RSMD Github](https://github.com/FAIR-IMPACT/RSMD-guidelines/issues/8) 🐛
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>21 Jun 2024, Online, FAIR-IMPACT: Open Call path1 second Workshop</li>
</ul>
</section>
</section>
