---
title: The Carpentries + Software Heritage
theme: serif
version: draft
---

<!--

Presentation about the carpentries on software heritage ambassadors plenary at 7/12/2022.

Plenary agenda:
>  1. Introductions (15 min)
>  2. News and tour of the new features (10 min)
>  3. Activities by ambassadors (10 min + 20 min)
>  4. Joenio : training material for the Carpentries (10 min + 10 min)
>  5. Brainstorming (10 min)
>  6. Community event (5 min)

Talk title:

?

Talk description:

?
Joenio : training material for the Carpentries (10 min + 10 min)

Target audience:
- ?

Topics to include in the talk:
- ?

Links / references:

- https://zkamvar.github.io/rstudio-conf-2022/#p56
- https://docs.google.com/presentation/d/1fYTlCSQAkVPSalyFAcFxwqRIQ2Ez7YvQDIwOExyg5ts

-->

<section>
07 dec 2022<br/>Software Heritage ambassadors plenary
<div style="background-color: #AAB; box-shadow: 1px 1px 5px #AAE;">
![](/files/software-heritage-logo-title.svg)
<br/>+<br/>
![](/files/the-carpentries-logo.svg)
</div>
###### Joenio Marques da Costa
</section>

<section>
![](/files/the-carpentries-logo.svg)

[carpentries.org](https://carpentries.org)

---

<br/>spreading software and data science skills to researchers worldwide
</section>

<section data-background="/files/carpentries-never-teach-alone.jpeg">
<div style="color:white" class="box-blue">
<h3 style="color:white">Workshops</h3>

- 2-days, active learning
- Feedback to learners throughout the workshop
- Trained instructors
- Friendly learning environment
</div>
<div style="color:white" class="box-green">
![](/files/the-carpentries-logo-swc.svg)
![](/files/the-carpentries-logo-lc.svg)
![](/files/the-carpentries-logo-dc.svg)
</div>
</section>

<section>
<img src="/files/carpentries-slidedeck-general-graph.png" width="70%" style="box-shadow: 1px 1px 5px #AAE;" />
</section>

<section>
Some examples of workshops:

- [Automation and Make](http://swcarpentry.github.io/make-novice)
- [Data Management with SQL for Ecologists](https://datacarpentry.org/sql-ecology-lesson)
- [Introduction to Cloud Computing for Genomics](https://datacarpentry.org/cloud-genomics)
- [Introduction to R for Geospatial Data](https://datacarpentry.org/r-intro-geospatial)
- [Introduction to Working with Data](https://librarycarpentry.org/lc-data-intro)
- [Version Control with Git](http://swcarpentry.github.io/git-novice)

</section>

<section data-background="/files/carpentries-instructor-community-map-2022.png">
<div class="box-blue">
<h3 style="color:white">Instructors around the world</h3>

<a style="color:white; text-decoration:underline;" href="https://carpentries.org/instructors">carpentries.org/instructors</a>
</div>
</section>


<section>
# Oportunity!

- [FAIR Data & Software](https://librarycarpentry.org/lc-fair-research) (**status pre-alpha**)

<small>_source-code: [github.com/LibraryCarpentry/lc-fair-research](https://github.com/LibraryCarpentry/lc-fair-research)_</small>
</section>

<section data-background="/files/swh-ambassador-badge.svg">
<div class="box-white">
### Become an Instructor
![](/files/the-carpentries-logo.svg)

[carpentries.org/become-instructor](https://carpentries.org/become-instructor)
</div>
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>7 December 2022, Online, <a href="https://www.softwareheritage.org/ambassadors/">Software Heritage Ambassadors</a></li>
</ul>
</section>
</section>
