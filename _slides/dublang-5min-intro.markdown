---
title: "dublang: Short 5min introduction"
lead: >
  dublang lighting talk slides presented at FOSDEM 2024
theme: night
version: 2024.02.04
---

<section>
<img style="position: absolute; left: 20%; top: 0.7em; width: 10%" src="https://dublang.4two.art/artwork/dublang-logo.svg"/>
## dublang

### multi-language<br/>live coding system

#### _by Joenio M. Costa_
</section>

<section data-background-image="/files/goodbye-baloon-inverse.jpg">
#### Joenio Marques da Costa
<a href="http://joenio.me">http://joenio.me</a>

&nbsp;

<img style="position:relative; float:right; width: 40%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 500px; font-size: 0.95em">
 - Research Software Engineer
 - Work at [CorTexT](https://www.cortext.net) Platform
 - Software Heritage Ambassador
 - Debian Contributor
 - Live Coder and Visual Artist
</div>
</section>

<section>
##  dublang

- the name is inspired by the musical style dub
- dub consists of remixes of existing music
- dublang consists of remixes of existing software
</section>


<section data-background-color="white">
<h4 style="color:black">dublang architecture</h4>

<img style="width: 80%" src="https://dublang.4two.art/diagrams/plugin-architecture.png"/>
</section>

<section>
#### dublang code example

<pre style="box-shadow: none; font-size: 0.75em">
<code>
#!supercollider

{ SinOsc.ar([220, 140]) }.play

#!tidalcycles

d1 $ sound "808bd(3,8)"
   # shape 0.2

</code>
</pre>
</section>

<section>
#### dublang demo video

<iframe width="853" height="480" src="https://www.youtube.com/embed/i4snGjrhFwg" frameborder="0" allowfullscreen></iframe>
</section>

<section style="font-size: 0.6em; color: black" data-background-color="white">
| Plugin         | Description |
| -------------- | --------------------- |
| alda           | Alda programming language for music composition    |
| espeak         | eSpeak speech synthesizer                                     |
| festival       | Festival Speech Synthesis System                              |
| foxdot         | FoxDot Python-based language for making music            |
| fudi           | FUDI Pure Data networking protocol |
| git            | Git version control system                                    |
| gource         | Gource version control visualization tool                     |
| lebiniou       | Le Biniou music visualization tool                            |
| mpv            | MPV media player                                          |
| obs            | OBS Studio screencasting and streaming app                    |
| puredata       | Pure Data (Pd) visual programming language                    |
| sardine        | Sardine Python musical instrument                        |
| sm             | SM displays multi-line text fullscreen as large as possible |
| supercollider  | SuperCollider real-time audio synthesis environment |
| tidalcycles    | Tidal Cycles live coding environment and algorithmic patterns  |

</section>

<section>

### dublang webpage
- [https://dublang.4two.art](https://dublang.4two.art)

&nbsp;

### dublang source code
- [https://codeberg.org/joenio/dublang-dev](https://codeberg.org/joenio/dublang-dev)

</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>25 Nov 2023, Online, <a href="https://algoravebrasil.gitlab.io">Algorave Brasil 2023</a></li>
</ul>
</section>
</section>

[youtube-equinox-2020]: https://www.youtube.com/watch?v=qcE_a8IXk8o
