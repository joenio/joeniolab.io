---
title: Noosfero
---

<section data-background="/files/backgrounds/ocean.jpg">
# Noosfero

<img src="/files/noosfero.png" />

"Uma plataforma web livre para redes sociais e de economia solidária"
</section>

<!-- ********************************************************************** -->

<section>
## O Noosfero é

* Software Livre (Affero GPL)
* Desenvolvido colaborativamente
* Idealizado pela Colivre

[http://noosfero.org](http://noosfero.org)
</section>

<section>
### Funcionalidades do Noosfero

* Rede social
* Blog e notificação de comentários
* Compartilhamento de interesses
* Discussões temáticas
* Agenda de eventos
* Catálogo de serviços e produtos
</section>

<!-- ********************************************************************** -->

<section data-background="/files/backgrounds/timeline.jpg">
<div class="box-yellow">
## Histórico

O Noosfero v0.1.0 nasceu em 2007 entre uma parceiria da Colivre com:

* Fórum Brasileiro de Economia Solidária
* Ynternet.org Fondation (Suiça)

<img src="/files/fbes.png" />
<img src="/files/ynternet-org.png" />
</div>
</section>

<section data-background='white'>
  <img src="/files/colivre.png" />
  <p><strong>Colivre:</strong> Cooperativa de tecnologias livres</p>
  <ul>
    <li>Software Livre</li>
    <li>Economia Solidária e Autogestão</li>
    <li>Cooperativismo</li>
  </ul>
  <p>2005 ~ hoje [http://colivre.coop.br](http://colivre.coop.br)</p>
</section>

<section data-background="/files/backgrounds/timeline.jpg">
<div class="box-yellow">
#### FBES

## <span class="url">Cirandas.net</span>

Objetivo: "Fortalecer os empreendimentos e divulgar a Economia Solidária"

#### Ynternet.org

## <span class="url">Zen3.net</span>

Objetivo: "Plataforma para exercício pleno da 'cidadania digital' (netizenship)"
</div>
</section>

<section data-background="/files/backgrounds/timeline.jpg">
<div class="box-yellow">
### Desde então surgiram muitos outros parceiros

#### E o Noosfero hoje é utilizado por

ASL, USP, Serpro, SGPR, Blogoosfero, OCEB, UnB, Qualipso, IPF, Kliceo, UCSal, MPOG, UFBA

</div>
</section>

<section data-background="/files/backgrounds/timeline.jpg">
<p style="text-shadow:-2px -1px 3px #000;font-size:48px">E deste a primeira
versão do Noosfero lançada em 2007 foram lançadas mais de **200** versões</p>
</section>

<!-- ********************************************************************** -->

<section>

<section data-background="/files/screenshots/blogoosfero.jpg" data-background-transition="slide">
<div class="box-gray">
# blogoosfero.cc
</div>
</section>

<section data-background="/files/screenshots/cirandas.jpg" data-background-transition="slide">
<div class="box-gray">
# cirandas.net
</div>
</section>

<section data-background="/files/screenshots/fga-unb.png" data-background-transition="slide">
<div class="box-gray">
# fga.unb.br
</div>
</section>

<section data-background="/files/screenshots/stoa.jpg" data-background-transition="slide">
<div class="box-gray">
# social.stoa.usp.br
</div>
</section>

<section data-background="/files/screenshots/softwarelivre.png" data-background-transition="slide">
<div class="box-gray">
# softwarelivre.org
</div>
</section>

<section data-background="/files/screenshots/ucsal.jpg" data-background-transition="slide">
<div class="box-gray">
# www.ucsal.br
</div>
</section>

<section data-background="/files/screenshots/participa-br.png" data-background-transition="slide">
<div class="box-gray">
# participa.br
</div>
</section>

<section data-background="/files/screenshots/participa-ma.png" data-background-transition="slide">
<div class="box-gray">
## participa.ma.gov.br
</div>
</section>
<section data-background="/files/screenshots/ripe.jpg" data-background-transition="slide">
<div class="box-gray">
# ripe.ufba.br
</div>
</section>
<section data-background="/files/screenshots/softwarepublico.png" data-background-transition="slide">
<div class="box-gray">
## softwarepublico.gov.br
</div>
</section>
</section>

<!-- ********************************************************************** -->

<section data-background="white">
"instituições e usuários se tornam parte da comunidade"

<img width="80%" src="/files/noosfero-contributors-by-month.png" />

<small>[http://www.openhub.net/p/noosfero](http://www.openhub.net/p/noosfero)</small>
</section>

<!-- ********************************************************************** -->


<section data-background="#c4a000">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Créditos

<small>Imagens utilizadas nesta apresentação</small>

<ul style="font-size: 24px">
  <li>http://jindra12.deviantart.com/art/Ocean-211965086</li>
  <li>http://commons.wikimedia.org/wiki/File:Few_people_working_on_repairing_pond.jpg</li>
  <li>http://realsaw.deviantart.com/art/timeline-138456373</li>
  <li>http://ayudawordpress.com/varnish-como-servicio-en-wordpress</li>
  <li>http://sparklestarcat.deviantart.com/art/Monument-keyboard-358167802</li>
</ul>
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>11 Mai 2018, ???</li>
</ul>
</section>
</section>
