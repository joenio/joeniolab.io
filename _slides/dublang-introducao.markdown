---
title: "dublang: Introducao"
lead: >
  ...
theme: serif
version: draft
---

<section>
## dublang: Introducao

author: Joenio Marques da Costa
</section>

<section>
### Nome

o nome _dublang_ é inspirado no _dub_ e consiste em remixes de software existentes

_dublang_ = _dub_ + _language_
</section>

<section>
### Definicao

dublang é um sistema de live coding multi-linguagem numa interface integrada
</section>

<section>
### Objetivos

1. separar cliente x servidor
1. interface de usuario unica
1. extensivel com plugins
1. gerenciar "projetos" de arte
</section>

<section>
### Plugins

supercollider, tidal cycles, foxdot, sardine, pude data, le biniou, mpv, espeak, gource, etc...
</section>

<section>
#### Exemplo: PLugin mpv

<iframe width="560" height="315" src="https://www.youtube.com/embed/eaPy6QqdMdg?si=k_DlwRYaT8lxMt7_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</section>



<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>25 Nov 2023, Online, <a href="https://algoravebrasil.gitlab.io">Algorave Brasil 2023</a></li>
</ul>
</section>
</section>

[youtube-equinox-2020]: https://www.youtube.com/watch?v=qcE_a8IXk8o
