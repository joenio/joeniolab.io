---
title: Teste de Software
theme: white
version: draft
---

<section>
# Teste de Software

</section>

<section>
# Definição

## O que é Teste de Software?
</section>

<section>
### Uma das principais causas que levam a testes de software ruims é o fato de programadores começarem com uma falsa definição de **teste de software**

# O que é Teste de Software?
</section>

<section>
### O que é Teste de Software?

1. <span class="fragment highlight-red">Testar é o processo de demonstrar ausência de erros no software</span>
<!-- Testing is the process of demonstrating that errors are not present -->
1. <span class="fragment highlight-red">O propósito de testar é mostrar que o software executa suas funções corretamente</span>
<!-- The purpose of testing is to show that a program performs its intended functions correctly -->
1. <span class="fragment highlight-red">Testar é o processo de estabelecer a confiança de que o software faz o que ele deveria fazer</span>
<!-- Testing is the process of establishing confidence that a program does what it is supposed to do -->

<span class="fragment fade-in">**Testar é o processo de executar o programa com a intenção de encontrar erros**</span>
<!-- Testing is the process of executing a program with the intent of finding errors -->
</section>

<section>
## <span style="color: blue">Testar é o processo de executar o programa com a intenção de encontrar erros</span>
</section>

<section>
## Bem-sucedido e Mal-sucedido

Uma forma de reforçar uma boa definição para teste é analisar o significado de teste **bem-sucedido** e teste **mal-sucedido**
</section>

<section>
## O que é um teste
# (1) bem-sucedido?
# (2) mal-sucedido?
</section>

<section>
Muitos gestores de projetos chamam testes que <span style="color:green">passam</span> de:

* **Execução de teste bem-sucedida**

E testes que <span style="color:red">falham</span> de:

* **Execução de teste mal-sucedido**
</section>

<section>
### No entanto... essa definição está invertida!
</section>

<section>
Um teste bem construído é **bem-sucedido** quando encontra erros que podem ser corrigidos.

O mesmo teste é também **bem-sucedido** quando eventualmente estabelece que não há mais erros a ser descobertos.
</section>

<section>
O único teste **mal-sucedido** é aquele que não examina apropriadamente o software.

Um teste que não encontra erros pode ser considerado **não-sucedido**, uma vez que o conceito de software sem erros é irreal.
</section>

<section>
## Entretanto...

É importante lembrar que, em geral, é impraticável, ou mesmo impossível, encontrar todos os erros de um programa.

Isto gera o desafio de encontrar o equilíbrio ideal de quanto teste é necesário e/ou possível, uma vez que há implicações econômicas no processo de testar software..
</section>

<section>
### Princípios de Teste de Software

* Uma parte necessária de um caso de teste é a definição das saídas ou resultados esperados
</section>

<section>
### Princípios de Teste de Software

* Inspecione minuciosamente os resultados de cada teste
</section>

<section>
### Princípios de Teste de Software

* Casos de teste devem ser escritos para entradas inválidas e não-esperadas mas também para aquelas válidas e esperadas
</section>

<section>
### Princípios de Teste de Software

* A possibilidade de existência de mais erros numa seção do programa é proporcional ao número de erros já encontrados nesta seção
</section>

<section>
### Princípios de Teste de Software

* Teste é uma tarefa extremamente criativa e intelectualmente desafiadora
</section>

<section>
Modificar um software existente é mais propenso a erros do que escrever um novo software.

Por conta disso um bom conjunto de testes reduz essa tendência e reduz o custo de manutenção.
</section>

<section>
Uma boa recomendação é desenvolver casos de teste usando métodos
**caixa-preta** e na sequência desenvolver testes adicionais usando métodos
**caixa-branca**.
</section>

<section>
<img src="/files/White_Box_Testing_Approach.png" width="100%" />
</section>

<section>
Exemplo de teste **caixa-branca**:

Teste de módulos (ou testes unitários) é o processo de testar
individualmente subprogramar, subrotinas, ou procedimentos de um programa.

Testar com foco nos pequenos blocos de software primeiro.
<!--
Module testing (or unit testing) is a process of testing the individ-
ual subprograms, subroutines, or procedures in a program. That is,
rather than initially testing the program as a whole, testing is first
focused on the smaller building blocks of the program. The moti-
vations for doing this are threefold.
-->
</section>
<!--
É necessário dois tipos de informação ao projetar casos de teste para testes unitários:

1. A especificação do módulo
2. O código-fonte do módulo
Test-Case Design
You need two types of information when designing test cases for a
module test: a specification for the module and the module’s source
code. The specification typically defines the module’s input and out-
put parameters and its function.
Module testing is largely white-box oriented.
-->


<section>

![níveis de testes](/files/test-level.png)

</section>


<section>
## Unidade de software

Uma unidade é o menor componente possível de software capaz de ser testado.

* Realiza uma única função de maneira coesiva
* Pode ser compilado separadamente
* Representa uma tarefa singular do ponto de vista gerencial
* Contém código que cabe numa única página ou tela
</section>

<section>
Em testes unitários há um esforço de detectar defeitos relacionados a funcionalidade e estrutura de cada unidade.
</section>

<section>
## Testes de integração

Testes de integração possui duas metas principais:

1. Detectar defeitos que ocorrem nas interfaces das unidades de software
1. Juntar unidades individuais em um subsistema funcional e finalmente num sistema completo e pronto para testes de sistema
</section>

<!--
There is some simple testing of
unit interfaces when the units interact with drivers and stubs. However,
the interfaces are more adequately tested during integration test when
each unit is finally connected to a full and working implementation of
those units it calls, and those that call it. As a consequence of this assembly
or integration process, software subsystems and finally a completed sys-
tem is put together during the integration test. The completed system is
then ready for system testing
-->

<section>
## Testes de sistema

Testar o sistema como um todo, já com testes unitários e testes de integração finalizados.

O objetivo é garantir que o sistema está deacordo com os seus requisitos.

**caixa-preta**
</section>


<section>
Testes de sistema avaliam tanto o comportamento funcional quanto os requerimentos de qualidade como por exemplo:

* Confiabilidade
* Usabilidade
* Performance
* Segurança

Esta fase de testes é especialmente útil para detectar erros externos de hardware ou interfaces com outros projetos de software.
</section>

<!--
System
test evaluates both functional behavior and quality requirements such as
reliability, usability, performance and security. This phase of testing is
especially useful for detecting external hardware and software interface
defects, for example, those causing race conditions, deadlocks, problems
with interrupts and exception handling, and ineffective memory usage.

When integration tests are completed, a software system has been assem-
bled and its major subsystems have been tested. At this point the devel-
opers/testers begin to test it as a whole. System test planning should begin
at the requirements phase with the development of a master test plan and
requirements-based (black box) tests. System test planning is a compli-
cated task. There are many components of the plan that need to be pre-
pared such as test approaches, costs, schedules, test cases, and test pro-
cedures. All of these are examined and discussed in Chapter 7.
System testing itself requires a large amount of resources. . 
After system test the software will be turned over to users for evaluation
during acceptance test or alpha/beta test. The organization will want to
be sure that the quality of the software has been measured and evaluated
before users/clients are invited to use the system. In fact system test serves
as a good rehearsal scenario for acceptance test.
-->


<section>
### Tipos de testes de sistema

* Teste funcional
* Teste de performance
* Teste de stress
* Teste de configuraçao
* Teste de segurança
* Teste de recuperação
</section>

<section>
## Teste funcional de sistema

Testes funcionais de sistema tem a muita sobreposição com os testes de aceitação.

Muito frequentemente os mesmos conjuntos de teste podem ser aplicados em ambos.

Ambos demonstram a funcionalidade do sistema.
</section>

<!--
of the system’s functionality. Functional tests at the system level are used
to ensure that the behavior of the system adheres to the requirements
specification. All functional requirements for the system must be achiev-
able by the system. For example, if a personal finance system is required
to allow users to set up accounts, add, modify, and delete entries in the
accounts, and print reports, the function-based system and acceptance
tests must ensure that the system can perform these tasks. Clients and
users will expect this at acceptance test time.
Functional tests are black box in nature. The focus is on the inputs
and proper outputs for each function. Improper and illegal inputs must
also be handled by the system. System behavior under the latter circum-
stances tests must be observed. All functions must be tested.
Many of the system-level tests including functional tests should be
designed at requirements time, and be included in the master and system
test plans (see Chapter 7). However, there will be some requirements
changes, and the tests and the test plan need to reflect those changes.
Since functional tests are black box in nature, equivalence class partition-
ing and boundary-value analysis as described in Chapter 4 are useful
-->

<section>
## Testes de aceitação

Os testes de aceitação representam as expectativas dos clientes e usuários.
</section>

<section>
![níveis de testes](/files/test-level.png)
</section>

<section>
# Test Harness

Código auxiliar desenvolvido para dar suporte ao teste das unidades e componentes de software.

O _Harness_ consiste de drivers que executam o código a ser testado e emcapsulam os módulos que serão testados.

<!--
The auxiliary code developed to support testing of units and components is called
a test harness. The harness consists of drivers that call the target code and stubs
that represent modules it calls.

T h e T es t H a r n e s s
In addition to developing the test cases, supporting code must be devel-
oped to exercise each unit and to connect it to the outside world. Since
the tester is considering a stand-alone function/procedure/class, rather
than a complete system, code will be needed to call the target unit, and
also to represent modules that are called by the target unit. This code
called the test harness, is developed especially for test and is in addition
to the code that composes the system under development. The role is of
the test harness is shown in Figure 6.5 and it is defined as follows:
-->
</section>


<section>
HelloWorld em Perl:

* Mojolicious (Perl): https://mojolicious.org/

Mojolicious é um framework Perl para aplicações web.
</section>

<section>

<pre>
<code style="font-size:1.2em">
# hello.pl

use Mojolicious::Lite;

get '/' => {
  text => 'Hello world!'
};

app->start;
</code>
</pre>

</section>

<section>
Executar HelloWorld:

<pre>
<code style="font-size:1.2em">
$ morbo hello.pl
Server available at http://127.0.0.1:3000
</code>
</pre>

Request (GET) HelloWorld:

<pre>
<code style="font-size:1.2em">
$ curl http://localhost:3000

Hello world!
</code>
</pre>
</section>

<section>
## Test Harness (Perl)

* Test::More
* Test::Mojo
</section>

<section>

<pre>
<code style="font-size:1.2em">
# hello-test.t
use Test::More;
use Mojo::File 'path';
use Test::Mojo;

my $hello = path(__FILE__)->dirname->sibling('hello.pl');
my $t = Test::Mojo->new($hello);

$t->get_ok('/')->content_like(qr/Hello world!/);

done_testing();
</code>
</pre>

</section>


<section>
Executar teste:

```console
$ prove hello-test.t
```
</section>
