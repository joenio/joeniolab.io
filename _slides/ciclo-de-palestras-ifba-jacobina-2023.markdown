---
title: "CICLO DE PALESTRAS: Trabalhar e viver com tecnologias livres e abertas, é possível?"
theme: serif
version: 2023.07.11
---

<section>
#### CICLO DE PALESTRAS

### Trabalhar e viver com tecnologias livres e abertas, é possível?
</section>

<section>
<div style="position:relative; width: 500px; height: 600px; float:left">
<img style="position:absolute; left:0; top:20px" width="80%" src="/files/sequencia.gif" />
<h4 style="position:absolute; text-align: center; margin:-15px -10px; padding:0; z-index:100">Joenio Marques da Costa</h4>
<a style="position:absolute; z-index:100; bottom:40px; left:5px; margin:0 40px" href="http://joenio.me">http://joenio.me</a>
</div>
<div>
Engenheiro de Software<br/><small>(Research Software Engineer)</small><br/>
Software Livre<br/><small>(Free Software, Open Source)</small><br/>
Arte e Musica<br/><small>(Live Coding, Visual Music)</small><br/>
CorTexT Platform<br/><small>(A digital lab for humanities)</small><br/>
RISIS<br/><small>(European Research Infrastructure for STI)</small><br/>
</div>
</section>

<section>
### O que sao tecnologias<br/>Livres e Abertas?

([slides sobre software livre](/software-livre))
</section>

<section>
<section>
### Experiência pessoal com tecnologias<br/>Livres e Abertas.

([timeline](/timeline))
</section>

<section>
### 2000 a 2005

- 2000: UCSAL, Analizo, Perl Mongers
- 2002: Estagio, Slackware Linux, G4U
- 2004: Estagio, Cold Fusion, Desenvolvimento web
- 2005: Trabalho JaCotei, Perl OO, Web spiders, FISL e YAPC::Brasil
</section>

<section>
### 2006 a 2010

- 2006: PSL-BA, Festival Software Livre BA, SaferNet, Colivre, FISL e YAPC::Brasil
- 2007: Comunidade Perl, Festival sofware livre bahia III
- 2008: Conisli, YAPC::Brasil 2008, Rails Summit, Colaboracao Foswiki e Noosfero
- 2009: Colivre no FISL, YAPC::SA
- 2010: Artigo Analizo no CBSOFT 2010, YAPC::Brasil, inicio contribuicao Debian
</section>

<section>
### 2010 a 2015

- 2012: Divulgacao e palestras Noosfero
- 2013: Rails Girls SSA, Noosfero no Latinoware, Autonomo + Colivre
- 2014: Projeto Participa.br, Inicio mestrado UFBA
- 2015: Projeto portal da Juventude, Projeto portal do Software Publico no LAPPIS
</section>

<section>
### 2016 a 2020

- 2016: Live coding no Calango Hacker Clube
- 2017: Projeto Ecossistema de Software Livre do MinC no LAPPIS
- 2018: Performance e Oficinas de Live Coding, Django Girls, MiniDebConf
- 2019: MediaLab UnB, Album `}bio{borgs`, Obras e performances de Lice Coding, DebConf 19, Demoscene no #18.art
- 2020: Desempregado, CNJ no Projeto mais Justica, Franca
</section>

<section>
### 2020 a hoje
- 2021: Web-exposicao EmMeio#13.0, CEAM NEAZ
- 2022: Performance Live Coding Paris, The Carpentries, DebConf22, Software Heritage
- 2023: FOSDEM, Performances, Workshops e palestras de Live Coding
</section>

</section>

<section>
Como você acha que é possível trabalhar e viver com tecnologias livres e abertas nos tempos atuais?
</section>

<section data-background="#c4a000" data-background-transition="slide">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>13 Jul 2023, IFBA - Jacobina, CICLO DE PALESTRAS: Trabalhar e viver com tecnologias livres e abertas, é possível?</li>
</ul>
</section>
</section>
