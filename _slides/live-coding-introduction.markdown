---
title: Introduction and demonstration of Live Coding technique and tools - Séminaire Scientifique DIGIS
version: 2019.04.22
theme: serif
---

<section>

# Live Coding

### Introduction and demonstration of technique and tools

---

### Séminaire Scientifique DIGIS
</section>

<section data-background-image="/files/goodbye-baloon-inverse.jpg" style="color:white">
<h4 style="color:white">Joenio Marques da Costa</h4>
<a style="color:#4444FF" href="http://joenio.me">http://joenio.me</a>

&nbsp;

<img style="position:relative; float:right; width: 40%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 500px; font-size: 0.95em">
 - Research Software Engineer
 - Work at <a style="color:#4444FF" href="https://www.cortext.net">CorTexT</a> Platform
 - Software Heritage Ambassador
 - Debian Contributor
 - Live Coder and Visual Artist
</div>
</section>

<section>
## Live Coding is...

* Sound art, visual art, others...
* A community building technology free from utilitarism constraints
* Sound samples and sound synthesis
</section>

<section data-background="/files/yamaha-dx7.jpg">
<!-- além do oscilador, outros componentes básicos fazem parte de praticamente qualquer sintetizador: amplificador, filtro, envelope e LFO -->
<div class="box-gray">
<table><tr>
<td>
<h3 style="color:white">Sample</h3>
<audio src="/files/water-to-glass.wav" controls>
  <a href="/files/water-to-glass.wav">water-to-glass.wav</a>
</audio>
</td>
<td>
<h2 style="color:white">x</h2>
</td>
<td>
<h3 style="color:white">Synthesizer</h3>
<audio src="/files/pitfall-ouro.wav" controls>
  <a href="/files/pitfall-ouro.wav">pitfall-ouro.wav</a>
</audio>
</td>
</tr></table>
<small style="color:white">both concepts created from studies at the _electroacoustic_ field (or _musique concrète_)</small>
</div>
<!-- conceito 1: samples e conceito 2: sintetizadores, ambos criações das pesquisa do campo da eletroacústica -->
</section>

<section data-background="/files/buchla_100_NYU.jpg">
<div class="box-gray">
<h2 style="color: white">Synthesizers</h2>
<h3 style="color: white">Synthesizers are instruments able to generate all kind of sound.</h3>
</div>
</section>

<section>
## Oscilators

Synthesizers are strong related with the **oscilators** evolution.

<!-- osciladores são componentes eletrônicos capazes de gerar sinais eletrônicos periódicos, possuem diversas aplicações, como gerar sinais de rádio por exemplo -->
<img src="/files/integrierter-quarzoszillator.jpg" /><img src="/files/waveforms.svg" width="36%"/>
</section>

<section>
## First analog synthesizers

<!-- os primeiros sintetizadores originalmente surgiram os sintetizadores analógicos, surgiram em paralelo: -->

<img src="/files/buchla-100_NYU.jpg" width="45%" /> <img src="/files/moog-synthesizer_1964_NY.jpg" width="45%" />

Buchla 100 (1965) and Moog Synthesizer (1964)

<!--
(1) Buchla & Associates, Berkeley, California em 1965 criou o "Buchla 100 series Modular Electronic Music System"
    Em 1967 Morton Subotnick compôs o álbum "Silver Apples Of The Moon" inteiramente composto no Buchla 100

(2) Robert Moog, Cornell, NY em 1964 vendeu seu primeiro protótipo "Moog synthesizer"
    Em 1968 Wendy Carlos lança o álbum "Switched-On Bach" com músicas Johann Sebastian Bach tocadas por ele num Moog
-->
  <aside class="notes">
    the first synthesizers were the analog
     ones, and the two key inventions this
     time was Buhcla 100 and Moog Synthesizer
     from 1965 and 1964 respenclty, research
     and experiments before the release of
     these two models started in paralell
  </aside>
</section>

<section>
# Software

<!-- a partir da disseminação destes sintetizadores surgiram uma infinidade de produtos, chegando aos sintetizadors em software, ou softsynth -->
Since the first analog synthesizers many others products were created, including:

## Softsynth

<!-- dada esta introdução apresento alguns sintetizadores (softsynth) existentes no debian e ferramentas relacionadas a manipulação sonora e musical, gravação, edição e outras ferramentas -->

  <aside class="notes">
    softsynths are synthesizers implemented
     in software form, they have the same
     principles of any other synth and we
     have so many options in Debian
    but before start to see some softsynths
     is necessary to know a little about
     jack
  </aside>
</section>

<section>

<img src="/files/supercollider.svg" width="20%" style="vertical-align: middle" /> <span style="font-size:1.5em">&nbsp;SuperCollider</span>

_SuperCollider is an audio server, programming language, and IDE for sound synthesis and algorithmic composition._

[supercollider.github.io](https://supercollider.github.io)
</section>

<section>
### SuperCollider<br/>Demo

<span style="font-size: 8em">🎧</span>
</section>

<section>
### Live Coding<br/>Community

<table><tr>
<td style="text-align: center">
<img src="/files/300px-Toplap.png" />
<br/>
<a href="https://toplap.org">TOPLAP</a>
<br/>
(2004)
</td>
<td style="text-align: center">
<img src="/files/algorave_logo.png" />
<br/>
<a href="https://algorave.com">Algorave</a>
<br/>
(2011)
</td>
</tr></table>
</section>

<section>
### Live Coding<br/>Community

<table><tr>
<td style="text-align: center">
<img src="/files/clic_sticker.png" width="50%" />
<br/>
<a href="https://colectivo-de-livecoders.gitlab.io">CLiC</a>
<br/>
(2018)
</td>
<td style="text-align: center">
<img src="/files/algoravebr_logo.png" width="40%" />
<br/>
<a href="https://algoravebrasil.gitlab.io">Algorave Brasil</a>
<br/>
(2019)
</td>
</tr></table>
</section>

<section data-background-color="black" style="color:white">
<h3 style="color:white">Live Coding<br/>Community</h3>

<table><tr>
<td style="text-align: center">
<img src="/files/cookie_paris_400x400.jpg" width="50%" />
<br/>
<a style="color:#4444FF" href="https://www.cookie.paris">Cookie Collective</a>
<br/>
(Paris, Fr)
</td>
</tr></table>
</section>

<section data-background="/files/iclc-2017.png">
<div class="box-green">
<h4 style="color:white">International Conference on Live Coding</h4>

<a style="color:#4444FF" href="https://iclc.toplap.org">iclc.toplap.org</a>
</div>

</section>

<section data-background-color="#FFE53A">
## People
</section>

<section>
### Alex McLean

<img width="40%" src="/files/avatar-yaxu.jpeg"/><br/>
[yaxu.org](https://yaxu.org)<br/>
`Tidal Cycles`
</section>

<section>
### Olivia Jack

<img width="40%" src="/files/avatar-ojack.jpg"/><br/>
[ojack.xyz](https://ojack.xyz)<br/>
`Hydra`
</section>

<section>
### Antonio Roberts

<img width="40%" src="/files/avatar-hellocatfood.png"/><br/>
[hellocatfood.com](https://www.hellocatfood.com)<br/>
`(Algo|Afro) Futures`
</section>

<section>
### Iris Saladino

<img width="40%" src="/files/avatar-iris.jpg"/><br/>
[1riss.github.io](https://1riss.github.io)<br/>
`CLiC`
</section>

<section>
### Crash Server

<img width="40%" src="/files/avatar-crashserver.jpg"/><br/>
[crashserver.fr](https://crashserver.fr)<br/>
`France`
</section>


<section>
## Free Software
</section>

<section>
![](/files/livecoding-book-users-manual.webp)

[Live Coding: A User's Manual](https://livecodingbook.toplap.org)
</section>

<section>
### Referencies

- [Awesome Live Coding](https://github.com/toplap/awesome-livecoding) - A curated list of live coding languages and tools
- [Awesome Music](https://github.com/noteflakes/awesome-music) - A categorized collection of awesome music libraries, tools, frameworks and software
</section>

<section>
### Videos and documentaries

* [on-the-fly.documentary](https://www.youtube.com/watch?v=ntFMuvv2-TY&list=LL&index=6) (2022)
* [Algorave Generation Resident Advisor](https://www.youtube.com/watch?v=S2EZqikCIfY) (2019)
* [Live-Coding programming masterly music](https://www.youtube.com/watch?v=Ix2b_qFYfAA) (2018)
* [Algorave: algorithmic dance culture](https://www.youtube.com/watch?v=nAGjTYa95HM&t=704s) (2017)
</section>


<section>
<img width="60%" src="/files/tidalcyclesid.svg" />

_TidalCycles is a live coding environment for musical improvisation and composition, it is a domain-specific language embedded in Haskell._

[tidalcycles.org](https://tidalcycles.org)
</section>


<section>
# Hydra

Live Coding networked visuals in the browser

[hydra.ojack.xyz](https://hydra.ojack.xyz)

### By: Mari Moura
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>11 March 2024, UGE, Marne-la-Vallee, France, <a href="https://digis.hypotheses.org/1328">Séminaire Scientifique DIGIS</a></li>
</ul>
</section>
</section>
