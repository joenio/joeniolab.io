---
title: Software Heritage Symposium and summit 2023
theme: white
version: 2023-02-06
---

<!--

Presentation about the carpentries on software heritage symposium 07/02/2023.

Target audience:
- SWH community
- SWH ambassadors

-->

<section>
<div style="background: #EFEFF0; border-radius: 10px; padding: 5px 0 5px 0">
![](/files/software-heritage-logo-title.svg)
<br/>+

![](/files/the-carpentries-logo.svg)
<br/>
</div>
</section>

<section data-background='white'>
![](/files/software-heritage-logo-noicon.svg)

![](/files/SWH-logo_anim-texte1.gif)

_Universal software source code archive_
</section>

<section>
<div style="background: #EFEFF0; border-radius: 10px; padding: 5px 0 0 0">
![](/files/the-carpentries-logo.svg)

spreading software and data science skills to researchers worldwide

<small>[carpentries.org](https://carpentries.org)</small>
</div>
</section>

<section data-background='#EFEFF0'>
<div style="background: white; border-radius: 10px; padding: 5px 0 5px 0">
The Carpentries History

<img src='/files/SWCDChistory.png'/>
</div>
<small>source: [carpentries.github.io/instructor-training](https://carpentries.github.io/instructor-training/15-carpentries)</small>
</section>

<section style='font-size: 0.7em'>
<div style="background: #EFEFF0; border-radius: 10px; padding: 5px 0 5px 0">
<span style='font-size: 1.4em'>The Carpentries Audience</span>

- Software Carpentry<br/>
  _researchers who need to program more effectively_

- Data Carpentry<br/>
  _researchers who are dealing with significant data_

- Library Carpentry<br/>
  _people in library and information related roles_
</div>
</section>

<section data-background="/files/carpentries-never-teach-alone.jpeg" style='font-size: 0.8em'>
<div style="color:white; border-radius: 10px" class="box-blue">
<h3 style="color:white">Workshops</h3>

- 2-days, active learning
- centrally-organised or self-organised
- feedback to learners throughout the workshop
- trained instructors
- friendly learning environment
</div>
</section>

<section data-background="/files/carpentries-never-teach-alone.jpeg">
<div style="background: white; border-radius: 10px; padding: 5px 0 30px 0; font-size: 0.8em">
Some examples of workshops:

- [Automation and Make](http://swcarpentry.github.io/make-novice)
- [Data Management with SQL for Ecologists](https://datacarpentry.org/sql-ecology-lesson)
- [Introduction to Cloud Computing for Genomics](https://datacarpentry.org/cloud-genomics)
- [Introduction to R for Geospatial Data](https://datacarpentry.org/r-intro-geospatial)
- [Introduction to Working with Data](https://librarycarpentry.org/lc-data-intro)
- [Version Control with Git](http://swcarpentry.github.io/git-novice)
</div>
</section>

<section data-background="/files/carpentries-instructor-community-map-2022.png">
<div class="box-blue" style='border-radius: 10px'>
<h3 style="color:white">Instructors around the world</h3>

<a style="color:white; text-decoration:underline;" href="https://carpentries.org/instructors">carpentries.org/instructors</a>
</div>
</section>

<section>
<span style='font-size: 0.8em'>The Carpentries Annual Report 2021</span>
<img style="border: 1px dashed #BBB; border-radius: 10px" width="70%" src='/files/carpentries-annual-report-2021.png'/>
<small>source: [carpentries.org/reports](https://carpentries.org/reports)</small>
</section>

<section>
<div style="background: #EFEFF0; border-radius: 10px; padding: 5px 0 5px 0">
![](/files/software-heritage-logo-title.svg)
# ?
![](/files/the-carpentries-logo.svg)
<br/>
</div>
</section>


<section style='font-size: 0.6em'>
### Roadmap

<div style='text-align: left; margin: auto 15%'>
<span style='margin-left: -25px'>☑ Choose: ❌ create a new lesson or ❎ add SWH to existing ones<br/></span>
<span style='margin-left: -25px'>☑ Identify potential Carpentries lessons and episodes to contribute, sort by priority<br/></span>
<span style='margin-left: -25px'>☐ List SWH topics, concepts and features to add on each identified lesson<br/></span>
<span style='margin-left: -25px'>☐ Choose the 1st lesson, 1st episode to add SWH material<br/></span>
</div>
</section>

<section data-background="#ffef9f">
Let's work

<img src='/files/cat-typing-2.gif' style='border-radius: 10px; box-shadow: 0 0 5px gray'/>
<!-- gif source: https://www.collegeessayadvisors.com/8-pieces-of-resume-advice-you-need-to-hear-before-applying-for-an-internship/cat-typing-2-2/ -->
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this slide was used</small>

<ul style="font-size: 24px">
  <li>7 February 2023, Paris, <a href="https://www.softwareheritage.org/symposium-and-summit-2023">Software Heritage Symposium and summit 2023</a></li>
</ul>
</section>
</section>
