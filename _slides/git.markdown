---
title: Git
version: 2022.02.10
---

<!--
TODO conteudos para adicionar:

  * O que é git
  * Primeiros passos com git
  * Conceitos
  * Commits
  * Merges
  * Forks
  * Branches

bons links em pt-br com git para nao-devs:

  * https://blog.rocketseat.com.br/iniciando-com-git-github/
  * https://br.udacity.com/blog/post/tutorial-iniciantes-git-github
-->

<section>
<img src='/files/git-goodness.gif' />
</section>

<section data-background="white">
{% include slides_sobre.html %}
</section>

<section>
**Git** é um sistema de controle de versões distribuído, usado principalmente no
desenvolvimento de software, mas pode ser usado para registrar o histórico de
edições de qualquer tipo de arquivo.

[http://git-scm.com](http://git-scm.com)
</section>


<section>
**Sistema de controle de versões** na função prática da Ciência da Computação e da Engenharia de Software, é um software que tem a finalidade de gerenciar diferentes versões no desenvolvimento de um documento qualquer.

[http://pt.wikipedia.org/wiki/Sistema_de_controle_de_versões](http://pt.wikipedia.org/wiki/Sistema_de_controle_de_versões)
</section>

<section data-background="white">
<table>
  <tr>
    <td style="vertical-align: middle"><b>Versões?</b></td>
    <td><img src="/files/Revision_controlled_project_visualization-2010-24-02.svg" /></td>
  </tr>
</table>
</section>

<section>
Principais vantagens

* Trabalho em equipe
* Resgatar versoes anteriores
* Rastreabilidade
</section>

<section>
![](/files/Resumes-in-Order-by-Date.png)

<small>
  fonte: [The Non-Developers Guide to Git and GitHub](https://wpmudev.com/blog/the-non-developers-guide-to-git-and-github)
</small>
</section>

<section>
como instalar?

[https://www.atlassian.com/br/git/tutorials/install-git](https://www.atlassian.com/br/git/tutorials/install-git)
</section>

<section>
`git init .`
</section>

<section>
crie um arquivo texto com o nome `participantes.txt`
</section>

<section>
`git add participantes.txt`
</section>

<section>
`git commit`
</section>

<section>
`git log`
</section>

<section>
modifique e salve o arquivo `participantes.txt`
</section>

<section>
`git status`

`git diff`
</section>

<section>
`git add participantes.txt`
</section>

<section>
`git commit -m "fulano adicionado"`
</section>

<section>
`git log`
</section>

<section>
gitg

![](/files/org.gnome.gitg.svg)

[https://wiki.gnome.org/Apps/Gitg](https://wiki.gnome.org/Apps/Gitg)
</section>

<section data-background="#c4a000" data-background-transition="slide">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>10 Fev 2022, UnB/CEAM - Online, disciplina Metodologia e Técnicas de Pesquisa Científica</li>
</ul>
</section>
</section>
