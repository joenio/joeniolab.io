---
title: Software Management Plan and Software Heritage
version: 2024.08.27
theme: beige
---

<!--

- Organization: Sabrina
- SWH ambassadors invited:
  - Alexis
  - Bertrand
- duration: 2 h
  - Welcome and round table: 30’
  - Warm up: 10’
  - Collaborative work: 60'
  - Conclusions and bye: 20’

Room: https://webinaire.numerique.gouv.fr//meeting/signin/25763/creator/12492/hash/c526628213700754f4746189e11ca99c464f8972

-->

<section>
<small>August 27th, 2024</small>

## Software Management Plan

### and Software Heritage

---

by Joenio Marques da Costa

</section>

<section data-background-image="/files/goodbye-baloon-inverse.jpg" style="color:white">
<h3 style="color:white">Joenio Marques da Costa</h3>
<a style="color:#4444FF" href="http://joenio.me">http://joenio.me</a>

&nbsp;

<img style="position:relative; float:right; width: 35%; top: -20px" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 550px; font-size: 0.96em">
 - Research Software Engineer
 - Work at <a style="color:#4464AF; font-weight:bold; text-decoration:underline" href="https://www.cortext.net">Cortext.net Platform</a>
 - Software Heritage Ambassador
 - Debian Developer
 - Live Coder and Visual Artist
</div>
</section>

<section>
## Agenda

- Duration: ~ 2 h
  - Welcome: 10 min
  - Round table: 20 min
  - Warm up: 10 min
  - Collaborative work: 60 min
  - Conclusions and bye: 20 min
</section>

<section>
### Round table
<span style="font-size:6em">🗣<small style="vertical-align:baseline">🎙</small></span>
- Your profile?
- Role in your institution?
</section>

<section>
### Warm up
<span style="font-size:5em">🖹<span style="vertical-align:middle">✍</span></span>
- Has your institution made a decision regarding SMP?
- What are the current practices in your community?
</section>

<section>
### Collaborative work: Question (1.a)
<span style="font-size:4em">🖹<span style="vertical-align:middle">✍</span></span>
- In which documents do you keep metadata of your Research Software project?
- How is it updated by the different actors?
- Who are the actors?
</section>

<section>
### Collaborative work: Question (1.b)
<span style="font-size:4em">🖹<span style="vertical-align:middle">✍</span></span>
- How to solve it?
- Proposals?
- Examples?
</section>


<section>
## Questions (1.a) and (1.b)
# Conclusions
</section>

<section>
### Collaborative work: Question (2)
<span style="font-size:4em">🖹<span style="vertical-align:middle">✍</span></span>
- How to add <span class="fragment highlight-red">SWH</span> into it?
- In which topic in a specific SMP model (Elixir, ODIPOR, PRESOFT, etc) the <span class="fragment highlight-red">SWH features (Archiving, Referencing)</span> can be linked?
</section>


<section data-background='white'>
![](/files/software-heritage-logo-noicon.svg)

![](/files/SWH-logo_anim-texte1.gif)

_Universal software source code archive_
<small style="display:block">[softwareheritage.org](https://www.softwareheritage.org)</small>
</section>


<section>
![](/files/software-heritage-logo-title.svg)<br/>
<img src="/files/swh-dataflow-merkle-1.png" style="margin-top:-20px" />
<br/>🛈 <small style="vertical-align: middle">
_More than 150M projects, almost **10 billion** unique source files as of January 2021_
</small>
</section>


<section data-background='white'>
### Software Heritage archive
<img src='/files/browse_search_swh.jpeg' width='30%' />

_The long term source code archive._

<small>[archive.softwareheritage.org](https://archive.softwareheritage.org)</small>

<aside class="notes">
Do a live demonstration archiving the repository
[github.com/joenio/swhid-citation-example](https://github.com/joenio/swhid-citation-example)
that is not yet archived.
</aside>
</section>

<section data-background='white'>
### Software Hash identifier (SWHID)
<img src='/files/swhid_provider_resolver.png' width='30%' />

_Intrinsic identifiers for digital objects._

<small>[www.swhid.org](https://www.swhid.org)</small>
</section>

<section>
<img src='/files/swhid-citation-example.png' width='60%' style='box-shadow: 1px 1px 5px gray'/>

<small>Source code: [github.com/joenio/swhid-citation-example](https://github.com/joenio/swhid-citation-example)</small>
</section>

<section>
### Collaborative work: Question (2)
<span style="font-size:4em">🖹<span style="vertical-align:middle">✍</span></span>
- How to add SWH into it?
- In which topic in a specific SMP model (Elixir, ODIPOR, <span class="fragment highlight-red">PRESOFT</span>, etc) the SWH features (Archiving, Referencing) can be linked?
</section>


<!--
Formation :

"Les Plans de Gestion des Logiciels de la Recherche"

23-24 mai 2024

<small>[http://igm.univ-mlv.fr/~teresa/presoft/2024FormationPlansGestionLogicielsRecherche](http://igm.univ-mlv.fr/~teresa/presoft/2024FormationPlansGestionLogicielsRecherche)</small>

Gustave Eiffel University
-->


<section>
## PRESOFT

#### Pérennisation des logiciels de la recherche

* Research Software Management Plan
* PRESOFT template V3.2<br/>[https://hal.science/hal-01802565](https://hal.science/hal-01802565)
</section>

<!--
## Definition

- Software Management Plan (SMP)
  - Plan de Gestion de Logiciel (PGL)
- Research Software Management Plan (RSMP)
- PRESOFT - Pérennisation des logiciels de la recherche
-->

<!--
- Moindre Hoilahoudine, chef project a la recherche, ia, modeles
  - Departement d'appui a la recherche (DAR) UGE
  - Plano gestion se tornara obrigatorio logo como ja eh para dados
- Teresa Gomez-Diaz
  - Protocole CDUR : Citation, Dissemination, Use, Research (2019)
- Teresa, PGL, PRESOFT
  - EOSC Task Force, infra quality research software (2023)

## Key concepts?
- Soon, having a SMP for Research Software will be mandatory as it is today for Research Data.
- Q: How to define the level of contribution of each Research Software author in a team?
  - A: The amount of contribution of each author is defined by the team, it can vary among different projects.
    - It include also non-developers actors.
## My thoughts
- Hard to keep metadata spread in many different documents by different actors
  - CITATION.cff, codemeta.json, PRESOFT, package.json, others...
-->

<section>
## Key concepts?

- SMP is a document that allows to identify Steps, Actors and Responsables
- PRESOFT document can evolve over time
- Lifecycle is the PRESOFT vision, if one specific information is not available now it can be available next year
</section>

<section>
<img src="/files/presoft-cortext-2024.png" width="70%"/>
</section>


<section>
#### PRESOFT, Section 3.2
<small>Usage and distribuition objectives<br/>
Software preservation</small>

<img src="/files/presoft-section-3-2.png" width="70%"/>
</section>


<section>
#### PRESOFT, Section 6.1
<small>Distribution organisation<br/>
Persistent identifier</small>

<img src="/files/presoft-section-6-1.png" width="70%"/>
</section>


<section>
## SMP models and templates

- PRESOFT: [hal.science/hal-01802565](https://hal.science/hal-01802565)
- SSI: [bit.ly/SoftwareSustainabilityEvaluation](https://bit.ly/SoftwareSustainabilityEvaluation)
- Elixir: [github.com/elixir-europe/smp](https://github.com/elixir-europe/smp)
- eScience: [zenodo.org/records/7248877](https://zenodo.org/records/7248877)
- Opidor: [dmp.opidor.fr/public_templates](https://dmp.opidor.fr/public_templates)
</section>


<section>
### Collaborative work: Question (2)
<span style="font-size:4em">🖹<span style="vertical-align:middle">✍</span></span>
- How to add SWH into it?
- In which topic in a specific SMP model (Elixir, ODIPOR, PRESOFT, etc) the SWH features (Archiving, Referencing) can be linked?
</section>


<section>
### Conclusions: 15 min

<span style="font-size:6em">🗣<small style="vertical-align:baseline">🎙</small></span>

- Do you identify other ways to create interconnection between SWH and SMP?
- What would be your next practical steps regarding SWH and SMP?
</section>


<section style="font-size: 0.7em">
## References

- Rios, F. (2018). Incorporating Software Curation into Research Data Management Services: Lessons Learned.
  [https://doi.org/10.2218/ijdc.v13i1.608](https://doi.org/10.2218/ijdc.v13i1.608)

- Hermann, S et al. (2022). Documenting research software in engineering science.
  [https://doi.org/10.1038/s41598-022-10376-9](https://doi.org/10.1038/s41598-022-10376-9)

- Martinez-Ortiz, C et al. (2023). Practical guide to Software Management Plans (1.1).
  [https://doi.org/10.5281/zenodo.7589725](https://doi.org/10.5281/zenodo.7589725)
</section>


<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>27 August 2024, online, [Software Heritage] Work session about Software Management Plans</li>
</ul>
</section>
</section>
