---
title: Cultura Digital
theme: serif
version: 2018.05.11
---

<section>
# Cultura Digital
</section>

<!--
<section>
{% include slides_sobre.html %}
</section>
-->

<section>
1830, Telégrafo

<img src="/files/Tele-Grafo.jpg" style="box-shadow: 0 0 10px black" />
</section>

<section>
1860, Telefone

<img src="/files/telefone.jpg" style="box-shadow: 0 0 10px black" />
</section>

<section>
1896, Rádio

<img src="/files/Radioge.JPG" style="box-shadow: 0 0 10px black" />
</section>

<section>
1969, ARPANET

<img src="/files/arpanet.jpeg" style="box-shadow: 0 0 10px black" />
</section>

<section>
1976, Computador Pessoal

<img src="/files/Apple_I.jpg" style="box-shadow: 0 0 10px black" />
</section>

<section>
1990, Net Art

<img src="/files/netart.png" />

[Reabracadabra](https://anthology.rhizome.org/reabracadabra)
</section>

<section>
2000, Meme

<img src="/files/Chico-Buarque-Instagram.jpg" />
</section>

<section data-background="#c4a000" data-background-transition="slide">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>11 Mai 2018, UnB - Brasília, Aula: Movimentos Sociais, Participação e Novas Mídias</li>
</ul>
</section>
</section>
