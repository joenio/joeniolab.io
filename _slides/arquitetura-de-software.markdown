---
title: Arquitetura de Software
theme: serif
version: unb-mes-mds
---

<section>
# Arquitetura de Software
</section>

<section>
## Arquitetura de Software

### By: Guilherme Germoglio

Online: [http://cnx.org/content/col10722/1.9](http://cnx.org/content/col10722/1.9)

**CONNEXIONS**

Rice University, Houston, Texas
</section>

<section>
design de software é essencial no processo de desenvolvimento de software

<small>Cap 2</small>
</section>


<section>
projetar arquitetura é fazer design em alto nível

<small>Cap 2</small>

arquitetura é design, mas nem todo design é arquitetural

<small>Cap 4</small>
</section>

<section>
design é necessário em todos os níveis de detalhe durante
o processo de desenvolvimento do software

<small>Cap 2</small>
</section>

<section>
arquitetura é um veículo de comunicação

<small>Cap 4</small>
</section>

<section>
### A arquitetura facilita:

* A construção do sistema
* O entendimento do sistema
* O reuso durante o ciclo de vida do sistema
* A evolução do sistema
* A análise do sistema
* A gerência durante o desenvolvimento do sistema

<small>Cap 4</small>
</section>

<section>
documentar a arquitetura ajuda na divisão de tarefas entre os times

<small>Cap 4</small>
</section>

<section>
# Stakeholders

arquitetos;
engenheiros de requisitos;
designers;
desenvolvedores;
testadores;
responsáveis pela integração do software com outros sistemas;
mantenedores do software;
designers de outros sistemas;
gerente do desenvolvimento;
time de controle de qualidade do software.

<small>Cap 5</small>
</section>

<section>
é fundamental enteder a relação entre os stakeholders e os atributos de qualidade do software

<small>Cap 5</small>
</section>

<section>
# Atributos de Qualidade

???
???
???

<small>Cap 6</small>
</section>

<section>
desempenho;
escalabilidade;
confiabilidade;
disponibilidade;
segurança;
manutenibilidade;
portabilidade;
extensibilidade.

<small>Cap 6</small>
</section>

<section>
# Técnicas de Design Arquitetural

<small>Cap 7</small>
</section>

<section>
### Princípios de design arquitetural:

* uso da abstração ou níveis de complexidade
* separação de preocupações
* uso padrões e estilos arquiteturais

<small>Cap 7</small>
</section>

<section>
## Documentação da Arquitetura
</section>

<section>
* documento de arquitetura auxilia no processo de design
* um único diagrama não é suficiente para conter a quantidade de informação que deve ser mostrada por um arquiteto. Por isso, a necessidade de múltiplas visões da arquitetura.
</section>

<section>
### Definição: design de software

"É tanto o processo de definição da arquitetura, módulos, interfaces e outras
características de um sistema quanto o resultado desse processo."

<small>Cap 2</small>
</section>

<section>
</section>
