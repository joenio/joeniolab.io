---
title: Software Livre
version: 2022.02.03
---

<section data-background="#ffffff" data-background-transition="slide">
# Software Livre
<img src="/files/gnu.png" />

"conhecimento deve estar sempre disponível para permitir a evolução da humanidade"
</section>

<section>
{% include slides_sobre.html %}
</section>

<section data-background="#4e9a06" data-background-transition="slide">
# Software livre

> Por _software livre_ devemos entender aquele software que
> respeita a liberdade e senso de comunidade dos usuários.
</section>

<section data-background="#4e9a06" data-background-transition="slide">
### As quatro liberdades essenciais

1. Executar o programa para qualquer fim
1. Estudar como funciona e adaptá-lo
1. Redistribuir cópias para ajudar o próximo
1. Distribuir cópias modificadas

> “Permitir a distribuição de cópias é permitir a solidariedade social”
> -- Richard Stallman
</section>

<section data-background="#4e9a06" data-background-transition="slide">
> “Permitir a distribuição de cópias é permitir a solidariedade social.”

Richard Stallman
</section>

<section data-background="#4e9a06" data-background-transition="slide">
> As quatro liberdades permitem que os usuários controlem o software
> e o que ele faz.

> Sem essas liberdades os usuários não controlam o
> software e passam a ser controlados por ele.
</section>

<section data-background="#a40000">
### Software "não-livre" é anti-ético

Desenvolvedores de software **não-livre** ou software **proprietário** exercem
controle sobre o software, e por meio dele, controlam os seus usuários, o que
faz de tais softwares um instrumento de poder injusto
</section>

<section data-background="white">
<img src="/files/rms_slide_microsoft_control_users.png" />
</section>

<section data-background="white">
![](/files/rms_slide_amazon_kindle.png)

<small><i>[Amazon Erases Orwell Books From Kindle](https://www.nytimes.com/2009/07/18/technology/companies/18amazon.html), The New York Times, July 17, 2009</i></small><br/>
<small><i>[Amazon Kindle users surprised by 'Big Brother' move](https://www.theguardian.com/technology/2009/jul/17/amazon-kindle-1984), The Guardian, Fri 17 Jul 2009</i></small>
<small><i>[Amazon's Software Is Malware](http://www.gnu.org/proprietary/malware-amazon.html)</i></small>
</section>

<section>
### História do software livre

* No início (década de 60) o software era livre por "natureza"
* Em 1970 a IBM começou a vender seus softwares separados do hardware
* Em 1980 quase todo software era "proprietário", isto desencadeou um movimento...
</section>

<section data-background="white">
### O Projeto GNU

<table style="text-align: center"><tr>
  <td style="width: 360px; text-align: center">
    <img src="/files/gnu.png" style="background: white" />
    1983, GNU
  </td>
  <td style="font-size: 32px; vertical-align: top">
    <p><em>Uma maneira de trazer de volta o espírito cooperativo que
    prevalecia na comunidade de computação nos seus
    primórdios.</em></p>

    <a href="http://gnu.org">http://gnu.org</a>
  </td>
</tr></table>
</section>

<section data-background="white">
### A Fundação do Software Livre

<table style="text-align: center"><tr>
  <td style="width: 360px; text-align: center">
    <img src="/files/fsf.png" style="background: white" />
    <p>1985, FSF</p>
  </td>
  <td style="font-size: 32px; vertical-align: top">
    <p><em>Fundação sem fins lucrativos criada para institucionalizar o
    Projeto GNU, bem como obter fundos para desenvolver e proteger o
    software livre.</em></p>

    <a href="http://fsf.org">http://fsf.org</a>
  </td>
</tr></table>
</section>

<section>
## GNU GPL e Copyleft

Richard Stallman criou os mecanismos legais necessários para a
existência do software livre

<img src="/files/Richard_Stallman_at_LibrePlanet_2019.jpg" width="30%" />
</section>

<section>
## O que são licenças de software?

Definições expressas aos usuários de quais ações são autorizadas ou
proibidas a respeito do uso do software
</section>

<section>
Licença de software proprietário reserva todos os direitos ao autor do software

vs

Licença de software livre dá todos os direitos aos usuários do software
</section>

<section data-background="white">
**Copyleft** é uma forma criativa de usar a legislação de proteção dos direitos
autorais -- **Copyright** -- com o objetivo de retirar barreiras à utilização,
difusão e modificação de uma obra criativa.

[http://copyleft.org](http://copyleft.org)
</section>

<section>
Software Livre (free software)

## ≠

Código Aberto (open source)

---

<small>
Podcast Papo Livre #18 - História do Software Livre:<br/>
[https://papolivre.org/18](https://papolivre.org/18)
</small>
</section>

<section>
## Licenças do Projeto GNU

<img src="/files/gpl.png" style="background: white" />

* GNU GPL - General Public License
* GNU LGPL - Lesser General Public License
* GNU Affero GPL - General Public License
* GNU FDL - Free Documentation License
</section>

<section>
## Só o GNU tem licenças livres? Não

* A licença do Perl
* A Licença do X11
* A licença modificada do BSD
* A licença da Zlib

[http://gnu.org/licenses/license-list.html](http://gnu.org/licenses/license-list.html)
</section>

<section>
### Copyright - all rights reserved

<img src="/files/copyright.png" style="background: white" />

<ul style="font-size: 38px">
  <li>O "direito de autor" ou copyright é a proteção de obras literárias e
  artísticas, se aplica também à software</li>
  <li>Não há qualquer formalidade para proteger a obra, o direito exclusivo
  nasce da criação</li>
</ul>
</section>

<section>
### Copyleft - all rights reversed

<img src="/files/copyleft.png" style="background: white" />

Copyleft é uma forma de usar o Copyright com
o objetivo de retirar barreiras à utilização, difusão e modificação de uma obra
criativa

<small>[http://pt.wikipedia.org/wiki/Copyleft](http://pt.wikipedia.org/wiki/Copyleft) | [http://copyleft.org](http://copyleft.org)</small>
</section>

<section>
#### Documentário: InProprietário

<iframe width="853" height="480" src="//www.youtube.com/embed/MKDn9quw5sc" frameborder="0" allowfullscreen>
</iframe>

<small>
Trabalho de conclusão do curso de Comunicação Social e Jornalismo de
2008 dos alunos: Daniel Pereira Bianchi e Johnata Rodrigo de Souza
<br/>
[https://we.riseup.net/docs/inproprietario](https://we.riseup.net/docs/inproprietario)
</small>
</section>

<section>
> "O movimento do software livre criou novos
> paradigmas na produção de software."

* Equipes distribuídas e com diversos tamanhos
* Trabalho voluntário + trabalho remunerado
* Comunicação síncrona e assíncrona via Internet
* Repositório de código distribuído e descentralizado
* Valorização dos indivíduos sobre o processo
</section>

<section>
### Ganhos sociais

* Transferência de tecnologia
* Transparência do processo de produção
* Oportunidades de negócio "para todos"
* Oportunidade de aprendizado
* Maior colaboração entre pessoas
</section>

<section>
### Exemplos de softwares livres

<ul style="list-style: none; font-size: 32px; text-align: center; color: black">
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//kernel.org"><img width="150px" src="/files/linux.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Linux</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//debian.org"><img width="140px" src="/files/debian.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Debian</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//libreoffice.org"><img width="130px" src="/files/libreoffice.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">LibreOffice</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//mozilla.org/firefox"><img width="150px" src="/files/firefox.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Firefox</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//inkscape.org"><img width="150px" src="/files/inkscape.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Inkscape</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//gimp.org"><img width="150px" src="/files/gimp.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Gimp</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//ardour.org"><img width="150px" src="/files/ardour.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Ardour</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//www.videolan.org/vlc"><img width="150px" src="/files/vlc.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">VLC</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//noosfero.org"><img width="150px" src="/files/noosfero.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Noosfero</p></li>
  <li style="height: 240px; width: 170px; float: left; border: 1px solid #DDD; display: block; margin: 5px; background: white; position: relative">
    <a href="//www.vim.org"><img width="150px" src="/files/vim.png" /></a><p style="position: absolute; bottom: 0; width: 100%; margin: 0">Vim</p></li>
</ul>
</section>

<section>
<section>
### Eu gostei e quero colaborar

## #comofaz

</section>

<section>
### Mozilla

## #comofaz

<span style="font-size: 48px">
[http://whatcanidoformozilla.org](http://whatcanidoformozilla.org)
</span>
</section>

<section>
### Debian

## #comofaz

<span style="font-size: 48px">
[http://wiki.debian.org/HelpDebian](http://wiki.debian.org/HelpDebian)
</span>
</section>

<section>
Você pode contribuir!

<p class="fragment">Saber inglês é importante</p>
<p class="fragment">Desenvolver Python, Java, Perl, Ruby, Javascript, ...</p>
<p class="fragment">Encontrar e relatar problemas ou bugs</p>
<p class="fragment">Fazer tradução</p>
<p class="fragment">Escrever documentação</p>
<p class="fragment">Criar temas, layouts, design, interface de usuário</p>
<p class="fragment">Ensinar outras pessoas</p>
<p class="fragment">ou...</p>
</section>

</section>

<section data-background="#204a87" data-background-transition="slide">
Você pode utilizar software livre
### ♡

1. Troque seu Windows por GNU/Linux
1. Escreva seus documentos usando LibreOffice
1. Navegue utilizando Mozilla Firefox
1. Use Gimp ao invés de Photoshop
1. Use Inkscape ao invés de Corel Draw
</section>

<section data-background="#c4a000" data-background-transition="slide">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>17 Ago 2015, STI UFBA - Salvador, treinamento de Noosfero avançado para sysadmins</li>
  <li>23 Out 2015, Faculdade JK - Brasília, II Seminário de Educação e Novas Tecnologias</li>
  <li>09 Mar 2016, UFBA - Salvador, Palestra na disciplina Computador, Ética e Sociedade</li>
  <li>11 Mai 2018, UnB - Brasília, Aula: Movimentos Sociais, Participação e Novas Mídias</li>
  <li>03 Fev 2022, UnB/CEAM - Online, disciplina Metodologia e Técnicas de Pesquisa Científica</li>
</ul>
</section>
</section>
