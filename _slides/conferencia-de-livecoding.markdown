---
title: Conferencia de Live Coding
theme: serif
version: draft
---

<!--

Duracao: 2h

Conferência e bate-pepo aberto sobre o live coding abordando o histórico da
prática, as ferramentas mais comuns, demonstração prática e ao vivo, bem como
referências dos artistas centrais e periféricos desta linguagem artística.

* Historico live coding, toplap, algorave, clic, algorave brasil
* Examplo demonstracao com tidal cycles (via dublang)
* Congressos, shows, pesquisa academica, experimentos, publicacoes
* Ferramentas, sonic pi, hydra, orca, tidal, gibber, DAW, jack, supercollider, pipewire, obs
* Albuns, artistas e figuras expoentes da comunidade mundial
* Politica, diversidade, questoes tecno-sociais e debate

-->

<section>
# Live Coding

## Bate-papo aberto
</section>

<!-- (TODO este slide faz sentido numa palestra para publico ja experiente em live coding
<section>
#### Antes de tudo, criou-se o dub

<iframe width="560" height="315" src="https://www.youtube.com/embed/y651C7aNXRc?start=29" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Lee 'Scratch' Perry (1936 - 2021)

King Tubby (1941 - 1989)
</section>
-->

<section data-background-image='/files/ratimbum-senta-que-la-vem-historia.gif'>
![](/files/ratimbum-senta-que-la-vem-historia.gif)

<span style='background-color: black; color: white; border-radius: 5px; padding: 5px'>"Senta que lá vem a História"</span>
</section>

<section>
### Community

<table><tr>
<td style="text-align: center">
<img src="/files/300px-Toplap.png" />
<br/>
<a href="https://toplap.org">TOPLAP</a>
<br/>
(2004)
</td>
<td style="text-align: center">
<img src="/files/algorave_logo.png" />
<br/>
<a href="https://algorave.com">Algorave</a>
<br/>
(2011)
</td>
</tr></table>
</section>

<section>
### Community

<table><tr>
<td style="text-align: center">
<img src="/files/clic_sticker.png" width="50%" />
<br/>
<a href="https://colectivo-de-livecoders.gitlab.io">CLiC</a>
<br/>
(2018)
</td>
<td style="text-align: center">
<img src="/files/algoravebr_logo.png" width="40%" />
<br/>
<a href="https://algoravebrasil.gitlab.io">Algorave Brasil</a>
<br/>
(2019)
</td>
</tr></table>
</section>


<!-- Exemplo demonstracao com tidal cycles (via dublang) -->
<section>
### Demonstracao ao vivo

<span style="font-size: 8em">🎧</span>
</section>

<section>
### Documentarios
## Eventos
## Pesquisas
### Publicacoes
</section>

<section>
<iframe width="560" height="315" src="https://www.youtube.com/embed/S2EZqikCIfY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
Algorave Generation Resident Advisor (2019)

See also: [on-the-fly.documentary](https://www.youtube.com/watch?v=ntFMuvv2-TY&list=LL&index=6) (2022)
</section>

<section>
<iframe width="560" height="315" src="https://www.youtube.com/embed/PsfTdFUQUVA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Live coding : de la musique codée en direct pour danser ou rêver 
</section>

<section>
<iframe width="560" height="315" src="https://www.youtube.com/embed/yt5XPxJXnew" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

ICLC 2023, Utretch, Holanda
</section>

<section>
![](/files/livecoding-book-users-manual.webp)

[Live Coding: A User's Manual](https://livecodingbook.toplap.org)
</section>

<section>
## Algorave Brasil 2023

Este ano, em Novembro,<br/>data ainda a definir
<ul style="font-size: 0.6em">
  <li>Algorave Brasil 2020 - <a href="https://algoravebrasil.gitlab.io/2020">algoravebrasil.gitlab.io/2020</a></li>
  <li>Algorave Brasil 2021 - <a href="https://algoravebrasil.gitlab.io/2021">algoravebrasil.gitlab.io/2021</a></li>
  <li>Algorave Brasil 2022 - <a href="https://algoravebrasil.gitlab.io/2022">algoravebrasil.gitlab.io/2022</a></li>
  <li>Algorave Brasil 2023 - <a href="https://algoravebrasil.gitlab.io/2023">(site em breve)</a></li>
</ul>
</section>

<section>
# Ferramentas
</section>

<section>
# Sonic Pi

The Live Coding music synth for everyone

[sonic-pi.net](https://sonic-pi.net)
</section>

<section>
# Hydra

Live Coding networked visuals in the browser

[hydra.ojack.xyz](https://hydra.ojack.xyz)
</section>

<section>
#  ORCΛ

An esoteric live coding environment with two-dimensional syntax

[hundredrabbits.itch.io/orca](https://hundredrabbits.itch.io/orca)
</section>

<section>
# SuperCollider

A platform for audio synthesis and algorithmic composition, used by musicians,
artists, and researchers working with sound

[supercollider.github.io](http://supercollider.github.io)
</section>

<section>
# Tidal Cycles

Is a language for Live Coding pattern

[tidalcycles.org](https://tidalcycles.org)
</section>

<section>
# Pure Data

An open source visual programming language that can be used to process and
generate sound, video, 2D/3D graphics, and interface sensors, input devices,
and MIDI

[puredata.info](https://puredata.info)
</section>

<section>
# Audacity

A free, cross-platform digital audio editor

[audacityteam.org](https://www.audacityteam.org)
</section>

<section>
# Ardour

A cross-platform digital audio workstation emphasizing audio recording

[ardour.org](http://ardour.org)
</section>

<section>
Referencias

- [Awesome Live Coding](https://github.com/toplap/awesome-livecoding) - A curated list of live coding languages and tools
- [Awesome Music](https://github.com/noteflakes/awesome-music) - A categorized collection of awesome music libraries, tools, frameworks and software
</section>

<section>
# JACK e PipeWire

<small>demonstracao com: amsynth + guitarrix ou rakarrack + pipewire, audacity...</small>
</section>

<section>
## Artistas e Pesquisadores
</section>

<section>
### Alex McLean

<img width="40%" src="/files/avatar-yaxu.jpeg"/><br/>
[yaxu.org](https://yaxu.org)<br/>
`Tidal Cycles`
</section>

<section>
### Olivia Jack

<img width="40%" src="/files/avatar-ojack.jpg"/><br/>
[ojack.xyz](https://ojack.xyz)<br/>
`Hydra`
</section>

<section>
### Antonio Roberts

<img width="40%" src="/files/avatar-hellocatfood.png"/><br/>
[hellocatfood.com](https://www.hellocatfood.com)<br/>
`(Algo|Afro) Futures`
</section>

<section>
### Iris Saladino

<img width="40%" src="/files/avatar-iris.jpg"/><br/>
[1riss.github.io](https://1riss.github.io)<br/>
`CLiC`
</section>

<section>
### Crash Server

<img width="40%" src="/files/avatar-crashserver.jpg"/><br/>
[crashserver.fr](https://crashserver.fr)<br/>
`France`
</section>

<section>
## Debate

### Questoes socio-tecnicas

# Diversidade

<small>
\- [ICLC 2023 - Poetry Attack 01](https://iclc.toplap.org/2023/catalogue/proof/performance/poetry-attack-01.html)
</small>
</section>

<section>
## Extra 1

# Demoscene

<small>
\- [Demo Art e Visual Music em perspectiva](/demoart-visualmusic)<br/>
\- [Demoscene no Brasil: Um Mapeamento Quasi- Sistemático da Literatura](https://zenodo.org/record/5803520)
</small>
</section>

<section>
## Extra 2

# Chiptune

<small>
\- [Demoscene no Brasil: Um Mapeamento Quasi- Sistemático da Literatura](https://zenodo.org/record/5803520)
</small>
</section>

<section>
## Extra 3

# Machinima

<small>
\- [Demoscene no Brasil: Um Mapeamento Quasi- Sistemático da Literatura](https://zenodo.org/record/5803520)
</small>
</section>

<section data-background="#c4a000" data-background-transition="slide">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>05 de Junho de 2023, UFRN, Natal, Brasil, <a href="https://tour23.4two.art">Live Coding Tour 2023</a></li>
</ul>
</section>
</section>
