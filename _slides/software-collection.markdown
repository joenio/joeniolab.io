---
title: Software Collection
theme: white
version: draft
---

<section>
### Software Collection

the importance of software quality for science
</section>

<section>
[bssw.io](https://bssw.io)

![](/files/software-collection/bssw.png)
</section>


<section>
[ideas-productivity.org](https://ideas-productivity.org)

![](/files/software-collection/ideas-productivity.png)
</section>

<section>
[wssspe.researchcomputing.org.uk](https://wssspe.researchcomputing.org.uk)

![](/files/software-collection/wssspe.png)
</section>

<section>
[www.software.ac.uk](https://www.software.ac.uk)

![](/files/software-collection/software-ssi.png)
</section>

<section>
[society-rse.org](https://society-rse.org)

![](/files/software-collection/society-rse.png)
</section>

<section>
[www.esciencecenter.nl](https://www.esciencecenter.nl)

![](/files/software-collection/esciencecenter.png)
</section>

<section>
[www.softwareheritage.org](https://www.softwareheritage.org)

![](/files/software-collection/softwareheritage.png)
</section>

<section>
[sofair.org](https://sofair.org)

![](/files/software-collection/sofair.png)
</section>

<section>
[codeisscience.com](https://codeisscience.com)

![](/files/software-collection/codeisscience.png)
</section>

<section>
[software-carpentry.org](https://software-carpentry.org)

![](/files/software-collection/software-carpentry.png)
</section>

<section>
[adore.software](https://adore.software)

![](/files/software-collection/adore-software.png)
</section>


<section>
[research-software-directory.org](https://research-software-directory.org)

![](/files/software-collection/research-software-directory.png)
</section>

<section>
[software-metadata.pub](https://software-metadata.pub)

![](/files/software-collection/software-metadata.png)
</section>

<section>
[citation-file-format.github.io](https://citation-file-format.github.io)

![](/files/software-collection/citation-file-format.png)
</section>

<section>
[codemeta.github.io](https://codemeta.github.io)

![](/files/software-collection/codemeta.png)
</section>

<section>
[www.researchsoft.org](https://www.researchsoft.org)

![](/files/software-collection/researchsoft.png)
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>13 November 2023, Online, <a href="https://recherche.data.gouv.fr/fr/page/data-univ-eiffel-atelier-data-universite-gustave-eiffel">
    Data Univ Eiffel - RDV DATA sur les plateformes dediees aus logiciels
  </a></li>
</ul>
</section>
</section>
