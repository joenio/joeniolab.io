---
title: "Sonic Pi"
lead: >
  Oficina ...
theme: serif
version: draft
---

<section>
<img width="30%" src="/files/sonic-pi-logo.png" />

## Sonic Pi
</section>

<section>
### Live Coding

Um dos espectos mais excitantes do Sonic Pi eh a possibilidade
de escrever e modificar codigo ao vivo para fazer musica, assim como
uma performance de guitarra ao vivo, isto quer dizer que apos alguma pratica
voce pode fazer suas gigs com ele.
</section>

<section>
### Agenda

- A importancia do Software Livre
- Visao geral da GUI do Sonic Pi
- Como instalar e configurar Sonic Pi
- Conceitos de sample e sintetizador
- Explorando o Sonic Pi
- Conceitos de envelope ADSR
- Funcoes da linguagem Sonic Pi
</section>

<section>
## A importancia do Software Livre

[ir para slides sobre software livre](/software-livre)
</section>

<section data-background-image="/files/sonic-pi-GUI.png">
<div class="box-blue" style="color:white; font-size: 0.75em">
<h3 style="color:white">A interface grafica (GUI) do Sonic Pi</h3>

- Controles de Play
- Editor de codigo
- Controles do editor de codigo
- Informacoes e ajuda
- Visualizacao
- Logs, help
</div>
</section>

<section>
## Como instalar e configurar o
# Sonic Pi
</section>

<section>
<!-- conceito 1: samples e conceito 2: sintetizadores, ambos criações das pesquisa do campo da eletroacústica -->
## Sample

<audio src="/files/water-to-glass.wav" controls>
  <a href="/files/water-to-glass.wav">water-to-glass.wav</a>
</audio>

--

## Sintetizador

<audio src="/files/everything.wav" controls>
  <a href="/files/everything.wav">everything.wav</a>
</audio>
<audio src="/files/pitfall-ouro.wav" controls>
  <a href="/files/pitfall-ouro.wav">pitfall-ouro.wav</a>
</audio>

--

<!-- samples: são gravaçoes, arquivos de áudio (colocar exemplos de dois ou três sons no slide e dar play na apresentação) -->
  <aside class="notes">
    samples are used nowadays in almost
     kind of pop music, hip hop is a
     key exemple

    ! play samples
  </aside>
</section>

<section>
## Sample

<pre><code class="ruby">
sample :drum_cymbal_soft

</code></pre>

--

## Sintetizador

<pre><code class="ruby">
play 60

</code></pre>

--
</section>


<section>
## Sample, Play e Sleep

<pre><code class="ruby">
sample :drum_cymbal_soft
sleep 0.5
play 60
sleep 0.5
sample :drum_cymbal_soft
sleep 0.5
play 60
sleep 0.5
sample :drum_cymbal_soft

</code></pre>
</section>


<section>
## Live Loop

<pre><code class="ruby">
live_loop :meuloop do
  sample :drum_cymbal_soft
  sleep 0.5
  play 60
  sleep 0.5
end

</code></pre>
</section>

<section>
## Parametro '`rate`'

<pre><code class="ruby">
live_loop :meuloop do
  sample :drum_cymbal_soft, rate: 0.5
  sleep 0.5
  play 60
  sleep 0.5
end

</code></pre>

adicione o parametro `rate` com valor `0.5`<br/>
(valores possiveis vao de `0` ate `1`)
</section>

<section>
## Comentarios

<pre><code class="ruby">
live_loop :meuloop do
  #sample :drum_cymbal_soft, rate: 0.5
  sample :drum_cymbal_soft, rate: 0.1
  sleep 0.5
  #play 60
  play 120
  sleep 0.5
end

</code></pre>

linhas iniciadas com **#** nao sao executadas
</section>

<section data-background-image="https://www.proibidoler.com/wp-content/uploads/2018/03/o-auto-da-compadecida-2000-direitos-humanos-768x515.jpg">
<div class="box-gray" style="border-radius: 10px">
<h3 style="color: white; text-shadow: 0 0 3px gray;">Não existem erros,</h3>
<h3 style="color: white; text-shadow: 0 0 3px gray;">Apenas oportunidades</h3>
</div>
</section>

<section data-background-image="https://www.proibidoler.com/wp-content/uploads/2018/03/o-auto-da-compadecida-2000-direitos-humanos-768x515.jpg">
<div class="box-gray" style="border-radius: 10px; color: white">
<h2 style="color: white; text-shadow: 0 0 3px gray;">Parametro '`amp`' (Amplitude)</h2>

<pre><code class="ruby">
sample :drum_cymbal_soft, amp: 0.5
sleep 0.5
play 60, amp: 0.5
sleep 0.5

</code></pre>

`amp` com valor 0.5 ajusta volume em `50%`<br/>
(valores recomendados vao de `0` ate `1`)
</div>
</section>

<section data-background-image="https://www.proibidoler.com/wp-content/uploads/2018/03/o-auto-da-compadecida-2000-direitos-humanos-768x515.jpg">
<div class="box-gray" style="border-radius: 10px; color: white">
<h2 style="color: white; text-shadow: 0 0 3px gray;">Parametro '`pan`' (Panning)</h2>

<pre><code class="ruby">
sample :drum_cymbal_soft, pan: -1
sleep 0.5
play 60, pan: 1
sleep 0.5

</code></pre>

valor `-1` som `100%` para o lado esquerdo<br/>
valor `1` som `100%` para o lado direito<br/>
(valores vao de `-1` ate `1`)
</div>
</section>

<section>
## Selecionando o sintetizador

<pre><code class="ruby">
use_synth :saw
play 60

</code></pre>
</section>


<section>
# Desafio!

Fazer 2 live loops cada um com um sintetizador diferente tocando de maneira intercalada.
</section>


<section>
## Envelope ADSR
#### (Attack, Decay, Sustain, Release)

![](https://sonic-pi.net/media/images/tutorial/env-release.png)
</section>


<section>
### Envelope ADSR
#### (Attack, ~~Decay~~, ~~Sustain~~, Release)

![](https://sonic-pi.net/media/images/tutorial/env-attack-release.png)

<pre><code class="ruby">
play 60, attack: 0.7, release: 4

</code></pre>
</section>




<section>
### Envelope ADSR
#### (Attack, ~~Decay~~, ~~Sustain~~, Release)

![](https://sonic-pi.net/media/images/tutorial/env-long-attack-short-release.png)

<pre><code class="ruby">
play 60, attack: 4, release: 0.7

</code></pre>
</section>


<section>
### Listas e a funcao `choose`

<pre><code class="ruby">
loop do
  play choose([60, 65, 72])
  sleep 1
end

</code></pre>
</section>

<section>
### Outras funcoes de loop

<pre><code class="ruby">
3.times do
  play 60
  sleep 1
end

</code></pre>

<pre><code class="ruby">
loop do
  play 60
  sleep 1
end

</code></pre>
</section>

<section>
### Adicionar samples
</section>

<section>
### Extras

- Gravando com Sonic Pi
- Master e mixer com Audacity
- Transmissao com OBS
</section>

<section data-background="#c4a000">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Presentation history

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>05 de Junho de 2023, UFRN, Natal, Brasil, <a href="https://tour23.4two.art">Live Coding Tour 2023</a></li>
</ul>
</section>
</section>
