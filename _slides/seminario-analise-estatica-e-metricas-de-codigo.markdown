---
title: Seminário sobre métricas de código-fonte e ferramentas de análise estática
lead: >
  Slides do seminário sobre métricas de código-fonte e ferramentas de análise
  estática para a disciplina de pós-graduação de Engenharia de Software da USP
theme: simple
version: 2024.11.12
---

<section>
### Seminário

## Análise estática e<br/> métricas de código-fonte

&nbsp;
<br/>
<small>_12 de Novembro de 2024_</small>
</section>

<section data-background-image="/files/goodbye-baloon-inverse.jpg">
<h4 style="color:white">Joenio Marques da Costa</h4>
<a href="http://joenio.me/sobre" style="color:blue; text-shadow: 0 0 2px gray">http://joenio.me/sobre</a>

<br/>

<img style="position:relative; float:right; width: 28%" src="/avatars/joenio-2020-800x800.jpg" />
<div style="float: left; max-width: 650px; font-size: 0.9em; color: white">
- Engenheiro de Software para Pesquisa
- Trabalha na Plataforma <a href="https://www.cortext.net" style="color:blue; text-shadow: 0 0 2px gray">Cortext.net</a>
- Embaixador Software Heritage
- Embaixador Data Univ Eiffel
- Desenvolvedor Debian
- Artista Computacional
</div>
</section>

<section>
### Análise estática

É um ramo que possui muitas das suas abordagens em comum com os estudos da área
de análise de programas (program analysis), especialmente na área de
compiladores, onde atua especialmente nas primeiras etapas do processo de
compilação.

<!-- source: dissertacao -->
</section>

<section>
### Análise de programas

A analise de programas trata, de modo geral, da descoberta de problemas e fatos
sobre programas, ela pode ser realizada sem a necessidade de executar o
programa (análise estática) ou com informações provenientes de sua execução
(análise dinâmica).

<!-- source: dissertacao -->
</section>

<section>
#### Análise dinâmica com Devel::NYTProf

![](/files/nytprof-flamegraph-analizo-2024.png)

<small>
_Powerful fast feature-rich Perl source code profiler._<br/>
[metacpan.org/pod/Devel::NYTProf](https://metacpan.org/pod/Devel::NYTProf)
</small>
</section>

<!--
Program analysis is all about analyzing software code to learn about its properties.
https://www.cs.cmu.edu/~aldrich/courses/17-396/program-analysis.pdf

Previsao e futuro:

- In ten years the global code base will approach 500 billion lines of code (40-60% of which will be Cobol) 2007 + 10 = 2017.
- Looking almost twice as far into the future, in the year 2025 the global code base should top 1 trillion lines of code (less than half of which will be Cobol) = 2025.
- Essentially doubling the age of software engineering, in fifty years the global code base will include trillions of lines of code equivalents. 2007 + 50 = 2057.
  - Binkley, D. (2007). Source Code Analysis: A Road Map. Future of Software Engineering (FOSE ’07). doi:10.1109/fose.2007.27, https://sci-hub.ru/10.1109/FOSE.2007.27
-->

<section>
### Análise estática de código-fonte

Análise estática de código-fonte tem como objetivo prover informações acerca de
um programa a partir do seu código-fonte sem necessidade de execução, e sem
requerer qualquer outro artefato do programa além do próprio código.

<!-- source: dissertacao -->
</section>


<!--
Análise estática de código-fonte é o primeiro passo para coletar informações necessárias em diversas atividades de verificação, medição e melhoria da qualidade de software.

(CRUZ; HENRIQUES; PINTO, 2009; KIRKOV; AGRE, 2010).

source: dissertacao

Ela é realizada com base no cóodigo-fonte e a partir daí descobre problemas e propriedades de sua qualidade estrutural.

(CHESS; WEST, 2007).

source: dissertacao
-->

<section>
### Exemplos de uso de análise estática de código-fonte:

- Verificação de tipos
- Verificação de estilo
- Localização de bugs
- Cálculo de Métricas
</section>

<section data-background="/files/backgrounds/tape.jpg" data-transition='zoom'>
<div class="box box-white" style="text-shadow: 0px 0px 2px white; color:black">
<h2 style="text-shadow: 0px 0px 2px white; color:black">Métricas de software</h2>

Uma função cujas entradas são dados de software e cuja saı́da é um valor
numérico, que pode ser interpretado como o grau em que um software possui um
determinado atributo que afeta sua qualidade.
</div>

<small style="text-shadow: 0px 0px 1px #999; font-weight: bold; color:black">
COMMITTEE, S. . S. E. S. et al. Ieee std 1061-1998 — IEEE standard for a software quality
metrics methodology. IEEE Computer Society, Tech. Rep, 1998.
</small>
</section>

<section data-background="/files/backgrounds/tape.jpg" data-transition='zoom'>
<div class="box box-white" style="text-shadow: 0px 0px 2px white; color:black">
<h2 style="text-shadow: 0px 0px 2px white; color:black">Métricas de código-fonte</h2>

Métricas de código-fonte cobrem aspectos de tamanho, complexidade e qualidade,
e podem indicar aspectos relevantes à manutenibilidade de um programa.
</div>
</section>

<section data-background="/files/backgrounds/tape.jpg" data-transition='zoom'>
<div class="box box-white" style="text-shadow: 0px 0px 2px white; color:black">
<h4 style="text-shadow: 0px 0px 2px white; color:black">
Categorias de métricas de código-fonte
</h4>

| **Categoria**    | **Métrica**    |
| ---------------- | -------------- |
| Code Quality     | Lines of Code  |
| Code Design      | Empty Interfaces |
| Code Performance | Prefer Literals over Evaluation |
| Object Oriented  | Lack of Cohesion in Methods |

</div>

<small style="text-shadow: 0px 0px 1px #999; font-weight: bold; color:black">
Bhutani, V., Toosi, F.G. & Buckley, J. Analysing the Analysers: An Investigation of Source Code Analysis Tools. Applied Computer Systems, 2024, Riga Technical University, vol. 29 no. 1, pp. 98-111. https://doi.org/10.2478/acss-2024-0013
</small>
</section>


<section>
### CBO<br/>acoplamento entre objetos

_mede o número classes <abbr title="acessos à atributos ou métodos">acessadas</abbr> pela classe analisada_

<img src="/files/formula-cbo.png" style="box-shadow: 0px 0px 5px gray" />

<img src="/files/ci-cj.png" style="box-shadow:0px 0px 5px gray; background-color:white; padding:5px 5px 0; vertical-align:middle" /> <span style="color:#777">indica acesso à atributos ou métodos</span>
</section>

<section>
### LCOM4<br/>ausência da coesão em métodos

_mede os métodos e atributos acessados dentro de uma mesma classe_

calculado através dos componentes fracamente conectados de um grafo não-orientado
</section>


<section>
## Complexidade estrutural

Uma medida da complexidade de software calculada em termos
do acoplamento (CBO) e coesão (LCOM4).

<img src="/files/formula-sc.png" style="box-shadow: 0px 0px 5px gray" />

_Mede a complexidade estrutural de uma classe._
</section>

<!--
## Sistemas complexos

sistemas complexos são sistemas compostos de várias partes que
interagem entre si com a habilidade de gerar novas qualidades
no comportamento coletivo

<small>
Mitchell, M. Complexity - A Guided Tour. [S.l.]: Oxford University Press, 2009.
</small>

<section>
### Sistemas de software como sistemas complexos

a partir da sua estrutura interna sistemas de software podem ser
caracterizados como um sistema complexo artificial
</section>
-->

<section>
# Complexidade

Quanto maior a complexidade de um sistema de software,
maior é o esforço para compreendê-lo, modificá-lo e evoluí-lo.

<small>
Darcy, D. P. et al. The structural complexity of software: An experimental test. IEEE
Transactions on Software Engineering, v. 31, n. 11, p. 982–995, Nov. 2005. ISSN 0098-
5589.
</small>
</section>


<section>
![](/files/structural-complexity-terceiro2012.png)

<!-- source: https://gitlab.com/flosspapers/sc-factors/-/blob/pdf/csmr2012-terceiro.pdf -->
</section>


<section>
### A ideia de que programas de computador podem ser utilizados para analisar código-fonte de outros programas tem uma história de mais de 50 anos...

<!-- source: dissertacao -->
</section>

<section>
### Histórico da área de estudo sobre métricas de software

- 1976, First book on software metrics (Gilb, 1976)
- 1971, First publication using KLOC to measure complexity (Akiyama, 1971)

<small>
_Software metrics: successes, failures and new directions_
<br/>
1999, [doi.org/10.1016/S0164-1212(99)00035-7][]
</small>
</section>

[doi.org/10.1016/S0164-1212(99)00035-7]: https://doi.org/10.1016/S0164-1212(99)00035-7

<section>
Ainda vale a pena?

Sim.

Mas:

# Context is King

<small>
_Revisiting the debate: Are code metrics useful for measuring maintenance effort?_
<br/>
2022, [doi.org/10.1007/s10664-022-10193-8](https://doi.org/10.1007/s10664-022-10193-8)
</small>
</section>

<section data-background="#222">
# Ferramentas
</section>

<section>
Ferramentas de análise estática de código-fonte estão organizadas em partes ou
componentes, responsáveis por implementar três funções básicas:

- 1) Extração de dados
- 2) Geração de representação intermediária
- 3) Análise
</section>

<section>
#### Anatomia da análise estática de código-fonte

<img src="/files/static-analysis-representation.png" style="box-shadow: 0px 0px 5px gray; background-color:white" />
</section>

<!--
- Extração de dados, usualmente chamado de analisador sintático (ou parser )
- Representação intermediária facilitar análise e transformação de dados e possivelmente adição de metadados abstraindo aspectos particulares do programa e da linguagem de programação
- Analise, pode gerar conhecimento quantitativo ou qualitativo, como, por exemplo, m ́etricas de software ou minera ̧c ̃ao de dados, respectivamente. T ́ecnicas de visualiza ̧ c ̃ao podem ser usadas para apoiar este processo
-->

<section>
### Ferrramentas para cálculo de número de linhas de código

| **Nome**  | **URL** |
| --------- | ------- |
| sloccount | [dwheeler.com/sloccount](https://dwheeler.com/sloccount) |
| cloc      | [github.com/AlDanial/cloc](https://github.com/AlDanial/cloc) |
| linguist  | [github.com/github-linguist/linguist](https://github.com/github-linguist/linguist) |
| scc       | [github.com/boyter/scc](https://github.com/boyter/scc) |
| tokei     | [github.com/XAMPPRocky/tokei](https://github.com/XAMPPRocky/tokei) |

</section>

<section data-background-color="#333">
<img src='/files/analizo.png' />

Uma ferramenta livre e extensível para análise estática de código-fonte.

www.analizo.org
</section>

<section data-background-color="#333">
### Métricas calculadas pelo Analizo

<img src='/files/analizo-metrics-architecture.png' />
</section>

<section data-background-color="#333">
## Analizo module metrics:

- Afferent Connections per Class (ACC)
- Average Cyclomatic Complexity per Method (ACCM)
- Average Method Lines of Code (AMLOC)
- Coupling Between Objects (CBO) [analizo doc](https://metacpan.org/pod/Analizo::Metric::CouplingBetweenObjects)
- Lack of Cohesion of Methods (LCOM4)
- Structural Complexity (SC)
- etc...
</section>

<section data-background-color="#333">
## Analizo global metrics:

- Change Cost [analizo doc](https://metacpan.org/pod/Analizo::GlobalMetric::ChangeCost)
- Total Abstract Classes
- Total Coupling Factor
- Total Lines of Code
- Methods per Abstract Class
- etc...
</section>

<section data-background-color="#333">
### Analizo graph

<img src='/files/analizo-graph.png' style='width: 60%; box-shadow: 0 0 6px gray'/>
</section>

<section>
Analizo static analysis Extractor:

## Doxyparse

Doxyparse modifies the default output of Doxygen and dumps the dependencies
among code elements in a YAML format.

[github.com/doxygen/doxygen](https://github.com/doxygen/doxygen)
</section>

<section data-background-color="#222">
## Instalação do Analizo

&nbsp;

### `apt install analizo`

&nbsp;

<small>
Ou:<br/>
https://github.com/analizo/analizo/blob/dev/INSTALL.md
</small>
</section>

<section>
## Demonstração

- `analizo metrics <input>`
- `analizo graph <input>`
</section>

<section>
### Analizo para código-fonte Python?

- Analizo `dev` branch não lançado ainda, adicionou um extrator (análise estática) para Python usando [Pyan-Analizo](https://github.com/LeaoLuciano/pyan-analizo), um fork do [Pyan](https://github.com/Technologicat/pyan).
- Uma nova versão do analizo incluindo suporte a Python será lançada assim que o fork Pyan-Analizo for incorporado no upstream Pyan.
- Trabalho de merge sendo feito na [issue #189](https://github.com/analizo/analizo/issues/189)
</section>

<section data-background="red">
# 🗣

# Bate-papo
</section>

<section data-background="#75E">
### Métricas Open Source e Comunidade

## Black Duck - Open Hub

[openhub.net](https://openhub.net)
</section>

<section data-background="#333">
![](/files/chaoss-logo-white-2.png)

open source community health on a global scale

[chaoss.community](https://chaoss.community)
</section>

<section data-background="#333">
![](/files/chaoss-logo-white-2.png)

- Metric: Code Changes Lines
- Metric: Clones
- Metric: Time to Close
</section>


<section data-background="#999">
<img style="width:10%" src="/files/chaoss-logo-white-2.png"/>
![](https://i0.wp.com/blog.bitergia.com/wp-content/uploads/2020/02/cropped-bitergia_new-1.png)

### Bitergia Analytics

[chaoss.biterg.io](https://chaoss.biterg.io)


</section>

<section data-background="#333">
<img style="width:10%" src="/files/chaoss-logo-white-2.png"/>
![](/files/oss-compass-logo.svg)

[oss-compass.org](https://oss-compass.org)
</section>

<section data-background="green">
#### Working Group

![](/files/chaoss-logo-white-2.png)

Scientific Open Source Software

(SciOSS)

[YouTube playlist videos](https://www.youtube.com/watch?v=Hwwrm2Zd9lo&list=PL60k37cxI-HTwtG3dOw7BjVNtooXIUe0H)
</section>

<section data-background="blue">
# Research Software
</section>


<section data-background="blue">
### Software para Pesquisa<br/><small>(Research Software)</small>

É qualquer software criado para apoiar pesquisas,
usado para coletar, processar ou analisar dados.

Podem ser protótipos escritos pelos cientistas,
ou produtos completos desenvolvidos profissionalmente.

<small class="cite" style="color:#DDD">
M. Gruenpeter, "Defining Research Software: a controversial discussion".<br/>
Zenodo, sept. 13, 2021. doi: [10.5281/zenodo.5504016][].
</small>
</section>

<section>
## Metadados e Métricas FAIR

- [Citation File Format (CFF)](https://citation-file-format.github.io)
- [The CodeMeta Project](https://codemeta.github.io)
- [Research Software MetaData (RSMD)](https://fair-impact.github.io/RSMD-guidelines)
- [FAIR Research Software Metrics (FRSM)](https://fair-impact.eu/metrics-software)

<small>_Findable, Accessible, Interoperable and Reusable (FAIR)._</small>
</section>

<section>
#### Analizo software paper citations

![](/files/analizo-paper-citations-evolution.png)
</section>

<section>
#### Incentivos de reputação e práticas de Software para Pesquisa

<img src="/files/scientific-reputation-diagram.png" style="background: white" />

<small class="cite">
HOWISON, J.; HERBSLEB, J. D. "Scientific software production: incentives and collaboration".<br/>
In: Proceedings of the ACM 2011 conference., 2011. doi: [10.1145/1958824.1958904][]
</small>
</section>

<section>
#### Making Software FAIR

<img width="60%" src="https://sofairoa.github.io/documentation/img/workflow.png"/>

[sofair.org](https://sofair.org)
</section>

<section>
#### Automated Software Metadata Publication

<img width="70%" src="https://docs.software-metadata.pub/en/latest/_images/workflow-overview.svg"/>

[software-metadata.pub](https://software-metadata.pub)
</section>

<section>
#### FAIRsFAIR Research Data Object Assessment Service

<img width="70%" src="/files/software-collection/f-uji.png" />

[www.f-uji.net](https://www.f-uji.net)
</section>

<section data-background="#c4a000" data-background-transition="slide">
<section>
{% include slides_obrigado.html %}
</section>
<section>
### Histórico de apresentações

<small>Onde e quando esta apresentação foi realizada</small>

<ul style="font-size: 24px">
  <li>12 de Nov 2024, Online, USP seminários engenharia de software</li>
</ul>
</section>
</section>

[10.5281/zenodo.5504016]: https://doi.org/10.5281/zenodo.5504016
[10.1145/1958824.1958904]: https://doi.org/10.1145/1958824.1958904
