---
title: Software Heritage - OLIO Networks
theme: beige
transition: convex
version: 2022.12.13
---

<!--

Presentation about software heritage to OLIO Networks at 13/12/2022.

Talk title:

Software Heritage, first steps: What is and how to use it?

Talk description:

In this talk we will learn the basics about Software Heritage project, the
goals and the main issues addressed by the project. With practical examples,
we'll see how to archive software persistently and how to cite software
properly in scientific publications using SWHID.

Target audience:
- Researchers, social science, Sciences Humaines et Sociales (SHS)

Topics to include in the talk:
- Talk structure: 15 minutes talk + 10 minutes questions
- Simple presentation from the point of view of the user
- Global overview about SWH
- What are the theoretical difference between SWH x VCS (git, svn, etc)
  - [idea] Do a link with The Carpentries git for novices
- Overview about control version systems and probably other topics to discuss together
- mostrar os numeros do arquivo pode ser bom p convencer novos usuarios
  - https://docs.softwareheritage.org/devel/architecture/overview.html

Links:

- https://olio.hypotheses.org

-->

<section>
### Software Heritage, first steps:<br/>What is it and how to use it?

![](/files/OLIO-logo-250x100px.png)
<br/><small>
13 December 2022<br/>
Les entrepôts de données: un outil au service des principes FAIR
</small>
</section>

<section data-background='white'>
Joenio Marques da Costa

<img src='/files/image_ambassador_22_joenio-768x430.png' width='50%' />

<small>
Research Software Engineer at [LISIS](http://umr-lisis.fr)<br/>
[CorTexT platform](https://cortext.net) and Risis Core Facility (RCF) project
</small>

<small style='font-size: 0.4em'>
[softwareheritage.org/2022/11/22/22nd-ambassador-joenio-marques-da-costa](https://www.softwareheritage.org/2022/11/22/22nd-ambassador-joenio-marques-da-costa)
</small>
</section>

<section data-background='white'>
![](/files/software-heritage-logo-noicon.svg)

![](/files/SWH-logo_anim-texte1.gif)

_Universal software source code archive_
</section>

<section>
![](/files/software-heritage-logo-title.svg)<br/>
![](/files/swh-dataflow-merkle-1.png)
<br/><small>_180 million software projects_</small>
</section>

<section>
# why?

- 2015, Google Code and Gitorious.org shutdown
- 2019, BitBucket announces Mercurial VCS sunset
- 2020, BitBucket erases 250.000+ repositories
- 2021, Inria's old gforge.inria.fr was shut down
- 2022, GitLab.com considers erasing all projects that are inactive for a year
</section>

<section>
### Software Heritage
# =
### GitHub, GitLab, BitBucket, Google Code ... ?
<small class='fragment'>
[Version Control with Git](http://swcarpentry.github.io/git-novice) <img src='/files/the-carpentries-logo.svg' width='15%' style='margin-left: 5px; margin-top: 5px' />
</small>
</section>

<section data-background='white'>
![](/files/shw-users-page.png)

Features

</section>

<section data-background='white'>
### Software Heritage archive
<img src='/files/browse_search_swh.jpeg' width='30%' />

[archive.softwareheritage.org](https://archive.softwareheritage.org)
</section>

<section data-background='white'>
### Software Heritage identifiers
<img src='/files/swhid_provider_resolver.png' width='30%' />

_Intrinsic identifiers for digital objects._
</section>

<section>
![](https://www.softwareheritage.org/wp-content/uploads/2020/06/software_ids-Page-2.png)

<small>_One type of identifier can’t answer all use cases, we need both intrinsic identifiers and extrinsic identifiers for software research outputs._<br/><br/>
[softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers](https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers)</small>
</section>

<section>
### What can be identified with a SWHID?

<img src='/files/upload_9d9303cc5952cf3d9e101aa8313ca276.png' width='70%' />

<small>
[softwareheritage.org/faq/#32_What_can_be_identified_with_a_SWHID](https://www.softwareheritage.org/faq/#32_What_can_be_identified_with_a_SWHID)
</small>
</section>

<section>
SWHID howtos:
- [HOWTO archive and reference your code](https://www.softwareheritage.org/howto-archive-and-reference-your-code)
- [Archiving and Referencing Source Code with Software Heritage](https://link.springer.com/chapter/10.1007/978-3-030-52200-1_36)
- [https://ctan.org/pkg/biblatex-software](https://ctan.org/pkg/biblatex-software)

Examples of SWHID use:
- [Toward Long-Term and Archivable Reproducibility](https://arxiv.org/pdf/2006.03018.pdf)
- [Carving out the low surface brightness universe with NoiseChisel](https://arxiv.org/pdf/1909.11230.pdf)
- [AutoClassWeb: a simple web interface for Bayesian clustering of omics data](https://bmcresnotes.biomedcentral.com/articles/10.1186/s13104-022-06129-6#availability-of-data-and-materials)
</section>

<section>
<img src='/files/swhid-citation-example.png' width='70%' style='box-shadow: 1px 1px 5px gray'/>
</section>

<section>
### Extra references:

- [FAIR Principles for Research Software (FAIR4RS Principles)](https://doi.org/10.15497/RDA00068)
- [Five recommendations for fair software](https://fair-software.eu)
- [Research Software Directory](https://research-software-directory.org)
</section>

<section>
### CorTexT Manager
<img src='/files/cortext-fake-CM-05.jpg' width='80%' style='box-shadow: 1px 1px 3px gray'/>
<br/><span>[docs.cortext.net](https://docs.cortext.net)</span>
</section>

<section data-background="#c4a000">
<section>
{% include slides_thanks.html %}
</section>
<section>
### Presentation history

<small>Where and when this presentation was done</small>

<ul style="font-size: 24px">
  <li>13 December 2022, Online, <a href="https://olio.hypotheses.org/1085">Les entrepôts de données : un outil au service des principes FAIR</a></li>
</ul>
</section>
</section>
