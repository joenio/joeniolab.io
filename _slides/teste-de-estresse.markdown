---
title: Teste de estresse - Juster.com.br
theme: serif
version: draft
---

<section>
### Teste de estresse

## Juster.com.br
</section>

<section>
> Teste de estresse é sobre exercitar o sistema sob condições hostis ao seu
> funcionamento.
> 
> Aumenta-se a carga progressivamente até o momento em que a aplicação começa a
> sofrer e finalmente cai ou falha.
</section>

<section>
### O que testar?

1) Testar cada serviço individualmente

* Servidores Web: Apache, Nginx
* Servidores de mensagem: XMPP, Rocket.chat
* Servidores de banco de dados
* Serviço de email
* Serviço de balanceamento de carga

2) Testar toda a aplicação com testes _funcionais_.

</section>

<section>
### Como testar?

Ferramentas específicas para teste de performance:

* [httperf](http://github.com/httperf/httperf)
* [siege](http://www.joedog.org/siege-home)
* [ab](http://httpd.apache.org/docs/2.2/en/programs/ab.html)
* [tsung](http://tsung.erlang-projects.org)
</section>

<section>
<section>
Estas opções não são muito úteis para testar a aplicação, algumas ferramentas
para testes automatizados de software servem melhor para este propósito:

* [Capybara](http://teamcapybara.github.io/capybara/)
* [RSpec](http://rspec.info)
</section>

<section>
  <img src="/files/screenshots/codigo-capybara.png" style="box-shadow: 0px 0px 5px gray" />
</section>

<section data-background="/files/backgrounds/capybara.jpg">
<p style="text-align: left"><img src="/files/phantomjs-logo.png" style="box-shadow: 0px 0px 5px gray" /></p>
<p style="text-align: left"><img src="/files/chrome-logo.png" style="box-shadow: 0px 0px 5px gray" /></p>
</section>

<section>
  <img src="/files/screenshots/capybara-poltergeist.png" style="box-shadow: 0px 0px 5px gray" />

  <img src="/files/screenshots/capybara-selenium.png" style="box-shadow: 0px 0px 5px gray" />
</section>

<section data-transition="fade">
  <img src="/files/screenshots/selenium-chromedriver-step1.png" style="box-shadow: 0px 0px 5px gray" />
</section>

<section data-transition="fade">
  <img src="/files/screenshots/selenium-chromedriver-step2.png" style="box-shadow: 0px 0px 5px gray" />
</section>

<section data-transition="fade">
  <img src="/files/screenshots/selenium-chromedriver-step3.png" style="box-shadow: 0px 0px 5px gray" />
</section>

</section>

<section>
<section>
#### Selenium possui uma IDE para Firefox<br/> _útil para gravar e executar suítes de teste_

  <img src="/files/screenshots/selenium-ide.png" style="box-shadow: 0px 0px 5px gray" />
</section>

<section>
Selenium é suportado na maioria dos softwares e serviços de teste de performance.

<img src="/files/jmeter-logo.svg" /><br/>
<img width="50%" src="/files/blazemeter-logo.svg" />
</section>

<section data-background="/files/backgrounds/tape.jpg" data-transition='zoom'>
#### JMeter

<img src="/files/screenshots/jmeter-grafico.png" style="box-shadow: 0px 0px 5px black" />
<img src="/files/screenshots/jmeter-relatorio.png" style="box-shadow: 0px 0px 5px black" />
</section>

<section>
> **throughput** representa quantas requisições/tempo o servidor trata,
> representa algo real, o servidor de fato está lidando com este número de
> requisições por minuto, é possível ajustar os parâmetros do teste para
> descobrir o máximo throughput possível do servidor

* JMeter = 10,9/seg
</section>

<section data-background="/files/backgrounds/tape.jpg" data-transition='zoom'>
#### BlazeMeter

<img src="/files/screenshots/blazemeter-screenshot.png" style="box-shadow: 0px 0px 5px black" />

* Throughput = 20.9/seg
</section>

<section data-background="/files/backgrounds/tape.jpg" data-transition='zoom'>
#### Taurus (possui integração com BlazeMeter)

<img src="/files/screenshots/tautus-screenshot.png" style="box-shadow: 0px 0px 5px black" />
</section>

</section>

<section>
<section>
### Custo do BlazeMeter $$$

<img src="/files/blazemeter-valores.png" style="box-shadow: 0px 0px 5px black; max-width: 130%; margin-left: -15%" width="130%" />
</section>

<section>
### Opções ao BlazeMeter e JMeter

<a href="http://grinder.sourceforge.net"><img src="/files/grinder-logo.png" style="box-shadow: 0px 0px 5px black; background-color: #444" /></a>
<a href="http://locust.io"><img src="/files/locust-logo.png"  style="box-shadow: 0px 0px 5px black; background-color: #444" /></a>
<a href="http://gatling.io"><img src="/files/gatling-logo.png" style="box-shadow: 0px 0px 5px black; background-color: #444" /></a>
<a href="http://grafana.com"><img src="/files/grafana-logo.svg" style="box-shadow: 0px 0px 5px black; background-color: #444" /></a>
</section>

<section>
### Qual é o melhor caminho?

1. BlazeMeter
 * pró: Menor curva de aprendizado e implementação
 * contra: Custo mensal mínimo de $99
1. JMeter
 * pró: Custo mensal de $0 (software livre)
 * contra: Maior investimento inicial de implementação
</section>
</section>


<section>
<section>
### É necessário monitorar os recursos

O teste de estresse vai dar diagnóstigo geral dos limites da aplicação mas não
dará muitos indícios sobre quais pontos merecem melhoria.
</section>

<section>
<img src="/files/newrelic-logo.svg" /><br/>
<img src="/files/zabbix-logo.png" />
<img src="/files/nagios-logo.png" />
<img src="/files/icinga-logo.png" />
<img src="/files/munin-logo.png" />
</section>
</section>
