---
title: Cliente de email de linha de comando mutt
---

TODO: testar e configurar urlview para extrair links em mensagens HTML pois o mutt exibe apenas text/plain

arquivo .mailcap para definir comandos de visualizar arquivos html com lynx, links ou w3m
(talvez esse arquivo seja onde defino qual programa usar para abrir pdf e imagens, padrao usa
xv e image magick, quero usar os padrão do meu desktop gnome)

documentar como usar o mutt para gerenciar emails usando imap, smtp, anti spam,
filtros e indexação para busca

mutt + msmtp + mbsync + imapfilter + spamassassin + vim + notmutch

usando servidor de email oferecido pelo Gandi gratuitamente na compra de qualquer domínio

cliente de email feito para usar com o notmuch: alot
escritp em python usa ima lib pra escrever text ui, parece bom, irei testar



mesma duvida minha sobre mbsync+bogofilter (ou outro filtro), o getmail tem integracao mais facil
http://thread.gmane.org/gmane.mail.mu.general/2611

https://www.neomutt.org/contrib/useful-programs

bogofilter + imapfilter
https://gist.github.com/battlemidget/5758764

imapfilter para filtrar spam:
https://iranzo.github.io/blog/2015/08/28/filtering-email-with-imapfilter/

talvez o bogofilter seja melhor opcao para spam (tem tb razor, pyzor, spamassassin, etc...)
https://www.linux.com/learn/stop-spam-bogofilter-linux

o sup-mail é muito bugado e lento!

bom tutorial com mbsync e msmtp:
https://bostonenginerd.com/posts/notmuch-of-a-mail-setup-part-1-mbsync-msmtp-and-systemd/
falta executar: systemctl --user daemon-reload

systemd --user:
https://serverfault.com/questions/700862/do-systemd-unit-files-have-to-be-reloaded-when-modified#700956
https://mikewilliamson.wordpress.com/2015/08/26/running-a-rails-app-with-systemd-and-liking-it/

segunda parte do tutorial mostra como usar o cliente notmuch
https://bostonenginerd.com/posts/notmuch-of-a-mail-setup-part-2-notmuch-and-emacs/

=====

vantagem do mbsync, nao preciso criar na mao os maildir, ele cria
automaticamente o que vem do remoto

desvantagem: interrompe a sync mas n da erro, apenas para, volta com ctrl+C e executando novamente

estou recebendo o mesmo warning relatado abaixo
https://sourceforge.net/p/isync/bugs/18/

====

MINHA SELECAO:

* cliente: mutt
* recuperar emails: mbsync
* enviar emails: msmtp
* filtrar spam e arquivar: imapfilter
* contatos e busca: notmutch
* antispam: spam assassin

=====

anti spam, parece que o jeito "moderno" de fazer com IMAP é através do imapfilter
https://github.com/lefcha/imapfilter
https://roylez.info/2016-02-10-mutt-multi-imap-accounts/

o imapfilter permite organizar tudo do lado do servidor antes de baixar local,
inclusive mover entre pastas, mover para pasta de spam, apagar, marcar como lido, etc...


spamassassin é o numero 1 anti-spam, para instalar no Debian

**irei utilizar spamassassin no meu setup de email para filtrar spams**

apt install spamassassin - isso instala tb o cliente spamc que é o jeito mais eficiente
do que ter que executar o comando spamassassin via linha de comando diretamente

sudo systemctl enable spamassassin
sudo systemctl start spamassassin

para treinar o que é spam e o que não é, definido como ham basta executar
os comandos passando como argumento as minhas pastas do mutt, como abaixo:

	sa-learn --spam ~/Mail/saved-spam-folder
	sa-learn --ham ~/Mail/inbox
	sa-learn --ham ~/Mail/other-nonspam-folder

no meu caso real, o setup e comandos devem ser o seguinte:

treinar com mensagens de spams previamente selecionadas:

	sa-learn --spam ~/Maildir/joenio@joenio.me/Junk/

executei o comando acima em 27/03/2019, produziu a seguinte saída:

Learned tokens from 239 message(s) (296 message(s) examined)

executei novamente em 14/07/2019, produziu a seguinte saída:

Learned tokens from 110 message(s) (404 message(s) examined)

treinar com hams (mensagens nao-spams):

	sa-learn --ham ~/Maildir/joenio@joenio.me/Archives/

saida da execucao do comando acima:

Learned tokens from 8488 message(s) (8497 message(s) examined)


em seguida usar o cliente spamc para testar se uma mensagem é spam e junto
ao imapfilter mover os spams para a pasta Junk

https://raymii.org/s/blog/Filtering_IMAP_mail_with_imapfilter.html

13 julho 2019:
* tenho 2300 mensagens no INBOX (a maioria spam)
* tenho 418 na pasta Junk (enquanto escrevia o relato abaixo o numero subiu para 419)

(ao executar o imapfilter com o spamc deveria mover a maioria do spam no INBOX para Junk)

vou executar e testar... mas creio que o numero 418 é inclui o filtro, o checkmail.sh estava rodando
euqnanto eu editava o script lua do imapfilter, tem no Junk mensagens de Julho, verificar se sao
todas spam e alterar o script lua do imapfilter para mover ao inves de copiar (usei copia para testar)

rodei novamente em 16 julho 2019 (funcionou, reduziu spans):
* INBOX = 1300 ~
* Junk = 1400 ~

após revisar as mensagens movidas para o Junk folder, e validar que uma amostra é
de fato spam, rodei novamente o sa-learn na pasta Junk:

	sa-learn --spam ~/Maildir/joenio@joenio.me/Junk/

resultado:
Learned tokens from 1075 message(s) (1481 message(s) examined)

em 17 Julho 2019:

	sa-learn --spam ~/Maildir/joenio@joenio.me/Junk/

resultado:

Learned tokens from 118 message(s) (1600 message(s) examined)

se ocorreu tudo bem devo rodar o sa-lear --spam no Junk para atualizar a base do spam assassin

(sup wiki) Configuring the way email leaves

msmtp http://msmtp.sourceforge.net/ (ja usei no passado junto com mutt, funciona bem e parece ainda ser bem referenciado)
putmail
sendEmail
ssmtp https://tracker.debian.org/pkg/ssmtp

Configuring the way email is retrieved

OfflineIMAP
mbsync/isync
Fetchmail
mailsync

getmail nao separa os "labels/pastas" e baixa tudo do servidor num único lugar

tive o seguinte problema com o mbsync https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=871918
workaround: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=871765

===

esmtp http://esmtp.sourceforge.net/ (nao esta sendo mantido mais, 2009)

msmtp and nullmailer are good choices as light weight MTAs
https://lists.gt.net/gentoo/user/263767

nullmailer http://untroubled.org/nullmailer/

===

nmh parece uma boa escolha, simples, continua evoluindo http://www.nongnu.org/nmh/
  (muito hard-core, comandos, escrever os campos do email num TXTzao, etc)
mmh (outro igual ao nmh) http://marmaro.de/prog/mmh/
notmuch tambem parece bom - tutorial iniciando https://notmuchmail.org/getting-started/
  (nao serve pra ler emails, mas é bom pra buscar contatos)
mutt, o bom e velho, mas quero testar algo novo (no debian o pacote é feito com base no projeto neomutt)
  (o mutt/neomutt é o que ainda há de mais novo)
mail, mailx, etc, pode ser bom mas é muito trampo
cleancode mail tb parece bom mas aparenta ser un projeto meio isolado do mundo
gnu mailutils
gnus (emacs)
elm
sup (foi inspiracao pro notmuch) mas continua vivo e parece bom tb! sup-heliotrope.github.io

temos ferramentas de apoio (roda por baixo dos panos mas podem ser usados diretamente)

offlineimap (o mbsync tem sido citado como mais rápido e mais resiliente que o offlineimap)
sendmail
postfix

Mail retrieval agent (MRA): https://en.wikipedia.org/wiki/Mail_retrieval_agent

fetchmail (standalone)
getmail (standalone) replacement for fetchmail, devel ativo!

podem existir ferramentas que fazem mais de uma função, é bem comum, mas vou preferir aquelas
que faze apenas um trabalho bem feito!

(The concept of an MRA is not standardized in email architecture)

formatos de armazenamento

mailbox
maildir (maildir é o melhor formato, tem uma pá de variacoes, mas nada mto relevante)
nmh (ferramenta e tb formato de arquivo)

na prática são vários maildirs (INBOX, Sent, Drafts, etc)

outras ferramentas

mairix - indexacao de emails, entende formatos de armazenamento

sup-mail é muito lento em relaçao ao mutt e ainda dá uns pau crash as vezes

INSTALACAO E CONFIGURACAO

notmuch: apt-get install notmuch
         notmutch (abre assistente pedindo, nome, email e local do maildir)

getmail: apt-get install getmail4
         criar arquivo de configuração manualmente (ver http://pyropus.ca/software/getmail/configuration.html#configuring)
         o getmail nao cria o maildir caso ainda n exista, é preciso criar o diretorio maildir manualmente
           mkdir -p ~/mail/{cur,new,tmp}
           chmod -R 0700 ~/mail
         executa o getmail na linha de comando sem parametros, vai baixar os emails novos, no principio baixa tudo!
