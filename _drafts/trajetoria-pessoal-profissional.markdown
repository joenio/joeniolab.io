---
title: Trajetoria... ainda sem titulo.
lead: >
  Este é um ...
---

![ballon flying in the sky picture](/files/goodbye-baloon-inverse.jpg)

Desde o último [relato profissional em 2016](/carta-de-despedida-da-colivre)
algumas experiências talvez valham a pena ser contadas, aqui neste post
farei um resumo das experiencias que tive e projetos que trabalhei até
chegar aos dias atuais.

Em algum momento após a saída da Colivre eu mudei para Brasília e tive a ótima
oportunidade de trabalhar em projetos super desafiadores sempre com pessoas
incríveis, entre eles Participa.br, Portal do Software Público Brasileiro e
Justiça Presente.

Ao ...
