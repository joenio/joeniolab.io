---
title: Aventuras com OpenGL
---

Por algum tempo tenho utilizado o software de visualização musical projectM
para experimentos em arte e tecnologia, resolvi fazer algumas gambiarras em
seu código fonte e para isso foi necessário entender o mínimo de OpenGL, assim
resolvi documentar este meu primeiro contato para sistematizar o aprendizado.

Primeiro, OpenGL é uma especificação mantida pelo grupo Khronos, segundo, as
inúmeras implementações são mantidas pelos fabricantes de placas de vídeo e pelos
interessados no tema, no Linux vários projetos implementam esta especificação.

O grupo Khronos mantém uma série de outros padrões para Gráficos 3D, Realidade
Virtual, Realidade Aumentada, Computação Paralela, Redes Neurais, and Vision
Processing. A atual da especifficação OpenGL é a versão 4.6 de outubro de 2019.

Existem 2 modos de programação em OpenGL, um modo com maior nível de abstração chamado immediate mode,
e um outro modo mais próximo do core do OpenGL chamado core-profile, o modo immediate mode é desaconselhado
pela própria especificação e tem se tornado depreciada em função do core-profile pois este
último apesar de ter uma curva maior de aprendizado permite maior controle e uso dos
recursos disponíveis no OpenGL.

Desenvolver no modo core-profile é a abordagem moderna para desenvolver com
OpenGL, apesar da especificação atual ser 4.5 a versão 3.3 contém praticamente
todos os recursos das versões mais modernas, a partis da versão 3.3 as mudanças
são especialmente de funcionalidades extras mas nenhuma mudança no core, de
forma que aprender o core da versão 3.3 permite programar em qualquer versão
subsequente.

OpenGL possui suporte a extensões permitindo que os fabricantes lancem novas
técnicas através dos seus drivers, as extensões mais populares tendem a se
tornar parte da especificação em versões futuras.

OpenGL é uma máquina de estados, esse estado tem o nome de context, ao trabalhar
com ele estamos usando funções para mudança de contexto chamadas state-changing
e operações para alterar o estado corrente chamadas de funções state-using.

OpenGL é independente de plataforma e fabricante, portanto não se preocupa com
questões relativas a desktop que varia de ambiente para ambientes, assim
para usar OpenGL é necessário utilizar de forma auxiliar alguma biblioteca
que entenda o desktop e como minimamente criar uma janela para então o OpenGL
possa ser renderizado nesta janela, a criação da janela não é responsabilidade
do OpenGL e é necessário usar algum framework para isso.

Diversos projetos e frameworks são feitos para preencher esse espaço dando uma
forma fácil de criar uma janela com um contexto OpenGL para nós implementarmos
o que queremos renderizar nele, algumas bibliotecas populares para isso são
GLUT, GLFW, SFML e SDL. Essas bibliotecas são diferentes de GLEW, GL3W, GLAD que são
extension loaders (OpenGL Loading Library).

<!-- https://www.khronos.org/opengl/wiki/OpenGL_Loading_Library -->

Existem muitas versões diferentes de drivers OpenGL, e cada uma é específica do
fabricante, a localização da maioria das funções não são conhecidas em tempo de
compilação e precisão ser descobertas em tempo de execução, são específicas de
sistema operacional, portanto alguém precisa fazer esse papel, é possível
implementar manualmente usando métodos como `wglGetProAddress()` ou fazer uso
de algum loader como o GLEW que faz esse trabalho para nós.

Dependencias:

<pre class="terminal">
<code>
sudo apt install libglew-dev libglfw3-dev build-essential make
</code>
</pre>

Hello world básico para testar as bibliotecas e includes:

```c
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  printf("hello world\n");
  return 0;
}

```

Makefile para auxiliar a compilação:

```makefile
main: main.c
	$(CC) $? -o $@ `pkg-config glfw3 glew --libs --cflags`
```

Para compular e testar:

<pre class="terminal">
<code>
make
./main
hello world
</code>
</pre>

É necessário iniciar o GLFW, GLEW e então começar a usar as funções OpenGL, o primeiro passo
é informar ao OpenGL a área da janela disponível para renderizar suas funções com a função
`glViewport()`.

Tudo no OpenGL está num espaço 3D mas a janela é um espaço 2D com array de pixels, então
grande parte do trabalho do OpenGL é transformar as coordenadas 3D para pisels 2D, esse
processo de transformação é gerenciado pelo graphics pipeline do OpenGL, esse pipeline é
dividido em duas grandes partes, primeiro transdormar coordenadas 3D em coordanadas 2D e segundo
transformar coordenadas 2D em pixels com cor. O pipeline geral possui diversas etapas onde
as saídas de uma etapa é repassado como entrada da etapa seguinte, cada etapa é especializada
numa única função e podem ser paralelizadas, os inúmeros unidades de processamento das GPUs
processam programadas chamados shaders.

Shaders são escritos em OpenGL Shading Language (GLSL), aqui surge vertex,
vertec data, vertex attributes, primitives, vertex shader. As coordenadas em
OpenGL são dadas em valores normalizados (normalizated device coordinates -
NDC) variando de -1.0 à 1.0 e qualquer valor fora desse intervalo são regiões
fora da tela, são clipped pela etapa no pilepile antes do fragment shader. A
coordenada 0.0 é o centro da tela, as coordenadas NDC informadas no código são
convertidas em coordenadas no espaço da tela (screen-space coordinates) no
passo do pipeline viewport transform usando as coordenadas da tela passadas
pelos parâmetros no método `glViewport()`.

O primeiro passo é passar as coordenadas para o etapa inicial do pipeline, vertex shader,
isso é feito criando uma área de memória na GPU, essa área de memória é gerenciada
pelo vertex buffer objects (VBO), enviar dados da CPU para a placa gráfica é relativamente
lento, então é aconselhável enviar o máximo de dados necessários de uma só vez.

A função `glBufferData()` copia a estrutura de dados definida em nosso código para a memória
da placa gráfica, o buffer de memória. O último parâmetro desda função indica como a placa
gráfica irá gerenciar os dados, se os dados não se alteram muito, se os dados alteram frequentemente,
ou se os dados irão alterar a cada vez que for desenhado. Isto define em que típo de memória
será armazenado, se numa memória mais rápida para escrita, ou mais rápida para leitura, etc.

https://learnopengl.com/Getting-started/Hello-Triangle
