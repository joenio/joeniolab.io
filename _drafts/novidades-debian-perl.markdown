---
title: Novidades Debian Perl
lead: >
  Novidades ...
---

Atualizacoes e novidades em empacotamento Perl no Debian.

Standard-Version = 4.6.1

lidando com repositorio individual de um pacote com dpt:

$ dpt checkout libfoo-perl

This will go to $DPT_PACKAGES/libfoo-perl and invoke gbp-pull(1). If the directory does not exist, gbp-clone(1) is used to create the initial clone.

entrar na pasta do pacote e importar novidades do upstream

$ dpt import-orig

1. debian/changelog
2. debian/copyright
3. debian/control
4. check with cme check dpkg-control debian/control
5. debian/watch
6. upload com dpt push

# como atualizar um unico pacote

entrar na pasta do pacote: dpt cd <pacote>
atualizar copia local do pacote: gbp pull

# debian functional tests

como adicionar testes no pacote debian, alem dos eventuais testes que existam no upstream:

* https://ci.debian.net/doc/file.TUTORIAL.html

no build nao se ve os testes, eh necessario executar autopkgtest depois que constroi o pacote

# debian pristine-tar branch

* https://manpages.debian.org/unstable/pristine-tar/pristine-tar.1.en.html
* https://wiki.debian.org/GitSrc
* https://wiki.debian.org/Projects/DebSrc3.0

# importante lintian unstable

eu uso testing no desktop, mas para buildar pacotes eh ideal usar unstable/sid, entao
a solucao eh usar holodev para criar container com sid, (ideia para holodev: criar perfis
de container, holodev --environment debian-dev).

sudo apt install devscripts debhelper git-buildpackage pkg-perl-tools
sudo apt install devscripts debhelper git-buildpackage
sudo pbuilder create

# build pacote, 30 july 2022

os dois ultimos posts documentei o uso do pbuilder, hoje testei o cowbuilder dentro de
um container sid criado com holodev, funcionou bem.

```sh
cowbuilder create
```

git-pbuilder

# dica do dam via irc, 01 aug 2022

remover AUTHORS and alterar d/changelog para unstable

deu dica de usar (dch -r) para atualizar o d/changelog de UNRELEASED para unstable
* eh mto bom, altera para unstable e atualiza a data

# autopkgtest no analizo falhando, 02 ago 2022

dam me avisou que o analizo ta falhando, messagem copiada abaixo.

: sorry I didn't test this earlier, but all autopkgtests fail.
I run them in a chroot with `autopkgtest package_ver_arch.changes -- schroot sid`

Legal saber q ele rodou o autopkgtest com o comando abaixo:

    autopkgtest package_ver_arch.changes -- schroot sid

Eu to testando hoje com o lxc da seguinte forma:

    sudo autopkgtest $PWD -- lxc autopkgtest-sid

Mas o pacote ta falhando com as seguintes mensagens abaixo:

```
autopkgtest [23:00:25]: @@@@@@@@@@@@@@@@@@@@ summary
smoke-tests          FAIL non-zero exit status 2
autodep8-perl-build-deps FAIL non-zero exit status 1
autodep8-perl        FAIL non-zero exit status 1
autodep8-perl-recommends FAIL non-zero exit status 1
and now I need to go to bed, but will be back tomorrow evening (UTC+3)
```

Parece que o erro é por rodar os testes como root (ainda n estou certo mas
parece que autopkgtest) roda como root, rodei os testes dentro dum container
lxc como root e tive o seguinte erro:

```
t/Analizo/Batch/Job/Git.t ................................................ fatal: unsafe repository ('/HOLODEV/analizo/t/Analizo/Batch/Job/Git.t.tmpdir/evolution' is owned by someone else)
To add an exception for this directory, call:

	git config --global --add safe.directory /HOLODEV/analizo/t/Analizo/Batch/Job/Git.t.tmpdir/evolution
t/Analizo/Batch/Job/Git.t ................................................ 1/? 
#   Failed test 'changed_files died (Can't call method "changed_files" on an undefined value at t/Analizo/Batch/Job/Git.t line 89.)'
#   at t/Analizo/Batch/Job/Git.t line 212.
#   (in t::Analizo::Batch::Job::Git->changed_files)
fatal: unsafe repository ('/HOLODEV/analizo/t/Analizo/Batch/Job/Git.t.tmpdir/evolution' is owned by someone else)
To add an exception for this directory, call:

	git config --global --add safe.directory /HOLODEV/analizo/t/Analizo/Batch/Job/Git.t.tmpdir/evolution

#   Failed test 'git_checkout_should_actually_checkout died (Can't call method "prepare" on an undefined value at t/Analizo/Batch/Job/Git.t line 66.)'
#   at t/Analizo/Batch/Job/Git.t line 212.
#   (in t::Analizo::Batch::Job::Git->git_checkout_should_actually_checkout)
```

Consegui rodar o autopkgtest mas alguns testes estao falhando


```

Regenerating fonts cache... done.
Paramétrage de libpango-1.0-0:amd64 (1.50.7+ds-1) ...
Paramétrage de libcairo2:amd64 (1.16.0-6) ...
Paramétrage de libgd3:amd64 (2.3.3-6) ...
Paramétrage de libpangoft2-1.0-0:amd64 (1.50.7+ds-1) ...
Paramétrage de libpangocairo-1.0-0:amd64 (1.50.7+ds-1) ...
Paramétrage de libgvc6 (2.42.2-7) ...
Paramétrage de graphviz (2.42.2-7) ...
Paramétrage de autopkgtest-satdep (0) ...
Traitement des actions différées (« triggers ») pour libc-bin (2.33-8) ...
(Lecture de la base de données... 22492 fichiers et répertoires déjà installés.)
Suppression de autopkgtest-satdep (0) ...
autopkgtest [00:35:22]: test autodep8-perl-recommends: /usr/share/pkg-perl-autopkgtest/runner runtime-deps-and-recommends
autopkgtest [00:35:22]: test autodep8-perl-recommends: [-----------------------

    #   Failed test '/usr/bin/perl -wc /usr/share/perl5/Test/Analizo/BDD/Cucumber/Extension.pm exited successfully'
    #   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 124.

    #   Failed test '/usr/bin/perl -wc /usr/share/perl5/Test/Analizo/Class.pm exited successfully'
    #   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 124.

    #   Failed test '/usr/bin/perl -wc /usr/share/perl5/Test/Analizo.pm exited successfully'
    #   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 124.
    # Looks like you failed 3 tests of 54.

#   Failed test 'all modules in analizo pass the syntax check'
#   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 127.
# Looks like you failed 1 test of 4.
/usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t .. 
1..4
ok 1 - Package analizo is known to dpkg
ok 2 - Got status information for package analizo
ok 3 - Got file list for package analizo
# Subtest: all modules in analizo pass the syntax check
    1..54
    ok 1 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Directories.pm exited successfully
    ok 2 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Git.pm exited successfully
    ok 3 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Job/Directories.pm exited successfully
    ok 4 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Job/Git.pm exited successfully
    ok 5 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Job.pm exited successfully
    ok 6 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Output/CSV.pm exited successfully
    ok 7 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Output/DB.pm exited successfully
    ok 8 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Output.pm exited successfully
    # Name "FFI::Platypus::TypeParser::basic_type" used only once: possible typo at /usr/lib/x86_64-linux-gnu/perl-base/XSLoader.pm line 111.
    # Name "FFI::Platypus::TypeParser::ffi_type" used only once: possible typo at /usr/lib/x86_64-linux-gnu/perl-base/XSLoader.pm line 111.
    # Name "FFI::Platypus::keep" used only once: possible typo at /usr/lib/x86_64-linux-gnu/perl-base/XSLoader.pm line 111.
    ok 9 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Runner/Parallel.pm exited successfully
    ok 10 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Runner/Sequential.pm exited successfully
    ok 11 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch/Runner.pm exited successfully
    ok 12 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Batch.pm exited successfully
    ok 13 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command/files_graph.pm exited successfully
    ok 14 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command/graph.pm exited successfully
    ok 15 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command/help.pm exited successfully
    ok 16 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command/metrics.pm exited successfully
    ok 17 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command/metrics_batch.pm exited successfully
    ok 18 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command/metrics_history.pm exited successfully
    ok 19 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command/tree_evolution.pm exited successfully
    ok 20 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Command.pm exited successfully
    ok 21 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Extractor/Doxyparse.pm exited successfully
    ok 22 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Extractor.pm exited successfully
    ok 23 - /usr/bin/perl -wc /usr/share/perl5/Analizo/FilenameFilter.pm exited successfully
    ok 24 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Filter/Client.pm exited successfully
    ok 25 - /usr/bin/perl -wc /usr/share/perl5/Analizo/GlobalMetric/ChangeCost.pm exited successfully
    ok 26 - /usr/bin/perl -wc /usr/share/perl5/Analizo/GlobalMetric/MethodsPerAbstractClass.pm exited successfully
    ok 27 - /usr/bin/perl -wc /usr/share/perl5/Analizo/GlobalMetric/TotalAbstractClasses.pm exited successfully
    ok 28 - /usr/bin/perl -wc /usr/share/perl5/Analizo/GlobalMetrics.pm exited successfully
    ok 29 - /usr/bin/perl -wc /usr/share/perl5/Analizo/LanguageFilter.pm exited successfully
    ok 30 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/AfferentConnections.pm exited successfully
    ok 31 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/AverageCycloComplexity.pm exited successfully
    ok 32 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/AverageMethodLinesOfCode.pm exited successfully
    ok 33 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/AverageNumberOfParameters.pm exited successfully
    ok 34 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/CouplingBetweenObjects.pm exited successfully
    ok 35 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/DepthOfInheritanceTree.pm exited successfully
    ok 36 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/LackOfCohesionOfMethods.pm exited successfully
    ok 37 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/LinesOfCode.pm exited successfully
    ok 38 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/MaximumMethodLinesOfCode.pm exited successfully
    ok 39 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/NumberOfAttributes.pm exited successfully
    ok 40 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/NumberOfChildren.pm exited successfully
    ok 41 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/NumberOfMethods.pm exited successfully
    ok 42 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/NumberOfPublicAttributes.pm exited successfully
    ok 43 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/NumberOfPublicMethods.pm exited successfully
    ok 44 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/ResponseForClass.pm exited successfully
    ok 45 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metric/StructuralComplexity.pm exited successfully
    ok 46 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Metrics.pm exited successfully
    ok 47 - /usr/bin/perl -wc /usr/share/perl5/Analizo/Model.pm exited successfully
    ok 48 - /usr/bin/perl -wc /usr/share/perl5/Analizo/ModuleMetric.pm exited successfully
    ok 49 - /usr/bin/perl -wc /usr/share/perl5/Analizo/ModuleMetrics.pm exited successfully
    ok 50 - /usr/bin/perl -wc /usr/share/perl5/Analizo.pm exited successfully
    # Can't locate Test/BDD/Cucumber/Extension.pm in @INC (you may need to install the Test::BDD::Cucumber::Extension module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.34.0 /usr/local/share/perl/5.34.0 /usr/lib/x86_64-linux-gnu/perl5/5.34 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.34 /usr/share/perl/5.34 /usr/local/lib/site_perl) at /usr/lib/x86_64-linux-gnu/perl-base/parent.pm line 16.
    # BEGIN failed--compilation aborted at /usr/share/perl5/Test/Analizo/BDD/Cucumber/Extension.pm line 7.
    not ok 51 - /usr/bin/perl -wc /usr/share/perl5/Test/Analizo/BDD/Cucumber/Extension.pm exited successfully
    # Can't locate Test/Class.pm in @INC (you may need to install the Test::Class module) (@INC contains: /root/perl5/lib/perl5/5.34.0/x86_64-linux-gnu-thread-multi /root/perl5/lib/perl5/5.34.0 /root/perl5/lib/perl5/x86_64-linux-gnu-thread-multi /root/perl5/lib/perl5 /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.34.0 /usr/local/share/perl/5.34.0 /usr/lib/x86_64-linux-gnu/perl5/5.34 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.34 /usr/share/perl/5.34 /usr/local/lib/site_perl) at /usr/lib/x86_64-linux-gnu/perl-base/parent.pm line 16.
    # BEGIN failed--compilation aborted at /usr/share/perl5/Test/Analizo/Class.pm line 6.
    not ok 52 - /usr/bin/perl -wc /usr/share/perl5/Test/Analizo/Class.pm exited successfully
    ok 53 - /usr/bin/perl -wc /usr/share/perl5/Test/Analizo/Git.pm exited successfully
    # Can't locate Test/MockObject/Extends.pm in @INC (you may need to install the Test::MockObject::Extends module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.34.0 /usr/local/share/perl/5.34.0 /usr/lib/x86_64-linux-gnu/perl5/5.34 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.34 /usr/share/perl/5.34 /usr/local/lib/site_perl) at /usr/share/perl5/Test/Analizo.pm line 15.
    # BEGIN failed--compilation aborted at /usr/share/perl5/Test/Analizo.pm line 15.
    not ok 54 - /usr/bin/perl -wc /usr/share/perl5/Test/Analizo.pm exited successfully
not ok 4 - all modules in analizo pass the syntax check
Dubious, test returned 1 (wstat 256, 0x100)
Failed 1/4 subtests 

Test Summary Report
-------------------
/usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t (Wstat: 256 Tests: 4 Failed: 1)
  Failed test:  4
  Non-zero exit status: 1
Files=1, Tests=4,  4 wallclock secs ( 0.02 usr  0.02 sys +  3.00 cusr  0.39 csys =  3.43 CPU)
Result: FAIL
autopkgtest [00:35:26]: test autodep8-perl-recommends: -----------------------]
autopkgtest [00:35:26]: test autodep8-perl-recommends:  - - - - - - - - - - results - - - - - - - - - -
autodep8-perl-recommends FAIL non-zero exit status 1
autopkgtest [00:35:26]: test autodep8-perl-recommends:  - - - - - - - - - - stderr - - - - - - - - - -

    #   Failed test '/usr/bin/perl -wc /usr/share/perl5/Test/Analizo/BDD/Cucumber/Extension.pm exited successfully'
    #   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 124.

    #   Failed test '/usr/bin/perl -wc /usr/share/perl5/Test/Analizo/Class.pm exited successfully'
    #   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 124.

    #   Failed test '/usr/bin/perl -wc /usr/share/perl5/Test/Analizo.pm exited successfully'
    #   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 124.
    # Looks like you failed 3 tests of 54.

#   Failed test 'all modules in analizo pass the syntax check'
#   at /usr/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/syntax.t line 127.
# Looks like you failed 1 test of 4.
autopkgtest [00:35:26]: @@@@@@@@@@@@@@@@@@@@ summary
smoke-tests          PASS
autodep8-perl-build-deps FAIL non-zero exit status 1
autodep8-perl        FAIL non-zero exit status 1
autodep8-perl-recommends FAIL non-zero exit status 1
```

Referencias:

Tutorial simples, curto e pratico feito no debci:
https://ci.debian.net/doc/file.TUTORIAL.html


Detalhes especificos do autopkgtest do time de perl, injeta uma serie de testes que "teoricamente" funciona
com qquer modulo CPAN (smokes, use.t, syntax.t, etc...):
https://perl-team.pages.debian.net/autopkgtest.html

debian/tests/control example:
https://salsa.debian.org/perl-team/modules/packages/pkg-perl-tools/blob/master/autopkgtest/examples/default-tests-control

# 03 ago 2022, continuando autopkgtest dive

https://salsa.debian.org/perl-team/modules/packages/pkg-perl-tools/blob/master/autopkgtest/scripts/runner#L24

autopkgtest backends:
* qemu
* chroot
* schroot
* lxc
* ???

alguns backends definem $HOME outros nao (schroot nao define $HOME)


segue lista de opcoes para rodar autopkgtest no diretorio do fonte do pacote local

sem chroot ou virtualizacao, roda direto no host, util para rodar dentro do holodev por ex:

    autopkgtest --output-dir /tmp/output-dir -B  . -- null

para rodar com lxc:

    autopkgtest . -- lxc --sudo autopkgtest-sid

sobre o parametro --sudo copiado da documentacao do autpkgtest:

If your user can get root privileges with sudo, you can call autopkgtest as
your normal user and specify ``-s`` (``--sudo``) so that the container
can be started as root.

ref: https://sources.debian.org/src/autopkgtest/5.22/doc/README.running-tests.rst/

See autopkgtest-virt-lxc(1) manpage. This also explains how to build containers.

particularly autopkgtest-build-lxc(1) for con‐ veniently creating standard autopkgtest containers.

abaixo trecho do manpage:

       Create a suitable debootstrap-based container for Debian or Ubuntu template, e.  g. a Debian sid one (will be named autopkgtest-sid):
              autopkgtest-build-lxc debian sid

possivel solucao eh definir no git que tudo eh diretorio seguro dentro do autopkgtest apenas

    git config --global --add safe.directory '*'


tentei criar o debian/tests/pkg-perl/smoke-setup com o comando acima, rodei o autopkgtest
mas n funcionou, alguns testes sao executados antes aparentemente dessa etapa e falha igual.


#### para buildar imagem qemu com autopkgtest

    sudo autopkgtest-build-qemu unstable autopkgtest-sid.img


    autopkgtest-build-qemu unstable ~/src/debian/images/autopkgtest-sid.img


entao depois disso para rodar o autopkgtest do pacote dentro do qemu

    sudo autopkgtest $PWD -- qemu autopkgtest-sid.img

finalmente com o autopkgtest com qemu parece ter resolvido a treta, eh mais lento
mas funciona, mto provavelmente pq o ambiente eh mais real, deve ter definido $USER $HOME etc


descobri um possivel bug no analizo, o modulo local::lib sendo usado em lib/Analizo.pm e outros esta tentand ocriar diretorio ~/perl no home do usuario, isto esta criando erro no autopkgtest como reportado por gregor no link abaixo:

* https://paste.ubuntu.com/p/hVWHNjmM9d/

no meu ambiente mesmo rodando com qemu onde tudo funciona bem tambem vejo
a mesma mensagem mas sem erro, copiado abaixo

* https://paste.ubuntu.com/p/hVWHNjmM9d/

    Skip blib/lib/auto/share/dist/Analizo/README (unchanged)
    PERL_DL_NONLAZY=1 "/usr/bin/perl" "-MExtUtils::Command::MM" "-MTest::Harness" "-e" "undef *Test::Harness::Switches; test_harness(1, 'blib/lib', 'blib/arch')" t/*.t t/Analizo/*.t t/Analizo/Batch/*.t t/Analizo/Batch/Job/*.t t/Analizo/Batch/Output/*.t t/Analizo/Batch/Runner/*.t t/Analizo/Command/*.t t/Analizo/Extractor/*.t t/Analizo/GlobalMetric/*.t t/Analizo/Metric/*.t t/Analizo/Metric/AfferentConnections/*.t
    Attempting to create directory /tmp/autopkgtest.2dVise/build.NZd/real-tree/debian/.debhelper/generated/_source/home/perl5
    # 
    # t::Analizo->constructor



* rodando com autopkgtest-lxc da erro de git security safe.directory
* rodando com qemu nao da esse erro mas falha alguns testes, inclusive ainda falha o erro com git security safe tambem!


## autopkgtest schroot

mk-sbuild do pacote ubuntu-de-tools torna a vida mais facil para criar chroots sem precisar root, basta adicionar o usuario ao grupo sbuild

e entao executar mk-sbuild <release>, exemplo mk-sbuild sid, sera criado com o nome <release>-<arch>, exemplo sid-amd64

depois basta rodar no codigo do pacote para buildar com esse chroot:

    autopkgtest . -- schroot sid-amd64

## 05 ago 2022, dam via IRC

the last five changes are from `dpt fixup` -- some of them are cosmetic, but help making the package look like most of the others maintained by the group

importante sempre atualizar o timestamp do changelog com o comando dch -r

## 06 ago 2022, apos corrigir autpgkttest no analizo dam encontrou novas coisas

copiado as dicas dadas no changelog, transcrever com descricoes de coisas a sempre revisar no pacote.

  TODO: t/sampes/kdelibs/parser.cpp probably misses its original copyright and
  licensing statement

  TODO: some files under t/samples have copyright holders and licenses that
  aren't described in debian/copyright

  TODO: t/sampes/android-framework/android-5.1.11-r38/MCLinker.cpp refers to
  missing LICENSE.txt

Isto me deu uma ideia criar um arquivo contendo um conjunto de projetos
completos (incluindo todo o codigo-fonte) para o analizo ser continuamente
testado a cada mudança, seja interna ou externa, a atualizacao de uma
dependencia é um exemplo de mudança externa, ... serve tambem par outros fins,
por exemplo agora mesmo uma dos itens TODO requer que eu tenha acesso ao codigo
fonte completo do projeto android 5.1.11-r38 para copiar o arquivo LICENSE.txt

Alem de criar o arquivo eh bom documentar os passos para recriar o arquivo para
aumentar a reprodutibilidade do projeto como um todo.

    sudo apt install repo
    cd ~/src/analizo/analizo-archive/
    mkdir android
    cd android
    repo init -u https://android.googlesource.com/platform/manifest -b android-5.1.1_r38
    repo sync # this can take a while

Referencia sobre obter o codigo fonte do android:
* https://source.android.com/setup/build/downloading

Ideia de arquitetura para usar o analizo-archive para testar mas tambem para
criar um banco de dados de metricas calculadas pelo analizo, de forma que
outras pessoas possam contribuir com seus proprios archives colaborando numa
base de dados comum.

```
    ________________
   |                |
   | archive server |
   |________________|
           ^                              ______________
           | N                         N |              |
           |          .-----install----> | user desktop |
        install      /                   |______________|
           |        /
           | N     / N
    _______________                       ________________
   |               | N                 1 |                |
   | analizo agent | -------call-------> | analizo server |
   |_______________|                     |________________|
                                                1 |
           | 1        __________________          |
           |         |                  | 1       |
         listen      | metrics database | <-write-'
           |         |__________________|
           |
           v 1
   _________________                  _____________
  |                 | N            N |             |
  | analizo toolkit | ----parse----> | source-code |
  |_________________|                |_____________|


```

analizo agent é um serviço (socket, http, arquivos?, outro?) que fica escutando
mensagens do analio toolkit contendo os resultados do analizo, sejam calculo de metricas,
grafo, e outros, desde que seja em formato textual, json, yaml, txt, etc.

o analizo toolkit precisa ser alterado (ou talvez o cache do analizo ja sirva?) para
identificar se analizo-agent esta instalado e disponivel, os requisitos iniciais eh que
os 2 estejam instalados localmente juntos, a ideia central eh desacoplar totalmente
do analizo essa funcao de agente com funcionalidas de fazer requisicao HTTP e tratar
muitas questoes necessarias para enviar os resultados do analizo para o metrics database.

metrics database é proposto inicialmente para centralizar metricas calculadas pelos
usuarios do analizo e tambem por instancias em servidor como o analizo-archive que sera
composto de um cojuntos de projetos exemplos que foram casos de usuarios reais onde se
encontrou erros ou mal funcionamento.

analizo archive é composto pelo analizo toolkit, analizo agent, analizo server,
metrics database e um dataset com codigo-fonte de alguns projetos... e algo
para orquestrar a execucao do analizo toolkit no dataset.

sobre orquestrador ver daca-quality

## debian/copyright analizo, 8 ago 2022

Ferramentas para ajudar criacao e atualizadao.

licensecheck --shortname-scheme=debian,spdx

scan-copyrights - usa o licensecheck e propoe conteudo para d/copyright


License: Apache-2.0
 On Debian systems the full text of the Apache-2.0 license can be found in
 /usr/share/common-licenses/Apache-2.0.

## 17 ago 2022

DAM via IRC: o analizo tem um arquivo que viola licenca samples/kdelibs/parser.cpp,
neste caso o upstream precisa ser corrigido ou entao o source no debian precisa
ser reempacotado, o dewbian distribui sources tambem, entao remover o arquivo problematico
apenas no deb final nao funciona.


DAM (via irc): dropping the offending file from the binary (.deb) package is not sufficient.
Debian also distributes the original source which still includes the file.
fixing needs that either upstream removes the file or (preferrably) includes
the original copyright and licensing. last resort is to repackage the original
source for debian (via Files-Excluded: in debian/copyright; see uscan(1) and
ml-origtargz(1))
*mk-origtargz

LER docs acima !!!

## 28 ago 2022, empacotando Crypt::Argon2

Notas iniciais sobre primeiro teste do comando `dpt gen-itp`, a doc diz ser um helper
para abrir bug ITP.

Funciona muito bem quando executado de dentro do repositorio do pacote, le o
control e copyright e gera o template perfeito para abrir o ITP.

Integra bem com o comando reportbug com a flag -r (--resume-saved) FILE

    dpt gen-itp | tee /tmp/mail
    reportbug /tmp/mail

para itp ver dw-itp
