---
title: O que é Live Coding?
---

https://www.youtube.com/watch?v=qcE_a8IXk8o&list=PLMBIpibV-wQLUXxRDiwz5JhoIf2CX_uM6

...

yaxu: o que é live coding?

iris: live coding é uma forma de arte,
é mais sobre comunidade e como construímos
tecnologia e como isto serve a formas de
interação humana num caminho livre de
amarras funcional ou pela indústria.

yaxu: não tem uma definição fixa, algumas
vezes vê live coding sendo definido de forma
muito estrita, pessoas dizem que você não
está fazendo live coding se não mostrar
sua tela, se você usa código escrito ontem
não é live coding, ele não concorda, uma
visão é que toda a vida da pessoa é uma obra
de live coding onde a pessoa altera dia após
dia.

thornton: live coding é uma abordagem de
coordenar um contexto criativo, gosta da
abordagem criativa de construir as próprias
ferramentas para realizar projetos de arte.

simon (ted the trumpet): live coding de fato
é uma comunidade, é um grupo de pessoas.

francesco (nesso): a coisa legal de live
coding é prática das pessoas, cada um em
seu próprio caminho, mas todos fazendo arte.
ele se vê num tipo particupar de música
eletrônica, e vê uma união em torno de
construir ferramentas de maneira colaborativa.

???: feedback loop entre colocar uma idéia
no código e verificar o que irá acontecer,
reagir a isto e continuar neste loop, todas
as pessoas fazendo ferramentas e frameworks
legais. feedback loop define live coding.

iris: live coding como uma forma do que surge
na mente em ideias transcritas para código sem
necessariamente passar por questões motoras
como seria na escrita com caneta e papel
por exemplo.

nesso: em comparação com tocar instrumentos
de corda por exemplo, ao praticar repete-se
a mesma coisa todos os dias, tipo arpeggio,
mas em live coding vc tenta algo novo todos
os dias.

thorn: concorda

yaxu: concorda, mas isso é um pouco
problematico, possui lados negativos tb.
dificil compor e crias cancoes pois faz
coisas novas todos os dias.

simon: a comparacao entre tocar um instrumento
"tradicional" e live coding não é tão simples,
tocar trompete numa improvisação de jazz por
exemplo é algo que se aproxima a prática de
live coding, onde a criatividade acompanha
um contexto de feedback e loop muito parecido
com o live coding.

iris: acha legal estar aqui criando cutura
digital, na internet contulra ao vivo, acha
legal fazer arte com código, parace algo do
futuro, mas na prática o ganho se vê pois
tudo pode ser conectado através daí.

yaxu: tem organizado este evento a algum
tempo, o contexto muda e temos que nos
adaptar e absorver as mudanças,
