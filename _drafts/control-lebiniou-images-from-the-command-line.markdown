---
title: Control Le Biniou images from the command line
---

Some Le Biniou definitions:
- **plugins**: visual effects, sound inputs, webcam input, images, outputs, etc
- **sequences**: the order plugins run, which images/colormaps are used
  - saved at `~/.lebiniou/sequences/`
- **themes**: library of images (default resolution is 960x540)
  - saved at `~/.lebiniou/images/`

## Some notes about key plugins

- video: render videos from `~/.lebiniou/videos`

## Creating themes

1. create new folder at `~/.lebiniou/images/`
2. copy the images into the new folder
3. resize the images to fits on Le Biniou default resolution
```sh
cd ~/.lebiniou/images/<theme>
mogrify -resize 960x540 -background black -gravity center -extent 960x540 *
```

## Start Le Biniou using the new theme

```sh
lebiniou -t ~<theme>
```

Or a comma-separated list of themes, example:

```sh
lebiniou -t ~theme1,~theme2 
```
