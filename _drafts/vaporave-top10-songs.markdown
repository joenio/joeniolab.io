---
title: Top 10 Vaporwave songs
---

### Macintosh Plus, Floral Shoppe, 2. リサフランク420 / 現代のコンピュー

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=571360365/size=small/bgcol=ffffff/linkcol=0687f5/track=2423539855/transparent=true/" seamless></iframe>

### esc 不在, - a - black horse, lay on me

<iframe style="height: 30px;" src="https://archive.org/embed/Esc-A-BlackHorse/04+-+lay+on+me.mp3" width="500" height="30" frameborder="0"></iframe>

### Dan Mason, Void, Now or Never (You and Me)

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=97966519/size=small/bgcol=333333/linkcol=0f91ff/artwork=none/track=2159397093/transparent=true/" seamless></iframe>

### COCAINEJESUS, We're Worried About You, Flesh (ft. HKE)

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=2404928988/size=small/bgcol=333333/linkcol=0f91ff/artwork=none/track=3354788525/transparent=true/" seamless></iframe>
