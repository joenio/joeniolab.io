---
title: Vapor Wave ...
---

![Default post image](/files/default-post-image.png)

anti-capitalista
anti-comsumista

https://www.esquire.com/entertainment/music/a47793/what-happened-to-vaporwave/

o texto acima apresenta uma perspectiva histórica do vapor wave passando pelos
principais artistas com links para ouvir os álbuns que marcam os principais
momentos.

## Hong Kong Express - 2047

+++

Álbum bem diferente do que eu costumo gostar mas HKE faz um som
sempre foda, vozes, loops, calmaria, swing e experimentação.

* https://dreamcatalogue.bandcamp.com/album/2047

## Nmesh - Pharma

+++

NMESH está aí no radar do Vapor Wave mas me parece que não tem tanto como
rotular o som. Um eletrônico meio vaporwave, meio Cabaret Nocturne, quase
Author & Punisher exagerando bastante e retirando bastante do peso insdustrial.
Esse álbum não tem definição, é ouvir para entender.

* https://nmesh.bandcamp.com/album/pharma

nmesh eh da nova onda vapor, quase nao-vapor mais, mas ta na turma e na comunidadeo
https://nmesh.bandcamp.com/album/pharma ++ bom
Mall Full Of Drugs 09:13 ++++

## COCAINEJESUS - Nervous

+++

Calma, batida massa, synths legais, melodia bonita.

* https://cocainejesus.bandcamp.com/album/nervous



## R23X - VELTAHL

+++

Som de game com beats, é isso! Foda! Tem uma pitada de vaporwave tb.

* https://dreamcatalogue.bandcamp.com/album/veltahl

## wosX - London 2041

+++

Pesado, mas com músicas surpreendentes, com melodias interessantes, baixos
distorcidos, ritmo, muito bom!

* https://dreamcatalogue.bandcamp.com/album/london-2041

## wosX - End Of World Rave

(fim do vaporwave, nova cena, "hardvapour")

* https://dreamcatalogue.bandcamp.com/album/end-of-world-rave

fonte: http://marcelsmusicjournal.com/post/136124700693/interview-dream-catalogue-founder-hke

## Halo Acid - Never Let Me Go

+++

Computers, algo que poderia ser feito com live coding penso.

Discogs: "Experimental/techno artist based in London, UK. Founder of Tekres."
* https://www.discogs.com/artist/5147502-Halo-Acid

* https://dreamcatalogue.bandcamp.com/album/never-let-me-go

## Yarky - Blu-Ray блюз

++

(legal, diferente, relacionado ao vaporwave mas n eh)

* https://vityanigabberwave.bandcamp.com/album/blu-ray

## Dan Mason - Void

++

* https://music.businesscasual.biz/album/void
* https://soundcloud.com/danmason420



## VHS LOGOS - Mantra

Vapor Wave BR!

* https://vhslogos.bandcamp.com
* https://soundcloud.com/vhslogos

U-MATIC é uma música foda:

https://www.youtube.com/watch?v=G7AKFF-fSFE

## Saint Pepsi

https://saintpepsi.bandcamp.com/
https://www.youtube.com/watch?v=WYvji5AXOfk&index=112&t=0s&list=LLv48AhVgiiRqDk30EZSbnMg
https://www.youtube.com/watch?v=Ki-fATpXa00&index=113&t=0s&list=LLv48AhVgiiRqDk30EZSbnMg


===================

VAPERROR : Acid Arcadia
https://www.youtube.com/watch?v=aqZdNSshk0c&index=337&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s

Windows96 : One Hundred Mornings
https://www.youtube.com/watch?v=o9zZ4Gj75xs&index=272&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s

wind96 - Plume Valley
https://www.youtube.com/watch?v=MFBOtWq7dsE&index=210&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s

Windows彡96 - Reflections (Full Album)
https://www.youtube.com/watch?v=NzTzGWWKevA&index=207&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s

猫 シ Corp. : Palm Mall Mars
https://www.youtube.com/watch?v=Cc5Yz1TnOVY&index=196&t=0s&list=LLv48AhVgiiRqDk30EZSbnMg

Vektroid - Telnet Complete - full album (2017)
https://www.youtube.com/watch?v=pYliALBwaSM&index=368&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s

Vektroid - Seed & Synthetic Earth - full album (2017)
https://www.youtube.com/watch?v=MUugfhRBiY8&index=376&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s


Broken HKE : DELETE
https://www.youtube.com/watch?v=ehY90jo3svA&index=423&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s


VEKTROID - CALM
https://www.youtube.com/watch?v=bCDmosw7z_w&index=430&list=LLv48AhVgiiRqDk30EZSbnMg&t=0s

Bubble Keiki 
https://www.viberate.com/artist/bubble-keiki
https://hydro.click/t/544109294-postfordism_and_chill_by_bubble_keiki_%E3%83%90%E3%83%96%E3%83%AB%E6%99%AF%E6%B0%97/
(dica do phil, muito bom)
