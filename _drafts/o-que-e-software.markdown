Em 1988 foi publicado no "Journal of Speculative Philosophy" por Peter Suber um
ensaio com o título "What is Software?". Este ensaio é uma tentativa de definir
software dinstinguindo especialmente em relação a dados, ruído e padrões
abstratos de informação.

Começa prevendo que a revolução computacional irá afetar a filosofia
profundamente provendo poderosos conjuntos de modelos e metáforas para pensar
sobre o pensamento.

Software é um conjunto de padrões digitais, textos, array de símbolos. O que torna
"digital" é a individualidade e descontinuidade dos bits separados. Diferentemente
dos padrões analógicos onde a informação que compreende o padrão é contínua e não
individualizada.

Um padrão analógico pode ser representado por um padrão digital com algum nível
de precisão. Chama isso de Digital Principle. Baseado neste princípio o software
pode ser representado na forma de padrões analógicos ou digitais, não se limitando
a este último.

Primeira tentativa de definição: software é padrão por sí só.

Além de uma infinidade de dificuldades filosóficas esta definição torna essencialmente
difícil distinguir entre dados e software, por exemplo. Também não ajuda a diferenciar
software e ruído.

Segunda tentativa de definição: software é um padrão legível e executável por uma máquina.

Esta definição torna difícil considerar um software na prateleira que não está
sendo executado até ser instalado em alguma máquina como software. Ou se não for
possível executar por conter muitos bugs? E se for um software que precisa de recursos
de hardware ainda não disponíveis na nossa época?

Há de haver o cuidado em classificar software ruim como não-software apenas por
não executar adequadamente e quebrar. Para isto será necessário incluir um novo elemento
relacionado a intenção ou propósito do programador.

Software irá executar a vontade do programador.

Isso sugere que, para entender o software, devemos entender intenções,
propósitos, metas ou vontade, o que aumenta o problema muito mais do que
originalmente antecipamos.

Software tem a qualidade de ser "legível" para máquinas.


Embora eu não faça nenhum compromisso aqui, eu suspeito que o problema do
software é exatamente o que é necessário para superar a oposição excessiva do
materialismo e do idealismo.

Liftability

Hardware is programmable, software is portable.

A capacidade de portar o software, a partir da tradução, informação magnética
para cartões perfurados, ou ...  more commonly done, the pattern embodied on
the magnetic substratum can be lifted and transposed to the medium of the
random access memory (RAM) of the computer, where it is available for use by
the processor. This is done whenever a program is run on a contemporary
computer.

It is this property of software that gives workers in the field of artificial intelligence (AI) their
hope of success. If the mind is reducible to the brain, and the brain is a complicated pattern of
neural switches, then the mind is a digital pattern that can be lifted and reinstalled in a silicon
substratum.

Liftability may enter our definition of software, but we should note that it is derivative, not
primary. It is a consequence of defining software as pattern first and embodied pattern second.
But despite its derivative status, liftability serves a valuable function in our definition. It
illuminates the exact sense in which software is independent of hardware and the physical media
that record the software pattern. Software is not defined as a pattern of any particular material; it
is defined as pattern that may be represented in many different materials. It may even be
represented in many different clods of the same material, as when one program is written in
magnetic oxide thousands of times.

The Softness of Software

A palavra "software" é de muitas maneiras uma cunhagem brilhante.


Parece-me que a suavidade do software tem duas fontes que raramente são
mantidas distintas: sua alterabilidade e sua idealidade.

Software is typically loaded into a machine through disks, tape, ROM (read-only memory)
modules, or cards. It is alterable in the sense that we can pull out one disk and put in another.
But that much is also true of light bulbs, rubber feet, and disk drives. To substitute one program
for another goes to the programmability of the hardware; to amend a given program goes to its
alterability in the present sense. Individual programs may be altered about as easily as text,
which is usually much easier than altering circuits or power supplies. Alterability also varies
with the material medium of the pattern; magnetic norths and souths are easier to rewrite than
holes in punched cards, which is the chief reason why magnetic media have come into
prominence. Clearly the softness of software is a matter of degree. Where the machine will not
accept different inputs, or will not allow users to set things up their own way, we say it is "hard
wired".



STEM -> STEAM (movimento que junta tudo!)
http://stemtosteam.org/
STEM = ciencia, tecnologia, engenharia e matemática
STEAM = STEM + Artes e Design

## 31 out 2022, notas de leitura

Notas sobre o artigo _Identifiers for Digital Objects: the Case of Software Source Code Preservation_.

Codigo-fonte de software é uma forma única de conhecimento.

O projeto SWH criou um arquivo universal de codigo-fonte de software.

A maioria das propostas de id digital existentes, como DOI, Ark, Handle, PURL, resolvem questoes
de (gerar novo label, associar, recuperar, etc) com resolver. Ou seja, ha um ponto de autoridade
que centraliza a responsabilidade, alem disso nenhum sistema eh verificavel, nao ha como
verificar que um certo label de fato esta associado com um objeto especifico. Mesmo com propostas
sendo discutidas ja ha algum tempo.

Os autores estudaram extensivamente os atuais digital identifier systems e foram forcados a abandonar
um apos outro a medida que avaliava os requisitos chaves para long-term universal source code archive,
e os atuais nao oferecem os requisitos necessarios. Dai a justificativa e a necessidade de criacao do SHW-ID.


Cosmo, R. D., Gruenpeter, M., & Zacchiroli, S. (2018). Identifiers for Digital Objects: The Case of Software Source Code Preservation. 1. https://doi.org/10.17605/OSF.IO/KDE56

ver ~/art/4two.art/swh-snd/
