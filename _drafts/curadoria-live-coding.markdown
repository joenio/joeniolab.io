---
title: Curadoria, coletânia, melhores músicas live-coding
---

Passos:
1. Listar artistas conhecidos
2. Ouvir e todas as músicas de cada artista
3. Selecionar melhores músicas
4. Documentar melhores músicas incluindo: nome, ano, álbum, fontes para escutar

Critérios de seleção (o que significa melhores?):
1. Músicas que agradam aos meus ouvidos no momento da escuta
2. Corte de escolha entre músicas próximas do POP
3. Um dos objetivos é divulgar live-coding, selecionar aquelas que tenham maior chance de agradar o público de música eletrônica

Além de postar no blog, num segundo momento criar coletânia em algum selo (Phil?):
1. Enviar email para os autores solicitando permissão para usar a música no álbum
2. Conversar com Phil sobre a idéia

## T.Y.P.E

+++

Live Coding.

https://twitter.com/typeensemble
https://soundcloud.com/typeensemble

## Benoît and the Mandelbrots

http://www.the-mandelbrots.de/

## Alexandra Cardenas

...

## Renick Bell

## YAXU

## kindohm

## josecaos

coletania varios artistas
https://noodsradio.com/shows/graham-dunning-live-coding-special-28th-april-22
