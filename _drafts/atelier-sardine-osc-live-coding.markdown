Instructions preparing the environment:
- https://md.interhacker.space/zJldMpS5QiKlaIxtHjPnGQ?view

Seguindo os passos, copia abaixo dos comandos utilizados:

Primeiro verificar versao python, minimo versao 3.10.

python3 --version

Depois instalar sardine via pip.

python3 -m venv .venv
source .venv/bin/activate
pip install .
