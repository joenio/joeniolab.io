---
title: Monte uma estação metereológica em casa
---

![logo monitora cerrado](/files/monitora-cerrado-logo.jpg)

Neste post irei mostrar como montar uma estação metereológica do projeto
[Monitora Cerrado][monitora-cerrado]. Um projeto desenvolvido por membros do
[Calango Hacker Clube][calango-clube] e comunidade local Arduino de Brasília
e região.

Primeiro passo é instalar no Arduino IDE o suporte a placas [ESP8266][esp8266],
o tutorial utilizado como guia está disponível no site
[FilipeFlop][filipe-flop], link do tutorial abaixo:

* [Como programar o NodeMCU com IDE Arduino 67][filipe-flop-tutorial]

Hardware utilizado nessa estação:

* ESP8266 12E
* BME280 sparkfun
* DHT22
* Sensor chuva
* Resistor 10K

Versões do Arduino IDE e bibliotecas utilizadas:

* Arduino IDE 1.8.7
* Adafruit Unified Sensor (essa precisa instalar)
* BME280 Adafruit 1.0.8 (essa é necessária)


TODO:
 Documentar todas as bibliotecas e documentar instalacao passo-a-passo

SimpleDHT 1.0.6
DHT sensor library Adafruit 1.0.2 || 1.2.0

mapa de portas entre esp8266 e arduino
https://raw.githubusercontent.com/nodemcu/nodemcu-devkit/master/Documents/NODEMCU-DEVKIT-INSTRUCTION-EN.png
https://cdn.instructables.com/FR2/PEB6/IN35RYG6/FR2PEB6IN35RYG6.LARGE.jpg

BME 280 sparkfun: (essa nao funcionou)
https://cdn.sparkfun.com/assets/parts/1/1/1/2/6/13676-01.jpg



BME280 sparkfun:
https://learn.sparkfun.com/tutorials/sparkfun-bme280-breakout-hookup-guide/all

1. ligar SDO no ground
2. trocar 0x77 para 0x76 no Adafruit__.....h
3. SDA liga no D4 e SCL liga no D3


http://newtoncbraga.com.br/index.php/microcontrolador/143-tecnologia/14349-como-utilizar-o-sensor-de-temperatura-e-umidade-relativa-dht22-com-o-nodemcu-mic162

https://www.losant.com/blog/getting-started-with-the-esp8266-and-dht22-sensor

https://github.com/monitoracerrado/apresentacao_pycerrado2018/blob/ad6d39d3fc7d4e287f075a0cc79f62e37c138365/ESP8266_DH22.ipynb


https://www.filipeflop.com/blog/programar-nodemcu-com-ide-arduino/

https://www.filipeflop.com/blog/qual-modulo-esp8266-comprar/

https://calango.club/projetos/monitora_cerrado

[esp8266]: https://pt.wikipedia.org/wiki/ESP8266
[monitora-cerrado]: http://monitoracerrado.org
[calango-clube]: https://calango.club
[filipe-flop]: https://www.filipeflop.com
[filipe-flop-tutorial]: https://www.filipeflop.com/blog/programar-nodemcu-com-ide-arduino
