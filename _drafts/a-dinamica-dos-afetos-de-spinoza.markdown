---
title: A dinâmica dos afetos de Espinoza
---

Os vídeos de Espinosa sobre Afectos que eu assisti foram (deixar apenas links sobre afectos / ou todos?):

Amauri Ferreira: Spinoza I - Mente e corpo, em 17/01/13
https://www.youtube.com/watch?v=7sNKeAE6Vg0

Amauri Ferreira: Spinoza II - Desejo, alegria e tristeza, em 25/05/13
https://www.youtube.com/watch?v=ywH_GgmHXwo

Amauri Ferreira: Spinoza III - Afetos-paixões e afetos-ações, em 10/02/14
https://www.youtube.com/watch?v=dmBx8p6_nvs

Amauri Ferreira: Spinoza - Definições dos afetos I
https://www.youtube.com/watch?v=I8N7J5eyzk4

Amauri Ferreira: Spinoza - Definições dos afetos II
https://www.youtube.com/watch?v=dUbgx1_k20k

Amauri Ferreira: Spinoza - Deus, ou seja, a Natureza
https://www.youtube.com/watch?v=0Ic54MhpFB8
