---
title: Musicas e artistas que ando escutando ...
---

![Default post image](/files/default-post-image.png)

Resumo de álbuns e artistas que venho escutando nos últimos 2 anos, sempre tive
o hábito de buscar novidades na música, e nos anos de 2015 e 2016 especialmente
me bateu curiosidade de descobrir novos ritmos, novos sons. Ao dizer novos, me
refiro a coisas novas para mim, sons, ritmos, artistas, albuns que mesmo sendo
antigos eu ainda não conhecia.

| Avaliação  | Descrição                                                                          |
|------------|------------------------------------------------------------------------------------|
| `--`       | Não gostei e não quero nunca mais ouvir este álbum                                 |
| `+-`       | Não gostei tanto, mas merece uma segunda chance algum dia, pode virar ++ no futuro |
| `++`       | Gostei e vou adicionar na minha lista para ouvir repetidas vezes                   |
| `+++`      | Gostei muito e vou ouvir todos os dias até "enjoar", vai ser difícil               |

## Jazz Jamaica

Jazz com Reggae

[Mais sobre Jazz Jamaica na Wikipédia (em inglês).](https://en.wikipedia.org/wiki/Jazz_Jamaica)

## Dawn Penn

"no no no" é lindo, a voz dela é coisa de deus!

https://en.wikipedia.org/wiki/Dawn_Penn

---

## Toe

math rock

https://en.wikipedia.org/wiki/Toe_%28band%29


## Ken Boothe

https://en.wikipedia.org/wiki/Ken_Boothe

## Curtis Mayfield

## Monsieur Periné

## Karina Buhr

## Augustus Pablo

## Linton Kwesi Johnson

## Bota Pagodão

O coletivo B_T_PGDAO, varios artistas, produtores, DJs, etc... dentre eles:
Som Peba, ATTOOXXA, ...
https://www.sympla.com.br/b-t-pgdao-4__71042

[Pro coletivo B_T_PGDÃO, o pagodão é o punk baiano](https://noisey.vice.com/pt_br/article/btpgdao-coletivo-entrevista)

[OZ e ÀTTØØXXÁ viram mais uma página do pagodão baiano no novo disco BLVCKBVNG](https://noisey.vice.com/pt_br/article/oz-attooxxa-blvckbvng-disco)

https://www.suamusica.com.br/AMPOficial/blvckbvng-oz-e-attxxa

Bass Music na Bahia, com nomes de destaque como Lord Breu, Loro Voodoo e Mauro Telefunksoul

Rafa Dias, músico por trás do ÀTTØØXXÁ

[Rafa Dias (ÀTTØØXXÁ): “A música eletrônica hoje é o que liga o mundo”](http://revistagambiarra.com.br/site/rafael-dias-attooxxa-a-musica-eletronica-hoje-e-o-que-liga-o-mundo/)

A.MA.SSA, seu primeiro EP, Derruba os Muros da Cidade!,
https://soundcloud.com/amassa

Pedro Marighella, Também atende pela alcunha de Som Peba
https://soundcloud.com/0m4 ^ pedro
https://soundcloud.com/sompeba

https://soundcloud.com/braunation

[Pagode numa noite de verão](http://atarde.uol.com.br/muito/noticias/1840343-pagode-numa-noite-de-verao)
visão geral bem clara do movimento que deu origem ao coletivo B_T_PGDÃO

---

## RAP

novos:

rashid
tássia reis
ricon sapiência
bitrinho
lurdez da luz
lívia cruz
haikaiss
síntese
rodrigo ogi

primórdio:

pepeu

das antigas:

thaide + dj hum
max b.o.
dmn
gabriel o pensador
rappin' hood
sabotage
faccao central
rzo
mv bill
dexter 509-e
da guedes
face da morte
quinto andar

outros novos:

kamau
renan inquerito
xis
rico dalasam
rael
rapadura xique chico
nave savave
m.sário
projota
amiri


civil civic - dica do luan
Sleep Party People

https://www.tattoodo.com/a/2016/11/the-conceptual-tattoos-of-taiom/

vox sambou (haiti)

mateus estrela / maraũ
https://soundcloud.com/marauoficial/albums


Pratagy ++
https://pratagy.bandcamp.com/album/pictures
https://www.youtube.com/watch?v=YhxyCZUE2Ck (album legal, Pictures)
Bio: https://reportere.com/2017/02/01/biopratagy/

Pratagy tocou com Cheese, este regravou uma música do Pratagy
https://www.youtube.com/watch?v=1kqo9g4SbxE

Zebrabeat Orquestra

PNK SABBTH (eletronico do pará!)
https://www.youtube.com/watch?v=bjU6hG5bF8o
https://reportere.com/2017/04/03/pnksabbath/

Danilo  me passou um som chamado: Dajangologie (1940)

Engelwood - Pastel Beach (Full Album)
https://www.youtube.com/watch?v=gdPpp6X6lGk&t=124s

Bane's World - Drowsy (Full Album)
https://www.youtube.com/watch?v=ZuN7vmymy9Q

SOFT PORN - Tsunami Boy (Vol. I)
https://www.youtube.com/watch?v=T7GUDxi6W-k

"My Favorite Way" by Black Drawing Chalks (OFFICIAL) (dica do hilmer)
https://www.youtube.com/watch?v=aqBveUYEEwQ

---

nao recomendado a sociedade

coletania pessoal, as melhores musicas de cada algum de todos os tempos, de tudo que ouço, de todos os estilos:

tempos dificeis, racionais, holocausto urbano
homem na estrada, racionais, raio x do brasil
capitulo 4 versiculo 3, racionais, sobrevivendo no inferno
magico de oz, racionais, sobrevivendo no inferno
festa no gueto, rincon sapiencia, sp gueto br
the score, gasoline, snap your neck back
6000 dias, boogarins, manual
collector's inside collection, jupiter maca, plastic soda
m bala, civilizacion y barbarie, ramiro musotto
ah uh (onomatopeias), lurdez da luz, lurdez da luz (ep)
beatle george, jupiter maca, uma tarde na fruteira

---

notas aleatorias:

A musica Mac DeMaco parece a melodia da musica de Tim Maia ???


procurando por dream pop encontrei a banda "VARSITY - So Sad, So Sad" (gostei! +-)

tame impala eh dream pop tb? (boa banda, gostei mto!!!)

monophonics: rock sould moderno psicodelico bem bacana, suinado massa, batida bala
https://www.youtube.com/watch?v=xyWHRC8A_uU&feature=youtu.be
https://www.everipedia.com/monophonics/

Heartless Bastards - banda com rock melodico, vocais bonitinhos, baladinhas ...
++ bem legal https://www.youtube.com/watch?v=gmX-ceF-N1k

Future Islands +++
https://www.youtube.com/watch?v=4S9YtRnlPMk (vocal bonito, romantico, melodico...)
https://en.wikipedia.org/wiki/Future_Islands
https://www.youtube.com/watch?v=g0LJNEQz-E4 +++

Le Couleur ++
https://www.youtube.com/watch?v=uABsQge1VTQ

https://en.wikipedia.org/wiki/Bowery_Electric (interessante)
https://www.youtube.com/watch?v=KAo4svd7IR4

https://en.wikipedia.org/wiki/Onra (rap frances, experimental, interessante)
https://www.youtube.com/watch?v=Jz0mywgQsds

De La Soul Feat. Chaka Khan - All Good +++
https://www.youtube.com/watch?v=PA_NP0k-G3o

https://en.wikipedia.org/wiki/Erykah_Badu (bom)
https://www.youtube.com/watch?v=9hVp47f5YZg

Madvillain - Madvillainy Full Album
https://www.youtube.com/watch?v=DWY22Blwouc

Cymande ++ (soul, swingue, bom...)
https://www.youtube.com/watch?v=9OURRNQ6t1s

La Femme ++
https://www.youtube.com/watch?v=fNQ_3TCnAyw&t=626s

Breakbot +++ (dica da mariel)

Head Hunters | Herbie Hancock | 1973 | Full Album +++ (pedrada! funk!)
https://www.youtube.com/watch?v=3m3qOD-hhrQ

Föllakzoid [LP] (2009) - Föllakzoid
https://www.youtube.com/watch?v=OgRi5aTeZ2k

Bane's World - Drowsy (Full Album)
https://www.youtube.com/watch?v=ZuN7vmymy9Q&list=RDZuN7vmymy9Q#t=0

Cluster - Curious (caralho que disco loucaço, decada de 70, experimentacao eletronica pura!)
https://www.youtube.com/watch?v=q1uu3lYQVnw

influenced by Cluster include David Bowie, Robert Rich, John Foxx (formerly of Ultravox), Alex Paterson of The Orb,[10] Coil,[11] Oval, To Rococo Rot, and Mouse on Mars.[12]

Moebius & Plank - Rastakraut Pasta (FULL EP) +++
https://www.youtube.com/watch?v=3cCNuSU5ncs

Pnk SBBHT
https://www.youtube.com/watch?v=bjU6hG5bF8o

Homeshake (ex guitarra de marc demarco, faz um som com guitarras bonitas, eletronico leve, bonitinho...) +++
https://en.wikipedia.org/wiki/Homeshake

Yellow Days (bom!) +++
https://www.youtube.com/watch?v=AejUtBNzyOI
https://soundcloud.com/yellow-days

Kendrick Lamar (RAP bom mesmo!), o cara é uma metraladora
https://pt.wikipedia.org/wiki/Kendrick_Lamar

Lauryn Hill (soul, voz linda, atitude, mulher foda p caralho!
https://www.youtube.com/watch?v=FJ5BXfXUYwM

The Black Keys - Gold On The Ceiling (rock leve, com pegada, guitarras, massinha...)
https://pt.wikipedia.org/wiki/The_Black_Keys

Beach Fossils

The XX

Que foda esse King Krule, rock, reto, massa, leve, foda!!! Criou estilo Bluewave
https://en.wikipedia.org/wiki/King_Krule

Gotan Project (rap bom ) +tango +eletronico
https://www.youtube.com/watch?v=as59Id4eHhc

Don Carlos (dica do hilmer, reggae classico das "antiga")

Twin Sister

Tropic of Cancer (somzao psicodelico, echoesssss lindooo viajado...)

delinquent habits (rap bala top)
https://www.youtube.com/watch?v=w3qqN1BMnhk
https://en.wikipedia.org/wiki/Delinquent_Habits

Calle 13 (outro rap massa)

Animal Collective

Brockhampton (rap pesadão!) - mentira, me enganou, só tem as 3 primeiras músicas!
https://en.wikipedia.org/wiki/Brockhampton_%28collective%29
https://www.youtube.com/watch?v=vTDN_5f4sXU&t=125s

Soft Kill (indie bom)
https://www.youtube.com/watch?v=_J1j4Z37NuI

RATATAT
https://pt.wikipedia.org/wiki/Ratatat
https://www.youtube.com/watch?v=xlcywgEMuGI

Martian Subculture - Chewing Gum
https://www.youtube.com/watch?v=6DOfVewea84

the ha dance - dance music
https://www.youtube.com/watch?v=_URFoqkwWLY

Seduction Seduction (Vocal club mix)
https://www.youtube.com/watch?v=V_t9Xpj-xWw

https://pt.wikipedia.org/wiki/Masters_At_Work
https://www.youtube.com/watch?v=efLcPbJd7QY

Wiley - Eskimo EP
Wiley - Eskimo (Instrumental) [1/2]
https://www.youtube.com/watch?v=LkdEOY0bf4U

https://en.wikipedia.org/wiki/Grime_(music_genre)

tem uma pegada massa esse grime!!! btf


No Man by dj Technics
https://www.youtube.com/watch?v=PuAfXxHrUvk
dj technics - cuum on [1998]
https://www.youtube.com/watch?v=qHBdx1fA_Xs

https://en.wikipedia.org/wiki/Sany_Pitbull
funk carioca barril dobrado!
https://www.youtube.com/watch?v=bRi9_TM7Xg4

DJ Sandrinho - Sampler Louco
versao recente menor barrilzinho
https://www.youtube.com/watch?v=3jiMnLxzRKk

Pulse X
https://www.youtube.com/watch?v=4bMQTU2iI1E

Jah Shaka (dub raiz, crassico)
https://www.youtube.com/watch?v=6guQO9Jenos&feature=youtu.be&t=5m58s
https://www.youtube.com/watch?v=zWbS7VWV2SM

The Aggrovators - None Shall Escape Dub
https://www.youtube.com/watch?v=TZ4jV5bMbqs

Second Phase - Mentasm - R&S Records Classic
https://www.youtube.com/watch?v=u8VY5dxq5CU

Second Phase - Mind to Mind (1991) - bom pra viajar, bom pra estudar
https://www.youtube.com/watch?v=Pgvckp2MDT0

batida linda, isso é funk!
The beat club - security
https://www.youtube.com/watch?v=4FGrv_Rq-90

Cybotron - Clear
https://www.youtube.com/watch?v=fGqiBFqWCTU

Cybotron Cosmic Raindance
https://www.youtube.com/watch?v=qiSkGjqNv1M

Charanjit Singh - Ten Ragas to a Disco Beat (1982)
https://en.wikipedia.org/wiki/Charanjit_Singh_(musician)#Synthesizing:_Ten_Ragas_to_a_Disco_Beat
https://www.youtube.com/watch?v=BN8M2irJVJA
que pedra esse disco! inventou o acid house music 10 anos antes!

outra pedra!
https://en.wikipedia.org/wiki/Yellow_Magic_Orchestra
https://www.youtube.com/watch?v=hIw3kT2Q9Zk

QUE PEDRA!!!
https://www.youtube.com/watch?v=E0KT6hP5HbM&t=944s
https://analogafrica.bandcamp.com/album/space-echo-the-mystery-behind-the-cosmic-sound-of-cabo-verde-finally-revealed
http://www.abc.net.au/radionational/programs/dailyplanet/2016-09-22/7863222
https://www.theguardian.com/music/2016/may/22/space-echo-the-cosmic-sound-of-cabo-verde-review
http://m.redeangola.info/o-som-cosmico-de-cabo-verde-editado-pela-analog-africa/
http://www.cmjornal.pt/cm-ao-minuto/detalhe/editora-analog-africa-lanca-compilacao-em-torno-do-som-cosmico-de-cabo-verde
https://pt-br.facebook.com/OCafezinho/posts/1153659304657201

Celeste/Mariposa Afro-Baile
https://www.youtube.com/watch?v=2-PTrWXHgq0

Bruno Pernadas - Guitarras
que pedra!!! que swingue, guitas, batida, voz, affff
https://www.youtube.com/watch?v=mKr3xQS4kqk

Einstürzende Neubauten Sabrina +down ok+
(a musica mais foda e diferente/nova que ouvi nos ultimos meses - 26/ago/2017)
https://www.youtube.com/watch?v=CnnGYaqjW-A

808 State - In Yer Face
(pedrada clássica)
https://www.youtube.com/watch?v=ykIA2ddd8Bo

Killer Bong - The Cold in Moscow
hiphop experimental (very) do japao +down ok+
https://www.youtube.com/watch?v=mXgFvE7LhIw
https://www.youtube.com/watch?v=--8x6yerHgs

Belief Defect - Deliverance [R-M178]
)parada louca( +down ok+
https://www.youtube.com/watch?v=oBQN9_sOMfc

Delia Derbyshire
(pirmórdios da musica eletronica)
https://pt.wikipedia.org/wiki/Delia_Derbyshire
https://www.youtube.com/watch?v=jpdiMcEeTJA
https://www.youtube.com/watch?v=XVsqxNy8kkg

Entrevista com Lucas estrela em seu novo album citas algumas bandas preferidas:

Tortoise, sem dúvida, é uma das maiores influências. Foi a banda que fez eu me
apaixonar pela música instrumental. Escuto muito até hoje. Mas também poderia
citar Hamilton de Holanda, Radiohead, Ratatat, Hurtmold, Avishai Cohen

<<<<<<< Updated upstream
<<<<<<< Updated upstream
<<<<<<< Updated upstream


Suicide - Ghost Rider (1977)
https://www.youtube.com/watch?v=Dn7SBQ6X5HU
https://pt.wikipedia.org/wiki/Suicide
(punk rock, eletronico, indie, ecos na voz)
https://www.youtube.com/watch?v=qfIy0oKrbnw&list=PLLYETyiif6A_BlFL4SNIzw2QnEP8HF4Sl&index=3 (essa música é massa!)

o punk rock nasceu no Peru!!!!
Los Saicos - Wild Teen-Punk From Peru 1965 (Full Album) (remastered 2006)
https://www.youtube.com/watch?v=x8MVWaNxD_s
https://pt.wikipedia.org/wiki/Los_Saicos


Throbbing Gristle-20 Jazz Funk Greats (1979) HD
https://www.youtube.com/watch?v=8Fo-G5f3Q3o
diferente, suave, humor, ...


Nitzer Ebb - Join In the Chant
https://www.youtube.com/watch?v=63rYGgQ9UCA
sintetizador!!!!! massa!

Front 242 é barril!
https://www.youtube.com/watch?v=n4oFcaVY8iQ
=======
=======
>>>>>>> Stashed changes
=======
>>>>>>> Stashed changes
https://www.youtube.com/watch?v=XabBEdrL1uE
que RAP pedrada!!!
CVE
Chillin Villain Empire (NgaFish & Riddlore)
Referencia sobre o "pico" GoodLifes

Ellay Khule (outro rap L.A do GoodLifes)
https://www.youtube.com/watch?v=FTaFl-P_2kY

https://en.wikipedia.org/wiki/Project_Blowed


Cut Chemist bom++++++
https://www.youtube.com/watch?v=gHhlaspXVgo

Pigeon John - Growing Old
https://www.youtube.com/watch?v=sad0ND_O2dM
rapper falando de deus
<<<<<<< Updated upstream
<<<<<<< Updated upstream
>>>>>>> Stashed changes
=======
>>>>>>> Stashed changes
=======
>>>>>>> Stashed changes

## T.Y.P.E

+++

Live Coding.

https://twitter.com/typeensemble
https://soundcloud.com/typeensemble

https://en.wikipedia.org/wiki/Bruce_Haack
https://www.youtube.com/watch?v=O9KW8hWDs-8 Haackula (ultima musica)
pioneiro da musica eletronica desde anos 60, o album acima tem umas texturas que lembra kraftwerk
Bruce Haack - Electric Lucifer: Book II (1979) [Full Album] +++
Bruce Haack - Electric to Me Turn +++
The Electric Lucifer  https://en.wikipedia.org/wiki/The_Electric_Lucifer  +++ album doido, eletronico + rock progressivo
