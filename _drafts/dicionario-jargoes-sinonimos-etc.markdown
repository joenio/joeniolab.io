---
title: Dicionario ...
---

É bastante comum nomear coisas quando se cria um novo projeto de software,
seja um simples script, um makefile, decidir os nomes dos targets, seja
escolher nomes de funcoes, variaveis, arquivos, pastas, etc.

Uma ferramenta muito util sao dicionarios de jargoes, sinonimos, antonimos,
etc, onde pode-se consultar palavras por similaridade etc.

dictd (server) e dict (client) sao uma otima dupla e possui dicionarios:
- dict-gcide ingles
- dict-jargon lexico de jargoes
- dict-wn ingles

o gnome tem um cliente no pacote gnome-dictionary que traz uma intertface si,mples
para consultar sinonimos e antonimos, etc. Outro cliente grafico eh o artha...

```sh
apt install dictd dict dict-gcide dict-jargon dict-wn gnome-dictionary artha
```

Artha interface grafica
