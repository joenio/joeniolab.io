
sudo apt-get install build-essential cabal-install git jackd2

https://tidalcycles.org/Linux_installation

```sh
cabal update
cabal install tidal --lib
```

https://github.com/tidalcycles/vim-tidal

para instalar os binarios tidal e tidalvim no home ao inves de precisar executar como
root para instalar no padrao /usr/local/bin basta passar a variavel de ambiente prefix
da seguinte forma:

```sh
make install prefix=~
```

Isto vai istalar os binarios em $HOME/bin/


Testar se os pacotes tidal do haskell estao ok:

```sh
./bin/tidal
GHCi, version 8.8.4: https://www.haskell.org/ghc/  :? for help
Loaded package environment from /home/joenio/.ghc/x86_64-linux-8.8.4/environments/default

<no location info>: error:
    Ambiguous module name ‘Sound.Tidal.Context’:
      it was found in multiple packages: tidal-1.7 tidal-1.7
```

Se o erro acima ocorrer remova tudo do cabal e instale novamente

```sh
rm -rf ~/.cabal

cabal update
cabal install tidal --lib
```


Vamos testar novamente, a saga!

```sh
./bin/tidal
GHCi, version 8.8.4: https://www.haskell.org/ghc/  :? for help
Loaded package environment from /home/joenio/.ghc/x86_64-linux-8.8.4/environments/default

<no location info>: error:
    Could not load module ‘Sound.OSC.FD’
    It is a member of the hidden package ‘hosc-0.18.1’.
    You can run ‘:set -package hosc’ to expose it.
    (Note: this unloads all the modules in the current scope.)
[TidalCycles version 1.7.4]
Installed in /home/joenio/.cabal/store/ghc-8.8.4/tidal-1.7.4-b191e933ddf23fe19ad57df4b0e91310179a72a4d8450763eda0f32225dce658/share
Listening for external controls on 127.0.0.1:6010
Connected to SuperDirt.

<interactive>:39:24: error:
    Not in scope: ‘O.time’
    No module named ‘O’ is imported.
Loaded GHCi configuration from /home/joenio/.local/share/nvim/plugged/vim-tidal/bin/../Tidal.ghci
```

Esse erro eh causado por algo no arquivo de boot do tidal distribuido junto com o
plugin vim vim-tidal .local/share/nvim/plugged/vim-tidal/Tidal.ghci pois ao iniciar
sem esse arquivo (comentando todo ele) o erro desaparece, no entando o tidal n funciona pois
eh necessario haver algo no boot.

Interessante que no mexmo ambiente com os mesmo tidal instalado via cabal da tudo certo com o
tidal no atom, o atom possui um arquivo de boot diferente, vamos testar o arquivo de boot to Atom no
vim-tidal para validar o que ocorre;

```sh
cp .atom/packages/tidalcycles/lib/BootTidal.hs .local/share/nvim/plugged/vim-tidal/
cd .local/share/nvim/plugged/vim-tidal/
mv Tidal.ghci Tidal.ghci.disabled
mv BootTidal.hs Tidal.ghci
```

Issue relatada no vim-tidal https://github.com/tidalcycles/vim-tidal/issues/62

Depois disso basta configurar o nvim com o arquivo .config/nvim/init.vim

```vim
" Specify a directory for plugins
call plug#begin(stdpath('data') . '/plugged')

" Make sure you use single quotes
Plug 'tidalcycles/vim-tidal'
Plug 'vim-airline/vim-airline'
Plug 'itchyny/screensaver.vim'

" Initialize plugin system
call plug#end()

filetype plugin on
let g:tidal_target = "terminal"

" numero de caracteres de avanco do TAB
set tabstop=2

" troca TABs por espacos
set expandtab
```

Abrir um arauivo com extensao .tidal escrever um codigo tidal valido e pressionar CTRL + e.

O vim-tidal funciona melhor com nvim pois no vim eh necessario o tmux, incluindo mais uma dependencia.

Nao eh necessario rodar make install para usar com nvim, uma vez que o comando tidalvim so eh necessario para rodar o tmux com os parametros corredos para dar boot no tidal.
Na real, parcialmente necessario, pois ao executar o plugin a partir do vim ele chama outro binario instalado via make install, o binaril tidal, critica minha aqui, se o binario
faz parte do plugin pq o proprio plugin n sabe o PATH onde esta o binario e tenta char no PATH? Isso n faz sentido, irei tentar melhorar isso no codigo do plugin.

O neovim possui um sistema de plugin chamado vim-plug, simples e eficiente, existem outros mas por alguma razao estou usando este. Ele suporta instalar plugins direto de
repositorios github, gitlab, etc, e tambem do filesystem local o que permite eu rodar um foek do vim-tidal com minhas contribuicoes.

```vim
" Unmanaged plugin (manually installed and updated)
Plug '~/my-prototype-plugin'
Plug '~/src/vim-tidal'
```

https://github.com/junegunn/vim-plug

Para instalar plugins -> Reload .vimrc and :PlugInstall
Eh preciso reloadar ~/.local/nvim/init.vim e executar :PlugInstall

o jackd2 esta quebrado no Debian testing em 21 Nov 2021, sempre o Jack, ow sistema problematico!

Encontrei o erro num bug ja relatado aqui https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1000293

contribui respondendo ao bug enviando as informacoes do meu sistema na thread do bug com o comando abaixo, ele cria o corpo do email e abre o Thunderbird (configurado como cliente padrao no meu Gnome).

reportbug --mua=xdg-email

O link no reddit indica que compilando o jackd2 do repositorio da branch principal resolve, testei e realmente funcionou, os passos sao:

clone https://github.com/jackaudio/jack2

entrar em jack2
executar ./waf configure e se rodar com sucesso ./waf para compilar, os binarios serao gerados no diretorio build, basta entrar e rodar o jackd2 de la

Para rodar com minha placa de som behringer executar: ./jackd -d alsa -d hw:U192k
O jack rodou na linha de comando lindamente, depois ao executar o SuperDirt.start no SuperCollider IDE tudo iniciou com sucesso, o tidal via linha de comando com
o binario tidal tb, mas o plugin vim-tidal nao consegue iniciar o tidal pois esse binario nao esta no meu PATH, problema que pretendo resolver pois nao faz
sentido o plugin nao saber encontrer seu proprio binario!

Mas antes para efeitos de validar se o erro no plugin eh mesmo este coloquei o binario tidal em meu PATH: ln -s $PWD/bin/tidal ~/bin/

De fato resolve apos copiar o binario para o meu PATH, agora vamos tentar fazer esse plugin ser mais esperto...

Pronto abri um merge request, agora eu nao preciso mais ter o tidalvim ou tidal no meu PATH https://github.com/tidalcycles/vim-tidal/pull/68

outro step em melhorias no vim-tidal

tenho tidal instalavo dia cabal e o BootTidal.hs eh exatamente o mesmo fonte que o Tidal.ghci do vim-tidal e esta no seguite path:
.cabal/store/ghc-8.8.4/tidal-1.7.4-b191e933ddf23fe19ad57df4b0e91310179a72a4d8450763eda0f32225dce658/share/BootTidal.hs
a documentacao do hackage https://hackage.haskell.org/package/tidal-1.7.8/docs/Paths_tidal.html diz q eh possivel pegar o path do tidal programaticamente.
para testar basta executar os passos:
ghci
Prelude > import Paths_tidal
Prelude Paths_tidal> getDataDir
"/home/joenio/.cabal/store/ghc-8.8.4/tidal-1.7.4-b191e933ddf23fe19ad57df4b0e91310179a72a4d8450763eda0f32225dce658/share"

eh possivel rodar via linha de comando:
ghci -e Paths_tidal.getDataDir
mas imprime com quotes e nao achei como remover no haskel entao irei remover via bash mesmo com sed
ghci -e Paths_tidal.getDataDir

Entao posso remover esse codigo duplicado do vim-tidal

===

feramentas para facilitar uso do jack

https://aj-snapshot.sourceforge.io eh uma ferramenta que tira um snapshot as conexoes do jack e salva num arquivo
xml, eh possivel depois restaurar este arquivo, esta empacotado no debian, pode ser interessante para eu gerenciar
visualmente as conexoes com qjackctl ou outro qquer e entao salvar o xml para restaurar posteriormente, casos de uso pode ser
um ensaio ou preparacao para uma apresentacao ao vivo, salvo o snapshot e entao restauro facilmente no momento da performance.

para restaurar e remover -r antes as conexoes existentes:
aj-snapshot -xr test.snap

testei o aj-snapshot e salva corretamente e tambem restaura com sucesso.

https://github.com/OpenMusicKontrollers/patchmatrix parece ser uma interface visual interessante para fazer conexoes jack mas
nao esta empacotado no debian. Fiz download dos binarios fornecidor no github mas deu erro ao executar, talvez compilando funcione.



===================================

lalis sample packs freetousesounds


15 marco 2022
=============

re-instalando SC e Tidal, etc pois esta tudo zuado...

sudo apt install supercollider sc3-plugins

instalar ultima versao do superdirt, consultar em
https://github.com/musikinformatik/SuperDirt/releases

Quarks.checkForUpdates({Quarks.install("SuperDirt", "v1.7.3"); thisProcess.recompile()})

instalar tidal via cabal:

```sh
cabal update
cabal install tidal --lib
```

em caso de erro melhor tentar remover tudo instalado via cabal e instalar novamente,
basta remover ~/.cabal e ~/.ghc

entao para validar que esta ok basta executar tidal via linha de comando, este passo depende do binario tidal do hrung instalado no PATH,
caso contrario , um teste mais simples e basico sem depender do hrung eh executer o ghci com os parametros abaixo:

```sh
ghci -e Paths_tidal.getDataDir
```

18 julho 2024
=============

error iniciando tidalcycles via dublang:

```sh
$ dublang run tidalcycles
run service: tidalcycles
ghci started, pid = 307846
starting TCP server on 0.0.0.0:42002
TCP server started
exit code	1
exit signal	0
Loaded package environment from /home/joenio/.ghc/x86_64-linux-9.4.7/environments/default
GHCi, version 9.4.7: https://www.haskell.org/ghc/  :? for help

<command line>: cannot satisfy -package-id tidal-1.9.2-64db9fd2658449d3931680cd7c41a0fad0b65d7ebc38f7a56ade30b0d5ce8bc2: 
    tidal-1.9.2-64db9fd2658449d3931680cd7c41a0fad0b65d7ebc38f7a56ade30b0d5ce8bc2 is unusable due to missing dependencies:
      random-1.2.1.1-Aatha6E6c0aE6gEWaLlLCn
    (use -v for more information)

Uncaught Error: ...rocks-5.1/dublang/0.7.0-0/handlers/tidalcycles-start:35: E: unable to start tidalcycles
stack traceback:
	[C]: in function 'error'
	...rocks-5.1/dublang/0.7.0-0/handlers/tidalcycles-start:35: in function <...rocks-5.1/dublang/0.7.0-0/handlers/tidalcycles-start:31>
	[C]: in function 'run'
	...rocks-5.1/dublang/0.7.0-0/handlers/tidalcycles-start:64: in main chunk
	[C]: ?
```

solucao:

```sh
rm -rf ~/.cabal ~/.ghc
cabal update
cabal install tidal --lib
```
