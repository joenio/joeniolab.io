#!/bin/sh

# script to save HTML slides make with Reveal.js to PDF
# it uses chromium browser to take screenshots and then
# it uses pdfjoin to join many screenshots images into a single PDF file

mkdir -p tmp
cd tmp/

# save slides:
# https://joenio.me/dublang-5min-intro/#/0/

SLUG="dublang-5min-intro"
SLIDES="0 1 2 3 4 5 6 7"
for n in ${SLIDES}; do
  chromium --headless --screenshot="${SLUG}-${n}.png" "https://joenio.me/${SLUG}/#/${n}"
  gm convert "${SLUG}-${n}.png" "${SLUG}-${n}.pdf"
done
