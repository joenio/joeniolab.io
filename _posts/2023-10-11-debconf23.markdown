---
title: Conferência Debian 2023 (DebConf 23)
---

![](/files/debconf23_group.jpg)
<small>_photo by: Aigars Mahinovs_</small>

A 24a edicao da Conferencia Debian foi realizada em Kochi, India entre os
dias 3 e 17 de Setembro de 2023 e eu tive a oportunidade de participar gracas ao
apoio do
[projeto Debian][debian],
[Software in the Public Interest (SPI)][spi],
[Debian France][debian-fr],
[CorTexT Platform][cortext]
e do [Laboratoire Interdisciplinaire Sciences Innovations Sociétés (LISIS)][lisis].

Aqui neste post eu faco um resumo das atividades e palestras que realizei
durante a conferencia.

<br/>

## Talk "Debian: Resistance is Futile"

No dia 10 de Setembro (Domingo) as 17:00 na sala Anamudi eu fiz a talk performance
_Debian: Resistance is Futile_ com Live Coding utilizando [dublang][]
integrando Tidal Cycles, MPV e SM.

* [DebConf23 - Debian: Resistance is Futile][talk-56]

[![](/files/debconf23-talks-56-debian-resistance-is-futile.jpg)][talk-56]

<br/>

## BoF "use Perl; # Annual meeting of the Debian Perl Group"

Em 15 de Setembro (Sexta) 18:00 ajudei a realizar juntamente com o intrigeri o
tradicional BoF do time Perl.

* [DebConf23 - use Perl; # Annual meeting of the Debian Perl Group][talk-85]

[![](/files/debconf23-talk-85-use-perl.jpg)][talk-85]

<br/>

## Workshop "Live Coding tools on Debian: How to install and setup"

No dia 16 de Setembro (Sabado) as 15:30 realizei na sala Ponmudi um workshop de
live coding com Sonic Pi.

* [DebConf23 - Live Coding tools on Debian: How to install and setup][talk-57]

[![](/files/debconf23-talks-57-sonic-pi.jpg)][talk-57]

<br/>

## BoF "Live Coding for art, sound and visuals"

Em 17 de Setembro as 10:30 (Domingo) fiz um debate com a comunidade Debian
interessada em musica, arte, live coding e outros temas envolvendo arte e
debian.

* [DebConf23 - Live Coding for art, sound and visuals][talk-58]

[![](/files/debconf23-talks-58-bof-live-coding.jpg)][talk-58]

E assim foi minha partipacao este ano de 2023 na DebConf, espero poder estar
presente proximo ano na [DebConf24](https://wiki.debian.org/DebConf/24), e
planejo continuar estimulando o link entre as comunidades de Live Coding e a
Comunidade Debian atraves do time
[Debian Live Coding team](https://wiki.debian.org/Teams/LiveCoding)
criado em 2022 durante [DebConf22](https://debconf22.debconf.org).

[debian]: https://debian.org
[spi]: https://www.spi-inc.org
[debian-fr]: https://france.debian.net
[cortext]: https://www.cortext.net
[lisis]: http://umr-lisis.fr
[talk-56]: https://debconf23.debconf.org/talks/56-debian-resistance-is-futile
[dublang]: https://dublang.4two.art
[talk-57]: https://debconf23.debconf.org/talks/57-live-coding-tools-on-debian-how-to-install-and-setup
[talk-58]: https://debconf23.debconf.org/talks/58-live-coding-for-art-sound-and-visuals
[talk-85]: https://debconf23.debconf.org/talks/85-use-perl-annual-meeting-of-the-debian-perl-group
