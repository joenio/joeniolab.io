---
title: Relato 2º Encontro da Diáspora Brasileira de Ciência, Tecnologia e Inovação na França
---

![](/files/2o-encontro-diaspora-brasil-franca.jpg)

Em 18 novembro de 2022 foi realizado o [2º Encontro da Diáspora Brasileira de
Ciência, Tecnologia e Inovação na
França](/files/II%20Encontro%20da%20Di%C3%A1spora%20Brasileira%20de%20Ci%C3%AAncia%20v4.pdf)
na Embaixada do Brasil em Paris, onde tive a chance de estar presente, aqui
neste post compartilho brevemente algumas notas sobre o evento.

A primeira metade do evento contou com um painel composto por Susi Dal Belo,
gerente de pesquisa da L'Oreal, Andre Pierre Mattei, diretor de inovacao
SENAI/PE e CEO GrapeHawk, Mariana Mesel-Lemoine, chefe de acompanhamento de
carreiras do Institut Pasteur, Franceline Reynaud, professora na Universidade
de Lorraine. Cada participante compartilhou sua trajetoria e visao sobre a
relacao Brasil-Franca no tema Ciencia, Tecnologia e Inovacao.

![](/files/programa-2o-encontro-diaspora-brasil-franca.png)

Em seguida, o segundo painel apresentado por Debora Freitas, coordenadora do
[PUB-Paris][], faz um resumo da sua trajetoria academica e profissional desde o
Brasil em 2003 na UNIPAC ate chegar em Paris na Franca, incluindo experiencia
num doutorado sanduiche no departamento de fisica em Harvard, passando pela
PUC-Rio, ESPCI-Paris-PSL, e hoje como gerente de projetos P&D na
[Microfactory][].

Debora lembrou tambem sua colaboracao na criacao do [PUB-Paris][], surgido a
partir do 1º Encontro da Diáspora Brasileira de Ciência, Tecnologia e Inovação
na França realizado em 2021, finaliza convidando a todos para o proximo
encontro do PUB-Paris em 20 de Janeiro de 2023 as 19h na [Maison des
initiatives étudiantes (MIE)](https://mie.paris.fr).

Em seguida Luis Fernando apresenta a [APEB-BR][], onde é presidente, uma
associacao dedicada aos brasileiros na Franca com mais de 40 anos de
existencia, tendo participacao academica e cientificamente na organizacao de
diversos eventos, estimulando a criacao de redes de brasileiros e outras
nacionalidades, incluindo diversos paises lusofônicos, a [APEB-BR][] tem sido
uma referencia de apoio para instituicoes que visitam a Franca, e tem
participacao historica em momentos relevantes, como por exemplo, ter
participado dos esforcos de construcao constituicao de 88, e tambem servido de
apoio ao recepcionar embaixadores, politicos e outros atores que visitam o
pais.

Ainda sobre a [APEB-BR][], ouvimos as palavras da vice-presidente Isabella Vieira,
sobre a participacao da APEB-BR na promocao de eventos incluindo coloquios,
lancamento de livros, palestras, mesa-redondas, apresentacoes artisticas e
culturais, incluindo musica, cinema, teatro, entre outros. Isabella Vieira nos
convida para o proximo encontro da associacao que ocorrera em 19/11/2022 na
[Maison do Bresil][maisondubresil] a partir das 10h da manha e finaliza
lembrando que a participacao na APEB-BR é livre e aberta a todos.

Na sequencia, Roberto Abramovich, presidente da [France-Bresil Empreendedores
(FBE)][fbe] fundada em 2022, relata sua trajetoria ao sair do Brasil passando
pela Suica ate chegar na Franca, relata a atuacao da FBE na criacao de redes
visando reduzir o isolamento empresarial, trabalhando na solucao de problemas
que sao comuns entre empresarios que atuam em cenario internacional, desde
questoes juridicas e fiscais, ate apoio em como se posicionar ao fazer negocio
no pais.

Angela Santana diretora interina da [Maison du Bresil][maisondubresil] na
[CIUP][], parabeniza a organizacao do evento e recorda que foi na [Maison du
Bresil][maisondubresil] que surgiu a [APEB-BR][] a partir das muitas
reclamacoes sobre atrasos no pagamento de bolsas dos estudantes brasileiros que
ali viviam na epoca. Conta que a Maison sempre teve um papel central na rede de
estudantes brasileiros e que com a pandemia este papel se enfraqueceu devido ao
isolamento vivido por todos, no entanto o cenario esta mudando e o papel da
Maison tem voltado a normalidade. O comite da casa comecou a realizar
seminarios toda semana as quinta-feiras 19h para troca de conhecimento num
espirito multidisiplinar, incluindo temas de todas as areas da ciencia,
medicina, biologia, astrofisica, traducao, literatura, matematica, tecnologia,
etc...

<img style='border: 0; box-shadow: none;' src='/files/line-2o-encontro-diaspora-brasil-franca.png' />

O encerramento do evento foi realizada por Andre Maciel, ministro conselheiro
da Embaixada do Brasil, diz que este evento tem ampla importancia para o
aprimoramendo da missao da embaixada, acredita que o Brasil precisa ir ao mundo
e este evento tem um papel extremamente significativo neste sentido, fala dos
problemas dos paises emergentes e dos problemas classicos de quem trabalha no
campo internacional, enxerga o evento como um vetor de projecao nacional e
portanto sera sempre bem-vindo na embaixada

Finaliza destacando o papel historico da Ciencia nas Nacoes Unidas, hoje um
tanto quanto distante, mas em seu papel como brasileiro e diplomata gostaria de
relembrar o nome da brasileira [Bertha Lutz][bertha], sufragista, eleita
deputada, mandato caçado, e em 45 atuou na delegacao brasileira em Sao
Francisco onde se redigia a Carta das Nacoes Unidas, e por sua sugestao foi
incluido o principio da isonomia entre homens e mulheres na carta, uma
cientista, brasileira. Dai lembra do papel central dos cientistas em pautas
politicas e pretende estar presente na terceira edicao do evento no proximo ano
em 2023, fortalecendo sempre o tripe onde atua a embaixada ouvindo e dialogando
com os diversos atores do governo, empresas e academia.

[maisondubresil]: http://www.maisondubresil.org
[APEB-BR]: http://www.apebfr.org
[PUB-Paris]: https://pubparis.fr
[Microfactory]: https://www.microfactory.eu
[fbe]: https://www.fbe.digital
[CIUP]: https://www.ciup.fr
[bertha]: https://pt.wikipedia.org/wiki/Bertha_Lutz
