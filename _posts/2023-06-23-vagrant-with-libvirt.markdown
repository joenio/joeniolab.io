---
title: Vagrant + LibVirt
---

Usually Vagrant uses Virtualbox by default but how about if I want to run
Libvirt instead?

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian/testing64"
  config.vm.provision "shell", path: "vagrant.sh"
end
```

Requirements.

```sh
sudo apt install vagrant libvirt
```

Running.

```sh
vagrant up --provider=libvirt
```
