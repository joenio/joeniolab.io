---
title: KVM switch via software with Barrier
---

How to configure a KVM switch via software to share keyboard and mouse among
multiple computers on the same local network.

## KVM switch

> A KVM switch (with KVM being an abbreviation for "keyboard, video, and
> mouse") is a hardware device that allows a user to control multiple computers
> from one or more sets of keyboards, video monitors, and mice. 
> Source: [https://en.wikipedia.org/wiki/KVM_switch](https://en.wikipedia.org/wiki/KVM_switch)

## Barrier

> Barrier is software that mimics the functionality of a KVM switch allowing to
> use a single keyboard and mouse to control multiple computers by moving the
> mouse to the edge of the screen, or by using a keypress to switch focus to a
> different system. Source: [https://github.com/debauchee/barrier](https://github.com/debauchee/barrier)

### Setup Barrier server

<div class="alert alert-warning">
There is no support for Wayland!<br/>
* https://github.com/debauchee/barrier/issues/1251
* https://github.com/debauchee/barrier/issues/109
</div>


Run Barrier GUI to create the config file.

![Barrier server screenshot](/files/barrier-server-screenshot.png)

![Barrier server settings screenshot 1](/files/barrier-server-config-screenshot-2.png)

![Barrier server settings screenshot 2](/files/barrier-server-config-screenshot-1.png)

Config file is saved at `$HOME/.local/share/barrier/.barrier.conf`, see below
the config content for the settings made on the screenshots above.

```conf
section: screens
	maelcum:
		halfDuplexCapsLock = false
		halfDuplexNumLock = false
		halfDuplexScrollLock = false
		xtestIsXineramaUnaware = false
		preserveFocus = false
		switchCorners = none 
		switchCornerSize = 0
	lisis-joenio-dell:
		halfDuplexCapsLock = false
		halfDuplexNumLock = false
		halfDuplexScrollLock = false
		xtestIsXineramaUnaware = false
		preserveFocus = true
		switchCorners = none 
		switchCornerSize = 0
end

section: aliases
end

section: links
	maelcum:
		down = lisis-joenio-dell
	lisis-joenio-dell:
		up = maelcum
end

section: options
	relativeMouseMoves = false
	screenSaverSync = true
	win32KeepForeground = false
	clipboardSharing = true
	switchCorners = none 
	switchCornerSize = 0
end
```

Close the Barrier GUI and run Barrier server by command line, it will read the
config file saved by the GUI.

```sh
$ barriers --no-daemon --no-tray --disable-crypto --disable-client-cert-checking
started server (IPv4), waiting for clients
```

### Barrier client

```sh
$ barrierc --no-daemon 192.168.0.17
[2022-11-17T13:36:21] NOTE: started client
[2022-11-17T13:36:21] NOTE: connecting to '192.168.0.17': 192.168.0.17:24800
connected to server
```

Expected server output when the client connects successfully.

```
started server (IPv4), waiting for clients
[2022-11-17T13:36:21] NOTE: accepted client connection
[2022-11-17T13:36:21] NOTE: client "lisis-joenio-dell" has connected
```

## Demo video

<video controls>
  <source src="/files/barrier-demo.mp4" type="video/mp4">
</video>
