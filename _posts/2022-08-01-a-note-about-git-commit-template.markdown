---
title: A note about Git commit template
---

![git config commit.template manpage](/files/git-commit-template.gif)

> I always forget how to write good commit messages.

This post document how to use a template file for new commit messages.

First, create `~/.gitcommit` file with the content below.

```git
Fix X to allow Y to use Z

# Resolves: #123
# See also: #456, #789
#
# Commit message examples:
# -----------------------
# - Fix typo in README.md
# - Remove X & Add Y to allow Z
# - Update header logo
# - Add Polish language
# - Correct spelling of CHANGELOG
# - Drop support for Node 6
# - Fix failing CompositePropertySourceTests
# - Rework @PropertySource early parsing logic
# - Add tests for ImportSelector meta-data
# - Update docbook dependency and generate epub
# - Polish mockito usage
#
# If you need some background, read:
# ---------------------------------
# Capitalized, short (50 chars or less) summary
# 
# More detailed explanatory text, if necessary.  Wrap it to about 72
# characters or so.  In some contexts, the first line is treated as the
# subject of an email and the rest of the text as the body.  The blank
# line separating the summary from the body is critical (unless you omit
# the body entirely); tools like rebase can get confused if you run the
# two together.
# 
# Write your commit message in the imperative: "Fix bug" and not "Fixed bug"
# or "Fixes bug."  This convention matches up with commit messages generated
# by commands like git merge and git revert.
# 
# Further paragraphs come after blank lines.
# 
# - Bullet points are okay, too
# 
# - Typically a hyphen or asterisk is used for the bullet, followed by a
#   single space, with blank lines in between, but conventions vary here
# 
# - Use a hanging indent
#
# References:
# ----------
# - https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
# - https://www.conventionalcommits.org/en/v1.0.0/
# - https://www.freecodecamp.org/news/writing-good-commit-messages-a-practical-guide/ 
# - https://cbea.ms/git-commit/
```

Then, say to Git to use `~/.gitcommit` file as template for new commit messages.

<pre class="terminal">
<code>
git config --global commit.template ~/.gitcommit
</code>
</pre>

Voilà, now every time you run `git commit` the template message is displayed
and you'll never forget how to write good commit messages ;)


## See also

* [Go Contribution Guide: Good commit messages](https://tip.golang.org/doc/contribute#commit_messages)
* [How To Write Good Commit Messages](https://blog.mergify.com/how-to-write-good-commit-messages)

** The [gif](/files/git-commit-template.gif) used on header of this page was created with [ImageMagick](https://www.imagemagick.org).
<br/>*** Reference: [ostechnix.com/create-animated-gif-ubuntu-16-04](https://ostechnix.com/create-animated-gif-ubuntu-16-04)

<pre class="terminal">
<code>
convert -delay 480 -loop 0 git-commit-template*.png git-commit-template.gif
</code>
</pre>
