---
title: Métricas FAIR para Research Software (FRSM)
lead: >
  As Métricas FAIR para Research Software (FRSM) definido no contexto do
  projeto FAIR-IMPACT abrange um conjunto de métricas para avaliação automática
  dos princípios FAIR aplicados a Research Software (RS) no contexto do
  European Open Science Cloud (EOSC) para melhoria da qualidade e
  sustentabilide do RS.
---


- https://fair-impact.eu/metrics-software

O _draft_
[FAIR metrics for research software - Metrics for automated FAIR software assessment in a disciplinary context][10.5281/zenodo.10047401]
define um conjunto de 17 métricas para avaliação automática de research
software segundo os princípios FAIR4RS (FAIR for Research Software), o
documento, em versão _draft_, está aberto para comentários da comunidade [aqui
neste link][draft], será publicado no segundo semestre de 2024.

FAIR software é definido como research software (em português: software para
pesquisa) aderente aos princípios FAIR, o documento faz referência a outras
iniciativas que também definem métricas para avaliação e qualidade de software,
como o ISO/IEC SQuaRE, IEEE SWEBoK, entre outros, estes discutem métricas
relacionadas aos principios FAIR (exemplo: usabilidade), mas são focados
principalmente na indústria. A comunidade Open Source também tem propostas,
como o CHAOSS por exemplo, com objetivo principal de avaliar a saúde de
comunidades em escala global, outra iniciativa é o OSSF que define um checklist
com critérios para avaliação de projetos Open Source.

As métricas definidas pelo [FAIR4RS][10.15497/RDA00068] inspiraram a definição
das métricas para avaliação automática FRSM e deram também origem a muitos
outros guias publicados pela comunidade acadêmica e interessados no tema,
algumas métricas podem ser facilmente automatizadas, outras são mais subjetivas
e difíceis de avaliar automaticamente.

O documento resume numa tabela todas as métricas FAIR4RS, cada métricas pode
ter níveis distintos de aplicação, entre métricas que se aplicam ao nível de
código-fonte (code level), aspectos sobre como o software é desenvolvido
(software project level) e métricas que se aplicam a aspectos sobre como o
software é armazenado (repository level), há uma importante distinção entre
repositórios de código e repositórios de preservação.

AS métricas FAIR FRSM se referem primariamente a métricas "repository level",
abaixo segue uma transcrição, com tradução livre, das 17 métricas FRMS
definidos no contexto do projeto FAIR-IMPACT Software Metrics definidos a
partir da publicacao FAIR4RS de 2022.

| Identificador | Nome |
| ------------- | ---- |
| FRSM-01       | O software possui um identificador persistente único e global? |
| FRSM-02       | Os diferentes componentes do software possuem seus próprios identificadores? |
| FRSM-03       | Cada versão do software possui um identificador único? |
| FRSM-04       | O software inclui metadados descritivos que ajudam a definir seu propósito? |
| FRSM-05       | O software inclui metadados de desenvolvimento que ajudam a definir seu estado? |
| FRSM-06       | O software inclui metadados sobre os contribuidores e seus papéis? |
| FRSM-07       | Os metadados do software inclui o identificador do software? |
| FRSM-08       | O software tem um registro persistente dos metadados aberto, público e acessível? |
| FRSM-09       | O software é desenvolvido em um repositório de código / _forge_ que usa protocolos de comunicação seguindo padrões? |
| FRSM-10       | Os dados consumidos ou produzidos pelo software aderem a formatos abertos e referências aos formatos são fornecidas? |
| FRSM-11       | O software faz uso de APIs abertas que suportam leitura automatizadas por máquinas (machine-readable interface definition)? |
| FRSM-12       | O software oferece referências para outros objetos que suportam seu uso? |
| FRSM-13       | O software descreve os seus requisitos de uso? |
| FRSM-14       | O software possui casos de testes que demonstram e validam seu funcionamento? |
| FRSM-15       | O código fonte do software inclui informações de licenciamento para o software e qualquer outro software externo distribuído em conjunto? |
| FRSM-16       | O registro de metadados do software inclui informações de licenciamento? |
| FRSM-17       | O software inclui informações de provisionamento (provenance information) que descrevem o seu desenvolvimento? |

Cada metrica é detalhada no documento com uma descrição extendida, e inclui
links para quais princípios FAIR4RS a métrica faz referência e a quais
[recomendações RSMD](/o-guia-de-metadados-para-research-software-rsmd) estão
ligadas, incluindo um checklist de como avaliar a métrica e comentários gerais
com exemplos e sugestões.


O relatório traz ainda um caso de uso de domínio específico como demonstração de
como aplicar as métricas a partir de necessidades particular de uma dada área da ciência,
este caso 
descrito no documento é da ciências sociais, e pode ser um ponto positivo para quem trabalha com engenharia de software neste campo,
o caso de uso sao mapeadas para o guia
 CESSDA, [CESSDA Technical Guidelines for Social Science](https://docs.tech.cessda.eu/index.html).

As 17 metricas são renomeadas para este domínio específico com nomes como:
FRSM-01-CESSDA, FRSM-02-CESSDA, FRSM-03-CESSDA, FRSM-17-CESSDA e faz links com
as métricas definidas pelo CESSDA, como por exemplo: CMA1, CMA4, CMA7, etc.

O apendice final do documento faz um bom resumo da evolucao dos principios FAIR
dados para software, transcrito abaixo, sem detalhes (ver documento para mais
detalhes).

- FAIR Guiding Principles (2016)
- Towards FAIR Principles for research software (2020)
- Taking a fresh look at FAIR for research software (2021)
- FAIR4RS Principles Draft for RDA Community Review (2021)
- FAIR4RS Principles (2022)


Para saber mais sobre as métricas FRSM definidas pelo _draft_
[FAIR metrics for research software - Metrics for automated FAIR software assessment in a disciplinary context][10.5281/zenodo.10047401]
visite o link abaixo:
- [https://fair-impact.eu/metrics-software](https://fair-impact.eu/metrics-software)

[10.5281/zenodo.10047401]: https://doi.org/10.5281/zenodo.10047401
[draft]: https://docs.google.com/document/d/1Ye4bnClsTPyOhvoyKgIBF5GZDxHZQOqGrg1aY9Y5a8Y/edit?usp=sharing
[10.15497/RDA00068]: https://doi.org/10.15497/RDA00068
