---
title: Equinócio Eulerroom 2020, TOPLAP 16 anos
lead: >
  O Equinócio Eulerroom 2020 é o evento comemorativo de 16 anos da comunidade
  de live coding TOPLAP, realizado com transmissão online e sem interrupções de
  performances de artistas de todo o mundo entre os dias 20 e 22 de Março (no
  timezone UTC), transmitido ao vivo pelo YouTube no canal Eulerroom.
---

![cubos coloridos](/files/structure-synth-cubos.png)

O [Equinócio Eulerroom 2020][equinox] é o evento comemorativo de 16 anos da
comunidade de live coding [TOPLAP][], realizado com transmissão online e sem
interrupções de performances de artistas de todo o mundo entre os dias 20 e 22
de Março (no timezone UTC), transmitido ao vivo pelo YouTube no [canal
Eulerroom][youtube] e também pelo [Twitch][] e [Facebook][].

Live Coding é uma técnica artística para produção de sons e imagens através da
programação ao vivo, diferentes linguagens de programação de live coding estão
disponíveis, em sua maioria software livre e resultados de pesquisas dos
artistas neste campo, você pode verificar todas linguagens de programação para
live coding [aqui][awesome-livecoding].

A transmissão do evento se inicia às 21h do dia 18 de Março em horário do Brasil
(equivalente à 00h em UTC do dia 19) e se encerra às 22h30 do dia 22 de Março,
somando 98 horas de transmissão online sem interrupções, envolvendo mais de 165
performances representando o que há de mais atual em pesquisa e prática
artística no campo do live coding e improvisação ao vivo através de programação
de computador.

Entre os artistas o Brasil marca presença e está bem representado com as
seguintes apresentações:

| ------------------- | ---------------------------------------- | ------------------------------- |
| data                | artista                                  | descrição (link p/ vídeo)       |
| ------------------- | ---------------------------------------- | ------------------------------- |
| 18/03 23h30         | [ghales][] (Brasília)                    | [glitch minimal / ambient beats using TidalCycles](https://youtu.be/yb5ryLgM2ww) |
| 19/03 02h00         | [diegodukao][] (Rio de Janeiro)          | [Live coding with FoxDot](https://youtu.be/MJsSlNzYAUE) |
| 19/03 02h30         | [Alexandre Rangel][rangel] (Brasília)    | [Sonic Pi / Hydra / Blender3D performance](https://youtu.be/v86baM-TK0E) |
| 19/03 15h30         | [djalgoritmo](/djalgoritmo) (Brasília)   | [tidal cycles live coding performance](https://youtu.be/ujZfpil9bF4) |
| 20/03 19h00         | [Gil Fuser][gil] (São Paulo)             | Tidal + SuperCollider |
| 20/03 20h00         | [beise][] (Vitória)                      | livecoded antropophagia |
| 20/03 20h30         | [INRGD][] (São Paulo)                    | dark experimental |
| 21/03 08h30         | [Elihu Garrett][elihu] (São Paulo)       | Live Coding with Lisp |
| ~~21/03 17h30~~     | [Empurre o céu][aluwiz] (Belo Horizonte) | ~~Some live coding and sound improvisation. Acoustic sources being fed to a GNU system~~ |
| 22/03 00h30         | [Pietro Bapthysthe][pietro] (Brazil)     | Live coding duo jamming with FoxDot |

Os vídeos da transmissão do último aniversário de 15 anos estão disponíveis no
canal Eulerroom, confira abaixo.

## Vídeos de 15 anos TOPLAP

<iframe width="560" height="315" src="https://www.youtube.com/embed/AdOS3K0ADiA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[equinox]: http://equinox.eulerroom.com/?lang=pt
[toplap]: https://toplap.org
[youtube]: https://www.youtube.com/eulerroom/live
[twitch]: https://www.twitch.tv/eulerroom
[facebook]: https://www.facebook.com/eulerroom/posts/2532102923771786
[awesome-livecoding]: https://github.com/toplap/awesome-livecoding
[ghales]: https://ghales.top
[rangel]: https://www.alexandrerangel.art.br
[elihu]: https://github.com/elihugarret
[inrgd]: https://soundcloud.com/inrgd
[diegodukao]: https://pietrobapthysthe.bandcamp.com
[gil]: https://gilfuser.net
[beise]: https://hitnail.net
[pietro]: https://pietrobapthysthe.bandcamp.com
[aluwiz]: https://libertalia.world/@aluwiz
