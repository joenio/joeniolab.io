---
title: O arquivo universal de código-fonte Software Heritage (SWH)
lead: >
  O Software Heritage é um arquivo permanente e de longa duração para
  código-fonte de software, a principal missão do SWH é arquivar de maneira
  segura e definitiva o código-fonte de todos os projetos de software que se
  encontram publicamente disponíveis...
---

![](/files/SWH-logo_anim-texte1.gif)

O [Software Heritage][swh] (SWH) é um arquivo permanente e de longa duração para
código-fonte de software,
a principal missão do SWH é arquivar de maneira segura e definitiva o
código-fonte de todos os projetos de software que se encontram publicamente disponíveis em
formato de código-fonte, como por exemplo [GitHub][], [Gitlab][], [Sourceforge][],
repositórios de pacotes [PyPi][], [NPM][], [CPAN][],
e ainda repositórios de distribuições GNU/Linux como o
[Debian][], [ArchLinux][], [Fedora][], entre outros.

Os mecanismos de aquisição do SWH buscam por código-fonte em todos estes
repositórios públicos e injetam constantemente todo o código-fonte encontrado
no seu arquivo,
incluindo o histórico de mudanças do código-fonte.  Cada
arquivo de código-fonte adiquirido pelo SWH uma vez adicionado ao arquivo
jamais será removido, de forma que há uma garantia de permanencia e
disponibilidade, este é o compromisso e a missão do SWH e das
instituições parceiras que estão comprometidas com a preservação
do software em formato de código-fonte como um elemento central da sociedade.

A [UNESCO apoia a missão do SWH][unesco] e empresas tem constantemente declarado apoio, como Microsoft, Intel, Red Hat, Nokia e [outras][sponsors],
institutos de pesquisa e universidades como Sorbonne Université, Université de Paris, Università di Pisa Sorbone também declararam apoio.
Isto evidencia a importância e quão transversal é a necessidade de preservar o
código-fonte 
e o histórico
de alterações
de projetos de software disponíveis em repositorios públicos e abertos,
este é um poderoso recurso para todos, incluindo engenheiros, pesquisadores, governos, empresas, artistas, e outros.

Veja abaixo alguns slides de apresentações minhas sobre o SWH:
- [Software Heritage - Atelier Data Univ Gustave Eiffel](/software-heritage-uge-data)
- [Software Heritage - OLIO Networks](/software-heritage-olio)
- [Software Heritage Symposium and summit 2023](/swh-symposium-and-summit-2023)

No vídeo abaixo é possível conferir minha apresentação
[Software Heritage - Atelier Data Univ Gustave Eiffel](/software-heritage-uge-data)
no webinar
[RDV DATA - Les plateformes pour développer, partager et archiver les logiciels](https://clap.univ-eiffel.fr/videos/rdv-data-les-plateformes-pour-developper-partager-et-archiver-les-logiciels-13112023)
realizado em 13/11/2023.
<iframe class="video" src="https://clap.univ-eiffel.fr/permalink/v12666c5f7d7cnb97dk8/iframe/#start=780" allowfullscreen="allowfullscreen" allow="autoplay"></iframe>

Saiba mais sobre o Software Heritage em:
- [Software Heritage Documentation](https://docs.softwareheritage.org)
- [Software Heritage Blog](https://www.softwareheritage.org/blog)
- [Software Heritage Twitter](https://twitter.com/SwHeritage)
- [Software Heritage YouTube](https://www.youtube.com/channel/UCe26PlE3YKTXYvEY9HWdIlQ)

[github]: https://github.com
[gitlab]: https://gitlab.com
[sourceforge]: https://sourceforge.net
[pypi]: https://pypi.org
[npm]: https://npmjs.com
[cpan]: https://cpan.org
[debian]: https://debian.org
[archlinux]: https://archlinux.org
[fedora]: https://fedoraproject.org
[unesco]: https://en.unesco.org/softwareheritage
[sponsors]: https://www.softwareheritage.org/support/sponsors
[merkle]: https://docs.softwareheritage.org/devel/swh-model/data-model.html#swh-merkle-dag
[swh]: https://www.softwareheritage.org
