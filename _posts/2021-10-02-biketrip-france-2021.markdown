---
title: Bike trip 2021 from Paris to Nogent-le-Rotrou
layout: image-gallery
lead: >
  Fotos da viagem de bike na Franca realizada entre os dias 02 e 08 de Outubro
  de 2021 entre Paris e Nogent-le-Rotrou pela rota La Véloscénie.
---

Fotos da viagem de bike na Franca realizada entre os dias 02 e 08 de Outubro de 2021 entre Paris e Nogent-le-Rotrou pela rota
[La Véloscénie](https://www.veloscenie.com).
