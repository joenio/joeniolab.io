---
title: Bike trip 2021 in Netherlands, 12 days
layout: image-gallery
lead: >
  Fotos da viagem de bike na Holanda realizada entre os dias 26 de Julho e 06
  de Agosto de 2021 pelas cidades Den Haag, Dordrecht, Zwijndrecht, Nijmegen,
  Enschede, Zutphen, Deventer, Amsterdam, Haarlem, entre outras vilas e
  cidades.
---

Fotos da viagem de bike na Holanda realizada entre os dias 26 de Julho e 06 de
Agosto de 2021 (12 dias de pedal) pelas cidades Den Haag, Dordrecht,
Zwijndrecht, Nijmegen, Enschede, Zutphen, Deventer, Amsterdam, Haarlem, entre
outras vilas e cidades.

* 23 de Julho, saida de onibus de Paris, Franca para Den Haag, Holanda
* 26 de Julho, primeiro dia e inicio da viagem de bike pela Holanda
* 06 de Agosto, retorno de trem para a cidade de Den Haag
* 07 de Agosto, retorno de onibus de Den Haag, Holanda para Paris, Franca
