---
title: Como atualizar pacotes Perl no Debian - Passo-a-passo prático e detalhado
modulo: Data::UUID
lead: >
  Em posts anteriores sobre Debian mostrei como criar repositórios de pacotes e
  como criar pacotes novos e submeter ao projeto oficial, neste aqui irei
  mostrar como atualizar pacotes do sistema operacional livre Debian GNU/Linux
  e submeter esses pacotes ao projeto oficial através do time Debian Perl
  Group.
---

![Debian shot wallpaper por mdh3ll](/files/debian_shot_by_mdh3ll_v3.jpg)

Em posts anteriores sobre Debian mostrei como [criar repositórios de
pacotes](/aprenda-a-criar-repositorios-de-pacotes-debian) e como [criar pacotes
novos e submeter ao projeto oficial](/empacotando-modulos-perl-no-debian),
neste aqui irei mostrar como atualizar pacotes e submeter esses pacotes ao
Debian através do time [Debian Perl Group][debian-perl-group].

## Primeiro passo, instalar o pacote pkg-perl-tools

O [Debian Perl Group][debian-perl-group] mantém uma coleção de ferramentas para
auxiliar o empacotamento de [módulos][cpan] [Perl][] no Debian mantidos no
pacote [pkg-perl-tools][], apesar de não ser um requisito obrigatório é
altamente aconselhável utilizá-lo, o seu conjunto de ferramentas facilita
bastante o trabalho.

<pre class="terminal">
<code>
sudo apt install pkg-perl-tools
</code>
</pre>

## Próximo passo, instalar e configurar o myrepos

O [myrepos][] (mr) é uma ferramenta para gerenciar múltiplos repositórios de
controle de versão, Git, Bazaar, Mercurial, Darcs, entre outros, com ele é
possível baixar todos os repositórios de todos os pacotes mantidos pelo Debian
Perl Group com um único comando.

Primeiro, instale o _myrepos_.

<pre class="terminal">
<code>
sudo apt install mr
</code>
</pre>

Então, crie o arquivo de configuração `~/.mrconfig` com o seguinte conteúdo.

```ini
[src/pkg-perl]
chain = true
checkout = git clone git@salsa.debian.org:perl-team/modules/meta.git pkg-perl
```

A primeira linha deste arquivo indica o local onde os repositórios serão
mantidos, eu costumo usar `~/src/pkg-perl/`, mas você pode usar qualquer outro
caminho. Neste ponto já possível ler os repositórios e fazer `git clone` de
todos eles através do _myrepos_, a primeira execução costuma levar um certo
tempo devido ao elevado número de pacotes.

<pre class="terminal">
<code>
mr checkout
mr up
</code>
</pre>

O `mr checkout` deve ser executado uma única vez, já o `mr up` deve ser
executado sempre que for trabalhar com algum pacote, vale a pena executá-lo com
uma certa frequência.

> É importante dizer que é possível trabalhar sem o _myrepos_ mas eu aconselho
> bastante usá-lo, uma vez que ter os pacotes localmente traz algumas
> facilidades, como será visto mais à frente.

## Configurar o Debian Perl module packaging Tool

Essa é a principal ferramenta distribuída pelo pacote `pkg-perl-tools` para
auxiliar a manutenção de pacotes Perl, para configurar o `dpt` crie o arquivo
`~/.dpt.conf` com o mesmo caminho definido em `~/.mrconfig` acrescentando
_"/packages"_ ao final do caminho, veja o exemplo abaixo.

```ini
DPT_PACKAGES=/home/joenio/src/pkg-perl/packages
```

## Agora vamos selecionar alguns pacotes para atualizar

Lembra das vantagens em manter os pacotes localmente com o _myrepos_?  Pois
então, uma delas é o comando `dpt new-upstream`, ele lê todos os repositorios
locais (previamente baixados com `mr checkout` e atualizados com `mr up`) e
verifica quais possuem versões recentes no _upstream_.

Então, para gerar um relatório dos pacotes com versões novas no _upstream_
execute o seguinte.

<pre class="terminal">
<code>
dpt new-upstream
</code>
</pre>

Não esqueça de configurar a variável `DPT_PACKAGES` no arquivo `~/.dpt.conf`
antes de executar o comando acima, caso contrário o `dpt` não saberá onde
encontrar os pacotes.

Assim como o _myrepos_ o _dpt_ também é uma ferramenta opcional, é possível
trabalhar no empacotamento sem ele, os pacotes que necessitam de atualização,
por exemplo, podem ser também encontrados nos seguintes endereços.

* [https://udd.debian.org/dmd/?email1=pkg-perl-maintainers@lists.alioth.debian.org](https://udd.debian.org/dmd/?email1=pkg-perl-maintainers@lists.alioth.debian.org)
* [https://tracker.debian.org/teams/pkg-perl/+table/general/?tag=new-upstream-version](https://tracker.debian.org/teams/pkg-perl/+table/general/?tag=new-upstream-version)

> Apesar do _dpt_ ser opcional seu uso é aconselhável uma vez que evita
> trabalho repetitivo e ajuda a manter algumas boas práticas sugeridas pelo
> Debian Perl Group.

Independente da fonte, `dpt` ou os links acima, selecione o pacote que deseja
atualizar e comece a atualização, durante a escrita desse post eu selecionei os
pacotes abaixo e vou utilizá-los em alguns exemplos.

* [libdata-uuid-perl](https://tracker.debian.org/pkg/libdata-uuid-perl)
* [libemail-mime-perl](https://tracker.debian.org/pkg/libemail-mime-perl)
* [libemail-mime-contenttype-perl](https://tracker.debian.org/pkg/libemail-mime-contenttype-perl)
* [libmojolicious-plugin-openapi-perl](https://tracker.debian.org/pkg/libmojolicious-plugin-openapi-perl)

## Atualizar o repositório local do pacote

O primeiro passo é a atualização do pacote com a nova versão do _upstream_, mas
antes de fazer isso é importante garantir que temos a cópia mais recente do
pacote.

Para atualizar entre da pasta do pacote e rode `gbp pull`.

<pre class="terminal">
<code>
cd ~/src/pkg-perl/packages/libdata-uuid-perl/
gbp pull
</code>
</pre>

Geralmente o pacote já está atualizado, especialmente se o `mr up` foi
executado recentemente, mas é boa prática executar o comando acima para
garantir que temos a cópia mais recente e evitar surpresas.

## Atualizar o pacote com nova a versão do upstream

Agora é possível trazer a nova versão do _upstream_ atualizando o repositório
local do pacote.

<pre class="terminal">
<code>
dpt import-orig
</code>
</pre>

O `dpt` irá buscar a nova versão do móbulo Perl no _upstream_, CPAN ou
repositório Git, Gitlab ou Github e irá incorporar o novo código, vai também
atualizar alguns arquivos automaticamente, como `debian/changelog`,
`debian/upstream/metadata`, entre outros, além de realizar commits dessas
mudanças e de criar _tags git_ da nova versão.

> É possível utilizar `gbp import-orig --uscan --pristine-tar` no lugar do
> comando `dpt import-orig` mas o `gbp` não executa os inúmeros passos
> realizados automaticamente pelo `dpt`.

## Revisando e corrigindo o pacote

Neste ponto começa o trabalho manual do empacotamento, a nova versão do
_upstream_ gera impacto nas definições do pacote de forma que é necessário
verificar diversos detalhes, suas dependencias, quais patches tem aplicados, se
já foram incorporados no _upstream_, notas de _copyright_, entre outros ajustes
da nova versão, durante este processo é importante seguir ao máximo as
políticas e boas práticas do [Debian Policy][debian-policy] e do [Debian Perl
Group Policy][perl-policy].

Um pacote Debian é basicamente um conjunto de arquivos em `debian/*`
descrevendo suas dependencias, informações de copyright, instruções de build,
licenças, metadados, conflitos com outros pacotes, scripts de pré-instalação,
pós-instalação e muitas outras informações e automatizações necessárias ao
ciclo de vida de um pacote de software.

A partir daqui irei demonstrar as alterações realizadas em cada um dos arquivos
encontrados em `debian/*` durante a atualização dos pacotes que eu selecionei,
os exemplos não são exaustivos e se concentram nos ajustes mais comuns
realizados em pacotes Perl.

## Arquivo debian/changelog

O arquivo `debian/changelog` é sempre atualizado em novas versões, nele é
indicado a versão do pacote, se o `dpt import-orig` foi utilizado corretamente
não será necessário atualizar nada, mas se por algum motivo for necessário
atualizar manualmente o arquivo sugiro utilizar o `dch`.

Por exemplo, se a nova versão do pacote é `0.016` basta executar o seguinte.

<pre class="terminal">
<code>
dch -v 0.016
</code>
</pre>

O `dch` irá interpretar o arquivo `debian/changelog` e adicionará o conteúdo
necessário para a versão `0.016`.

#### Non-Maintainer Upload (NMU)

Atenção para esse detalhe pois o Lintian costuma reclamar disso, por muito
tempo convivi com um alerta sobre _NMU_ sem entender do que se tratava,
acontece que é prática usual em pacotes mantidos por times, como no caso do
Debian Perl Group, deixar explícito que o upload do pacote é feito pelo time e
não por um _Debian Developer (DD)_ individual, na prática o Lintian verifica se
o campo Maintainer no arquivo `debian/control` casa com a identificação de quem
está construindo o pacote localmente, e no caso de pacotes mantidos por times o
campo Maintainer é preenchido com a identificação do time e isso confunte o
Lintian, por isso é necessário adicionar o seguinte no `debian/changelog`para
evitar alertas sobre _Non-Maintainer Upload (NMU)_.

* `Team upload`

Veja, por exemplo, como ficou essa alteração para o pacote **libdata-uuid-perl**.

```changelog
libdata-uuid-perl (1.226-1) UNRELEASED; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.

  [ Joenio Marques da Costa ]
  * Import upstream version 1.226

 -- Joenio Marques da Costa <joenio@joenio.me>  Wed, 04 Nov 2020 21:52:38 +0100
```

Essa entrada deve estar no início do arquivo `debian/changelog`, para mais
detalhes sobre esse tema consulte as referências abaixo.

* [Debian Developer's Reference - NMUs vs team uploads](https://www.debian.org/doc/manuals/developers-reference/ch05.html#nmu-team-upload)
* [Debian Perl Group Policy - debian/changelog handling and versioning](https://perl-team.pages.debian.net/policy.html#debian%2Fchangelog_handling_and_versioning)

Commite as alterações do arquivo `debian/changelog`, adicione _"Gbp-Dch:
Ignore"_ na mensagem de commit, veja como ficou o commit no pacote
**libdata-uuid-perl**.

```git
commit effcb002bcee007ed5f14e9656495056b5f0de96 (HEAD -> master)
Author: Joenio Costa <joenio@joenio.me>
Date:   Wed Nov 4 22:14:44 2020 +0100

    team upload
    
    Gbp-Dch: Ignore
```

Veremos logo mais o motivo de adicionar _"Gbp-Dch: Ignore"_ na mensagem de
commit, por hora tenha em mente a importância de realizar commits atômicos a
cada alteração, declare exatamente o que foi feito de forma clara e direta.

## Arquivo debian/copyright

É importante verificar o _copyright_ de cada arquivo do projeto, isso costuma
mudar ao longo do tempo, passando entre mantenedores ou empresas e
instituições, é muito importante listar no arquivo `debian/copyright` o dono do
_copyright_ de cada arquivo do projeto, mantendo nomes e datas sempre
atualizados.

É possível utilizar ferramentas para auxiliar a identificar o _copyright_ dos
arquivos, o `grep` costuma ser uma boa opção por ser bastante simples e estar
sempre disponível.

<pre class="terminal">
<code>
grep -i -r --exclude-dir=.git --exclude-dir=debian copyright
</code>
</pre>

Eu costumo usar o comando acima e verifico manualmente os arquivos que me
chamam atenção, mas caso você queira algo mais automatizado verifique os links
abaixo para encontrar opções de ferramentas.

* [https://wiki.debian.org/CopyrightReviewTools](https://wiki.debian.org/CopyrightReviewTools)
* [BoF debian/copyright DebConf 2019](https://www.youtube.com/watch?v=2eOef3xdk5Y)

Se preferir adicione também o seu nome e endereço de email na seção _copyright_
dos arquivos `debian/*`, veja exemplo abaixo.

```
Files: debian/*
Copyright:
  © 2012-2013,2015, Jonas Smedegaard <dr@jones.dk>
  © 2020, Florian Schlichting <fsfs@debian.org>
  © 2020, Joenio Marques da Costa <joenio@joenio.me>
```

Mais uma vez, não esqueça de realizar commits para cada alteração lógica.

<pre class="terminal">
<code>
git commit -a -m 'Add myself to d/copyright'
</code>
</pre>

**Atenção!** Caso o _upstream_ não forneça informação sobre o _copyright_ de
algum arquivo adote o padrão [Berne Convention Comment][berne-convention].

## Arquivo debian/control

O arquivo `debian/control` é um dos principais arquivos de um pacote Debian, é
preciso estar atento especialmente à versão do `debhelper-compat`, deve-se
atualizar para a versão mais recente disponível. Cada versão traz mudanças na
forma de manter os pacotes, a versão **12**, por exemplo, simplificou a
manutenção do pacote permitindo remover o arquivo `debian/compat` e a
dependência ao `debhelper`, mantendo num único local a definição sobre qual
política Debian o pacote segue.

Então a primeira alteração comum no `debian/control` é atualizar a versão do
`debhelper-compat`.

```config
Build-Depends: debhelper-compat (= 13),
```

Remova também o arquivo `debian/compat` e a declaração de dependência ao pacote
`debhelper`, caso existam.

Verifique o [guia do mantenedor][dother-compat] para informações sobre o
significado do campo `compat` e a _manpage_ do `debhelper` na seção _NÍVEIS DE
COMPATIBILIDADE_ para encontrar a versão mais recente estável sugerida.

Seguindo a boa prática de commits atômicos e descritivos commite a alteração
sobre o `debhelper-compat`.

<pre class="terminal">
<code>
git commit -a -m 'update debhelper compat to 13'
</code>
</pre>

Ainda no arquivo `debian/control` atualize o pacote para a versão mais recente
da [Debian Policy][debian-policy], você pode verificar a versão mais recente
consultando o pacote `debian-policy`, edite o `debian/control` e atualize o
campo `Standards-Version`.

```
Standards-Version: 4.5.0
```

Commit!

<pre class="terminal">
<code>
git commit -a -m 'declare compliance with Debian Policy 4.5.0'
</code>
</pre>

É importante também revisar cada dependência declarada no `debian/control` e
analisar se ainda são válidas, eventualmente o _upstream_ alterou as
dependências, incluiu novas, removeu antigas, etc... é muito importante manter
apenas as dependências válidas.

Além de manter apenas as dependências válidas é boa prática manter a lista de
dependencias em ordem alfabética, eu costumo utilizar o próprio editor **vim**
para ordenar listas, seleciono a lista de dependencias e executo o comando
`:!sort`.

Commite as alteração adicionando _"Gbp-Dch: Ignore"_ na mensagem de commit, use
na mensagem de commit algo como _"(Re-)sort dependencies"_, veja no exemplo
abaixo o diff dessa correção para o pacote **libemail-mime-contenttype-perl**.

```diff
commit 70126faa23ad439d978f9d60c37d6cdd7ac6bee3

    (Re-)sort dependencies
    
    Gbp-Dch: ignore

diff --git a/debian/control b/debian/control
index 4b9399a..0f1406e 100644
--- a/debian/control
+++ b/debian/control
@@ -6,8 +6,8 @@ Section: perl
 Build-Depends: debhelper-compat (= 13)
-Build-Depends-Indep: perl,
-                     libtext-unidecode-perl <!nocheck>
+Build-Depends-Indep: libtext-unidecode-perl <!nocheck>,
+                     perl
 Standards-Version: 4.5.0
```

Adicione ao `debian/control` a seguinte definição também.

```config
Rules-Requires-Root: no
```

Exemplo dessa alteração para o pacote **libemail-mime-contenttype-perl**.

```diff
diff --git a/debian/control b/debian/control
index deacdf8..cbe1d0e 100644
--- a/debian/control
+++ b/debian/control
@@ -12,6 +12,7 @@ Standards-Version: 4.5.0
 Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libemail-mime-contenttype-perl.git
 Homepage: https://metacpan.org/module/Email::MIME::ContentType
+Rules-Requires-Root: no
 
 Package: libemail-mime-contenttype-perl
 Architecture: all
```

Commit!

<pre class="terminal">
<code>
git commit -a -m 'Set Rules-Requires-Root: no'
</code>
</pre>

Por fim, use o `cme` para analisar o `debian/control` e corriga os eventuais
alertas e problemas relatados.

<pre class="terminal">
<code>
cme check dpkg-control debian/control
</code>
</pre>

Durante a atualização do pacote **libemail-mime-perl**, por exemplo, o comando
acima deu a dica de que não é necessário definir a versão da dependência
_libemail-mime-encodings-perl (1.314)_ uma vez que a versão mais antiga nos
releases do Debian (oldoldstable, oldstable, stable, testing e unstable) são
superiores a **1.315** de forma que se torna desnecessário indicar a versão
dessa dependencia no `debian/control`.

Veja abaixo o diff dessa alteração realizada no pacote **libemail-mime-perl**.

```diff
commit 969faed0086947ae34025f8c09d59866cbf5ff18

    d/control: remove version from libemail-mime-encodings-perl (build & runtime)

diff --git a/debian/control b/debian/control
index e3c31ce..a371614 100644
--- a/debian/control
+++ b/debian/control
@@ -12,7 +12,7 @@ Build-Depends-Indep: perl,
                      libemail-mime-contenttype-perl (>= 1.022),
-                     libemail-mime-encodings-perl (>= 1.314),
+                     libemail-mime-encodings-perl,
                      libmodule-runtime-perl
@@ -29,7 +29,7 @@ Depends: ${misc:Depends},
          libemail-mime-contenttype-perl (>= 1.022),
-         libemail-mime-encodings-perl (>= 1.314),
+         libemail-mime-encodings-perl,
          libmodule-runtime-perl
```

## Arquivos em debian/patches/*

A pasta `debian/patches/` contém correções aplicadas ao código _upstream_
necessárias ao empacotamento, ao atualizar um pacote é necessário verificar se
as correções estão no _upstream_ e removê-los do pacote, caso contrário, os
arquivos de _patch_ devem ser atualizados.

Para remover um patch remova o arquivo de `debian/patches/` e sua referência de
`debian/patches/series`, por exemplo, o pacote **libemail-mime-perl** da versão
1.946 para 1.949 teve o patch `debian/patches/Allow-0-as-boundary-value.patch`
incorporado no _upstream_, como é possível ver no link abaixo.

* [https://github.com/rjbs/Email-MIME/issues/63](https://github.com/rjbs/Email-MIME/issues/63)

Dessa forma o patch deve ser removido do pacote.

<pre class="terminal">
<code>
git rm debian/patches/Allow-0-as-boundary-value.patch
</code>
</pre>

Além de remover o arquivo patch é necessário remover também a referência dele
no arquivo `debian/patches/series`.

```diff
diff --git a/debian/patches/series b/debian/patches/series
index d26b610..cdba319 100644
--- a/debian/patches/series
+++ b/debian/patches/series
@@ -1,2 +1 @@
 pod-whatis.patch
-Allow-0-as-boundary-value.patch
```

Feito isso basta commitar as mudança.

<pre class="terminal">
<code>
git commit -a -m 'Allow-0-as-boundary-value patch accepted on upstream'
</code>
</pre>

Isso deve ser feito para cada arquivo de _patch_ existente no pacote, caso
existam patches não incorporados no _upstream_ é necessário atualizá-los, para
atualizar os arquivos de patch usa-se o `quilt`

Primeiro instale o `quilt`.

<pre class="terminal">
<code>
sudo apt install quilt
</code>
</pre>

E então configure-o criando o arquivo de configuração `~/.quiltrc` com o
seguinte conteúdo.

```
QUILT_PATCHES=debian/patches
QUILT_DIFF_ARGS="--no-timestamps --no-index -pab"
QUILT_REFRESH_ARGS="--no-timestamps --no-index -pab"
```

> Veja as recomendações do Debian Perl Group sobre o uso do quilt em
> [Tips and Tricks](http://perl-team.pages.debian.net/howto/quilt.html#Tips_and_Tricks).

Para atualizar os arquivos de patch do pacote execute o seguinte.

<pre class="terminal">
<code>
quilt push
quilt refresh
</code>
</pre>

Commit!

<pre class="terminal">
<code>
git commit a -m 'refresh patches'
</code>
</pre>

## Arquivo debian/upstream/metadata

Uma das utilidades do arquivo `debian/upstream/metadata` é servir de fonte para
referências bibliográficas em trabalhos acadêmicos, possibilitando referenciar
corretamente os autores originais de um certo pacote presente no Debian.

É importante criar este arquivo caso ele não exista, o `dpt` pode ser utilizado
para isso.

<pre class="terminal">
<code>
dpt debian-upstream
</code>
</pre>

Caso o arquivo já exista é importante revisá-lo e garantir que ele tem os
metadados atualizados, consulte os dados nos arquivos `META.json` ou `META.yml`
e transfira para o `debian/upstream/metadata`.

> Consulte a _manpage_
> [dpt-debian.upstream](https://manpages.debian.org/stretch/pkg-perl-tools/dpt-debian-upstream.1.en.html)
> e leia a [Wiki do Debian sobre os Metadados
> Upstream](https://wiki.debian.org/UpstreamMetadata) para mais informações,
> propósito, histórico e sintaxe deste arquivo.

Commite as alterações.

<pre class="terminal">
<code>
git add debian/upstream/metadata
git commit -m 'd/u/metadata: update metadata'
</code>
</pre>

## Arquivo debian/watch

O arquivo `debian/watch` indica onde buscar novas versões _upstream_, use a
versão 4 do **uscan** neste arquivo e substitua expressões regulares pelas
strings especiais  `@ANY_VERSION@` e `@ARCHIVE_EXT@`.

Veja o exemplo abaixo do pacote **libapp-cell-perl**.

```diff
diff --git a/debian/watch b/debian/watch
index aa2bfb6..9132cd5 100644
--- a/debian/watch
+++ b/debian/watch
@@ -1,3 +1,3 @@
 # run "uscan --report" to check or "gpb import-orig --uscan" to update
 version=4
-https://metacpan.org/release/App-CELL .*/App-CELL-(\d[\d.]*)\.tar\.gz
+https://metacpan.org/release/App-CELL .*/App-CELL-@ANY_VERSION@@ARCHIVE_EXT@
```

Mais um exemplo do pacote **libemail-mime-contenttype-perl**.

```diff
diff --git a/debian/watch b/debian/watch
index b6f27cc..9ebe092 100644
--- a/debian/watch
+++ b/debian/watch
@@ -1,2 +1,2 @@
 version=4
-https://metacpan.org/release/Email-MIME-ContentType .+/Email-MIME-ContentType-([\d\.]+)\.(?:tar\.gz|tar|tgz)
+https://metacpan.org/release/Email-MIME-ContentType .*/Email-MIME-ContentType-v?@ANY_VERSION@@ARCHIVE_EXT@$
```

Commite as alterações.

<pre class="terminal">
<code>
git commit -a -m 'using uscan special strings on d/watch'
</code>
</pre>

## Arquivo debian/NEWS

Caso o pacote possua um arquivo `debian/NEWS.Debian` renomeie para
`debian/NEWS`.

## Arquivo debian/rules

Se o pacote usar `debhelper` **13** ou superior revise se o `debian/rules`
possui verificação explícita sobre o `DEB_BUILD_OPTIONS=nocheck` e remova essa
verificação uma vez que o `debhelper` **13** já faz isso automaticamente.

Mas se por algum motivo particular o pacote não puder usar `debhelper` **13**
ou superior então faça o pacote respeitar o parâmetro
`DEB_BUILD_OPTIONS=nocheck` adicionando verificação explícita, veja o diff
abaixo com essa alteração para o pacote **libanyevent-perl**.

```diff
+++ b/debian/rules
@@ -13,7 +13,9 @@ TEST_FILES = $(filter-out $(SKIP_TESTS), $(wildcard t/*.t))
        dh $@ -a
 
 override_dh_auto_test:
+ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
        PERL_ANYEVENT_LOOP_TESTS=1 xvfb-run -a dh_auto_test -- TEST_FILES="$(TEST_FILES)"
+endif
 
 override_dh_installexamples:
        dh_installexamples
```

Commite a alteração.

<pre class="terminal">
<code>
git commit -a -m 'debian/rules: honour DEB_BUILD_OPTIONS=nocheck'
</code>
</pre>

## Construir o pacote

Finalizada as correções mais comuns no pacote é hora _buildar_, é importante
fazer o _build_ do pacote num chroot limpo para evitar vícios do ambiente, como
pacotes e configurações específicas interferir no _build_.

Instale as dependências necessárias para constuir o pacote.

<pre class="terminal">
<code>
sudo apt install devscripts debhelper git-buildpackage
</code>
</pre>

Crie o sistema base do _chroot_ com a distribuição Debian Sid.

<pre class="terminal">
<code>
sudo pbuilder create
</code>
</pre>

Agora é possível construir o pacote dentro do chroot.

<pre class="terminal">
<code>
BUILDER=pbuilder git-pbuilder
</code>
</pre>

É importante manter o chroot sempre atualizado, então lembre de sempre executar
o comando abaixo antes de buildar um novo pacote.

<pre class="terminal">
<code>
sudo pbuilder update
</code>
</pre>

Se o pacote apresentar algum problema durante o _build_ é necessário investigar
e corrigir os erros e alertas, caso o pacote não apresente erros rode o Lintian
para verificar se há algo fora dos padrões, o `pbuilder` não executa o Lintian
automaticamente.

<pre class="terminal">
<code>
lintian -I
</code>
</pre>

Lembre de configurar o Lintian com as flags específicas para pacotes Perl, isto
inclui algumas verificações específicas definidas pelo Debian Perl Group, crie
o arquivo `~/.lintianrc` com o seguinte conteúdo.

```config
LINTIAN_PROFILE=pkg-perl
```

## Atualize o debian/changelog

Por fim, atualize o `debian/changelog`, lembra dos commits atômicos e de
algumas mensagens de commit com "Gbp-Dch: Ignore"? Pois aqui veremos a sua
utilidade, execute o seguinte.

<pre class="terminal">
<code>
gbp dch
</code>
</pre>

O `gbp` analisa os commits do pacote e atualiza o changelog automaticamente
ignorando aquelas mensagens com o comentário `Gbp-Dch: Ignore`, neste momento
altere também o cabeçalho do arquivo `debian/changelog` de **UNRELEASED** para
**unstable** e commit as alterações.

<pre class="terminal">
<code>
git commit -a -m 'update d/changelog'
</code>
</pre>

Ao alterar o cabeçalho para **unstable** estamos indicando ao time Debian Perl
Group que o pacote está pronto para upload, um DD irá revisar o trabalho e
realizar upload do pacote se estiver tudo ok, caso contrário o DD irá solicitar
correções no pacote colocando de volta o cabeçalho como **UNRELEASED** e
adicionará as solicitações de correção no próprio arquivo `debian/changelog`.

## Faça upload do pacote no repositório do time Debian Perl Group

Para escrever no repositório do Debian Perl Group é necessário fazer
parte do time, para isso é necessário seguir os seguintes passos.

* Criar uma conta no [Salsa][]
  * E configurar uma chave SSH associada ao perfil
* Solicitar ser adicionado ao grupo
  * Envie um email para debian-perl@lists.debian.org se apresentando brevemente
  * O email deve ser escrito em inglês e você deve indicar qual é seu username salsa

Atualize a configuração do `dpt` em `~/.dpt.conf` com seu TOKEN de acesso ao
[Salsa][salsa] para poder escrever nos repositórios do grupo e poder enviar as
atualizações do pacote.

```ini
DPT_PACKAGES=/home/joenio/src/pkg-perl/packages
DPT_SALSA_PRIVATE_TOKEN=***COPIE EU TOKEN AQUI***
```

Uma vez tendo usuário no Salsa e fazendo parte do time Debian Perl Group,
compartilhe o pacote enviando as atualizações para o repositório do grupo com o
seguinte comando.

<pre class="terminal">
<code>
dpt push
</code>
</pre>

Consulte o documento [upgrading to a new upstream release][upgrade-upstream]
para ter mais informações sobre atualização de pacotes Perl.

Bem, é isso, espero que este texto seja útil ajudando e incentivando mais
pessoas a empacotar software no Debian, uma atividade muito importante e que
requer um trabalho constante.

<small>
_a imagem utilizada no cabeçalho do post foi copiada de
[https://www.deviantart.com/mdh3ll/art/Debian-shot-311580879](https://www.deviantart.com/mdh3ll/art/Debian-shot-311580879)_
</small>

[linux]: http://www.kernel.org
[Debian]: http://debian.org
[cpan]: http://metacpan.org
[perl]: http://perl.org
[debian-perl-group]: http://perl-team.pages.debian.net
[pkg-perl-tools]: https://tracker.debian.org/pkg/pkg-perl-tools
[upgrade-upstream]: https://perl-team.pages.debian.net/git.html#upgrading_to_a_new_upstream_release
[myrepos]: https://myrepos.branchable.com
[Salsa]: https://salsa.debian.org
[perl-policy]: https://perl-team.pages.debian.net/policy.html
[berne-convention]: http://perl-team.pages.debian.net/copyright.html#Berne_Convention
[dother-compat]: https://www.debian.org/doc/manuals/maint-guide/dother.en.html#compat
[debian-policy]: https://www.debian.org/doc/debian-policy
