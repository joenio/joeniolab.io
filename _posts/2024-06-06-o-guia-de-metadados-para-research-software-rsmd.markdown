---
title: O Guia de metadados para Research Software (RSMD)
lead: >
  O Guia de metadados para Research Software (RSMD) é um abrangente conjunto de
  recomendações para melhoria dos princípios FAIR para Research Software (RS)
  criado no contexto do European Open Science Cloud (EOSC) para melhoria da
  qualidade e sustentabilide do RS.
---

O Guia de metadados para Research Software (do inglês: _[Research Software
MetaData Guidelines - RSMD][rsmd])_ é um abrangente conjunto de recomendações
para melhoria dos princípios FAIR aplicados a _Research Software (RS)_ no
contexto do _[European Open Science Cloud (EOSC)][eosc]_.

O guia foi desenvolvido como parte do projeto [FAIR-IMPACT][] e teve sua
[versão v1.0][rsmd-v1] publicada em 6 de março de 2024, representando uma
proposta inicial de padronização e formatos de metadados para descrição de RS,
o RSMD está disponível sob a licença _CC BY 4.0_ e pode ser citado da seguinte
forma:

> Gruenpeter, M., Granger, S., Monteil, A., Chue Hong, N., Breitmoser, E., Antonioletti, M., Garijo, D., González Guardia, E., Gonzalez Beltran, A., Goble, C., Soiland-Reyes, S., Juty, N., & Mejias, G. (2024). D4.4 - Guidelines for recommended metadata standard for research software within EOSC (V1.0). Zenodo. https://doi.org/10.5281/zenodo.10786147

A motivação para a criação do RSMD é o crescente reconhecimento ao papel que o
software tem para as pesquisas científicas, e a necessidade crescente dos
pesquisadores e desenvolvedores em descrever software de forma padronizada,
seja para arquivamento ou para publicação.

Diante disso e com objetivo de garantir que software seja arquivado,
referenciado, descrito e citado _(do inglês: archived, referenced, described
and cited - ARDC)_ e que os metadados sejam encontráveis, acessíveis,
interoperáveis e reusáveis _(do inglês: findable, accessible, interoperable,
and reusable - FAIR)_, o guia RSMD fornece um conjunto de recomendações para
descrição de Research Software, resumidas abaixo:

### Requisitos gerais de metadados

- RSMD-1.1: O software tem metadados incorporado no código-fonte?
- RSMD-1.2: O projeto de software tem metadados registrados publicamente em plataformas acadêmicas?
- RSMD-1.3: O software está disponivel em um sistema de controle de versão (VCS)?
- RSMD-1.4: O software segue as práticas e padrões da comunidade onde está inserido?
- RSMD-1.5: Os metadados em formato estruturado, legível por máquina, estão num único arquivo?

### Acessibilidade e preservação

- RSMD-2.1: O código-fonte do software está preservado no arquivo universal de código-fonte [Software Heritage](/o-arquivo-universal-de-codigo-fonte-software-heritage)?
- RSMD-2.2: O software está arquivado em algum repositório acadêmico?
- RSMD-2.3: O projeto está registrado em algum diretório on registro para metadados de software?

### Referência e identificação

- RSMD-3.1: As versões do software estão bem documentadas?
- RSMD-3.2: Fragmentos específicos de código ou algoritmos podem ser identificados (intrinsic identifiers)?
- RSMD-3.2: Arquivos específicos do código ou diretórios podem ser identificados (intrinsic identifiers)?
- RSMD-3.2: Existem identificadores diferentes para versão ou lançamento do software (intrinsic identifiers)?
- RSMD-3.3: Lançamentos e versões diferentes possuem identificadores únicos (extrinsic identifiers)?
- RSMD-3.3: O projeto pode ser identificado com um identificador único (extrinsic identifiers)?
- RSMD-3.3: Um módulo específico do software poder ser identificado (extrinsic identifiers)?
- RSMD-3.4: Existe algum padão ou esquema de versionamento sendo usado?
- RSMD-3.5: É possível identificar diferentes níveis de granularidade do software?

### Descrição e classificação

- RSMD 4.1: As informações como nome e descrição do software estão disponíveis no arquivo _README_ ou similar?
- RSMD 4.2: O registro de metadados contém dados como nome, descrição, domínio de aplicação, linguagem, data, versão, etc...?
- RSMD 4.3: É possível acessar artigos descrevendo o software via identificadores persistentes ou ao menos numa URL estável?
- RSMD 4.4: Os metadados intrínsecos contendo a descrição do software é legível por máquina?
- RSMD 4.5: O arquivo _README_ fornece informações adicionais, como por exemplo, histórico, funcionalidades, estado de desenvolvimento, etc?

### Crédito e atribuição

- RSMD-5.1: As informações sobre os autores estão disponíveis intrinsicamente nos metadados do código?
- RSMD-5.2: As informações sobre os autores estão disponíveis no registro extrínseco de metadados?
- RSMD-5.8: A lista de autores é uma lista exaustiva ou um autor coletivo é usado?
- RSMD-5.3: Identificador único é usado para pessoas (exemplo: ORCID)?
- RSMD-5.4: Os papéis dos autores e contribuidores estão descritos?
- RSMD-5.5: Uma forma de citação preferencial é oferecida?
- RSMD-5.6: O software é citado explicitamente e incluído na literatura bibliográfica?
- RSMD-5.7: Se aplicável, o artigo citando o software, usa a granularidade apropriada?

### Reuso, licenciamento e aspectos legais

- RSMD-6.1: Antes de definir uma licença, os donos dos direitos do software foram verificados?
- RSMD-6.2: A informação sobre licença está disponível do código-fonte (intrinsic metadata)?
- RSMD-6.6: Os seguintes atributos estão disponíveis no código-fonte jutamente com a licença: nome do software, ano, dono do copyright, informação de contato, licença?
- RSMD-6.3: A informação de licença está disponível no registro de metadados?
- RSMD-6.4: Os módulos de software externos usados no software estão identificados com seus autores e licença?
- RSMD-6.5: As versões e contribuições ao software são rastreáveis (VCS)?

### Dependências e ambiente de execução

- RSMD-7.1: As dependências do software estão documentadas?
- RSMD-7.2: Os requisitos do sistema operacional e do ambiente para execução do software estão documentados?
- RSMD-7.3: Os requisitos de hardware e as plataformas relevantes de hardware estão documentadas?
- RSMD-7.4: As instruções de compilação (build) estão disponíveis?
- RSMD-7.5: A documentação de usuário está disponível?
- RSMD-7.6: Os dados usados ou produzidos pelo software estão documentados ou disponíveis?

Este conjunto de recomendações, divididos em sete aspectos, representam ações
práticas para a melhoria da qualidade e sustentabilide do software produzido e
utilizado em pesquisas científicas, podendo ser adaptados ao contexto
particular de cada organização, mais informações sobre o guia RSMD pode ser
encontrada nos links abaixo:

- [Repositório GitHub com código fonte do RSMD](https://github.com/FAIR-IMPACT/RSMD-guidelines)
- [Página HTML do RSMD no GitHub pages][rsmd]
- [Versão 1.0 do RSMD publicado no Zenodo][rsmd-v1]
- [FAIR-IMPACT - Metrics for software](https://fair-impact.eu/metrics-software)

[rsmd]: https://fair-impact.github.io/RSMD-guidelines
[eosc]: https://open-science-cloud.ec.europa.eu
[fair-impact]: https://fair-impact.eu
[rsmd-v1]: https://doi.org/10.5281/zenodo.10786147
