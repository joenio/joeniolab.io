---
title: Poetry Attack 01 - ICLC 2023
layout: image-gallery
lead: >
  Informacoes e fotos da performance Poetry Attack 01 apresentada no ICLC 2023
  em Utrecht - Holanda por Joenio M Costa e Mari Moura.
---

**Poetry Attack 01** eh uma performance artistica composta com arte sonora,
musica, arte visual e arte performance (body art), e faz uso de Live Coding
atraves de ferramentas como Sonic Pi, Tidal Cycles, Super Collider, dublang e
outras.

Site:
- [poetryattack.4two.art](https://poetryattack.4two.art)

Autores:
- [Mari Moura](https://marimoura.4two.art)
- Joenio Marques da Costa

Exibicoes:
- Poetry attack 01 (Algorave Brasil), 2022
- [Poetry attack 01 (ICLC)](https://iclc.toplap.org/2023/catalogue/performance/poetry-attack-01.html), 2023
- [Poetry Attack 01 (tour23)](https://tour23.4two.art), 2023

Codigo fonte:
- [iclc23-poetry-attack-01](https://gitlab.com/joenio/iclc23-poetry-attack-01)

Fotos e videos:
- [Instagram - Poetry attack (Utrecht, 2023)](https://www.instagram.com/reel/CuSa2-0r5Yv)
- [Instagram - Poetry attack (Itacimirim, 2023)](https://www.instagram.com/reel/CuXFzTmghdz)
- [Instagram - Poetry attack (Salvador, 2023)](https://www.instagram.com/reel/CuJEJ8msADC)
