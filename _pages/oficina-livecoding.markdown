---
title: Oficina de Live Coding para iniciantes
---

- Duracao: 4h

Nesta oficina serão dmonstrados conceitos teóricos e práticos de Live Coding atraves de exercicios praticos com Sonic Pi, SuperCollider e Tidal Cycles.

Agenda:

- [Do sintetizador em hardware ao software, ou softsynths](/electronic-music-with-debian): 30 min
- [Definicao e historia do Live Coding](/live-coding) e a [A importancia do software livre](/software-livre): 30 min
- Apresentar exemplos de ferramentas de Live Coding: 15 min
- [Como instalar e configurar Sonic Pi](/sonic-pi): 30 min
- [Explorando o Sonic Pi](/sonic-pi): 30 min
- [A linguagem do Sonic Pi](/sonic-pi): 30 min
- Dúvidas, papo aberto e curiosidades: 30 min
- Extra: DAW, Audacity, LMMS, Ardour, LADSPA plugin, sox, Mixxx
- Extra: Jack, PipeWire, OSC, MIDI
- Extra: Como gerenciar os codigos e [Primeiros passos com Git](/git) + Gitlab, Github...: 30 min
- Extra: [Como instalar Tidal Cycles e primeiros passos](/debconf22-live-coding-workshop)
