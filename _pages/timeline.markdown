---
title:  Linha do tempo
---

_A linha do tempo abaixo descreve minha formação educacional, atuação
profissional, cultural e artística ao longo dos anos._

## 1994

* Conclusão do Ensino Fundamental (1º grau) na Escola Estadual José de Freitas Mascarenhas (EEJFM) em Camaçari, Bahia, Brasil.

## 1995

* Início do Ensino Médio (2º grau) no Centro Educacional Miguel Alves (CEMA) em Camaçari, Bahia, Brasil. Ano de conclusão: 1997.

## 1999

* Curso técnico/profissionalizante "Técnico em Informática" no instituto Pólo União de Ciências (PUC) na cidade de Camaçari, Bahia, Brasil. Concluido em 2000.

## 2000

* Inicio da graduação em "Bacharelado em Informática" na Universidade Católica do Salvador (UCSAL), Salvador, Bahia, Brasil. Ano de conclusão em 2009.<br/>
Trabalho final (monografia): [Extração de Informações de Dependência entre Módulos de Programas C/C++](http://wiki.dcc.ufba.br/Aside/ProjetoFinalJoenioCosta).<br/>
Criação do software livre [Doxyparse](https://github.com/doxygen/doxygen/tree/master/addon/doxyparse) (C++) e refatoração da ferramenta de análise estática de código-fonte [Analizo](http://www.analizo.org) (Perl).<br/>

* Participação no seminário "III Semana de Mobilização Científica" (SEMOC) na Universidade Católica do Salvador (UCSal) no mini-curso "Introdução a Redes ATM".

## 2001

* Primeira experiência profissional (voluntária) em escola de informática atuando com manutenção de computadores, impressoras e auxiliando na produção de material didático. Duração: 6 meses.

* Formação do primeiro projeto musical, a banda [Los Benzenos](http://losbenzenos.gitlab.io) na cidade de Camaçari, Bahia, com repertório entre músicas autorais e covers, uma das últimas apresentações foi realizada no "Dia Municipal do Rock Camaçari" abrindo o show da banda Plebe Rude.

## 2002

* Estagiário na Delegacia da Receita Federal de Camaçari realizando suporte técnico, admisnitração de servidores e estações Windows 2000, manutenção de hardware PC, atendimento a usuários, desenvolvimento de shell script para administração em Gnu/Linux Slackware. Conclusão: 2004.<br/>
Desenvolvi solução utilizando a ferramenta [g4u - Harddisk Image Cloning for PCs](http://www.fehu.org/~feyrer/g4u) para automatizar diversas tarefas de preparação de dekstops Windows 2000 que até então eram realizadas manualmente. Duração: 2 anos.

* Participação no mini-curso "Introdução à programação paralela" durante a oficina "V Semana de Mobilização Científica" (SEMOC) na UCSal, Salvador, Bahia, Brasil.

## 2004

* Estágio nos correios, Empresa Brasileira de Correios e Telégrafos, Salvador, Bahia , Brasil, trabalhando como programador desenvolvendo sistemas Web em DHTML, JavaScript, ColdFusion, Macromedia Dreamweaver MX, WebServices VB.Net, VisualStudio .Net e Microsoft SQL Server. Conclusão: 2005.<br/>
Desenvolvi em ColdFusion a primeira versão do sistema para gestão interna das cartas enviadas para o programa [Papai Noel dos Correios](http://blog.correios.com.br/papainoeldoscorreios).

## 2005

* Trabalho na empresa [Lebre Tecnologia e Informática](http://www.lebre.com.br) com desenvolvimento C#, VisualStudio .NET e tecnologias Microsoft. Duração: 3 meses.

* Trabalho de tempo integral como programador na empresa [JáCotei.com.br](https://www.jacotei.com.br) em Salvador, Bahia, Brasil, com desenvolvimento de software para coleta de dados via WebSpider, Web Crawler e Robots escritos em Perl e banco de dados Oracle, além de realizar manutenção no frontend da aplicação em ColdFusion. Conclusão: 2007.<br/>
Refatorei totalmente os scripts Perl de coleta e transformação de dados em uma arquitetura orientada a objetos utilizando as boas práticas de programação OO da linguagem Perl.

* Participação no "6º Fórum Internacional Software Livre" (FISL) realizado no Centro de Eventos da PUCRS, Porto Alegre, RS, onde participei do encontro da comunidade Perl YAPC::Brasil 2005 ao qual ajudei a organizar.

## 2006

* Organização do encontro sulamericano da comunidade Perl [YAPC::SA::2006](https://web.archive.org/web/20101122133228/http://www.perl.org.br/YAPC/SA2006/WebHomeEn) realizado durante o congresso CONISLI, desenvolvi e mantive o site do evento. Não estive presente no evento, apenas atuei na organização e divulgação.

* Organizei junto a comunidade baiana de software livre, [Projeto Software Livre Bahia](http://wiki.dcc.ufba.br/PSL/WebHome) (PSL-BA) o [3º Festival de Software Livre da Bahia](https://web.archive.org/web/20170925100801/http://wiki.softwarelivre.org/Festival3) realizado na UNIME em Lauro de Freitas, Bahia, Brasil.

* Trabalho realizado na organização da sociedade civil pela defesa dos direitos humanos na internet [SaferNet Brasil](https://safernet.org.br) como desenvolvedor de software trabalhando especialmente com Perl na manutenção de diversos sistemas de análise de dados, geração de relatórios e indicadores do portal da entidade. Conclusão: 2007.

* Fundei a [Cooperativa de Tecnologias Livres](https://colivre.coop.br) (Colivre) na cidade de Salvador, Bahia, empresa cooperativa dedicada ao desenvolvimento de soluções com software livre, fundada no espírito da cooperação e economia solidária. Conclusão: 2015.<br/>
Realizei desenvolvimento HTML, JavaScript, Ruby on Rails, Perl, administração de servidores Debian GNU/Linux, Banco de dados MySQL, PostgreSQL, gestão de projetos e coordenação de equipes, métodos ágeis, scrum, XP, coordenação de equipes em projetos de desenvolvimento de software, especificação e estimativas de novos projetos, direção e administração, gestão administrativa, controle e revisão financeira, desenvolvimento Wiki, desenvolvimento Ruby on Rails, administração de servidores Linux, implantação de servidor LDAP; administração de sistema de abertura de chamados RT, entre outras atividades.

* Participação no congresso "7º Fórum Internacional Software Livre" onde participei como organizador e palestrante do encontro da comunidade Perl [YAPC::Brasil 2006](https://web.archive.org/web/20101122184617/http://www.perl.org.br/bin/view/YAPC/YAPCBrasil2006).<br/>
[Relato colaborativo sobre o evento YAPC::Brasil 2006](https://web.archive.org/web/20110902102902/http://wiki.softwarelivre.org/bin/view/CoberturaWiki/Post20060419180822).

* Publiquei um módulo Perl no CPAN como software livre para consulta e coleta de dados bibliográficos na livraria Siciliano [WWW::Scraper::ISBN::Siciliano_Driver](https://metacpan.org/release/JOENIO/WWW-Scraper-ISBN-Siciliano_Driver-0.01).

## 2007

* Escrevi tutoriais sobre Perl num período bastante ativo em listas de discussão da comunidade, entre os textos técnicos sobre Perl estão: [Como criar arquivos PDF utilizando Template-Toolkit 2007](https://web.archive.org/web/20120522141635/http://www.perl.org.br/Artigos/ComoGerarPFDComTemplateToolkit), [Fazendo um HelloWorld falar vários idiomas 2007](https://web.archive.org/web/20130315182359/http://perl.org.br/Artigos/ComoImplementarSuporteL10N) e [Testes de Software em Perl](https://web.archive.org/web/20101225211923/http://www.perl.org.br/Artigos/TestandoSoftwaresEmPerl).<br/>
Também coordenei e colaborei num grande esforço de trabalho de [tradução da documentação Perl para o português Brasileiro](https://web.archive.org/web/20121105202824/http://perl.org.br/Perldoc/WebTranslation).

* Coordenei o Install Fest durante o [III Festival de Software Livre da Bahia](https://web.archive.org/web/20170925100801/http://wiki.softwarelivre.org/Festival3) na Unime em Lauro de Freitas, Bahia, e fui também instrutor do mini-curso "Perl: Coisas fáceis mais fáceis e coisas difíceis possíveis".

* Fiz parte da fundação da comunidade Perl local [Salvador Perl Mongers](https://web.archive.org/web/20081017060157/http://www.perl.org.br/Social/Salvador).

## 2008

* Participação no evento "Congresso Internacional de Software Livre 2008" (Conisli) onde participei do encontro nacional da comunidade Perl [YAPC::Brasil  2008](https://web.archive.org/web/20101122113042/http://www.perl.org.br/YAPC/BR2008/).

* Participei do congresso Rails Summit Latin America 2008 realizado em São Paulo organizado pela empresa Localweb.

* Início da dedicação exclusiva à Colivre me aprofundando em desenvolvimento Ruby e Perl colaborando especialmente com os projetos de software livre [Foswiki](https://foswiki.org) (Perl) e [Noosfero](https://gitlab.com/noosfero/noosfero) (Ruby on Rails).

## 2009

* Participação no "10º Fórum Internacional de Software Livre" como expositor no stand da empresa cooperativa Colivre.

* Organização do encontro sulamericano da comunidade Perl [YAPC::SA::2009](https://web.archive.org/web/20120222215214/http://www.perl.org.br/YAPC/SA2009/) colaborando especialmente na criação do site do evento, organização e divulgação.

* Formação no "Curso para conselheiros fiscais com ênfase em auditoria interna" realizado em Salvador, BA pela OCEB e SESCOOP/BA como sócio cooperado da empresa Colivre.

* Escrita e postagens em blog pessoal sobre:
[Facilitando o download de legendas no Legendas.tv](/facilitando-o-download-de-legendas-no-legendas.tv),
[Relato sobre o Rails Summit (e CONISLI) 2008](/relato-sobre-o-rails-summit-e-conisli-2008),
[Enviando notificações do Nagios3 via Jabber](/enviando-notificacoes-do-nagios3-via-jabber),
[Extraindo estatística contribuições por linha de código usando git-blame](/extraindo-estatistica-contribuicoes-por-linha-de-codigo-usando-git-blame) e
[Primeiro dia de Fisl 10](/primeiro-dia-de-fisl-10)

* Curso de inglês "English Course" na escola FISK do módulo "Spreading Wings" em Camaçari, Bahia.

* Participação no mini-curso "Gerenciamento ágil de projetos com SCRUM" realizado no evento Maré de Agilidade 2ª edição no Swell Salvador na faculdade Ruy Barbosa.

## 2010

* Criação da banda "Estado de Sítio" na cidade de Camaçari, curta participação na banda "Choque Frontal", também em Camaçari e retorno às atividades com a banda "Los Benzenos".

* Textos técnicos (e não técnicos) em blog pessoal:
[Rodando Cucumber + Selenium no Debian sid](/rodando-cucumber-selenium-no-debian-sid),
[Novamente, problemas com Cucumber e Selenium](/novamente-problemas-com-cucumber-e-selenium),
[4 meses sem comer carne](/4-meses-sem-comer-carne),
[Deixando o bash um pouco mais amigável](/deixando-o-bash-um-pouco-mais-amigavel) e
[AWStats, Varnish e virtualhosts](/awstats-varnish-e-virtualhosts).

* Publicação de trabalho acadêmico completo no [Congresso Brasileiro de Software: Teoria e Prática](http://wiki.dcc.ufba.br/CBSOFT) (CBSoft.2010) realizado em Salvador, Bahia com o título: [Analizo: an extensible multi-language source code analysis and visualization toolkit](http://www.analizo.org/publications/analizo-cbsoft2010-tools.pdf) e participação no "XXIV Simpósio Brasileiro de Engenharia de Software" (SBES) e "IV Simpósio Brasileiro de Componentes, Arquitetura e Reutilização de Software" (SBCARS).

* Apresentação de palestra "Foswiki - Uma plataforma wiki livre para gestão de conteúdo" durante o [YAPC::Brasil 2010](https://web.archive.org/web/20120304193806/http://www.perl.org.br/YAPC/BR2010/WebHome).

* Participação como orador nos casos de inovação no evento "6º Info UNEB" realizado na Universidade do Estado da Bahia (UNEB).

* Primeira contribuicao com o Debian empacotando modulos Perl, [maintainer dashboard](https://udd.debian.org/dmd/?email1=joenio%40joenio.me&email2=joenio%40colivre.coop.br&email3=joenio%40perl.org.br&packages=&ignpackages=&format=html)

## 2011

* Realização da palestra "Colivre, empreendendorismo e autogestão" em formato de oficina durante a "10º OID - Oficina de Inclusão Digital" em Vitória, ES, Brasil.

## 2012

* Consultoria no "Cartório do 1º Ofício de Registro de Imóveis" em Camaçari, Bahia com atividades de implantação de backup em nuvem, intranet, configuração de firewall pfSense, administração de banco de dados MySQL, desenvolvimento de site, solução de email, configuração de domínio NT em servidor Linux, apoio em comunicação entre equipes de trabalho e fornecedores de sistemas, treinamento técnico em ferramentas da atividade final do cartório, pesquisas de preço, orçamento e compras. Conclusão: 2016.

* Realização da atividade "Oficina Blogoosfero: Plataforma livre para autonomia da blogosfera brasileira" no evento Conexões Globais 2.0 na Casa de Cultura Mario Quintana, Porto Alegre, RS, durante o Fórum Social Temático.<br/>
Participação do encontro "Oficina sobre os princípios para uma rede social" do Fórum Social Mundial no III Fórum de Mídia Livre.

* Participação no evento "Palestra Paixão e Trabalho" com carga horária de 3 horas em Novembro pela empresa Connect RH Soluções Empresariais em Camaçari, Bahia.

## 2013

* Realização da atividade "Oficina sobre desenvolvimento de plugins para Noosfero" e participação do "Encontro da comunidade Noosfero" no Latinoware 2013.

* Colaborador do workshop de desenvolvimento em "Ruby on Rails" no [Rails Girls Salvador](http://railsgirls.com/salvador201310.html).

* Consultoria no "Cartório de Mata de São João" em Mata de São João, Bahia realizando pesquisas e orçamento de equipamentos de infra-estrutura de rede, implantação de rede local, interligação de redes site-to-site entre escritórios distintos via internet com VPN. Conclusão: 2016.

* Palestra "Empreendedorismo com tecnologias livres: Construindo um futuro inovador" apresentada na Semana de Ciência e Tecnologia do Instituto Federal da Bahia (IFBA) Jacobina, Bahia.

## 2014

* Consultoria técnica para o "Programas das Nações Unidas para o Desenvolvimento no Brasil" (PNUD) na especificação da construção dos códigos das metodologias de organização da informação e interação participativa do portal de participação social Participa.br, desenvolvido utilizando a ferramenta Noosfero para a "Secretaria Geral da Presidência da República" em Brasília realizando desenvolvimento de software, especificação e design de funcionalidades, gestão de equipes, estudos sobre algoritmos, plataformas e modelos de participação social. Duração: 11 meses.<br/>
  Sigla e Título do Projeto: BRA/12/018 – Desenvolvimento de Metodologias de Articulação e Gestão de Políticas Públicas para Promoção da Democracia Participativa<br/>
  Agência Executora Nacional: Secretaria-Geral da Presidência da República

* Mestrado no [Programa de Pós-graduação em Ciência da Computação](http://pgcomp.dcc.ufba.br) (PGCOMP) na Universidade Federal da Bahia (UFBA) com a pesquisa sobre "Sustentabilidade Técnica de Software Acadêmico no Domínio de Ferramentas de Análise Estática". Ano de conclusão: 2017.<br/>

## 2015

* Pesquisador no "Instituto Brasileiro de Informação em Ciência e Tecnologia" (IBICT) como consultor sobre Noosfero e os portais Participa.br e Juventude.gov.br realizando desenvolvimento de software, especificação e design de funcionalidades, interlocução e gestão de equipes, capacitação de usuários na plataforma Participa.br, revisão e controle de qualidade de código-fonte. Conclusão: Julho 2018.

* Publicação em blog pessoal: [Backup na nuvem com Tarsnap](/backup-na-nuvem-com-tarsnap) e [Aprenda a criar repositórios de pacotes Debian](/aprenda-a-criar-repositorios-de-pacotes-debian).

## 2016

* Desenvolvedor sênior no projeto "Evolução do [Portal do Software Público Brasileiro](https://softwarepublico.gov.br): pesquisa e desenvolvimento para uma nova geração integrada de plataformas abertas e colaborativas" realizando atividade de analista de desenvolvimento de sistemas e pesquisas sobre mecanismos e ferramentas inovadoras destinadas a gerir participações sociais por meio de redes sociais no contexto do Governo Brasileiro, garantia e controle de qualidade, revisão de código-fonte e gestão de equipes. Conclusão do projeto: 2017. 

* Pesquisas em arte e tecnologia, especialmente sobre música eletrônica e live coding, realização do evento [Oficina Live Coding](https://www.youtube.com/watch?v=mXW13ccuMIo) durante o evento #MusicHacking no [Calango Hacker Clube](http://calango.club) em Brasília, DF, sobre a linguagem de programação musical TidalCycles.

## 2017

* Consultoria para "Secretaria de Cultura DF", Brasília no projeto "Desenvolvimento e evolução de software para gestão de editais" em Python e Django. Duração: 6 meses.

* Consultor sênior no projeto "Ecossistemas de Software Livre" em parceiria do Lappis com Ministério da Cultura para pesquisa e prática usando técnicas e metodologias de desenvolvimento de software para aferição qualidade produto de software em ambiente experimental do Laboratório Avançado de Pesquisa, Produção e Inovação em Software (LAPPIS). Líder técnico no projeto "Ecossistemas de Software Livre" em parceiria com o Ministério da Cultura realizando pesquisas sobre DevOps e deploy contínuo utilizando containers Docker e Kubernetes. Conclusão: 2019.

* Consultoria, desenvolvimento de software Ruby e Javascript, análise de desempenho Juster.com.br (descontinuado).

## 2018

* Publicação de trabalho acadêmico completo no [XXXII Simpósio Brasileiro de Engenharia de Software](http://cbsoft2018.icmc.usp.br/#/sbes) (SBES) realizado em São Carlos, São Paulo, Brasil com o título: [On the sustainability of academic software: the case of static analysis tools](https://doi.org/10.1145/3266237.3266243).

* Performances de arte com música eletrônica e Live Coding na Galeria DeCurators, Brasília, DF, Brasil em [Performance Sonora Ciclo Curare](https://www.youtube.com/watch?v=B7tBh2AzdKE) colaborando com Philip Jones e Eufrasio Prates na exposição Objetos-poema de Luciana Ferreira.

* Apresentação como DJ no evento [La Pauta edição especial Michael Jackson](https://www.facebook.com/events/217880179063575/), [La Pauta edição Freddie Mercury](https://www.facebook.com/events/378889122855345) e [La Pauta edição David Bowie](https://www.facebook.com/events/2353637744884453/) realizados no Sindicato dos Jornalistas, Brasília, DF, Brasil.

* Organizei e coordenei a [Oficina de edição de imagem e vídeo com software livre](/edicao-de-imagem-e-video-com-software-livre) no Calango Hacker Clube, Brasília, DF.

* Participei e organizei o evento [Hackathon de programação musical e Live Coding](https://www.facebook.com/medialabunb/photos/a.266813103774890/554461135010084/) durante o #17.ART no Anexo do Museu Nacional, Brasília, DF, Brasil em colaboração com o laboratório Lappis, MediaLab UnB e Calango Hacker Clube.

* Professor voluntário na Universidade de Brasília (UnB) unidade Gama no curso de Engenharia de Software auxiliando na disciplina Gerência de Configuração e Evolução de Software (GCES).

* Idealizei e coordenei o primeiro ano do evento [MiniLappisConf](https://minilappisconf.lappis.rocks), uma atividade realizada semanalmente no laboratório Lappis para troca de conhecimento, divulgação, treinamento e experimentação, tendo gerenciado a participação e convite de palestrantes externos para falar sobre tecnologias inovadoras e desenvolvimento de software livre.

* Realizei no Calango Hacker Clube a [Oficina de Gitlab para iniciantes](/oficina-gitlab) e a [Oficina de Shell Script](/oficina-shellscript).

* Treinamento e workshop sobre "Chaves GnuPG" para educação e popularização de Ciência e Tecnologia no Brasil.

* Colaborador na organização do [Django Girls Brasília 2018](https://djangogirls.org/brasilia1).

* Organização do evento [MicroDebConf Brasília 2018](https://www.collabora.com/news-and-blog/blog/2018/10/02/microdebconf-brasilia-2018) realizado na UnB unidade Gama.

* Instrututor em curso "Treinamento de programação Ruby" para iniciantes em curso de curta duração para a equipe de tecnologia da Presidência da República.

* Banca de conclusão de curso de graduação do aluno Victor Henrique Magalhães Fernandes que realizou o trabalho "Um estudo empírico sobre métricas de código fonte do Android API Framework" no curso de Graduação em Engenharia de Software na Universidade de Brasília.

* Capitulo do livro "Participação social no software Noosfero", publicado por IBICT- [Participação social no software Noosfero](https://ridi.ibict.br/handle/123456789/1111)

## 2019

* Organização do evento [Colônia de Férias MediaLab (Laboratório Aberto) UnB Brasília 2019](https://www.facebook.com/events/621269571665790/) junto a pesquisadores do laboratório MediaLab do Instituto de Artes Visuais.

* Composição e lançamendo do álbum de música experimental ["}bio{borgs"](/bioborgs) e produção do vídeo oficial do álbum, em parceiria com Biophillick e Eufrasio Prates.

* Obra de arte visual de vídeo remix [Atari 2600 Vídeo Remix](/atari-2600-video-remix), envolvendo animação, colagens, edição e produção sonora.

* Performance musical e visual com o grupo [Nômade Lab](https://nomadelab.gitlab.io) durante o evento [Brasília Mapping Festival](https://www.brasiliamapping.com.br) realizado no Museu da República, Brasília.

* Participação na conferência internacional de desenvolvedores Debian [DebConf 19](https://debconf19.debconf.org) com apresentação da palestra [Electronic Experimental Music with Debian](https://debconf19.debconf.org/talks/87-electronic-experimental-music-with-debian).

* Performance e instalação [INTERFACES COMPUTACIONAIS AFETIVAS](https://www.facebook.com/events/1928004933992860/) com o Medialab UnB no Sesc DF, Gama, DF, Brasil, onde realizei performance com a linguagem de programação musical TidalCycles e o dispositivo [Teremim Aoristo](https://gitlab.com/musica-interativa/teremim-aoristo).

* Performance com a [Orquestra de Laptops de Brasília](http://bsblork.org) (BSBLOrk) na abertura da exposição "VOA BRASÍLIA" realizada na Galeria ArquiBrasília, Brasília, DF, Brasil e exposição de trabalho de vídeo-remix [Atari 2600 Vídeo Remix](/atari-2600-video-remix).

* Lançamento do álbum de música eletrônica experimental de Live Coding com TidalCycles e visual music [helloworld.tidal](http://joenio.me/helloworld.tidal).

* Colaboração no álbum coletânia [Las Plantas Sagradas Y La Rosa Negra: Composiciones Para Oferendas (Collaborative Fusion Orchestra)](http://joenio.me/las-plantas-sagradas).

* Apresentação do artigo "Demo Art e Visual Music em perspectiva" apresentado no evento [#18.ART](https://18art.medialab.ufg.br/brasil) sobre demoscene e suas similaridades com obras de Visual Music.

* Criação do instrumento musical [Teremim Aoristo](https://gitlab.com/musica-interativa/teremim-aoristo) durante o desenvolvimento do projeto Música Interativa construído com Arduíno, sensor HC-SR04 e SuperCollider.

* Professor voluntário na Universidade de Brasília (UnB) unidade Gama no curso de Engenharia de Software ministrando a disciplina Manutenção e Evolução de Software (MES). Período: 2019.2.

## 2020

* Desenvolvedor Python e Javascript em projeto utilizando Django REST Framework e Angular para o projeto Verão Itaipava 2020 em parceiria com a empresa PencilLabs, projeto de aplicativo mobile para contabilizar reciclagem de embalagens durante eventos no carnaval. Duração: 2 meses.

* Análise de performance e soluções de otimização para site em Drupal para o [IPC-UNDP](https://ipcig.org/) em parceiria com a empresa [PencilLabs](https://pencillabs.com.br), tempo médio de requisições ao site reduzido de 17 segundos para 1.5 segundos, a otimização foi alcançada através de estudos de otimização no PHP, Drupal, servidor de aplicação, banco de dados MariaDB e principalmente implantação de cache de páginas HTML com Varnish, além de implementação de testes de integração com Cypress e de performance com JMeter.

* Desenvolvedor Backend Sênior do [Programa das Nações Unidas para o Desenvolvimento](https://www.br.undp.org/) (PNUD) Brasil no projeto [Justiça Presente](https://www.cnj.jus.br/sistema-carcerario/justica-presente) do Conselho Nacional de Justica (CNJ) atuando com desenvolvimento Java e infraestrutura DevOps para melhoria, evolução e sustentação do Sistema Eletrônico de Execução Unificado (SEEU) utilizado em todos os estados da federação Brasileira pelos tribunais de Justiça.

* Pesquisador do laboratório LISIS na França em universidade Gustave Eiffel trabalhando nos projetos RISIS2 e CorTexT.

* Produção, organização e curadoria da web-exposição [EmMeio#12.0](https://emmeio.gitlab.io).

## 2021

* Produção, organização e curadoria da web-exposição [EmMeio#13.0](https://emmeio13.gitlab.io), [catálogo online PDF](http://medialab.unb.br/images/catalogos/catalogo-em_compressed.pdf).

* Professor voluntario na UnB da disciplipina _METODOLOGIA E TÉCNICAS DE PESQUISA CIENTÍFICA_ durante o curso [CURSO DE PÓS-GRADUAÇÃO LATO SENSU ESPECIALIZAÇÃO EM ESTUDOS AMAZÔNICOS](http://neaz.unb.br/especializacao-em-estudos-amazonicos) coordenado pelo NEAz entre o período 23/09/2021 a 18/10/2022.

## 2022

* Em 01 de Maio de 2022, Paris, apresentacao da performance [Du vent et du sable: une transposition de dune à Paris](/du-vent-et-du-sable).

* Fiz treinamento de 2 dias [The Carpentries Instructor Training](https://carpentries.github.io/instructor-training) e me tornei instrutor certificado The Carpentries em `July 15, 2022`. Agora tenho uma fotinha em [Our Instructors](https://carpentries.org/instructors). [https://doi.org/10.5281/zenodo.7612756](https://doi.org/10.5281/zenodo.7612756).

* Participacao na conferencia [DebConf22](https://debconf22.debconf.org) entre dias 10 e 24 de Julho na cidade de Prizren - Kosovo, 3 atividades realizadas: [Debian: Resistance is Futile](https://debconf22.debconf.org/talks/72-debian-resistance-is-futile), [Making sound and music with Live Coding: First steps on Debian](https://debconf22.debconf.org/talks/70-making-sound-and-music-with-live-coding-first-steps-on-debian), [Live Coding for art, sound and visuals](https://debconf22.debconf.org/talks/73-live-coding-for-art-sound-and-visuals).

* Em 27 Outubro 2022 oficialmente aceito como [embaixador do projeto Software Heritage](https://www.softwareheritage.org/ambassadors). SWH blog post: [Introducing our newest ambassador, Joenio Marques da Costa!](https://www.softwareheritage.org/2022/11/22/22nd-ambassador-joenio-marques-da-costa)

* Apresentacao [Software Heritage, first steps: What is and how to use it?](/software-heritage-olio) em 13 décembre 2022 no evento [Flash thématique « Les entrepôts de données : un outil au service des principes FAIR »](https://olio.hypotheses.org/1085).

## 2023

* FOSDEM 2023 talk [CorTexT Manager, a growing online platform in open research for social sciences](https://fosdem.org/2023/schedule/event/openresearch_cortext) at 4 February 2023, Brussels.

* Participacao no evento [Software Heritage (SWH) symposium and summit 2023](https://www.unesco.org/en/articles/software-source-code-documentary-heritage-and-enabler-sustainable-development), February 7th.

* Participacao no [RISIS International Conference 2023](https://www.risis2.eu/event/risis-international-conference-2023), November 6th to 8th, at Vienna TechGate and the AIT Austrian Institute of Technology.

* Apresentacao sobre o projeto Software Heritage, 13 novembre 2023, Atelier Data Univ Eiffel: "[Les plateformes pour développer, partager et archiver les logiciels](https://clap.univ-eiffel.fr/videos/rdv-data-les-plateformes-pour-developper-partager-et-archiver-les-logiciels-13112023)".

* The first edition of the [International workshop "Software, Pillar of Open Science"](https://www.ouvrirlascience.fr/international-workshop-software-pillar-of-open-science), [Ministry for Higher Education and Research, Paris](https://osm.org/go/0BOdyn6j6?node=719259601), 29th Nov. afternoon.

* Participei da conferencia internacional de Live Coding, International Conference on Live Coding (ICLC) 2023, que ocorreu entre os dias 19 e 23 de Abril de 2023 em Utretch na Holanda. Accepted works: [Video Category - Atari 2600 Video Remix [ICLC 2023]](https://www.youtube.com/watch?v=4rrxgw--eYU) and [ Poetry Attack 01 iclc2023 ](https://www.youtube.com/watch?v=n9zxAhXduH4)

* Organização [Algorave Brasil 2023](https://algoravebrasil.gitlab.io/eventos/2023/pt/), 25 de Novembro de 2023 (sabado), de 9h a 00h.

## 2024

* Participação no [FOSDEM 2024](https://fosdem.org/2024), Belgica. 3 and 4 February 2024. Talk: [dublang, a multi-language live coding system](https://fosdem.org/2024/schedule/event/fosdem-2024-2959-dublang-a-multi-language-live-coding-system/).

* Participação no [2024 Open Research Online Devroom talks](https://research-fosdem.github.io/2024-online-schedule), Talk: Analizo e o seu histórico de pesquisa e desenvolvimento entre 2008-2024. 10th of February, [Slides](https://joenio.me/analizo-15-years).

* Em Janeiro/2024 fui revisor do [ICLC 2024](https://iclc.toplap.org/2024)

* Embaixador [Data Univ Eiffel](https://www.univ-gustave-eiffel.fr/la-recherche/science-ouverte/donnees-et-logiciels-libres) (DATA de l’université Gustave Eiffel). [Data Univ Eiffel - Atelier data Université Gustave Eiffel](https://recherche.data.gouv.fr/fr/page/data-univ-eiffel-atelier-data-universite-gustave-eiffel).

* Selected at [IRCAM Forum Workshops 2024](https://forum.ircam.fr/collections/detail/les-ateliers-du-forum-de-lircam-2024-edition-speciale-les-30-ans) - 30th anniversary special edition in the CREATIVE SPACE room. Live performance: [Poetry Attack 01 - Joenio Marques da Costa, Mari Moura](https://forum.ircam.fr/article/detail/poetry-attack-01)

* 23-24 mai 2024, [Formation : "Les Plans de Gestion des Logiciels de la Recherche"](http://igm.univ-mlv.fr/~teresa/presoft/2024FormationPlansGestionLogicielsRecherche)

* 31 May, Talk with Ale Abdo about Cortext method to collect OpenAlex data on the online meeting: [2024 OpenAlex Virtual User Conference](https://help.openalex.org/events/user-meeting)

* Participação [Live Coding Study Day](https://journee.livecoding.fr/information), 23th April 2024 at the Maison des Sciences de l’Homme Paris Nord (20 avenue George Sand, 93210 Saint-Denis).

* Realização da [Oficina de empacotamento Debian](/oficina-pacote-debian) na disciplina USP [MAC0470/5856 - Desenvolvimento de Software Livre (2024)](https://edisciplinas.usp.br/enrol/index.php?id=118237), 14 e 16 de Maio de 2024, online.

* 11 March 2024, UGE, Marne-la-Vallee, France, <a href="https://digis.hypotheses.org/1328">Séminaire Scientifique DIGIS</a>, [youtube video](https://www.youtube.com/watch?v=f7iRQbiwm60).

* 11 Sept 2024 15h attending the [FAIR-IMPACT National Roadshow](https://fair-impact.eu/events/fair-impact-events/french-data-repositories-and-their-use-semantics) in France, Inrae, Paris.

* 12 Sept 2024 19h live coding performance at [Playground #4](https://plgrnd.cc/4) at Bordeaux, France. Me and Mari will present the art performance [Poetry Attack 01](https://poetryattack.4two.art), [event at meetup.com](https://www.meetup.com/fr-FR/creative-coding-bordeaux/events/302826374/).

* 2024-07-25 Debian NM process for Debian Developer non-uploading was approved and finished, Debian NM link: [NM process 1215](https://nm.debian.org/process/1215).

* 18 Oct 2024, palestra no [PUB Paris](https://pubparis.fr) sobre Software Para Pesquisa, [slides](https://joenio.me/sustentabilidade-de-software-para-pesquisa-pubparis-2024).

* 14-17 Nov 2024 MiniDebConf Toulouse and Capitole du Libre, [more info](https://wiki.debian.org/DebianEvents/fr/2024/Toulouse).
