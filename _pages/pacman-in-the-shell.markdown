---
title: pacman_in_the_shell
---

<iframe class="youtube" src="https://www.youtube.com/embed/xJTsyTRmtts?rel=0" frameborder="0" allowfullscreen></iframe>

```
-- artist: djalgoritmo
-- album: helloworld.tidal
-- year: 2019
-- track: 08
-- comment: the sounds are composed after (and by) the ghost in the shell video-remix
-- time: ~00:02:30

-- license: GPLv3+
-- copyright: 2019 @ djalgoritmo
```
