---
title: Nômade Lab EP
---

![nomade lab ep cover image](/files/nomade-lab-ep-cover.png)

* Ano de gravacao: 2019, 18 de Novembro de 2019
* Local de gravacao: Brasilia, estudio [Sala Fumarte](http://salafumarte.net)
* Ano de lançamento: 2022, 11 de Junho de 2022
* Artista: [Nômade Lab](https://nomadelab.gitlab.io)
* Faixas:
  * Faixa 01: 61 senadores
  * Faixa 02: 1 mar
  * Faixa 03: 1 gesto
  * Faixa 04: plantão
  * Faixa 05: xyz

<iframe class="soundcloud" width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1452016747&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>

Nômade Lab é:
* [Lorena Ferreira](https://lorenaferreira.gitlab.io)
* [Ghales Trilo](https://ghales.top/)
* Jackson Marinho
* [Joenio Costa](https://joenio.me)
* Leandro Muñoz
* [Phil Jones](http://www.synaesmedia.net)

<figure>
![nomade lab foto gravacao ep](/files/nomade-lab-ep/photo_2022-06-12_00-15-56.jpg)
<legend>Nômade Lab, 18/11/2019, Brasilia</legend>
</figure>

<figure>
![nomade lab foto gravacao ep](/files/nomade-lab-ep/photo_2022-06-12_00-16-02.jpg)
<legend>Nômade Lab, 18/11/2019, Brasilia</legend>
</figure>

<figure>
![nomade lab foto gravacao ep](/files/nomade-lab-ep/photo_2022-06-12_00-16-08.jpg)
<legend>Nômade Lab, 18/11/2019, Brasilia</legend>
</figure>

<figure>
![nomade lab foto gravacao ep](/files/nomade-lab-ep/photo_2022-06-12_00-16-11.jpg)
<legend>Nômade Lab, 18/11/2019, Brasilia</legend>
</figure>

<figure>
![nomade lab foto gravacao ep](/files/nomade-lab-ep/photo_2022-06-12_00-16-14.jpg)
<legend>Nômade Lab, 18/11/2019, Brasilia</legend>
</figure>

<figure>
![nomade lab foto gravacao ep](/files/nomade-lab-ep/photo_2022-06-12_00-16-17.jpg)
<legend>Nômade Lab, 18/11/2019, Brasilia</legend>
</figure>

<figure>
![nomade lab foto gravacao ep](/files/nomade-lab-ep/photo_2022-06-12_00-16-21.jpg)
<legend>Nômade Lab, 18/11/2019, Brasilia</legend>
</figure>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
## Imersão sonora: infinitas metamorfoses possíveis
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
Devir em escrita de intensidade de um corpo que se embriaga com a musicalidade do Nômade Lab, essa é a tarefa que me proponho aqui. Ouvir as músicas e quiçá expressar em palavras o que se sente não é tarefa fácil, por outro lado, é impossível calar diante de tal estesia sonora. Então começarei pelas repetições presentes nessas músicas. A repetição como potência própria, como potência de criação e pensamento, mas também a repetição como algo novo, como diferenciação e permanência. Propagação e constância fazem com que meu corpo se altere e que minhas sensações pulsem diante do que ouço. Tento entender, tento aprender, tento apreender, mas o instante se esvai como a água pelas mãos, sou efêmera diante de tais músicas. Sinto que é preciso manter o tino e o equilíbrio para não variar diante de algo tão vertiginoso. Algo se mostra diferente o tempo todo no tempo e no espaço. Algo invisível se revela o tempo todo. Exposto no vazio, as vezes creio que posso apreender, pera: É sobre 61 senadores, penso. Mas quanto mais penso menos ouço, e quanto mais ouço, menos decifro. Melhor deixar se levar pelo furacão, pela embriaguez de sensações. É uma escuta poética e deixar levar-se pelos prazeres efêmeros provocados pelas variedades sonoras é o que tem de mais raro.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
Todos os meus sentidos estão afetados: ouço com o tato, vejo com o paladar, sinto-me transubstanciada em sonoridades. Estou imersa em um mar de sensações, um gesto sonoro me conduz por um ambiente que não consigo distinguir. As sonoridades são pura ausência tácita de linguagem, ao me transubstanciar em sonoridades: sou mole, porosa e fluida.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
Ouvir tais músicas exigem um engajamento total, singular e único do meu corpo. Exigem que eu me invente, me repita, me diferencie... não escuto mais com os ouvidos, meu corpo todo vibra, minha respiração se altera, o olhar vareia – meditação: medito em ação, me edito em ação. Meu corpo dobra-se em quiasma, contorce-se, estou na dobra. E na dobra sou móvel e flexível.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
E eis que sou arrebatada, enfrento mais uma a passagem sonora e uma nova sensação se empodera do meu corpo. O tempo passa como que numa ampulheta quando a areia escorre por entre uma pequena fresta. Uma sonoridade que você começa seguir como quem percorre uma grande avenida, então vai aparecendo controladores de velocidade que te fazem pausar e reduzir a velocidade que seguia... um balbucio te faz entrar numa estrada sinuosa e você segue nela até que... de uma hora para outra entra voz, uma voz que você já ouviu, um áudio conhecido, um golpe se instala em seu percurso...  como se um pedestre se atravessasse em sua frente no momento que está em alta velocidade... aqui os controladores de velocidade são inúteis... sabe que não pode frear de vez pois em alta velocidade a derrapagem é certa, o impacto de uma freada é risco... o melhor é seguir em alta velocidade em direção ao pedestre, e por um instante não se sabe se é motorista ou pedestre, entre a dúvida do freio e o risco do atropelo... entre a vida e a morte: infinitas metamorfoses possíveis...
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
Sonoridades se repetem, mas a repetição é sempre algo novo, um eterno retorno como potência do querer... mergulho em cenário repetitivo e transgressivos... repetição como transgressão, liberdade e libertinagem... repetição como vontade, potência, desejo, vicio.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
Quebras incidentais com sons do cotidiano que se repetem e te atravessam, a sensação te atinge em cheio como um ato violento. Diante de tais músicas me sinto atingida por uma barra de ferro como Frida Kahlo,  barra que nos atravessa, nos perfura e marca uma vida. Toda a minha performance corporal ao ouvir as músicas do Nômade Lab é como a pintura de Frida, perpetuada pela barra de ferro atravessa no seu útero, não apenas o útero, a barra atravessa a vida.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
As músicas ora te conduzem em um mergulho profundo mar adentro, uma espécie de dança a dois com rotação, rodopios, rotações, giros, gravitações, saltos... ora as músicas te aprisionam... A única coisa que consigo dizer é que se a repetição é perigosa, quero morrer me repetindo, quero que me salve e me cure, quero overdose de repetição... repetir como vontade e liberdade, como variação.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
Variações: são repetições que se repetem e diferenças que se diferenciam.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
1 gesto engana, trapaceia e se faz generoso. O tempo transcorre, gasto horas, perco dia, enquanto escuto...
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
É tarde, o dia se faz noite a noite se faz dia, e ainda estou aqui, estou entre o rígido e o flexível e uma nova sonoridade me invade, me arrebata quando diz: pedra preciosa. Minha visão toca e meu tato vê, quase sou capaz deglutir, aliás, eu degluto como quem bebe água ao se afogar, ou inspirar água pelos pulmões... afogamento sonoro, isso resume minha estesia, já não é possível sair do turbilhão.
</div>

<div class="alert-info" style="padding-left: 10px; padding-right: 10px">
Sininhos, guitarra fazem o meu corpo dilatar, fluir e ser poroso, meu corpo vibra no espaço, minhas sensações são tão intensas que me fazem refletir sobre o limite do sensível, imerso nesse fragmento musical ele não existe... Eu sinto e minhas sensações são profundas demais. Me desequilibro, a cabeça varia e carne se faz verbo nessa escrita voraz e neste mergulho profundo e sem escafandro nas músicas do Nômade Lab.
</div>

<div class="alert-info" style="text-align: right; padding-left: 10px; padding-right: 10px">
Mari Moura<br/>
Natal, 2022
</div>

<iframe class="youtube" src="https://www.youtube.com/embed/uXBxY8NDFJs?rel=0" frameborder="0" allowfullscreen></iframe>

## 1 filme
 
_Um gesto é sempre um ato coletivo.
Quando se vê uma paisagem que emociona pelas suas várias nuances e sensações, você quer compartilhar.
Extasiados e impactados com a paisagem da janela de um AP em Paris decidimos compartilhá-la._

- _Vamos fazer um filme?_

_Com essa pergunta iniciamos a criação. De início pensamos em filmar a paisagem por 24h, depois optamos por 15h. No dia 27/03/2022 às 10h, estávamos com COVID e fizemos a gravação. 
Por 15h capturamos ininterruptamente o dia e a noite, mas não só, capturamos os pássaros voando, as luzes da cidade, o vento que faz a câmera balançar...
Paris a “cidade luz” se exibia com todas as suas nuances e encantamento, capturamos o momento em que ela se fazia noite e se fazia luz.
Esse filme que estamos a compartilhar é uma maneira de exprimir o nosso encantamento com essa paisagem, que se acompanha da música do Nômade Lab “1 gesto” e se faz filme.
A música se junta à paisagem e a enriquece criando uma atmosfera, quiçá vertiginosa e angustiante, com muito glitch, noise e loops, repetições que não se repetem, repetição que nunca se repete, repetição como potência de criação._
 
_Essa paisagem e essa sonoridade agora tornam-se filme e representam a nossa ancoragem, a nossa baliza no meio do mar, uma espécie de elemento de estabilidade da vida que escorre pelas mãos, do tempo que se observar, do dia que se faz noite, das luzes que acendem, dos pássaros que voam, da paisagem capturada por horas, mostrando suas nuances. 
Essa ancoragem é algo que transforma nosso olhar e nosso corpo. Mas é também uma fuga necessária para o desconhecido: a arte é uma substância viciante. É algo que faz pulsar o sangue que corre em nossas veias e sentir o pulsar do coração, é movimento, é fluidez...
Retomamos o gesto como um ato coletivo já presente em Monet ao pintar por dezenas de vezes a “Catedral de Rouen”, estudando o momento dos efeitos luminosos, ou de Cézanne quando pintou por inúmeras vezes sua “montanha mágica”, querendo dar a ver seus vários ângulos e perspectivas da visão. Motivos obcecados e triunfantes tornaram-se o emblema da modernidade pictórica e nos acompanham atualmente.
Aglomeramos uma música, uma paisagem e efeitos visuais, assim fizemos um filme cheio de nuances, para dar a ver o modo como essa paisagem nos impacta, assim recriamos a arte como gesto coletivo, político e poético.
Um gesto é sempre um ato coletivo._
 
_Joenio Costa e Mari Moura<br/>
Paris/ Natal, 2022_

--

### Fubar expo 2k23

O video-musica **1 gesto** foi submetido em 31 de Julho de 2023 no festival
`/’fu:bar/` 2023 (Zagreb, Croatia) na categoria **ART: Glitch Digital Media
Object (e.g. Image, Sound, Web, Video, etc.)**, foi aceito e o resultado foi
divulgado em 29 de Setembro de 2023 no site do evento em
[fubar.space](https://fubar.space).

* [Fubar 2k23 EXPO artist & work list](https://fubar.space/2023/expo-art)
* [Fubar expo 2k23 CATALOGUE](https://docs.google.com/spreadsheets/d/13OKJQxHRGoTN5FwfMPXyMFPfmcA5-4gsXEZ46TPGpMs/edit?usp=sharing)

![](/files/fubar-expo-2k23-expo-list.png)

