---
title: "Du vent et du sable: une transposition de dune à Paris (EN)"
---

`-` Lire cette page en [Fraçais](/du-vent-et-du-sable.fr). <br/>
`-` Leia esta página em [Português Brasil](/du-vent-et-du-sable). <br/>
`>` Read this page in [English](/du-vent-et-du-sable.en).

***Du vent et du sable: une transposition de dune à Paris***
is a territory transposition performatic act of the Sensitive State by the
artists Mari Moura (1st minister of Sensitive State) and Joenio M. Costa
(Advisor of Sensitive State). During the performance the first Dune in Paris
city will be installed, using sand shifted from the park _Parque da Cidade Don
Nivaldo Monte_, a facility aimed to preserve dunes located at Natal city
located in the _Estado do Rio Grande do Norte_, Brazil. The artists are gonna
to develop their actions using relational objects, online connection on social
networks and live coding.

<figure>
  <img src="/files/du-vent-et-du-sable.jpg" alt="Advertising flyer of the performance: Du vent et du sable">
  <legend>Advertising flyer of the performance: Du vent et du sable</legend>
</figure>

**Joenio M. Costa** is computational artist and experimental musician with
interest on algorithmic music, audiovisual, demoscene and live coding, he is
part of orchestra BSBLOrk ([bsblork.gitlab.io](https://bsblork.gitlab.io)) and the collective Nômade Lab
([nomadelab.gitlab.io/en](https://nomadelab.gitlab.io/en)), works as Research Software Engineer at the laboratory
of science and innovation LISIS ([umr-lisis.fr](https://umr-lisis.fr)).

**Mari Moura** ([@marimoura5](https://instagram.com/marimoura5)) is a
performance artist and theater actress interested on the relation between body
and technology. She teaches art at the technical school IFRN/BR and also is
part of the group _Grupo Estandarte de Teatro_
([@grupoestandartedeteatro](https://instagram.com/grupoestandartedeteatro)).
She researches the subjects around body, presence, art and technology at the PhD
program in the university _Universidade de Brasília_ (UnB) in collaboration
with the institute _Institut des sciences du sport-santé de Paris V_ (I3SP).

<figure>
  <img src="/files/du-vent-et-du-sable-montagem.png" alt="Du vent et du sable: une transposition de dune à Paris">
  <legend>Du vent et du sable: une transposition de dune à Paris</legend>
</figure>

All scripts, source-code, samples, poetry, photos, ideas and _everything else_
about the performance art "Du vent et du sable: une transposition de dune à
Paris" is available at
[gitlab.com/joenio/du-vent-et-du-sable](https://gitlab.com/joenio/du-vent-et-du-sable).

## Spread

![Banner](/files/du-vent-et-du-sable-post-instagram.png)

Folks, we (Joenio Costa and Mari Moura) invite you to attend on our art
performance named "Du vent et du sable: une transposition de dune à Paris" at
01 May 2022 19h on address 32 Boulevard Saint-Marcel, Paris. We are going to do
an action with live coding and art performance with _between-presence_ online and
we will install the 1st Dune of Paris city with sand shifted from Natal/RN
city, Brazil.

Who would like to arrive before 19h can watch the previews of the art
performance, music and drink starting at 15h with the presence of others
artists.

More informations at [{{ site.url }}/du-vent-et-du-sable.en](/du-vent-et-du-sable.en).

Thanks in advance, Joenio Costa and Mari Moura.

<iframe class="youtube" src="https://www.youtube.com/embed/LwmnLWWxJYA?rel=0" frameborder="0" allowfullscreen></iframe>

### Text for Twitter, Mastodon, etc

Folks, me and @Marimoura5 invite you to our art performance "Du vent et du
sable: une transposition de dune à Paris" with live coding and art performance
with online presence, where the 1st Dune of Paris city will be installed with
sand shifted from Natal city, Brazil.
+info [{{ site.url }}/du-vent-et-du-sable.en](/du-vent-et-du-sable.en).

<em>The texts and photos on this page were creaded by Joenio Costa and Mari Moura.</em>
