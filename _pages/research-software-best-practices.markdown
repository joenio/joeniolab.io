---
title: Research Software Best Practices
---

This page contains an opinionated list of Best Practices for Research Software
authors and for people citing Research Software in their publications.

## Archive and Preservation

- [Software Heritage (SWH)][swh]

## Persistent Identifiers

- [Software Hash ID (SWHID)][swhid]

## Intrinsic Metadata file format

- [The CodeMeta Project][codemeta]

## Extrinsic Metadata record

- [Research Software Directory (RSD)][rsd]

[swh]: https://www.softwareheritage.org
[codemeta]: https://codemeta.github.io
[swhid]: https://www.swhid.org
[rsd]: https://research.software
