---
title: Los Benzenos - Ensaio (2015)
---

![](/files/losbenzenos-photo.jpg)

<iframe class="soundcloud" width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/79116850&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

Gravado ao vivo no estúdio Canto do Grillo, Camaçari - Bahia - Brasil.

Integrantes:

* João Paulo (guitarra e voz)
* Danilo (bateria)
* Joenio (baixo)

Data de lançamento do álbum: 3 Fevereiro 2015.

- [losbenzenos.gitlab.io](https://losbenzenos.gitlab.io)

