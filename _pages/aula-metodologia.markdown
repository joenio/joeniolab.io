---
title: Aula de Metodologia e ferramentas de pesquisa
---

* Curso: [CURSO DE PÓS-GRADUAÇÃO LATO SENSU ESPECIALIZAÇÃO EM ESTUDOS AMAZÔNICOS](http://neaz.unb.br/especializacao-em-estudos-amazonicos)
  * Disciplina: METODOLOGIA E TÉCNICAS DE PESQUISA CIENTÍFICA

## Roteiro da aula 1, 03/02/2022

* software livre: https://joenio.me/software-livre
* wiki: https://joenio.me/wiki
* cultura hacker (pendente)

## Roteiro da aula 2, 10/02/2022

* OK git, controle de versoes de documentos texto, https://joenio.me/git
* OK github, repositorio de software
* OK trello
* OK Principios FAIR
  * Princípios FAIR aplicados à gestão de dados de pesquisa, https://ridi.ibict.br/handle/123456789/1182
  * https://wrco.ufpb.br/fair/index.html
* OK zenodo
* OK figshare, repositorio de dados
* OK https://osf.io/
* OK https://arxiv.org/, repositorio aberto de artigos
* OK archive.org e waybackmachine
* OK https://ourworldindata.org
* OK software heritage, arquivo de repositorios publicos de software
* OK jupyter https://colab.research.google.com
* OK latex, overleaf, https://pt.wikipedia.org/wiki/LaTeX https://www.overleaf.com/project
* orcid, identificador permanente (pid), doi
* OK scihub e libgen, desobediencia civil
* arduino, hardware livre
* cortext, https://www.vosviewer.com/
* https://root.cern.ch/
* https://www.nextflow.io/
* https://scrapy.org/
* Web of Science (WoS)
  * https://clarivate.com/webofsciencegroup/solutions/web-of-science/
* Google Scholar scholar.google.com
* ACM DL, https://dl.acm.org/
* periodicos.capes.gov.br
* Amazon AWS
* Heroku
* GCE

## Referencias

* Livro: Maciel, M. L., Abdo, A. H., & Albagli, S. (2015). Ciência aberta, questões abertas.
* Disponivel no link http://dx.doi.org/10.18225/978-85-7013-109-6
* Capitulo 1. Sarita Albagli. Ciência aberta em questão.
* Capitulo 12. Alexandre Hannud Abdo. Direções para uma academia contemporânea e aberta.

## Atividade da disciplina

Fazer resenha critica do capitulo abaixo:

* Capitulo 1. Sarita Albagli. Ciência aberta em questão.
