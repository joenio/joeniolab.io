---
title: "Du vent et du sable: une transposition de dune à Paris (FR)"
---

`>` Lire cette page en [Fraçais](/du-vent-et-du-sable.fr). <br/>
`-` Leia esta página em [Português Brasil](/du-vent-et-du-sable). <br/>
`-` Read this page in [English](/du-vent-et-du-sable.en).

***Du vent et du sable: une transposition de dune à Paris***
c'est un acte performatif de transposition du territoire de l'Etat Sensible
interprété pour les artistes Mari Moura (1ère ministre de l'État sensible) et
Joenio M. Costa (conseiller de l'État sensible). Pendant la performance art, la
première Dune de la ville de Paris sera installée, avec du sable déplacé du
Parque da Cidade Don Nivaldo Monte, une unité de conservation des dunes située
dans la ville de Natal dans l'État de Rio Grande do Norte au Brésil. Les
artistes développer leurs actions avec des objets relationnels, la connexion en
ligne sur le réseau social et live coding.

<figure>
  <img src="/files/du-vent-et-du-sable.jpg" alt="Flyer de la divulgation de la performance Du vent et du sable">
  <legend>Flyer de la divulgation de la performance Du vent et du sable</legend>
</figure>

**Joenio M. Costa** est un artiste informatique et musicien expérimental qui
s’intéresse par la musique algorithme, l’audiovisuel, demoscene et live coding,
il faire partie de orchestre BSBLOrk ([bsblork.gitlab.io](https://bsblork.gitlab.io)) et du collectif Nômade
Lab ([nomadelab.gitlab.io/en](https://nomadelab.gitlab.io/en)), il travaille comme Research Software Engineer
dans le laboratoire de science et innovation LISIS ([umr-lisis.fr](https://umr-lisis.fr)).

**Mari Moura** ([@marimoura5](https://instagram.com/marimoura5)) est une artiste de performance et théâtre, s’intéresse
par la associant corps et technologie. Elle travaille comme professeure d’art
dans l’enseignement technique et technologique dans l’institut IFRN/BR, elle
est membre de grupe Estandarte de Teatro([@grupoestandartedeteatro](https://instagram.com/grupoestandartedeteatro)). Effectue une recherche sur le corps,
la présence l’art et la technologie dans le programme du doctorat de
l’université UnB  en collaboration avec L’Institut des sciences du sport-santé
de Paris V (I3SP).

<figure>
  <img src="/files/du-vent-et-du-sable-montagem.png" alt="Du vent et du sable: une transposition de dune à Paris">
  <legend>Du vent et du sable: une transposition de dune à Paris</legend>
</figure>

Tout les scripts, les sources, samples, poésie, fotos, idées e _tout le plus_ a
respect te la performance "Du vent et du sable: une transposition de dune à
Paris" sont en ligne au
[gitlab.com/joenio/du-vent-et-du-sable](https://gitlab.com/joenio/du-vent-et-du-sable).

## Diffusion

![Banner](/files/du-vent-et-du-sable-post-instagram.png)

Salut les amis, on (Joenio Costa et Mari Moura) invité à assister à notre
performance << Du vent et du sable : une transposition de dune à Paris >> le
jour de la fête du travail le 1er mai 2022, à 19h au 32 Boulevard Saint-Marcel,
Paris. On réalisera une action avec un live coding et de l’art performance en
présence en ligne et on installera la première Dune dans la ville de Paris avec
du sable déplacé de la ville de Natal/RN, Brésil.

Toute personne qui souhaitent arriver avant 19h pourra suivre les
avant-premières de la performance, musique et boissons à partir de 15h, avec
présence d'autres artistes.

Plus des informations en [{{ site.url }}/du-vent-et-du-sable.fr](/du-vent-et-du-sable.fr).

Nous vous remercions d’avance,  Joenio Costa et Mari Moura

<iframe class="youtube" src="https://www.youtube.com/embed/LwmnLWWxJYA?rel=0" frameborder="0" allowfullscreen></iframe>

### Texte par Twitter, Mastodon, etc

Salut les amis, je et @Marimoura5 on invité à notre performance << Du vent et
du sable : une transposition de dune à Paris >> avec un live coding et de l’art
performance en présence en ligne, on installera la première Dune dans la ville
de Paris avec du sable de la ville de Natal/RN, Brésil.
+info [{{ site.url }}/du-vent-et-du-sable.fr](/du-vent-et-du-sable.fr)

<em>Les textes et fotos de cette page ils viennent de créer par Joenio Costa e Mari Moura.</em>
