---
title: Links favoritos e sites amigos
---

## Friend's sites and blogs

* [ravidwivedi.in](https://ravidwivedi.in) - Ravi Dwivedi <span style='color:gray'>#debconf #debian</span>
* [sarabouchard.com](https://www.sarabouchard.com) - Sara Bouchard <span style='color:gray'>#iclc23 #livecoding</span>
* [flordefuego.github.io](https://flordefuego.github.io) - Flor de Fuego <span style='color:gray'>#clic #livecoding</span>
* [1riss.github.io](https://1riss.github.io) - Iris Saladino <span style='color:gray'>#clic #livecoding</span>
* [alexandrerangel.art.br](https://www.alexandrerangel.art.br) - Alexandre Rangel <span style='color:gray'>#unb #medialab #livecoding</span>
* [hitnail.net](https://hitnail.net) - Gustavo Brunoro (beise) <span style='color:gray'>#algoravebrasil #livecoding</span>
* [berinfontes.com](https://berinfontes.com) - Bernardo Fontes (berin) <span style='color:gray'>#algoravebrasil #livecoding</span>
* [ghales.top](https://ghales.top) - Thales Grilo <span style='color:gray'>#unb #medialab #nomadelab</span>
* [bgo.la](https://bgo.la) - Bruno Gola <span style='color:gray'>#algoravebrasil #livecoding</span>
* [terceiro.xyz](https://terceiro.xyz) - Antonio Terceiro <span style='color:gray'>#salvador #colivre #debian</span>
* [valessiobrito.com.br](https://valessiobrito.com.br) - Valessio Brito <span style='color:gray'>#salvador #colivre #debian</span>
* [aurium.one](https://aurium.one) - Aurélio A. Heckert <span style='color:gray'>#salvador #colivre</span>
* [gomex.me](https://gomex.me) - Rafael Gomes (gomex) <span style='color:gray'>#salvador #colivre #devops</span>
* [ca.ios.ba](https://ca.ios.ba) - Caio Almeida <span style='color:gray'>#salvador #colivre #safernet</span>
* [melissawen.github.io](https://melissawen.github.io) - Melissa Wen <span style='color:gray'>#salvador #colivre #bsb</span>
* [abrahamraji.in](https://abrahamraji.in) - Abraham Raji <span style='color:gray'>#debconf #debian</span>
* [raju.dev](https://raju.dev) - Raju Devidas Vindane <span style='color:gray'>#debconf #debian</span>
* [tvaz.cc](https://tvaz.cc) - Tiago Bortoletto Vaz <span style='color:gray'>#salvador #colivre #debian</span>
* [nahfeld.de](https://www.nahfeld.de) - Markus van Well <span style='color:gray'>#iclc23 #ircamforum24</span>
* [grain-noir.com](https://grain-noir.com) - GRAIN noir <span style='color:gray'>#ircam #livecodingday24</span>
* [jaime-arias.fr](https://www.jaime-arias.fr) - Jaime Arias <span style='color:gray'>#softwareheritage #paris</span>
* [robillardstudio.github.io](https://robillardstudio.github.io) - Gaëtan Robillard <span style='color:gray'>#universitegustaveeiffel</span>
* [mdcc.cx](http://mdcc.cx) - Joost van Baal-Ilić <span style='color:gray'>#debconf #debian</span>
* [ttm.github.io](https://ttm.github.io) - Renato Fabri <span style='color:gray'>#bsb</span>
* [samueloph.dev](https://samueloph.dev) - Samuel Henrique <span style='color:gray'>#debian</span>
* [iggar.dev](https://iggar.dev) - Igor Garcia <span style='color:gray'>#salvador</span>
* [gwolf.org](https://gwolf.org) - Gunnar Wolf <span style='color:gray'>#debconf #debian</span>
* [thoughtstorms.info](https://thoughtstorms.info/view/PhilJones) - Phil Jones <span style='color:gray'>#bsb #nomadelab</span>
* [eda.mutu.net](https://eda.mutu.net) - Edlira Nano <span style='color:gray'>#minidebconf24 #toulouse</span>
* [xaviergodart.com](https://www.xaviergodart.com) - Xavier Godart <span style='color:gray'>#bordeaux #deezer</span>

## Podcasts

* [se-radio.net](https://www.se-radio.net) - Software Engineering Radio
* [us-rse.org/rse-stories](https://us-rse.org/rse-stories) - Research Software Engineer Stories
* [haskell.foundation/podcast](https://haskell.foundation/podcast/?s=09) - The Haskell Interlude
* [Machines That Fail Us #1: Making sense of the human error of AI](https://thehumanerrorproject.ch/listen-to-the-first-episode-of-the-machines-that-fail-us-podcast)

## Notas e Links

* [lalal.ai](https://www.lalal.ai) - Site para extrair vocais e instrumental de musicas.
* [cycle.travel](https://cycle.travel) - Site para calcular melhores rotas calmas para viagem de bicicleta.
* [rome2rio.com](https://www.rome2rio.com) - Site para calcular melhor modal de viagem combinando as opcoes mais baratas entre, onibus, trem, aviao etc.
* [hostelworld.com](https://www.hostelworld.com) - Site para pesquisar hostels.

## Live coding, arte, tech

* [marching.js](https://github.com/charlieroberts/marching)
* [Hydra](https://github.com/ojack/hydra)

## Synthesizers, tutorial

* [Intro to Synthesis Part 1 - The Building Blocks of Sound & Synthesis](https://www.youtube.com/watch?v=atvtBE6t48M)
* [Intro to Synthesis Part 2 - Types of Synthesis & Programming Examples](https://www.youtube.com/watch?v=gJkxGvhOS-M)
* [Intro to Synthesis Part 3 - Additional Synth Features, Performance Controls & Wrap Up](https://www.youtube.com/watch?v=zK3m8sMkTE4)

## Audio software tools

* [Partiels](https://forum.ircam.fr/projects/detail/partiels)

## Sites para estudar Francês

* [French pronunciation dictionary](https://forvo.com/languages/fr) - Audio de palavras e frases.
* [Reverso Conjugation](https://conjugator.reverso.net/conjugation-french.html) - Tempos verbais com audio.
* [Gymglish - Conjugaison française](https://www.gymglish.com/fr/conjugaison/vatefaireconjuguer) - Conjulgar verbos (dica Cédric).
* [How to Learn French Fast](https://www.wikihow.com/Learn-French-Fast) - Metodo para estudo.
* [How to Speak Basic French](https://www.wikihow.com/Speak-Basic-French) - Metodo para estudo.
* [How to Learn French](https://www.wikihow.com/Learn-French) - Metodo para estudo.

## Software development platforms

* [GitHub](https://github.com)
* [GitLab](https://gitlab.com)
* [Codeberg](https://codeberg.org)
* [sourcehut](https://sourcehut.org)
* [Git alternatives - Version Control Beyond Git](https://www.soundbarrier.io/posts/git_alternatives)
* [Fossil - Software Configuration Management System](https://fossil-scm.org)

## UI design guidelines

* [Material Design](https://material.io)

## Principios de design para command line interface

- [clig.dev](https://clig.dev) - Command Line Interface Guidelines
- [gnu.org/software/coreutils/manual](https://www.gnu.org/software/coreutils/manual/html_node/Opening-the-software-toolbox.html#Opening-the-software-toolbox) - GNU Coreutils: Opening the Software Toolbox
- [doi.org/10.1186/2047-217X-2-15](https://doi.org/10.1186/2047-217X-2-15) - Ten recommendations for creating usable bioinformatics command line software

## Useful commandline tools on Debian

* [pdf2svg](http://cityinthesky.co.uk/opensource/pdf2svg) - Convert PDF to SVG.
* [rsvg-convert](https://wiki.gnome.org/Projects/LibRsvg) - Convert SVG to PDF.
* [Add Fade In and Fade Out Effects With ffmpeg!](https://dev.to/dak425/add-fade-in-and-fade-out-effects-with-ffmpeg-2bj7)

## Associacoes free software Franca

* [TeDomum](https://tedomum.net/page/association) - Associação na França hospedando serviços web com free software.

## Centros culturais Paris

* [Biblioteca Centre Pompidou](https://www.bpi.fr) - Bons eventos publicados na agenda.
- [Centre Kalachakra](https://www.centre-kalachakra.com) - Centro Budista em Paris.

## Paris music, art, events

- [99 GINGER](https://www.99ginger.com) - Selo massa que organiza umas coisas legais em Paris
- [La Station — Gare des Mines](https://lastation.paris) - Espaço dedicado a eventos e projetos de arte experimental, musica e visual.
- [O.B.F SOUND SYSTEM](https://linktr.ee/obfsoundsystem) - Grupo q produz umas festa legais em Paris.
- [L'ALIMENTATION GÉNÉRALE](https://www.alimentation-generale.net) - Bom espaço para dançar, pequeno, som pesado, eletrônico bassss.
- [La Gare Le Gore](https://www.facebook.com/LaGareLeGore/) - Proximo ao La Station, jazz e musica experimental, entrada gratuita, ambiente e atmosfera muito bom.
- [MAINSD’ŒUVRES](https://www.mainsdoeuvres.org/?lang=fr) - Ambiente com muitos espacos distintos, teatro, show, etc... fui no evento WomenBeats #3 : LA PERLA + SHAROUH.

## Restaurantes veganos de Paris

- [https://secretsofparis.com/food-drink/vegan-paris](https://secretsofparis.com/food-drink/vegan-paris)

## Computer science, Electrotechnical

* [Words Matter: Alternatives for Charged Terminology in the Computing Profession](https://www.acm.org/diversity-inclusion/words-matter)
* [Electropedia: The World's Online Electrotechnical Vocabulary](https://www.electropedia.org)
- [Inclusive Naming Initiative](https://inclusivenaming.org)

<!--https://jitsi.github.io/handbook/docs/community/community-instances/ -->

## Dataset and science

- [https://ourworldindata.org](https://ourworldindata.org/contributed-most-global-co2)

## Manifestos e iniciativas de tecnologia crítica

* [The Critical Engineering Manifesto](https://criticalengineering.org)
* [Manifesto Tech](https://manifestotech.org)

## Jobs

* [Jobs at the European Commission](https://commission.europa.eu/jobs-european-commission_en)
* [UNESCO Careers](https://careers.unesco.org)
* [Choisissez le service public](https://choisirleservicepublic.gouv.fr/nos-offres)
* [Le Cnam recrute](https://presentation.cnam.fr/le-cnam-recrute)
* [Muséum national d'Histoire naturelle Recrute](https://recrutement.mnhn.fr/front-jobs.html)

## Fundings

* [Research Software Funding Opportunities](https://www.researchsoft.org/funding-opportunities)

## Documentarios

* [Maestro Jorge Antunes Polêmica e Modernidade](https://www.youtube.com/watch?v=bkxMpGgeBWA)
  * _Jorge ANtunes compos em 1961 a primeira música eletrônica brasileira_
* [1970: WENDY CARLOS and her MOOG SYNTHESISER - BBC Archive](https://www.youtube.com/watch?v=UsW2EDGbDqg)

## Residencias Artisticas

* [Interface](https://interfaceinagh.com/apply/residency) - Studio and residency programme for visual artists, dancers, writers and musicians

## Talks

* [Paul Davis, the creator of Ardour (unfa live interview 2022-10-02)](https://www.youtube.com/watch?v=ZndTAbjPqm0)
* [LibrePlanet 2022](https://framatube.org/c/libreplanet2022/videos?s=1)
* [Ars Electronica Channel](https://ars.electronica.art/planetb/en/live)

## Software development good practices

* [keepachangelog.com](https://keepachangelog.com)
* [semver.org](https://semver.org)
* [calver.org](https://calver.org)
* [makeareadme.com](https://www.makeareadme.com)
* [12factor.net](https://12factor.net)
* [choosealicense.com](https://choosealicense.com)

## Leituras

* [Hacking Perl in Nightclubs](https://www.perl.com/pub/2004/08/31/livecode.html) - About `feedback.pl` live coding tool

## Writing documentation guides

* [Write the Docs - A beginner’s guide to writing documentation](https://www.writethedocs.org/guide/writing/beginners-guide-to-docs/#what-to-write)

## Leituras

* [Hacking Perl in Nightclubs](https://www.perl.com/pub/2004/08/31/livecode.html) - About `feedback.pl`

## Quake 1 engines

* [Ironwail](https://github.com/andrei-drexler/ironwail)
* [QuakeSpasm](https://github.com/sezero/quakespasm)
* [ezQuake](https://ezquake.com)

## Desktop software tools

* [gromit](http://www.home.unix-ag.org/simon/gromit) - Draw on desktop screen
  - alternative: https://github.com/bk138/gromit-mpx

## Outros

- https://www.cnap.fr
- https://betonsalon.net
- [Servico em paris para recolhimento de moveis antigos](https://teleservices.paris.fr/ramen)
- [Plain Text Accounting (PTA)](https://plaintextaccounting.org)

## Free, gratis web hosting

- https://envs.net
- https://vern.cc
- https://projectsegfau.lt
- https://tildeverse.org

## DNS user-controlled top-level domain (TLD) registries

- [OpenNIC Project](https://opennic.org)

## Tatuagens e artistas favoritos

- [dynoz art attack](https://www.instagram.com/dynozartattack)
- [capitaineplum](https://capitaineplume.bigcartel.com)
- [ben volt](https://www.benvolttattoo.com)

## Filmes para ver ou rever

- [Weizenbaum. Rebel at Work](https://www.imdb.com/title/tt0932562)
- [Filmes recomendados por spike lee](https://x.com/camilopmachado/status/1497364492480565257?s=09)
- [documentario Eletronica:mentes](https://tab.uol.com.br/noticias/redacao/2019/06/25/documentario-eletronicamentes.htm)
- dias gomes - o pagagador de promessas
- dias gomes - santo inquerito
- [People are Knowledge](https://vimeo.com/26469276)
- Filmes do glauber rocha
- [Raul - O Início, o Fim e o Meio](https://www.adorocinema.com/filmes/filme-202608)
- [Filhos de João, Admirável Mundo Novo Baiano](https://www.adorocinema.com/filmes/filme-188361)
- [BABEL 2006- ALEJANDRO GONZALEZ IÑÑARITU](https://www.youtube.com/watch?v=FMlUw1bxNpc)
  - https://en.wikipedia.org/wiki/Babel_(film)
- [21 Grams - Alejandro González Iñárritu & Gustavo Santaolalla](https://www.youtube.com/watch?v=FeGcSy8kXYs)
  - https://en.wikipedia.org/wiki/21_Grams
- [Les saveurs du palais](https://www.youtube.com/watch?v=pO_yBItHJ0c) - bom para aprender frances, esta no netflix
- [Les Misérables (2019)](https://www.imdb.com/title/tt10199590)

### Filmes para Assistir

- Tempos Modernos (1936)
- Casablanca (1942)
- Cinema Paradiso (1988)
- O Grande Ditador (1940)
- Cidadão Kane (1941)
- Taxi Driver: Motorista de Táxi (1976)
- Por uns Dólares a Mais (1965)
- Monty Python em Busca do Cálice Sagrado (1975)
- Harakiri (1962)
- Rashomon (1950)
- Chinatown (1974)
- The Handmaid's Tale serie (2017)
- The Ring (2002)
- Eclats Noirs du Samba
  - [vimeo](https://vimeo.com/69238779),
    [youtube](https://www.youtube.com/watch?v=0tTOAhe5Lww),
    [film-documentaire](http://www.film-documentaire.fr/4DACTION/w_fiche_film/64195_1),
    [lemonde](https://www.lemonde.fr/archives/article/1989/01/01/eclats-noirs-du-samba-le-rire-de-zeze_4117654_1819218.html)

### Filmes para Assistir novamente

- Clube da Luta (1999)
- A Vida é Bela (1997)
- A Viagem de Chihiro (2001)
- Interestelar (2014)
- Cães de Aluguel (1992)
- 2001: Uma Odisséia no Espaço (1968)
- Nascido para Matar (1987)
- Meu Amigo Totoro (1988)
- V de Vingança (2005)
- Platoon (1986)

### Series

- The Expanse

### Animes

- Death Note
- Ghost in The Shell
- Serial Experiments Lain
- Totoro
- Princess Mononoke

## Free Music

- [Free Music Archive (FMA)](https://freemusicarchive.org/home)
- [Open.Audio](https://open.audio)

## HTML para iniciantes

- [HTML for People](https://htmlforpeople.com)

## Static Analysis tools and Source Code metrics

- [Linguist](https://github.com/github-linguist/linguist) - Usado pelo Github para detectar linguagem de programacao em repositorios
- [Guesslang](https://github.com/yoeo/guesslang) - Detect the programming language of a source code

## Livros de ciencia da computacao e engenharia de software

Lista de leitura fundamental para formacao em ciencia da computacao e eng de
software.

- Hackers: heroes of the computer revolution de steven levy
- Sciences of the artificial de herb simon
- The lisp 1.5 manual de john mccarthy
- The art of the metaobject protocol de kiszales, bobrow, rivera
- A control definition language de dave fisher
- Computation: finite and infinite machines de marvin minsky
- Newton's: principia
- Now it can be told de maj gen leslie
- Technics and human development vol 1 de lewis mumford's
- The myth of the machine series
- The c programing language de kernigan and ritchie
- Network programing de richard stevens
- Design patterns de gang of four
- Clean code de uncle bob
- The design of the unix operating system de maurice bach
- The mythical man month de frederik brooks
- The pragmatic programmer
- Code: the hidden language of the computer hardware and software de charles petzold
- Clean code: a handbook of agile software craftsmanship de robert c martin
- Eloquent javascript de martin haverbeek
- Code complete: a practical handbook of software construction de steve mcconnel
- Algorithms de robert bedgewick e kevin wayne
- Types and programming languages de benjamin c pierce
- Introduction to algorithms de thoman cormen
- Compilers de alfred v aho
- Purely functional data structures de chris okagaki
- Eloquent ruby de russ olsen
- Elements of programming de alexander stepanov
- Ethics in computing de joseph kizza
- Automata theory, language and computation de jeffrey d unllman
- Introduction to algorithms de thomas cormen
- The art of computer programming vol 1 de donald e knuth
- Structure and interpretation of computer programs de harold abelson et al
- Structured computer organization de andrew tanenbaum
- Programming languages: application and interpretation de shriram krishnamurti
- The self-taught programmer de cory althoff
- Data visualization made simple de kristen borski
- The soul of a new machine de tracy kidder
- Joel on software de joel spolsky
- The society of mind de marvin minsky
- Refactoring: improving the design of extinct code de martin fowler
- Computational complexity de christon papadimitrov

## Livros recentes de sci-fi and non-sci-fi

- [The Transparent Society, 1998](https://en.wikipedia.org/wiki/The_Transparent_Society)
- [Earth (Brin novel), 1990](https://en.wikipedia.org/wiki/Earth_%28Brin_novel%29)
- [Kiln People, 2002](https://en.wikipedia.org/wiki/Kiln_People)

## Benchmarking tools

- https://github.com/Gabriella439/bench
- https://github.com/sharkdp/hyperfine
