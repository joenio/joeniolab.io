---
title: Introduction and demonstration of Live Coding technique and tools - Séminaire Scientifique DIGIS
---

This commuication will be in two parts. Firstly, I will present Live Coding
technique for creation of music, sound art, and visual art using programming
languages with free software tools, such as SuperCollider, Sonic Pi, Tidal
Cycles and Hydra. Secondly, a live artistic improvisation with sound+visuals
will be presented using the dublang live coding tool.

- Duration: 2h

Schedule:

- [Live Coding Introduction](/live-coding-introduction) (Joenio): 25 min
- Break: 5 min
- Hydra, Crochet coding and other experiences (Mari): 25 min
- Question and answers: 15 min
- Live performance demonstration (Mari + Joenio): 30 min
- Question and answers: 20 min

More info:

- [digis.hypotheses.org/1275](https://digis.hypotheses.org/1275)
