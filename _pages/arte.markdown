---
title: Arte
---

Nesta página documento alguns dos meus projetos e obras de arte e tecnologia
realizados ou em andamento, a maior parte envolvem técnicas de live coding,
sempre utilizando software livre.

Eu tive a minha primeira banda em 2001 tocando contra-baixo e até 2015
participei de inúmeras bandas, a partir daí tenho me dedicado a arte
computacional e desde 2016 realizo obras e performances com live coding,
incluindo: música, arte sonora, visual music, net arte, cinema experimental,
demoscene, entre outros.

## 2010

- Album: [Estado de Sítio - Demo - 2010](/estado-de-sitio-demo)

## 2015

- Album: [Los Benzenos - Ensaio - 2015](/los-benzenos-ensaio)

## 2018

- (unreleased) Net arte: [um grande acordo [nacional] para estancar a sangria](http://musicwhiletrue.4two.art)

- Performance Sonora Ciclo Curare: Performances de arte com música eletrônica e Live Coding na Galeria DeCurators, Brasília, DF, Brasil em Performance Sonora Ciclo Curare colaborando com Philip Jones e Eufrasio Prates na exposição Objetos-poema de Luciana Ferreira.

- [Video YouTube - Performance Sonora Ciclo Curare na galeria DeCurators em 19/10/18, Brasília-DF](https://www.youtube.com/watch?v=B7tBh2AzdKE)

## 2019

- Video arte: [Atari 2600 Vídeo Remix](/atari-2600-video-remix)

- Album: [helloworld.tidal album](/helloworld.tidal)

- Video arte: [pacman_in_the_shell](/pacman-in-the-shell)

- Album:: [}bio{borgs - album 2019](/bioborgs)

- Música: [Las Plantas Sagradas Y La Rosa Negra](/las-plantas-sagradas)

- Evento: [Brasília Mapping Festival #SmartCities](/bmf-2019)

- Hardware: [Projeto Música Interativa - Teremim Aoristo](https://gitlab.com/musica-interativa/teremim-aoristo)

- Organização: [Algorave Brasília](https://algoravebrasilia.gitlab.io) - O evento que nunca aconteceu!

- Performance e instalação: [INTERFACES COMPUTACIONAIS AFETIVAS](https://www.facebook.com/events/1928004933992860/) com o Medialab UnB no Sesc DF, Gama, DF, Brasil, onde realizei performance com a linguagem de programação musical TidalCycles e o dispositivo [Teremim Aoristo](https://gitlab.com/musica-interativa/teremim-aoristo).

- Performance: BSBLOrk na exposição VOA BRASÍLIA, performance com a Orquestra de Laptops de Brasília (BSBLOrk) na abertura da exposição “VOA BRASÍLIA” realizada na Galeria ArquiBrasília, Brasília, DF, Brasil e exposição de trabalho de vídeo-remix Atari 2600 Vídeo Remix.

## 2020

- Video musica: [MANTRAM - Wireflores](/mantram)

- Produção: Produção, organização e curadoria da web-exposição [EmMeio#12.0](https://emmeio.gitlab.io).

## 2021

- Produção: Produção, organização e curadoria da web-exposição [EmMeio#13.0](https://emmeio13.gitlab.io), [catálogo online PDF](http://medialab.unb.br/images/catalogos/catalogo-em_compressed.pdf).

## 2022

- Performance: [Du vent et du sable: une transposition de dune à Paris](/du-vent-et-du-sable)

- Album: [Nômade Lab EP](/nomade-lab-ep)

- (unreleased) Net arte: [webbrowser.html](http://webbrowser.html.4two.art)

- Performance: [Poetry Attack 01](/poetry-attack-01-iclc-2023)

## 2023

- Video arte: [pastinha loop](/pastinha-loop)

- Video arte: [cubes 2800](/cubes-2800)

- Organização: [Algorave Brasil 2023](https://algoravebrasil.gitlab.io/eventos/2023/pt/), 25 de Novembro de 2023 (sabado), de 9h a 00h

## 2024

- Performance: [IRCAM Forum Workshops 2024](https://forum.ircam.fr/collections/detail/les-ateliers-du-forum-de-lircam-2024-edition-speciale-les-30-ans) - 30th anniversary special edition in the CREATIVE SPACE room. Live performance: [Poetry Attack 01 - Joenio Marques da Costa, Mari Moura](https://forum.ircam.fr/article/detail/poetry-attack-01).

## Outros

* [joenio.4two.art](http://joenio.4two.art)
* [4two.art](http://4two.art)

## Mais

- [Linha do tempo](/timeline)
