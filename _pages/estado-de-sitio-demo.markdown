---
title: Estado de Sítio - Demo (2010)
---

<iframe class="soundcloud" width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/310816206&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

Demo da banda Estado de Sítio, de Camaçari - Bahia - Brasil, gravado ao vivo no estúdio.

Formação:

* Marcelo (voz)
* Ítalo (guitarra)
* Bogus (bateria)
* Joenio (baixo)

Data de lançamento do álbum: 29 Junho 2010.

[<img style="width:40%" src="/files/estado-de-sitio-capa-frente.jpeg"/>](/files/estado-de-sitio-capa-frente.jpeg)
[<img style="width:40%" src="/files/estado-de-sitio-capa-fundo.jpeg"/>](/files/estado-de-sitio-capa-fundo.jpeg)
