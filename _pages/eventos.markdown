---
title: Eventos
---

Participacao em eventos, oficinas, palestras, bandas, performance artística,
material de ensino, slides, aulas e outras produções sobre minha partipação em
eventos, congressos e conferências.

## 2010, Palestra Foswiki, YAPC::Brasil

Apresentação de palestra “Foswiki - Uma plataforma wiki livre para gestão de conteúdo” durante o YAPC::Brasil 2010.

## 2016, Oficina Live Coding, Calango Hacker Clube, Brasília, DF

Oficina Live Coding durante o evento #MusicHacking no Calango Hacker Clube em Brasília, DF, sobre a linguagem de programação musical TidalCycles.

[<img width="100%" style="border-radius:10px" src="/files/calango-club-music-hacking-27-tidalcycles.jpg"/>](https://www.youtube.com/watch?v=mXW13ccuMIo)

## 2018, Hackathon de programação musical e Live Coding durante, Museu Nacional, Brasília, DF

Participei e organizei o evento Hackathon de programação musical e Live Coding
durante o #17.ART no Anexo do Museu Nacional, Brasília, DF, Brasil em
colaboração com o laboratório Lappis, MediaLab UnB e Calango Hacker Clube.

- [Evento no Facebook](https://www.facebook.com/medialabunb/photos/a.266813103774890/554461135010084)

## Slides

Apresentaçoes utilizadas em participação de eventos, congressos e oficinas.

<ul class="slides">
{% for slide in site.slides %}
  {% if slide.layout != 'redirect' %}
  <li class="slide">
    <a class="slide-link" href="{{ slide.url | remove: '/index.html' | prepend: site.baseurl }}">{{ slide.title }}</a>
    <em class="slide-version">{{ slide.version }}</em>
  </li>
  {% endif %}
{% endfor %}
</ul>

## Ensino

Planos de ensino e slides utilizados em oficinas, palestras,
participação de eventos, congressos, aulas, workshops, etc.

### Aulas

* [Plano de aula sobre Metodologia e ferramentas de pesquisa](/aula-metodologia)
* [Seminário sobre Métricas de código-fonte e ferramentas de análise estática](/seminario-analise-estatica)

### Oficinas

* [Plano de oficina sobre Gitlab](/oficina-gitlab)
* [Plano de oficina sobre Shell Script](/oficina-shellscript)
* [Plano de oficina sobre Live Coding](/oficina-livecoding)
* [Conferencia de Live Coding](/conferencia-livecoding)
* [Plano e material didático da oficina de empacotamento Debian](/oficina-pacote-debian)

## Mais

- [Linha do tempo](/timeline)
