---
title: pastinha loop
---

![pastinha loop video slide](https://codeberg.org/joenio/pastinha-loop/media/branch/main/pastinha-loop.png)

<iframe class="youtube" src="https://www.youtube.com/embed/eKiQNXJ6ghI?rel=0" frameborder="0" allowfullscreen></iframe>

Download and source-code: [codeberg.org/joenio/pastinha-loop](https://codeberg.org/joenio/pastinha-loop)

<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://open.audio/front/embed.html?&amp;type=track&amp;id=381079"></iframe>
