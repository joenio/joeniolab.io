---
title: Portfólio
---

## 2005, JáCotei.com.br

https://www.jacotei.com.br

com desenvolvimento de software para coleta de dados via WebSpider, Web Crawler e Robots escritos em Perl e banco de dados Oracle, além de realizar manutenção no frontend da aplicação em ColdFusion. Conclusão: 2007.<br/>

## 2006, Organização da sociedade civil pela defesa dos direitos humanos na internet SaferNet Brasil

https://safernet.org.br

como desenvolvedor de software trabalhando especialmente com Perl na manutenção de diversos sistemas de análise de dados, geração de relatórios e indicadores do portal da entidade. Conclusão: 2007.

## 2008, desenvolvedor Foswiki, empresa cooperativa Colivre

https://foswiki.org

## 2008, desenvolvedor e mantenedor Noosfero

https://gitlab.com/noosfero/noosfero

## 2014, Portal de participação social Participa.br, consultor no Programas das Nações Unidas para o Desenvolvimento no Brasil (PNUD)

https://participa.br

## 2016, Portal do Software Público Brasileiro, desenvolvedor sênior no laboratório Lappis

- https://softwarepublico.gov.br
- https://softwarepublico.gov.br/doc/

## 2020, Itaipava Verão 2020, projeto realizado com a [PencilLabs][]

Aplicativo mobile feito em Ionic e Angular com backend em Django Rest Framework, o aplicativo foi utilizado no carnaval 2020 numa campanha da Itaipava para reciclagem de embalagens de vidro, plástico e alumínio.

## 2020, SEEU - Justiça Presente

[Justiça Presente](https://www.cnj.jus.br/sistema-carcerario/justica-presente) do Conselho Nacional de Justica (CNJ) atuando com desenvolvimento Java e infraestrutura DevOps para melhoria, evolução e sustentação do [Sistema Eletrônico de Execução Unificado (SEEU)](https://www.cnj.jus.br/sistema-carcerario/sistema-eletronico-de-execucao-unificado-seeu) utilizado em todos os estados da federação Brasileira pelos tribunais de Justiça.

[pencillabs]: https://www.pencillabs.com.br/
