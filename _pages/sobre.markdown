---
title: Sobre mim
---

`>>` Leia esta página em [Português](/sobre). `*`<br/>
`--` Read this page in [English](/about).

[<img style="float:right; margin-top:-25px; margin-left:10px; margin-right: 10px; border: 0; box-shadow: inset 0 -3em 3em rgba(0, 0, 0, 0.1), 0 0 0 2px rgb(255, 255, 255), 0.3em 0.3em 1em rgba(0, 0, 0, 0.3);" src="/avatars/joenio-2024-nocolor-150x150.jpg"/>](/avatar)

<div style="text-align: justify">
{% include sobre.html %}

- [Instrutor The Carpentries](https://carpentries.org/instructors/#joenio)
- [Embaixador do Software Heritage](https://www.softwareheritage.org/ambassadors)
- [Desenvolvedor](https://qa.debian.org/developer.php?login=joenio@joenio.me) do [Debian](https://debian.org)
- Candidato PhD no [PGCOMP UFBA](https://pgcomp.ufba.br) e [UGE (Université Gustave Eiffel)](https://www.univ-gustave-eiffel.fr)
- Membro do [Laboratoire Interdisciplinaire Sciences Innovations Sociétés (LISIS)](http://umr-lisis.fr)
- Desenvolvedor backend nos projetos [Research Infrastructure for Science, technology and Innovation policy Studies (RISIS)](http://risis2.eu) e [CorTexT Platform](https://www.cortext.net)

## Contato

Contato pelo e-mail <a href="mailto:joenio@joenio.me">joenio@joenio.me</a> ou pelas [redes sociais](/curriculo).

</div>
