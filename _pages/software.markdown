---
title: Software
---

Trabalho profissionalmente com desenvolvimento de software desde 2005, tendo
trabalhado em diversos setores da iniciativa privada, pública, indústria e
pesquisa acadêmica, além de ter trabalhado também por muitos anos também como autônomo
e empreendedor.

Nesta página eu documento alguns dos projetos de software em desenvolvimento ou
finalizados, principalmente projetos de software livre.

## 2006, WWW::Scraper::ISBN::Siciliano_Driver

- [WWW::Scraper::ISBN::Siciliano_Driver](https://metacpan.org/pod/WWW::Scraper::ISBN::Siciliano_Driver)
- Papel: Autor

## 2009, Debian

- [debian.org](https://debian.org)
- Papel: Contribuidor

## 2010, Analizo

- [www.analizo.org](https://www.analizo.org)
- Papel: Mantenedor

## 2010, Doxyparse

- [github.com/doxygen/doxygen](https://github.com/doxygen/doxygen/tree/master/addon/doxyparse)
- Papel: Autor

## 2010, Foswiki VirtualHostingContrib

- [foswiki.org/Extensions/VirtualHostingContrib](https://foswiki.org/Extensions/VirtualHostingContrib)
- Papel: Co-autor

## 2012, google-apps

- [google-apps](https://metacpan.org/dist/GoogleApps/view/bin/google-apps)
- Papel: Autor

## 2012, Foswiki UnixUsersContrib

- [foswiki.org/Extensions/UnixUsersContrib](https://foswiki.org/Extensions/UnixUsersContrib)
- Papel: Autor

## 2013, UnRTF

- [UnRTF](https://metacpan.org/pod/UnRTF)
- Papel: Autor

## 2013, Graph::Writer::DSM

- [Graph::Writer::DSM](https://metacpan.org/pod/Graph::Writer::DSM)
- Papel: Autor

## 2015, holodev

- [github.com/joenio/holodev](https://github.com/joenio/holodev)
- Papel: Autor

## 2015, Noosfero

- [gitlab.com/noosfero/noosfero](https://gitlab.com/noosfero/noosfero)
- Papel: Desenvolvedor

## 2016, Alien::UnRTF

- [Alien::UnRTF](https://metacpan.org/pod/Alien::UnRTF)
- [github.com/joenio/p5-alien-unrtf](https://github.com/joenio/p5-alien-unrtf)
- Papel: Autor

## 2017, Alien::SLOCCount

- [Alien::SLOCCount](https://metacpan.org/pod/Alien::SLOCCount)
- [github.com/joenio/alien-sloccount](https://github.com/joenio/alien-sloccount)
- Papel: Autor

## 2017, Alien::Doxyparse

- [Alien::Doxyparse](https://metacpan.org/pod/Alien::Doxyparse)
- [github.com/joenio/alien-doxyparse](https://github.com/joenio/alien-doxyparse)
- Papel: Autor

## 2020, Cortext

- [cortext.net](https://cortext.net)
- Papel: Desenvolvedor

## 2020, RISIS RCF

- [rcf.risis.io](https://rcf.risis.io)
- Papel: Desenvolvedor

## 2021, dublang

- [dublang.4two.art](https://dublang.4two.art)
- Papel: Autor

## 2022, Dist::Zilla::Plugin::Authors

- [Dist::Zilla::Plugin::Authors](https://metacpan.org/pod/Dist::Zilla::Plugin::Authors)
- Papel: Autor
