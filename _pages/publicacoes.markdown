---
title: Publicações
---

Lista por ordem cronológica das minhas publicações, incluindo pesquisas sobre
ferramentas de análise estática e métricas de código-fonte, sustentabilidade de
software para pesquisa, software livre, demoscene, arte e live coding, entre
outros temas.

Você pode me encontrar também no [ORCID](https://orcid.org/0000-0002-5574-1370),
[Google Scholar](https://scholar.google.com.br/citations?user=F2cDhn8AAAAJ) ou no
[Currículo Lattes](http://lattes.cnpq.br/6773346025855005).

## 2024

### &#9900; [Assessing the free-open-source research software Analizo and publicising FRSM and RSMD](https://doi.org/10.5281/zenodo.14392148)
<span style='color:gray; margin-left:1.25em'>_2024. Joenio Marques da Costa. [FAIR-IMPACT: Expanding FAIR Solutions across EOSC, ID: 101057344](https://cordis.europa.eu/project/id/101057344)._</span>

## 2023

### &#9900; [Understanding Practices and Challenges of Developing Sustainable Research Software: A Pilot Interview](https://doi.org/10.5753/opensciense.2023.235677)
<span style='color:gray; margin-left:1.25em'>_2023. Daniela Feitosa, Christina von Flach, Joenio Costa. [3rd Workshop on Open Science Practices for Software Engineering](https://opensciense-org.github.io/opensciense2023/)._</span>

### &#9900; [Understanding Sustainability and FAIRness of Research Software](https://doi.org/10.5753/opensciense.2023.235707)
<span style='color:gray; margin-left:1.25em'>_2023. Daniela Feitosa, Christina von Flach, Joenio Costa. [3rd Workshop on Open Science Practices for Software Engineering](https://opensciense-org.github.io/opensciense2023/)._</span>

### &#9900; [Princípios e Práticas para Sustentabilidade do Software de Pesquisa](https://doi.org/10.5753/sbc.12853.0.3)
<span style='color:gray; margin-left:1.25em'>_2023. Christina von Flach, Joenio M. Costa, Daniela Feitosa. [42ª Jornada de Atualização em Informática (JAI) 2023](https://csbc.sbc.org.br/2023/jai/)._</span>

## 2021

### &#9900; [Demoscene no Brasil: Um Mapeamento Quasi-Sistemático da Literatura](https://doi.org/10.5281/zenodo.5803520)
<span style='color:gray; margin-left:1.25em'>_2021. Joenio Marques da Costa. [VIII Simpósio Internacional de Inovação em Mídias Interativas](https://siimi.medialab.ufg.br/p/38651-anais-2021)._</span>

## 2020

### &#9900; [Benefits and Challenges of Open-Source Health informatics Systems: A Systematic Search of Projects and Literature Review](https://doi.org/10.2196/preprints.18489)
<span style='color:gray; margin-left:1.25em'>_2020. Carla Rocha, Joenio Costa, Paulo Meirelles. [JMIR Preprints](https://preprints.jmir.org)._</span>

### &#9900; (Orientacoes e bancas) [Ferramenta de acompanhamento musical: uma implementação com tecnologias web](https://bdm.unb.br/handle/10483/30366)
<span style='color:gray; margin-left:1.25em'>_2020. Co-supervisor on Undergraduation course conclusion project, Universidade de Brasilia, Author: Hugo Neves de Carvalho._</span>

## 2019

### &#9900; [Demo Art e Visual Music em perspectiva](https://doi.org/10.31219/osf.io/48wfp)
<span style='color:gray; margin-left:1.25em'>_2019. Joenio Marques da Costa. [18o Encontro internacional de Arte, Ciência Tecnologia](https://art.medialab.ufg.br/p/28969-18-art-2019)._</span>

### &#9900; [Análise Estática de Código-Fonte](https://doi.org/10.48550/arXiv.1907.00143)<br/>
<span style='color:gray; margin-left:1.25em'>_2019. Joenio Marques da Costa. [Informally-published-arXiv-preprint](https://arxiv.org)._</span>

### &#9900; [Vivência Amazônica: aprendizados e culturas](https://aulp.org/atas-do-xxix-encontro-da-aulp-lisboa-2019)
<span style='color:gray; margin-left:1.25em'>_2019. Ana Paula Vidal Bastos, Enaile do Espirito Santo Iadanza, Ingrid Soares Albuquerque, Joenio Marques da Costa, Larissa Gomes Machado, Manoel Pereira de Andrade. [XXIX Encontro Associação das Universidades de Língua Portuguesa](https://sri.ufg.br/n/115431-xxix-encontro-da-associacao-das-universidades-de-lingua-portuguesa), Lisboa._</span>

## 2018

### &#9900; [On the sustainability of academic software: the case of static analysis tools](https://doi.org/10.1145/3266237.3266243)
<span style='color:gray; margin-left:1.25em'>_2018. Joenio Costa, Christina Chavez, Paulo Meirelles. [32nd Brazilian Symposium on Software Engineering, SBES](https://dl.acm.org/doi/10.1145/3266237)._</span>

### &#9900; [Participação social no software Noosfero](https://ridi.ibict.br/handle/123456789/1111)
<span style='color:gray; margin-left:1.25em'>_2018. Luana Melody Brasil, Barcelos Janinne, Joenio Costa, Estela Monteiro, Milton Shintaku. Book published by the [Instituto Brasileiro de Informação em Ciência e Tecnologia (IBICT)](https://www.gov.br/ibict)._</span>

### &#9900; (Orientacoes e bancas) [Um estudo empírico sobre métricas de código fonte do Android API Framework]( https://bdm.unb.br/handle/10483/215750)
<span style='color:gray; margin-left:1.25em'>_2018. Juri invited on Undergraduation course conclusion project, Universidade de Brasilia, Author: Victor Henrique Magalhães Fernandes._</span>

## 2017

### &#9900; (Mestrado) [Sustentabilidade Técnica de Software Acadêmico no Domínio de Ferramentas de Análise Estática](https://repositorio.ufba.br/handle/ri/32468)

<span style='color:gray; margin-left:1.25em'>_2017. Joenio M. Costa. Dissertação: [Programa de Pós-Graduação em Ciência da Computação (PGCOMP)](https://pgcomp.ufba.br)._</span>

## 2010

### &#9900; [Analizo: an extensible multi-language source code analysis and visualization toolkit](https://www.analizo.org/publications/analizo-cbsoft2010-tools.pdf)
<span style='color:gray; margin-left:1.25em'>_2010. Antonio Terceiro , Joenio Costa , João Miranda, Paulo Meirelles, Luiz Romário Rios, Lucianna Almeida, Christina Chavez, Fabio Kon. Tools Session of the [1st Brazilian Conference on Software](https://web.archive.org/web/20220808184108/https://wiki.dcc.ufba.br/CBSOFT/WebHome)._</span>

## 2009

### &#9900; (Graduação) [Extração de informações de dependência entre módulos de programas c/c++](https://github.com/joenio/monografia-ucsal-2009/blob/8d9556fe09c758cd90020d7842d4e2c28747d88e/monografia.pdf)
<span style='color:gray; margin-left:1.25em'>_2009. Joenio Marques da Costa. Undergraduation course conclusion project, [Universidade Católica do Salvador](https://ucsal.br)._</span>
