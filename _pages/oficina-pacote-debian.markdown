---
title: Oficina de empacotamento Debian
---

- Duracao: 2 x 2h

Agenda:
- Setup do ambiente de desenvolvimento Debian
- Criar um pacote 'Hello, world'
- Criar um novo pacote e submeter ao Debian
- Atualizar a versao de um pacote Debian

<!--
- Contribuir com o pacote kworkflow no Debian
  - https://tracker.debian.org/pkg/kworkflow
-->

Material de referência:
- [Tutorial de empacotamento Debian - Parte 1](/tutorial-pacote-debian-parte1)
- [Tutorial de empacotamento Debian - Parte 2](/tutorial-pacote-debian-parte2)
- [Tutorial de empacotamento Debian - Parte 3](/tutorial-pacote-debian-parte3)

Slides:
- [Debian - Introdução](/debian-introducao)

Veja também:
- [Aprenda a criar repositórios de pacotes Debian, 2015](/aprenda-a-criar-repositorios-de-pacotes-debian)
- [Empacotando módulos Perl no Debian, 2019](/empacotando-modulos-perl-no-debian)
- [Como atualizar pacotes Perl no Debian - Passo-a-passo prático e detalhado, 2020](/como-atualizar-pacotes-perl-no-debian)
- [debian-dev - my debian workflow helpers](https://gitlab.com/joenio/debian-dev)
- [gpg-keysign - My GnuPG key signing workflow](https://gitlab.com/joenio/gnupg-keysign)
