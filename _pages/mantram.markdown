---
title: MANTRAM - Wireflores
---

![bioborgs album cover 01](/files/mantram-2020-wireflores.jpg)

**Wireflores** é uma música composta com técnicas de live coding para o álbum
MANTRAM de Biophillick lançado em 20 de Maio de 2020.

* [Ouça o álbum MANTRAM completo no Bandcamp](https://biophillick.bandcamp.com/album/mantram)

<iframe class="youtube" src="https://www.youtube.com/embed/f9SRDF3WRdc?rel=0" frameborder="0" allowfullscreen></iframe>

Music Video da música Wireflores para o álbum MANTRAM de Biophillick e
convidados, filmado, editado e composto por Joenio M. Costa.

* [Veja aqui mais informações sobre o album](https://biophillick.com/MANTRAM)
