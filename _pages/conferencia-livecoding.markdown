---
title: Conferência de Live Coding
---

- Duracao: 2h

Conferência e bate-pepo aberto sobre o live coding abordando o histórico da
prática, as ferramentas mais comuns, demonstração prática e ao vivo, bem como
referências dos artistas centrais e periféricos desta linguagem artística.

Agenda:

Primeiros live coders:
- Lee Scratch Perry: https://www.youtube.com/watch?v=y651C7aNXRc



- Historico live coding, toplap, algorave, clic, algorave brasil
- Examplo demonstracao com tidal cycles (via dublang)
- Congressos, shows, pesquisa academica, experimentos, publicacoes
- Ferramentas, sonic pi, hydra, orca, tidal, gibber, DAW, jack, supercollider, pipewire, obs
- Albuns, artistas e figuras expoentes da comunidade mundial
- Politica, diversidade, questoes tecno-sociais e debate
- Perguntas

- [Do sintetizador em hardware ao software, ou softsynths](/electronic-music-with-debian): 30 min
- [Definicao e historia do Live Coding](/live-coding) e a [A importancia do software livre](/software-livre): 30 min
- Apresentar exemplos de ferramentas de Live Coding: 15 min
- [Como instalar e configurar Sonic Pi](/sonic-pi): 30 min
- [Explorando o Sonic Pi](/sonic-pi): 30 min
- [A linguagem do Sonic Pi](/sonic-pi): 30 min
- Dúvidas, papo aberto e curiosidades: 30 min
- Extra: DAW, Audacity, LMMS, Ardour, LADSPA plugin, sox, Mixxx
- Extra: Jack, PipeWire, OSC, MIDI
- Extra: Como gerenciar os codigos e [Primeiros passos com Git](/git) + Gitlab, Github...: 30 min
- Extra: [Como instalar Tidal Cycles e primeiros passos](/debconf22-live-coding-workshop)
