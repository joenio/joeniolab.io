---
title: Memes favoritos
---

Uma curadoria de memes fundamentais.

## A Vingança do Ídalo

<iframe class="youtube" src="https://www.youtube.com/embed/V1_rhX2gP9s?rel=0" frameborder="0" allowfullscreen></iframe>

* [A Vingança do Ídalo no IMDb](https://www.imdb.com/title/tt6471782)
* Producao: [Tela Class](https://pt.m.wikiquote.org/wiki/Tela_Class)

## Bátima Feira da Fruta

<iframe class="vimeo" src="https://player.vimeo.com/video/120126449?h=2e8636dbfb" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>


* [Video no Archive.org](https://archive.org/details/batima-feira-da-fruta)
* [Feira da Fruta na Wikipedia](https://pt.wikipedia.org/wiki/Feira_da_Fruta)
* [Blog Oficial Batima Feira da Fruta](http://batimahq.blogspot.com)
* [Bátima - Feira da Fruta no Facebook](https://www.facebook.com/batimafeiradafruta)

## This is Fine

<video controls>
  <source src="/files/paulo_crumbim_-_fiz_uma_releitura_dessa_obra_de_arte___-1498302952204873743.mp4">
</video>

Fonte: https://twitter.com/crumbim/status/1498302952204873743?s=09

## Confused Travolta

![confused travolta gif](/files/confused-travolta-giphy.gif)

Source: [giphy.com](https://giphy.com/gifs/high-quality-highqualitygifs-g01ZnwAUvutuK8GIQn).

* [confused batman travolta gif](/files/confused-batman-travolta-giphy.gif), [source](https://giphy.com/gifs/morphin-confused-batman-travolta-LnRks3nDpxL8AHLZzE)
* [confused travolta gif (green background)](/files/confused-travolta-green-background-giphy.gif), [source](https://giphy.com/gifs/nba-20k1punZ5bpmM)

## Trenzinho Carreta Furacão

<iframe class="youtube" src="https://www.youtube.com/embed/tmJ0tzAZ4aM?rel=0" frameborder="0" allowfullscreen></iframe>

## Bambam BIRL! A Hora da Aventura

<iframe class="youtube" src="https://www.youtube.com/embed/v0o6pJRF7BU?rel=0" frameborder="0" allowfullscreen></iframe>

## O Destino de Miguel

<iframe class="youtube" src="https://www.youtube.com/embed/O1zkrCrjcjA?rel=0" frameborder="0" allowfullscreen></iframe>

* https://www.dailymotion.com/video/x2ltid9
* https://filmow.com/o-destino-de-miguel-t21722/
* https://vimeo.com/52055435

## Gari agradecendo a ponta do baseado

<iframe class="youtube" src="https://www.youtube.com/embed/3-CZBIAFgG0?rel=0" frameborder="0" allowfullscreen></iframe>

## Julius: Se não comprar nada, o desconto é maior

![image meme julius, se não comprar nada, o desconto é maior](/files/julius-nao-comprar-desconto-maior.jpg)

Fonte: http://valoresreais.com/2021/02/15/se-eu-nao-comprar-nada-o-desconto-e-maior/
