---
title: Páginas
layout: page
---

* [Receitas favoritas](/receitas)
* [Coleção de livros distópicos](/livros)
* [Slides](/slides)
* [Sobre mim](/sobre)
* [djalgoritmo](/djalgoritmo)
* [Portfolio](/portfolio)
* [Links](/links)
