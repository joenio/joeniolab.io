---
title: Seminário sobre métricas de código-fonte e ferramentas de análise estática
---

<!-- TODO:
- [ ] criar slides para usar como guia no seminario
- [ ] testar Pyan-Analizo e documentar instalacao e uso
- [ ] fazer breve revisao de literatura sobre analise estatica e metricas
  - [ ] trazer as novidades do campo descobertas na literatura para o seminario
-->

- Duracao: 1h40 minutos
- Data: 12 de Novembro de 2024
- Disciplina de Engenharia de Software - USP

Agenda:
- Introdução a análise estática e métricas de código-fonte
- Ferramentas de análise estática, Analizo, Doxygen e Doxyparse
- Pyan-Analizo e análise estática de código-fonte Python
- Como instalar e usar Analizo para extrair métricas de código-fonte Python

Slides:
- [Seminário sobre métricas de código-fonte e ferramentas de análise estática](/seminario-analise-estatica-e-metricas-de-codigo)

Material de referência:
- [Análise Estática de Código-Fonte](https://doi.org/10.48550/arXiv.1907.00143)
- [On the sustainability of academic software: the case of static analysis tools](https://doi.org/10.1145/3266237.3266243)
- [Analizo: an extensible multi-language source code analysis and visualization toolkit](https://www.analizo.org/publications/analizo-cbsoft2010-tools.pdf)

Referências adicionais:
- [Analizo, 15 years of a multi-language Research Software tool for source code analysis](/analizo-15-years)
- [Analizo (draft)](/analizo)
- [Caracterização da complexidade estrutural em ferramentas de análise estática de código-fonte](/caracterizacao-analise-estatica)
- [On the Sustainability of Academic Software - The Case of Static Analysis Tools](/sustainability-academic-software)
- [Extração de informações de dependência entre módulos de programas c/c++](https://github.com/joenio/monografia-ucsal-2009/blob/8d9556fe09c758cd90020d7842d4e2c28747d88e/monografia.pdf)
- [Sustentabilidade Técnica de Software Acadêmico no Domínio de Ferramentas de Análise Estática](https://repositorio.ufba.br/handle/ri/32468)

Software:
- [github.com/analizo/analizo](https://github.com/analizo/analizo)
- [github.com/doxygen/doxygen](https://github.com/doxygen/doxygen)
- [github.com/LeaoLuciano/pyan-analizo](https://github.com/LeaoLuciano/pyan-analizo)
