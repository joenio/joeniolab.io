---
title: cubes 2800
---

![cubes 2800 structure synth rendered image](https://codeberg.org/joenio/cubes-2800/raw/branch/main/cubes-2800-no-remix.png)

<iframe class="youtube" src="https://www.youtube.com/embed/lSK7Uf0SW7Y?rel=0" frameborder="0" allowfullscreen></iframe>

source code and more info at [codeberg.org/joenio/cubes-2800](https://codeberg.org/joenio/cubes-2800)
