---
title: Currículo e redes sociais
---

## Curriculo

* Currículo resumido em `pdf`: [curriculo.pdf](/files/curriculo.pdf)
  * Currículo resumido (perfil artistico) em `pdf`: [djalgoritmo.pdf](/files/djalgoritmo.pdf)
* Currículo resumido em `pdf` (em inglês): [resume.pdf](/files/resume.pdf)
* Currículo lattes: <a href="http://lattes.cnpq.br/6773346025855005">http://lattes.cnpq.br/6773346025855005</a>

## Contatos e redes sociais

* Email: <a href="mailto:joenio@joenio.me">joenio@joenio.me</a>
* <a class="u-url" rel="me" href="https://www.linkedin.com/in/joenio">LinkedIn</a>
* <a class="u-url" rel="me" href="https://twitter.com/joenio">Twitter</a>
* <a class="u-url" rel="me" href="https://mastodon.social/@joenio">Mastodon</a>
* <a class="u-url" rel="me" href="https://github.com/joenio">GitHub</a>
* <a class="u-url" rel="me" href="https://gitlab.com/joenio">GitLab</a>
* <a class="u-url" rel="me" href="https://www.youtube.com/c/JoenioCosta">YouTube</a>
* <a class="u-url" rel="me" href="https://instagram.com/joenio.costa">Instagram</a>
* <a class="u-url" rel="me" href="http://soundcloud.com/joenio">SoundCloud</a>
* [Bandcamp](https://djalgoritmo.bandcamp.com)
* [Audius.co](https://audius.co/joenio)

[Linha do tempo](/timeline) com descrição da minha formação educacional,
atuação profissional, cultural e artística ao longo dos anos.
