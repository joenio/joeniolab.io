---
title: "Du vent et du sable: une transposition de dune à Paris (PT-BR)"
---

`-` Lire cette page en [Fraçais](/du-vent-et-du-sable.fr). <br/>
`>` Leia esta página em [Português Brasil](/du-vent-et-du-sable). <br/>
`-` Read this page in [English](/du-vent-et-du-sable.en).

***Du vent et du sable: une transposition de dune à Paris***
é um ato performático de transposição de território do Estado Sensível
realizado pelos artistas Mari Moura (1ª ministra do Estado Sensível) e Joenio
M. Costa (Assessor do Estado Sensível). Durante a performance será instalada a
primeira Duna da cidade de Paris, com areia deslocada do Parque da Cidade Don
Nivaldo Monte, uma unidade de conservação dunar situada na cidade de Natal do
Estado do Rio Grande do Norte no Brasil. Os artistas desenvolvem suas ações
com objetos relacionais, conexão on line em rede social e live coding.

<figure>
  <img src="/files/du-vent-et-du-sable.jpg" alt="Flyer de divulgacao da performance Du vent et du sable">
  <legend>Flyer de divulgacao da performance Du vent et du sable</legend>
</figure>

**Joenio M. Costa** é artista computacional e músico experimental com
interesse em música algorítmica, áudio-visual, demoscene e live coding, é
integrante da orquestra BSBLOrk ([bsblork.gitlab.io](https://bsblork.gitlab.io)) e do coletivo Nômade Lab
([nomadelab.gitlab.io](https://nomadelab.gitlab.io)), trabalha como pesquisador engenheiro de software
(Research Software Engineer) no laboratório de ciência e inovação LISIS
([umr-lisis.fr](https://umr-lisis.fr)).

**Mari Moura** ([@marimoura5](https://instagram.com/marimoura5)) é artista de performance e de teatro interessada na
relação entre corpo e tecnologia. É professora de arte no ensino técnico e
tecnológico no IFRN/BR e integrante do Grupo Estandarte de Teatro
([@grupoestandartedeteatro](https://instagram.com/grupoestandartedeteatro)). Realiza pesquisa em corpo, presença, arte e
tecnologia no programa de doutorado da universidade de Brasília (UNB) em
colaboração com o Institut des sciences du sport-santé de Paris V (I3SP).

<figure>
  <img src="/files/du-vent-et-du-sable-montagem.png" alt="Du vent et du sable: une transposition de dune à Paris">
  <legend>Du vent et du sable: une transposition de dune à Paris</legend>
</figure>

Todos os scripts, codigo-fonte, samples, poesias, fotos, ideias e _tudo o
mais_ sobre a performance "Du vent et du sable: une transposition de dune à
Paris" esta disponivel em
[gitlab.com/joenio/du-vent-et-du-sable](https://gitlab.com/joenio/du-vent-et-du-sable).

## Divulgação

![Banner](/files/du-vent-et-du-sable-post-instagram.png)

Texto para divulgação da performance "Du vent et du sable: une transposition de
dune à Paris" nas redes sociais, email e outros espaços.

Gente, nós (Joenio Costa e Mari Moura) convidamos vocês para presenciar a nossa
performance "Du vent et du sable: une transposition de dune à Paris" no dia da
festa do trabalho 01 de Maio de 2022, às 19h no 32 Boulevard Saint-Marcel,
Paris. Vamos realizar uma ação com live coding e performance arte em
entrepresença online e instalaremos a primeira Duna da cidade de Paris com
areia deslocada da cidade de Natal/RN, Brasil.

Quem quiser chegar antes das 19h poderá acompanhar as prévias da performance,
música e drink a partir das 15h com presença de outros artistas.

Mais informações em [{{ site.url }}/du-vent-et-du-sable](/du-vent-et-du-sable).

Agradecemos antecipadamente, Joenio Costa e Mari Moura.

<iframe class="youtube" src="https://www.youtube.com/embed/LwmnLWWxJYA?rel=0" frameborder="0" allowfullscreen></iframe>

### Texto para Twitter, Mastodon, etc

Gente, eu e @Marimoura5 convidamos para a performance “Du vent et du sable: une
transposition de dune à Paris” com live coding e performance arte em
entrepresença online, onde será instalada a primeira Duna de Paris com areia de
Natal/RN. +info [{{ site.url }}/du-vent-et-du-sable](/du-vent-et-du-sable)

### Texto para Telegram, WhatsApp, etc

Gente, eu e Mari vamos realizar uma performance chamada “Du vent et du sable:
une transposition de dune à Paris” com live coding e performance arte em
entrepresença online, onde será instalada a primeira Duna de Paris com areia de
Natal/RN. Caso tenham conhecidos ou estejam pela cidade fica o convite, +info
[{{ site.url }}/du-vent-et-du-sable](/du-vent-et-du-sable)

<em>Os textos e fotos desta pagina foram criados por Joenio Costa e Mari Moura.</em>
