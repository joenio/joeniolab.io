---
title: Tutorial de empacotamento Debian - Parte 2
package:
  cpan: "Getopt::EX::Hashed"
  url: "https://metacpan.org/pod/Getopt::EX::Hashed"
  debian: libgetopt-ex-hashed-perl
---

![Debian shot wallpaper por mdh3ll](/files/debian_shot_by_mdh3ll_v3.jpg)

Este tutorial descreve passo-a-passo como criar pacotes de bibliotecas da
linguagem de programação [Perl][] para o sistema operacional [Debian][debian]
e submetê-lo aos repositórios oficiais via [Debian Perl Group][debian-perl-group].

O Debian é um sistema operacional [livre][] criado e mantido por um grupo
independente de desenvolvedores espalhados ao redor do mundo, o projeto foi
iniciado em 1993 e tem sido desenvolvido abertamente desde então, sempre
seguindo o espírito livre do [projeto GNU][GNU].

## Roteiro deste tutorial

Esta é a segunda parte do **Tutorial de empacotamento Debian** e é importante
que você
tenha feito a parte 1, caso contrário volte a parte 1
e siga ao menos as etapas de configuração do ambiente de desenvolvimento, link abaixo.

- [Tutorial de empacotamento Debian - Parte 1](/tutorial-pacote-debian-parte1#configurar-o-ambiente-de-desenvolvimento)

Após fazer as configurações básicas e instalar os pacotes necessários vamos iniciar
a parte 2 deste tutorial seguindo o roteiro abaixo.

* Selecionar um dos pacotes candidatos para empacotamento
* Criar o pacote da biblioteca selecionada
* Construir o pacote e verificar problemas
* Executar testes automatizadod do pacote
* Submeter o pacote aos repositórios oficiais via Debian Perl Group

### Pacotes candidatos para empacotamento

Seleciona uma das bibliotecas abaixo, algumas já possuem [bug do tipo RFP](https://wnpp.debian.net/?type%5B%5D=RFP&project=perl&description=&owner%5B%5D=yes&owner%5B%5D=no&col%5B%5D=reporter&sort=dust%2Fasc)
aberto no
 [sistema de acompanhamento de bugs](https://wiki.debian.org/pt_BR/BTS) (Bug Tracking System - BTS) do Debian,
as demais são bibliotecas Perl com [upload recente no cpan.org](https://metacpan.org/recent)
ainda nao empacotados no Debian, e que possuam uma ou mais dependencias reversas, isso significa que a biblioteca tem
usuários.

* [Crypt::OpenSSL::CA](https://metacpan.org/pod/Crypt::OpenSSL::CA) - [Bug RFP #1016950](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1016950)
* [Date::Holidays::AW](https://metacpan.org/pod/Date::Holidays::AW)
* [Date::Holidays::NL](https://metacpan.org/pod/Date::Holidays::NL)
* [DateTime::Format::Atom](https://metacpan.org/pod/DateTime::Format::Atom) - [Bug RFP #875785](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=875785)
* [Devel::AssertOS](https://metacpan.org/pod/Devel::AssertOS)
* [Getopt::EX::Hashed](https://metacpan.org/pod/Getopt::EX::Hashed) **esta foi a biblioteca que eu selecionei e os exemplos escritos serão com ela, por favor selecione uma biblioteca diferente**
* [Git::CPAN::Patch](https://metacpan.org/pod/Git::CPAN::Patch) - [Bug RFP #733922](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=733922)
* [Hash::Wrap](https://metacpan.org/pod/Hash::Wrap)
* [Mail::GPG](https://metacpan.org/pod/Mail::GPG) - [Bug RFP #866593](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=866593)
* [Module::Generic](https://metacpan.org/pod/Module::Generic)
* [Net::AMQP::RabbitMQ](https://metacpan.org/pod/Net::AMQP::RabbitMQ) - [Bug RFP #1005216](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1005216)
* [PDL::Graphics::PLplot](https://metacpan.org/pod/PDL::Graphics::PLplot) - [Bug RFP #763203](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=763203)
* [Perl::Critic::Plicease](https://metacpan.org/pod/Perl::Critic::Plicease)
* [Prima](https://metacpan.org/pod/Prima)
* [Term::ANSIColor::Concise](https://metacpan.org/pod/Term::ANSIColor::Concise)
* [WebService::SSLLabs](https://metacpan.org/pod/WebService::SSLLabs) - [Bug RFP #853780](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=853780)

<!--
* [Mo](https://metacpan.org/pod/Mo) **problema ao criar, nao cria com nome libmo-perl**
* [Mo::utils](https://metacpan.org/pod/Mo::utils)
* [Mo::utils::Language](https://metacpan.org/pod/Mo::utils::Language)
-->

## Selecionar um dos pacotes abaixo para empacotamento

Selecione um dos pacotes candidatos acima e siga os passos que se seguem adaptando
sempre o nome da biblioteca e o nome do pacote que você escolheu, o tutorial irá descrever os passos
para o pacote {{ page.package.cpan }}, você deve selecionar um pacote diferente entre os pacotes acima,
ou se preferir pode selecionar outra biblioteca qualquer que ainda não esteja empacotada no Debian.

<!--
Neste tutorial será demonstrado
como submeter pacotes ao projeto oficial,
resolver e documentar questões sobre licenças, e uma série de outras
informações relacionadas ao processo de empacotamento Debian, 
-->

## Criar o pacote da biblioteca selecionada

Antes de criar a versão inicial do pacote verifique se o pacote não está
empacotado no Debian.

<pre class="terminal">
<code>
sudo apt-file update
dh-make-perl locate {{ page.package.cpan }}
</code>
</pre>

```
== dh-make-perl 0.124 ==
Using cached Contents from Wed May 15 21:03:54 2024
Getopt::EX::Hashed is not found in any Debian package
```

Garantindo que o pacote não está presente, vamos seguir e criar o template inicial
do pacote usando o `dh-make-perl`.

<pre class="terminal">
<code>
dh-make-perl --pkg-perl --cpan {{ page.package.cpan }}
</code>
</pre>

## Correções básicas no template inicial

O `dh-make-perl` criou o pacote inicial {{ page.package.debian }} para a biblioteca {{ page.package.cpan }},
é necessário realizar algumas correções.

### Arquivo `debian/copyright`

A primeira correção é o ano e o email do autor do pacote, será preciso buscar essas
duas informações no código fonte da biblioteca {{ page.package.cpan }}.

```config
Files: *
Copyright: <INSERT COPYRIGHT YEAR(S) HERE>, Kazumasa Utashiro
License: Artistic or GPL-1+
```

Podemos analisar os arquivos manualmente abrindo de um em um para encontrar
essa informação ou usar os comandos abaixo para nos auxiliar.

[grep](https://manpages.debian.org/testing/grep/grep.1p)
<pre class="terminal">
<code>
grep -i -r --exclude-dir=.git --exclude-dir=debian copyright
</code>
</pre>

<!--
Ferramentas para ajudar criacao e atualizadao do `debian/copyright`.
scan-copyrights - usa o licensecheck e propoe conteudo para d/copyright
-->

[licensecheck](https://manpages.debian.org/testing/licensecheck/licensecheck.1p)
<pre class="terminal">
<code>
licensecheck --shortname-scheme=debian,spdx --copyright --recursive .
</code>
</pre>


[scan-copyrights](https://manpages.debian.org/testing/libconfig-model-dpkg-perl/scan-copyrights.1p)
<pre class="terminal">
<code>
scan-copyrights
</code>
</pre>

Analisando a saída de cada um desse comandos foi possível identificar que o ano
do copyright do {{ page.package.cpan }} é `2021-2024` e o email do author é `kaz@utashiro.com`, essas duas
alterações no arquivo `debian/copyright` nos dá o seguinte diff abaixo.

```diff
joenio@debian-dev-main:~/tutorial/libgetopt-ex-hashed-perl$ git diff
diff --git a/debian/copyright b/debian/copyright
index 4860058..018e342 100644
--- a/debian/copyright
+++ b/debian/copyright
@@ -12,7 +12,7 @@ DISCLAIMER: This copyright info was automatically extracted
  with this file.
 
 Files: *
-Copyright: <INSERT COPYRIGHT YEAR(S) HERE>, Kazumasa Utashiro
+Copyright: 2021-2024, Kazumasa Utashiro <kaz@utashiro.com>
 License: Artistic or GPL-1+
 
 Files: debian/*
```

Commit.

<pre class="terminal">
<code>
git commit debian/copyright -m 'd/copyright: add upstream copyright year/email'
</code>
</pre>

Remova também o **DISCLAIMER** adicionado pelo `dh-make-perl` e commit.

<pre class="terminal">
<code>
git commit debian/copyright -m 'd/copyright: remove dh-make-perl disclaimer'
</code>
</pre>

No exemplo seguido aqui
essas duas alterações são suficientes mas é possível que a
biblioteca que você escolheu necessite de mais alterações, é possível que
exista mais de um nome declarado como dono do copyright nos arquivos de
código-fonte, é necessário adicionar cada nome com seus respectivos ano e email.

A título de exemplo é possível definir algo como no exemplo abaixo, com múltiplos nomes,
e ano diferentes no arquivo `debian/copyright`.


```
Files: debian/*
Copyright: 2006, Florian Ragwitz <rafl@debian.org>
 2008-2014, gregor herrmann <gregoa@debian.org>
 2008, Damyan Ivanov <dmn@debian.org>
License: Artistic or GPL-1+
```

<!--
Eu costumo usar o comando acima e verifico manualmente os arquivos que me
chamam atenção, mas caso você queira algo mais automatizado verifique os links
abaixo para encontrar opções de ferramentas.

* [https://wiki.debian.org/CopyrightReviewTools](https://wiki.debian.org/CopyrightReviewTools)
* [BoF debian/copyright DebConf 2019](https://www.youtube.com/watch?v=2eOef3xdk5Y)

Se preferir adicione também o seu nome e endereço de email na seção _copyright_
dos arquivos `debian/*`, veja exemplo abaixo.

```
Files: debian/*
Copyright:
  © 2012-2013,2015, Jonas Smedegaard <dr@jones.dk>
  © 2020, Florian Schlichting <fsfs@debian.org>
  © 2020, Joenio Marques da Costa <joenio@joenio.me>
```

Mais uma vez, não esqueça de realizar commits para cada alteração lógica.

<pre class="terminal">
<code>
git commit -a -m 'Add myself to d/copyright'
</code>
</pre>
-->

#### O que fazer se upstream não informar corretamente o ano do copyright?

Caso o _upstream_ não forneça informação sobre o _copyright_ de
algum arquivo
será necessário documentar isso de alguma forma, uma opção é
adotar o padrão [Berne Convention Comment][berne-convention] e adicionar o seguinte
comentário.

<!--
Se o upstream não informar o ano nas notas de copyright do código fonte então
será necessário documentar isso de alguma forma, as opções são (1) fazer um
comentário como feito no
[liblog-dispatch-config-perl/debian/copyright][liblog-dispatch-config-perl/copyright]
ou (2) utilizar o padrão [Berne Convention Comment][berne-convention].

(1) liblog-dispatch-config-perl:

```yaml
Comment: Rationale from the author was:
  All of my modules available at http://search.cpan.org/~miyagawa/ with the
  statement "AUTHOR: Tatsuhiko Miyagawa" are, unless otherwise noted,
  Copyright (c) Tatsuhiko Miyagawa.
```

(2) Berne Convention:
-->

```yaml
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.
```

### Arquivo `debian/control`

Verifique o arquivo `debian/control` no seu editor de texto preferido e
se necessário atualize o campo `Standards-Version` para versão `4.7.0`.

```config
Standards-Version: 4.7.0
```

Faça commit da mudança.

<pre class="terminal">
<code>
git commit debian/control -m 'declare compliance with Debian Policy 4.7.0'
</code>
</pre>

Verifique também a versão do `debhelper-compat`, deve estar com
a versão mais recente `13`.

```config
Build-Depends: debhelper-compat (= 13),
```

E commit caso tenha alterado o arquivo.

<pre class="terminal">
<code>
git commit debian/control -m 'update debhelper compat to 13'
</code>
</pre>

Atualize a descrição do pacote,
tanto para a descrição curta (_synopsis_), quanto para a descrição longa (_extended description_),
veja por exemplo o diff com a descrição do {{ page.package.debian }}.

```diff
joenio@debian-dev-main:~/tutorial/libgetopt-ex-hashed-perl$ git diff
diff --git a/debian/control b/debian/control
index d5bdf38..a5db9da 100644
--- a/debian/control
+++ b/debian/control
@@ -20,19 +20,10 @@ Architecture: all
 Depends: ${misc:Depends},
          ${perl:Depends},
          libscalar-list-utils-perl
-Description: Hash store object automation for Getopt::Long
- Getopt::EX::Hashed is a module to automate a hash object to store command
- line option values for Getopt::Long and compatible modules including
- Getopt::EX::Long. Module name shares Getopt::EX prefix, but it works
- independently from other modules in Getopt::EX, so far.
+Description: Perl module to automatically store Getopt::Long options into Hash
+ The Getopt::EX::Hashed is a module to automate a hash object to store command
+ line option values for Getopt::Long and compatible modules, including
+ Getopt::EX::Long.
  .
- Major objective of this module is integrating initialization and
- specification into single place. It also provides simple validation
- interface.
- .
- Accessor methods are automatically generated when is parameter is given. If
- the same function is already defined, the program causes fatal error.
- Accessors are removed when the object is destroyed. Problems may occur when
- multiple objects are present at the same time.
- .
- This description was automagically extracted from the module by dh-make-perl.
+ The module name shares the Getopt::EX namespace, but it works independently
+ from other modules with the Getopt::EX prefix.
```

Commit as modificações feita na descrição.

<pre class="terminal">
<code>
git commit debian/control -m 'rephrase synopsis and extended description'
</code>
</pre>

#### Atenção sobre dependendências declaradas no `debian/control`

Não se deve incluir dependencias para os pacotes `perl-modules` ou `perl-base`,
apenas para `perl`, lembrar que `perl` é fornecido por `${perl:Depends}`.
Verifique a versão das dependências, nem sempre as sugestões dos
desenvolvedores no `Makefile.PL` está correta, ou pode ocorrer do Debian não
possuir versão mais antiga que as versões no `Makefile.PL`.

<!--

Neste ponto já temos o pacote minimamente pronto, vamos testar e usar o
`lintian` para debugar possíveis problemas.

É importante também revisar cada dependência declarada no `debian/control` e
analisar se ainda são válidas, eventualmente o _upstream_ alterou as
dependências, incluiu novas, removeu antigas, etc... é muito importante manter
apenas as dependências válidas.


Além de manter apenas as dependências válidas é boa prática manter a lista de
dependencias em ordem alfabética, eu costumo utilizar o próprio editor **vim**
para ordenar listas, seleciono a lista de dependencias e executo o comando
`:!sort`.

Commite as alteração adicionando _"Gbp-Dch: Ignore"_ na mensagem de commit, use
na mensagem de commit algo como _"(Re-)sort dependencies"_, veja no exemplo
abaixo o diff dessa correção para o pacote **libemail-mime-contenttype-perl**.

```diff
commit 70126faa23ad439d978f9d60c37d6cdd7ac6bee3

    (Re-)sort dependencies
    
    Gbp-Dch: ignore

diff --git a/debian/control b/debian/control
index 4b9399a..0f1406e 100644
--- a/debian/control
+++ b/debian/control
@@ -6,8 +6,8 @@ Section: perl
 Build-Depends: debhelper-compat (= 13)
-Build-Depends-Indep: perl,
-                     libtext-unidecode-perl <!nocheck>
+Build-Depends-Indep: libtext-unidecode-perl <!nocheck>,
+                     perl
 Standards-Version: 4.5.0
```
-->

Verifique se `debian/control` contém a seguinte definição para declarar que o
_build_ do pacote não precisa de _root_ e pode ser executado com usuário comum,
que é o caso da maioria dos pacotes Perl.

```config
Rules-Requires-Root: no
```


<!--
Exemplo dessa alteração para o pacote **libemail-mime-contenttype-perl**.

```diff
diff --git a/debian/control b/debian/control
index deacdf8..cbe1d0e 100644
--- a/debian/control
+++ b/debian/control
@@ -12,6 +12,7 @@ Standards-Version: 4.5.0
 Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libemail-mime-contenttype-perl.git
 Homepage: https://metacpan.org/module/Email::MIME::ContentType
+Rules-Requires-Root: no
 
 Package: libemail-mime-contenttype-perl
 Architecture: all
```

Commit!

<pre class="terminal">
<code>
git commit -a -m 'Set Rules-Requires-Root: no'
</code>
</pre>
-->

Por fim, use o `cme` para analisar o `debian/control` e corriga os eventuais
alertas e problemas relatados.

<pre class="terminal">
<code>
cme check dpkg-control debian/control
</code>
</pre>

Se a saída for semelhante ao conteúdo abaixo significa que não foi encontrado
nenhum erro ou alerta e o arquivo `debian/control` está ok.

```
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
```

### Arquivo `debian/changelog`

Será necessário alterar este arquivo pois nele
é onde fazemos referência ao número do bug report no sistema de bugs do Debian,
veremos isso mais adiante,
algo
necessário para pacotes que serão submetidos ao repositório oficial do Debian.

<!--

Se por algum motivo for necessário
atualizar manualmente o arquivo sugiro utilizar o `dch`.

Por exemplo, se a nova versão do pacote é `0.016` basta executar o seguinte.

<pre class="terminal">
<code>
dch -v 0.016
</code>
</pre>

O `dch` irá interpretar o arquivo `debian/changelog` e adicionará o conteúdo
necessário para a versão `0.016`.

#### Non-Maintainer Upload (NMU)

Atenção para esse detalhe pois o Lintian costuma reclamar disso, por muito
tempo convivi com um alerta sobre _NMU_ sem entender do que se tratava,
acontece que é prática usual em pacotes mantidos por times, como no caso do
Debian Perl Group, deixar explícito que o upload do pacote é feito pelo time e
não por um _Debian Developer (DD)_ individual, na prática o Lintian verifica se
o campo Maintainer no arquivo `debian/control` casa com a identificação de quem
está construindo o pacote localmente, e no caso de pacotes mantidos por times o
campo Maintainer é preenchido com a identificação do time e isso confunte o
Lintian, por isso é necessário adicionar o seguinte no `debian/changelog`para
evitar alertas sobre _Non-Maintainer Upload (NMU)_.

* `Team upload`

Veja, por exemplo, como ficou essa alteração para o pacote **libdata-uuid-perl**.

```changelog
libdata-uuid-perl (1.226-1) UNRELEASED; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.

  [ Joenio Marques da Costa ]
  * Import upstream version 1.226

 -- Joenio Marques da Costa <joenio@joenio.me>  Wed, 04 Nov 2020 21:52:38 +0100
```

Essa entrada deve estar no início do arquivo `debian/changelog`, para mais
detalhes sobre esse tema consulte as referências abaixo.

* [Debian Developer's Reference - NMUs vs team uploads](https://www.debian.org/doc/manuals/developers-reference/ch05.html#nmu-team-upload)
* [Debian Perl Group Policy - debian/changelog handling and versioning](https://perl-team.pages.debian.net/policy.html#debian%2Fchangelog_handling_and_versioning)

Commite as alterações do arquivo `debian/changelog`, adicione _"Gbp-Dch:
Ignore"_ na mensagem de commit, veja como ficou o commit no pacote
**libdata-uuid-perl**.

```git
commit effcb002bcee007ed5f14e9656495056b5f0de96 (HEAD -> master)
Author: Joenio Costa <joenio@joenio.me>
Date:   Wed Nov 4 22:14:44 2020 +0100

    team upload
    
    Gbp-Dch: Ignore
```

Veremos logo mais o motivo de adicionar _"Gbp-Dch: Ignore"_ na mensagem de
commit, por hora tenha em mente a importância de realizar commits atômicos a
cada alteração, declare exatamente o que foi feito de forma clara e direta.

## Atualize o debian/changelog

Por fim, atualize o `debian/changelog`, lembra dos commits atômicos e de
algumas mensagens de commit com "Gbp-Dch: Ignore"? Pois aqui veremos a sua
utilidade, execute o seguinte.

<pre class="terminal">
<code>
gbp dch
</code>
</pre>

O `gbp` analisa os commits do pacote e atualiza o changelog automaticamente
ignorando aquelas mensagens com o comentário `Gbp-Dch: Ignore`, neste momento
altere também o cabeçalho do arquivo `debian/changelog` de **UNRELEASED** para
**unstable** e commit as alterações.

<pre class="terminal">
<code>
git commit -a -m 'update d/changelog'
</code>
</pre>

Ao alterar o cabeçalho para **unstable** estamos indicando ao time Debian Perl
Group que o pacote está pronto para upload, um DD irá revisar o trabalho e
realizar upload do pacote se estiver tudo ok, caso contrário o DD irá solicitar
correções no pacote colocando de volta o cabeçalho como **UNRELEASED** e
adicionará as solicitações de correção no próprio arquivo `debian/changelog`.


-->


<!--
Passe a opção `--force-sign` para assinar os arquivos do pacote, se desejar
informar um mirror alternativo passe o parâmetro `--othermirror "deb
http://local/mirror stable main"`.
-->


## Construir o pacote num ambiente chroot isolado

É importante manter o chroot sempre atualizado, então lembre de sempre executar
o comando abaixo antes de buildar um novo pacote.

<pre class="terminal">
<code>
sudo pbuilder update
</code>
</pre>

### Gerar o pacote e verificar a conformidade com lintian

Construir o pacote dentro do chroot além da vantagem de isolamento possui também a
comodidade de não precisar instalar manualmente as dependências de build do pacote pois
o `pbuilder` toma conta de instalar tudo que está declarado no `debian/control`.

<pre class="terminal">
<code>
BUILDER=pbuilder git-pbuilder
</code>
</pre>

Lembre de rodar o Lintian com a flag específicas para pacotes Perl `--profile
pkg-perl`, isto inclui algumas verificações específicas definidas pelo Debian
Perl Group. Adicione também a flag `--info` para ver mais detalhes com explicação sobre os alertas.

<pre class="terminal">
<code>
lintian -I --profile pkg-perl --info
</code>
</pre>

O lintian produziu os dois alertas abaixo para o pacote {{ page.package.debian }}

```lintian
W: libgetopt-ex-hashed-perl: initial-upload-closes-no-bugs [usr/share/doc/libgetopt-ex-hashed-perl/changelog.Debian.gz:1]
W: libgetopt-ex-hashed-perl source: newer-standards-version 4.7.0 (current is 4.6.2)
```

O primeiro alerta **initial-upload-closes-no-bugs** será corrigido na próxima etapa,
já o alerta **newer-standards-version** pode ser ignorado pois aparentemente
se trata de um falso-positivo que será atualizado numa próxima versão do lintian como
pode ser visto no commit abaixo.

- [https://salsa.debian.org/lintian/lintian/-/commit/ffa795f9003ac32b68d49594808ef8a42907885f](https://salsa.debian.org/lintian/lintian/-/commit/ffa795f9003ac32b68d49594808ef8a42907885f)

## Executar o testes funcionals do Debian com autopkgtest

O autopkgtest do time de Perl injeta uma serie de testes
que "no caso geral" funciona com qualquer biblioteca do CPAN (exemplo: smokes, use.t, syntax.t,
etc...), veja detalhes no link abaixo.

- [https://perl-team.pages.debian.net/autopkgtest.html](https://perl-team.pages.debian.net/autopkgtest.html)

<!--
debian/tests/control example:
- https://salsa.debian.org/perl-team/modules/packages/pkg-perl-tools/blob/master/autopkgtest/examples/default-tests-control

como adicionar testes no pacote debian, alem dos eventuais testes que existam no upstream:
-->

No link abaixo há um tutorial simples, curto e pratico feito no [debci](https://ci.debian.net) sobre o autopkgtest:

* [https://ci.debian.net/doc/file.TUTORIAL.html](https://ci.debian.net/doc/file.TUTORIAL.html)

<!--
no build nao se ve os testes, eh necessario executar autopkgtest depois que constroi o pacote

# 03 ago 2022, continuando autopkgtest dive

https://salsa.debian.org/perl-team/modules/packages/pkg-perl-tools/blob/master/autopkgtest/scripts/runner#L24

autopkgtest backends:
* qemu
* chroot
* schroot
* lxc
* ???
-->

Vamos usar o autopkgtest combinado com schroot,
a ferramenta `mk-sbuild` torna a vida mais facil para criar chroots sem
precisar root, basta adicionar o seu usuario ao grupo sbuild.
Após adicionar seu usuário ao grupo `sbuild` é necessário fazer logout e login
no Debian.

<pre class="terminal">
<code>
sudo apt-get install  ubuntu-dev-tools
sudo adduser joenio sbuild
</code>
</pre>

Entao execute `mk-sbuild <release>`, sera criado um chroot com o nome
`<release>-<arch>`, exemplo `sid-amd64`.

<pre class="terminal">
<code>
mk-sbuild sid
</code>
</pre>

depois basta rodar no codigo do pacote para buildar com esse chroot:

<pre class="terminal">
<code>
autopkgtest . -- schroot sid-amd64
</code>
</pre>

As últimas linhas impressas no terminal após a execução do `autopkgtest` informa que o pacote
rodou os testes com sucesso.

```
All tests successful.
Files=1, Tests=4,  0 wallclock secs ( 0.01 usr  0.01 sys +  0.05 cusr  0.00 csys =  0.07 CPU)
Result: PASS
autopkgtest [12:43:16]: test autodep8-perl-recommends: -----------------------]
autopkgtest [12:43:16]: test autodep8-perl-recommends:  - - - - - - - - - - results - - - - - - - - - -
autodep8-perl-recommends PASS (superficial)
autopkgtest [12:43:16]: @@@@@@@@@@@@@@@@@@@@ summary
autodep8-perl-build-deps PASS
autodep8-perl        PASS (superficial)
autodep8-perl-recommends PASS (superficial)
```

## Atualizar o arquivo `debian/changelog` com o número do bug

Ao empacotar um software é importante registrar que temos a intenção de
empacotar este software para que a comunidade Debian tenha conhecimento de que
alguém está trabalhando nisso e assim evitar re-trabalho e sobreposição, um
pseudo-pacote chamado `wnpp` ([Work-Needing and Prospective Packages][wnpp])
é utilizado para concentrar os bugreports relativos a novos pacotes.

Os bugs reportados no pseudo-pacote `wnpp` recebem tags indicando o tipo do
"bug", neste caso queremos reportar um bug indicando que estamos trabalhando
(ou iremos trabalhar) num novo pacote, a tag para isso é a **ITP** (Intent To
Package), consulte a [documentação oficial do `reportbug`][reportbug] para
saber como configurá-lo e execute o seguinte comando para registrar um bug do
tipo ITP.

Mas se você selecionou um dos pacotes candidatos com bug do tipo **RFP** já aberto
então não é necessário abrir um **ITP**, basta referenciar o número do bug **RFP** no
arquivo `debian/changelog`.
Deve ser informado no arquivo `debian/changelog` uma mensagem referenciando o
fechamendo de um bug do tipo [ITP][] ou [RFP][], em resumo:

<!--
### Tipos de bugs WNPP

Pacotes que precisam de trabalho e futuros pacotes, WNPP (Work-Needing and Prospective Packages) abreviado em inglês, é
uma lista de pacotes que precisam de novos(as) mantenedores(as) e potenciais
pacotes no Debian.
-->

- **ITP** - Intenção de empacotar ("Intent To Package") você mesmo.
- **RFP** - Requisição de empacotamento ("Request For Package") para uma outra pessoa.

### Reportar bug do tipo ITP

É possível usar a ferramenta `reportbug`, uma ferramenta de linha de comando para reportar bugs do sistema
Debian em conjunto com o `msmtp`. Os dois juntos permitem enviar emails para abertura de bugs
diretamente da linha de comando, o [msmtp][] pode ser útil também em outros contextos, como por
exemplo no uso da ferramenta [caff][] para assinaturas de chaves GPG,
mas
a configuração do `reportbug` e do `msmtp` está fora de escopo deste tutorial e você deve enviar o email
usando seu cliente de email preferido.
Mas se quiser se aventurar e usar o `reportbug` e o `msmtp` consulte os
links abaixo.

- [https://wiki.debian.org/msmtp](https://wiki.debian.org/msmtp)
- [https://wiki.debian.org/reportbug](https://wiki.debian.org/reportbug)

[msmtp]: https://wiki.debian.org/msmtp
[caff]: https://wiki.debian.org/caff

Vamos utilizar o `dpt gen-itp` para gerar o conteúdo do email,
basta executar o comando abaixo a partir do diretório do pacote, no meu caso é `{{ page.package.debian }}`.
O `gen-itp` irá ler a descrição curta do `debian/control`
e outras informações dos arquivos no `debian/`.

<pre class="terminal">
<code>
dpt gen-itp > /tmp/mail
cat /tmp/mail
</code>
</pre>

```
From: =?UTF-8?B?Sm9lbmlvIE1hcnF1ZXMgZGEgQ29zdGEgPGpvZW5pb0Bqb2VuaW8ubWU+?=
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: ITP: libgetopt-ex-hashed-perl -- Perl module to automatically store Getopt::Long options into Hash
Date: Wed, 15 May 2024 23:25:32 +0200

Package: wnpp
Owner: Joenio Marques da Costa <joenio@joenio.me>
Severity: wishlist
X-Debbugs-CC: debian-devel@lists.debian.org, debian-perl@lists.debian.org

* Package name    : libgetopt-ex-hashed-perl
  Version         : 1.0601
  Upstream Author : Kazumasa Utashiro
* URL             : https://metacpan.org/release/Getopt-EX-Hashed
* License         : Artistic or GPL-1+
  Programming Lang: Perl
  Description     : Perl module to automatically store Getopt::Long options into Hash

The Getopt::EX::Hashed is a module to automate a hash object to store command
line option values for Getopt::Long and compatible modules, including
Getopt::EX::Long.

The module name shares the Getopt::EX namespace, but it works independently
from other modules with the Getopt::EX prefix.

The package will be maintained under the umbrella of the Debian Perl Group.

--
Generated with the help of dpt-gen-itp(1) from pkg-perl-tools.
```

Copie e cole os campos de assunto, destinatário e corpo do email, veja exemplo
abaixo usando o Thunderbird, use o seu cliente de email e envie a mensagem para `submit@bugs.debian.org`.

<a href="/files/thunderbird-libgetopt-ex-hashed-open-itp-bug.png">
<img src="/files/thunderbird-libgetopt-ex-hashed-open-itp-bug.png" style="width:100%; border: 0; box-shadow: 0 0 0"/>
</a>

<!--
Pode-se usar o `reportbug` para ler o arquivo `/tmp/mail` e enviar o email
para abrir o bug ITP para o pacote {{ page.package.debian }}, mas aqui neste tutorial
use o seu cliente de email preferido para enviar este email.
-->

Aguarde alguns minutos e verifique a mensagem nos servidores do Debian, você deve receber uma
cópia do email semelhante a [esta mensagem aqui](https://lists.debian.org/debian-perl/2024/05/msg00008.html), ela mensagem contém o número do bug, adicione o número do bug
no arquivo `debian/changelog` com a mensagem "(Closes: #&lt;numero do bug&gt;)", use o comando `dch`
para auxiliar nesta tarefa.

<pre class="terminal">
<code>
dch --closes 1071196
</code>
</pre>

Você deve substituir o número **1071196** pelo número do seu bug,
veja
como ficou o changelog do `{{ page.package.debian }}`.

```
libgetopt-ex-hashed-perl (1.0601-1) UNRELEASED; urgency=low

  * Initial release. (Closes: #1071196)

 -- Joenio Marques da Costa <joenio@joenio.me>  Wed, 15 May 2024 21:14:29 +0200
```

<pre class="terminal">
<code>
git commit debian/changelog -m 'd/changelog: add ITP bug number'
</code>
</pre>

<!--
A mensagem automática com o número do bug ITP criado para o pacote `{{
page.pacote }}` recebida dos servidores do Debian foi a seguinte.

<a href="/files/thunderbird-libgetopt-ex-hashed-perl-debian-itp-bug.png">
<img src="/files/thunderbird-libgetopt-ex-hashed-perl-debian-itp-bug.png" style="width:100%; border: 0; box-shadow: 0 0 0"/>
</a>


Enquanto esperamos o email de confirmação com o número do bug podemos
configurar nosso acesso aos repositórios do Debian Perl Group.
-->

### Alterar pacote de UNRELEASED para unstable

Esta é a última mudança no pacote antes de fazer upload do pacote,
o pacote deve ser indicado como
`unstable`, isto significa que os pacotes sempre entram na distribuição
`unstable`, a partir disso segue seu fluxo até chegar na versão `stable`,
passando pela `testing`.

Atualizar `debian/changelog` e atualize no
cabeçalho do arquivo de UNRELEASED para unstable.
O comando `dch -r` (`--release`) atualizar automaticamente o `debian/changelog` de UNRELEASED para
unstable, e atualiza a data.

<pre class="terminal">
<code>
dch --release
</code>
</pre>

<pre class="terminal">
<code>
git commit debian/changelog -m 'd/changelog: ready for review and upload'
</code>
</pre>

## Fazer upload do pacote no repositório do time Debian Perl

Está tudo pronto para fazer upload, falta apenas ter acesso ao repositório do Debian Perl Group no Salsa,
Salsa é uma instância do Gitlab e funciona como uma plataforma de desenvolvimento colaborativa dentro do Debian.
Para escrever no repositório do Debian Perl Group é necessário fazer
parte do time, para isso é necessário seguir os seguintes passos.

* Criar uma conta no [Salsa][] se registrando aqui [https://salsa.debian.org/users/sign_up](https://salsa.debian.org/users/sign_up)
  * Configurar uma chave SSH associada ao seu perfil
* Solicitar ser adicionado ao time Debian Perl Group
  * Envie um email para debian-perl@lists.debian.org se apresentando brevemente
  * O email deve ser escrito em inglês e você deve indicar qual é seu username salsa
  * Veja um exemplo aqui: [https://lists.debian.org/debian-perl/2022/12/msg00000.html](https://lists.debian.org/debian-perl/2022/12/msg00000.html)
* Registrar um token em [https://salsa.debian.org/profile/personal_access_tokens](https://salsa.debian.org/profile/personal_access_tokens)
  e adicionar a variável `DPT_SALSA_PRIVATE_TOKEN` no arquivo `~/.dpt.conf` com este token

A conta no Salsa precisa ser validada por um administrador, isso pode levar algum tempo e deve-se aguardar.
Use seu nome real ao inscrever-se pora reduzir falsos positivos na heurística de detecção de spammers, mais sobre Salsa em:
- [https://wiki.debian.org/pt_BR/Salsa/Doc](https://wiki.debian.org/pt_BR/Salsa/Doc).

### Configurar dpt com o token Salsa

Uma vez que seu usuário Salsa foi criado e você foi adicionado ao time Debian Perl Group,
basta atualizar a configuração do `dpt` em `~/.dpt.conf` e adicionar o TOKEN de acesso do
[Salsa][salsa] para poder escrever nos repositórios do grupo e poder enviar as
atualizações do pacote.

```ini
DPT_SALSA_PRIVATE_TOKEN=***COPIE EU TOKEN AQUI***
```

Uma vez tendo usuário no Salsa e fazendo parte do time Debian Perl Group,
compartilhe o pacote enviando as atualizações para o repositório do grupo com o
seguinte comando.

<!--
## Configurar o Debian Perl module packaging Tool

pacotes que necessitam de atualização,
por exemplo, podem ser também encontrados nos seguintes endereços.

* [https://udd.debian.org/dmd/?email1=pkg-perl-maintainers@lists.alioth.debian.org](https://udd.debian.org/dmd/?email1=pkg-perl-maintainers@lists.alioth.debian.org)
* [https://tracker.debian.org/teams/pkg-perl/+table/general/?tag=new-upstream-version](https://tracker.debian.org/teams/pkg-perl/+table/general/?tag=new-upstream-version)

## 05 ago 2022, dam via IRC

some of them are cosmetic, but help making the package look like most of the others maintained by the group

importante sempre atualizar o timestamp do changelog com o comando dch -r

<pre class="terminal">
<code>
dpt fixup
</code>
</pre>

-->

### Upload

É necessário solicitar acesso ao [Salsa do time Perl do Debian][perl-team]
enviando um email com uma curta apresentação pessoal para o seguinte email <a
href="mailto:debian-perl@lists.debian.org">debian-perl@lists.debian.org</a>.
Após ter acesso ao repositório do grupo Perl é hora de fazer upload do pacote
no repositório do grupo, mas antes faça uma revisão final no pacote:

* Verifique se o status do pacote em `debian/changelog` foi alterado de UNRELEASED para unstable
* Garanta que há uma mensagem `(Closes: #NNNNNN)` no `debian/changelog` indicando o número do bug ITP
* Verifique a versão correta de "Standards-Version:", aqui a versão utilizada foi 4.3.0
* Use o formato correto no arquivo `debian/copyright`, atualmente usar [DEP-5][dep5]
* Verificar o COPYRIGHT de cada arquivo (usar grep ou ack) para buscar a quem pertence o copyright de cada arquivo
* Registre um token em [https://salsa.debian.org/profile/personal_access_tokens](https://salsa.debian.org/profile/personal_access_tokens)
  e defina este token na variável de ambiente `DPT_SALSA_PRIVATE_TOKEN` para que o `dpt` tenha acesso de escrita no servidor Salsa.

Feito as verificações finais, use o comando abaixo para criar e configurar um
novo projeto no Salsa, e subir o repositório local do nosso novo pacote `{{
page.package.debian }}`.

<pre class="terminal">
<code>
dpt salsa pushrepo
</code>
</pre>

O módulo `{{ page.package.cpan }}` empacotado para `{{ page.package.debian }}` durante a escrita
deste post foi submetido ao repositório do Debian Perl Group e pode ser visualizado em:

* [https://salsa.debian.org/perl-team/modules/packages/{{ page.package.debian }}](https://salsa.debian.org/perl-team/modules/packages/{{ page.package.debian }})

<a href="/files/salsa-libgetopt-ex-hashed-perl.png">
<img src="/files/salsa-libgetopt-ex-hashed-perl.png" style="width:100%" />
</a>

Para saber mais detalhes de como fazer upload do seu pacote veja a documentação
[pushing to salsa.debian.org][pushing-to-salsa], além do token é necessário
também fazer upload de sua chave ssh no Salsa, ele é uma instância do Gitlab,
então basta acessar sua conta ir em preferências e configurar uma chave ssh por
lá.

Algum tempo após o upload o pacote um DD (Debian Developer) irá
revisar o pacote e caso encontre problemas vai solicitar alterações mudando o status em
`debian/changelog` para `UNRELEASED`.

Bem, é isso, espero que este texto seja útil ajudando e incentivando mais
pessoas a empacotar software no Debian, uma atividade muito importante e que
requer um trabalho constante.



<!--
## Veja também

Posts relacionados:
* [Aprenda a criar repositórios de pacotes Debian](/aprenda-a-criar-repositorios-de-pacotes-debian)
* [Como atualizar pacotes Perl no Debian - Passo-a-passo prático e detalhado](/como-atualizar-pacotes-perl-no-debian)
* [Empacotando módulos Perl no Debian](/empacotando-modulos-perl-no-debian)

Ver:
* http://localhost:4000/oficina-debian/
* https://salsa.debian.org/joenio/dworkflow
* https://gitlab.com/joenio/debian-dev


## lintian overrides

Para o erro abaixo:


W: analizo: script-not-executable [home/joenio/perl5/lib/perl5/auto/share/dist/Analizo/bash-completion/analizo]

SOlucao eh criar o arquivo debian/analizo.lintian-overrides com o seguinte conteudo:

analizo: script-not-executable [home/joenio/perl5/lib/perl5/auto/share/dist/Analizo/bash-completion/analizo]

-->

Video demo sobre o `dpt`

- [dpt(1) Uploading a new upstream release](https://peertube.debian.social/w/mEXRGfui1nPbvVDvQCXSQs)

<small>
_a imagem utilizada no cabeçalho do post foi copiada de
[https://www.deviantart.com/mdh3ll/art/Debian-shot-311580879](https://www.deviantart.com/mdh3ll/art/Debian-shot-311580879)_
</small>

[linux]: http://www.kernel.org
[Debian]: http://debian.org
[cpan]: http://metacpan.org
[perl]: http://perl.org
[debian-perl-group]: http://perl-team.pages.debian.net
[pkg-perl-tools]: https://tracker.debian.org/pkg/pkg-perl-tools
[upgrade-upstream]: https://perl-team.pages.debian.net/git.html#upgrading_to_a_new_upstream_release
[myrepos]: https://myrepos.branchable.com
[Salsa]: https://salsa.debian.org
[perl-policy]: https://perl-team.pages.debian.net/policy.html
[berne-convention]: http://perl-team.pages.debian.net/copyright.html#Berne_Convention
[dother-compat]: https://www.debian.org/doc/manuals/maint-guide/dother.en.html#compat
[debian-policy]: https://www.debian.org/doc/debian-policy

[debian]: http://debian.org
[livre]: http://debian.org/intro/free
[GNU]: http://www.gnu.org
[repositorio]: http://www.debian.org/releases/stable/amd64/release-notes/ch-whats-new.pt-br.html#newdistro
[APT]: http://pt.wikipedia.org/wiki/Advanced_Packaging_Tool
[linux]: http://www.kernel.org
[cpan]: http://metacpan.org
[perl]: http://perl.org
[plataformas]: http://www.debian.org/ports
[modulo-url]: http://metacpan.org/pod/{{ page.modulo }}
[naming-policy]: http://perl-team.pages.debian.net/policy.html#Package_Naming_Policy
[description-policy]: https://www.debian.org/doc/debian-policy/ch-binary.html#s-descriptions
[ITP]: https://wiki.debian.org/ITP
[RFP]: https://wiki.debian.org/RFP
[pkg-perl-git]: http://perl-team.pages.debian.net/git.html
[liblog-dispatch-config-perl/copyright]: https://salsa.debian.org/perl-team/modules/packages/liblog-dispatch-config-perl/blob/debian/1.04-1/debian/copyright
[berne-convention]: http://perl-team.pages.debian.net/copyright.html#Berne_Convention
[wnpp]: https://wiki.debian.org/WNPP
[reportbug]: https://wiki.debian.org/reportbug
[lintian-override]: https://lintian.debian.org/manual/section-2.4.html
[gnupg]: https://www.gnupg.org
[perl-team]: https://salsa.debian.org/perl-team
[dep5]: https://dep-team.pages.debian.net/deps/dep5/
[pushing-to-salsa]: http://perl-team.pages.debian.net/git.html#pushing_to_salsa.debian.org
[pet]: http://pet.debian.net/pkg-perl/pet.cgi
[autopkgtest]: https://perl-team.pages.debian.net/autopkgtest.html
